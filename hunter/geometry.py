"""
Module to calculate geometric information on earth surface.

All input parameters are per default in degrees, not radians.
All distances are on ground, not in air on a specific flight level.

See also https://www.movable-type.co.uk/scripts/latlong.html
"""
from math import asin, atan, atan2, cos, degrees, fabs, floor, radians, sin, sqrt
import random
from typing import Optional, Tuple

import networkx as nx
import pyproj
import shapely.geometry as shg

EQURAD = 6378137.0


METRES_TO_FEET = 3.2808399
METRES_TO_NM = 0.000539957


MISSING_ELEV = -9999


def feet_to_metres(feet: float) -> float:
    return feet / METRES_TO_FEET


def metres_to_feet(metres: float) -> float:
    return metres * METRES_TO_FEET


def metres_to_nm(metres: float) -> float:
    return metres * METRES_TO_NM


def nm_to_metres(nm: float) -> float:
    return nm / METRES_TO_NM


def knots_to_ms(knots: float) -> float:
    return nm_to_metres(knots)/3600


class Orientation:
    __slots__ = ('hdg', 'pitch', 'roll')

    def __init__(self, hdg: float, pitch: float = 0., roll: float = 0.) -> None:
        self.hdg = hdg  # true heading
        self.pitch = pitch
        self.roll = roll


class Position:
    __slots__ = ('lon', 'lat', 'alt_m')

    def __init__(self, lon: float, lat: float, alt_m: float = 0.) -> None:
        self.lon = lon
        self.lat = lat
        self.alt_m = alt_m

    @property
    def alt_ft(self) -> float:
        return metres_to_feet(self.alt_m)

    def __str__(self) -> str:
        return 'lon={}, lat={}, alt_m={}'.format(self.lon, self.lat, self.alt_m)


EDGE_ATTR_COST = 'cost'  # networkx edge attribute for the weight in Dijkstra shortest path
EDGE_ATTR_MAX_SPEED = 'max_speed'  # networkx edge attribute for the max speed (in m/s) on the edge


class WayPoint:
    """A node on a networkx graph with specific attributes for targets.

    NB: there is no object for network edges.
    """
    def __init__(self, index: int, lon: float, lat: float, ground_m: float = 0., alt_m: float = 0.) -> None:
        self.index = index  # either manually set or OSM ID
        self.lon = lon
        self.lat = lat
        self.ground_m = ground_m  # the ground elevation of the point
        self.alt_m = alt_m  # the altitude at which the target moves

        # special attributes for routing etc.
        self.is_point_of_interest = False  # landmarks
        self.is_heli_landing = False
        self.is_bonus_point = False

    def __str__(self) -> str:
        return 'WayPoint {}: lon={}, lat={}, alt_m={}'.format(self.index, self.lon, self.lat, self.alt_m)

    @property
    def alt_ft(self) -> float:
        return metres_to_feet(self.alt_m)

    def make_position(self) -> Position:
        return Position(self.lon, self.lat, self.alt_m)


def random_next_way_point(graph: nx.Graph, this_wp: WayPoint, prev_wp: Optional[WayPoint]) -> WayPoint:
    """In a graph of WayPoints find a random next WayPoint if possible not going back to previous one."""
    neighbors = list(graph.neighbors(this_wp))
    if prev_wp is not None and len(neighbors) > 1 and prev_wp in neighbors:  # try not to go back to prev WayPoint
        neighbors.remove(prev_wp)
    return random.choice(neighbors)


def random_way_point(graph: nx.Graph) -> WayPoint:
    return random.choice(list(graph.nodes))


def closest_way_point(graph: nx.Graph, position: Position) -> WayPoint:
    """The Waypoint in a graph, which is closest to the specified position."""
    closest_wp = None
    closest_dist = 9999999999.
    for wp in list(graph.nodes):
        dist = calc_distance_pos_wp(position, wp)
        if dist < closest_dist:
            closest_wp = wp
            closest_dist = dist
    return closest_wp


def calc_aspect(first_hdg: float, second_hdg: float) -> float:
    """Normalized aspect between two headings -> 0-180 degrees - used for relative aspects of aircraft."""
    aspect = first_hdg - second_hdg
    if aspect < 0:
        aspect *= -1
    if aspect > 180:
        aspect = 360 - aspect
    return aspect


def calc_distance(lon1: float, lat1: float, lon2: float, lat2: float) -> float:
    """Distance on Earth's surface not taking into account altitude by default.
    https://en.wikipedia.org/wiki/Geographical_distance"""
    phi1 = radians(lat1)
    phi2 = radians(lat2)
    delta_phi = radians(lat2 - lat1)
    delta_lambda = radians(lon2 - lon1)
    a = sin(delta_phi / 2) * sin(delta_phi / 2) + cos(phi1) * cos(phi2) * sin(delta_lambda / 2) * sin(delta_lambda / 2)
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    return EQURAD * c


def calc_distance_pos(pos1: Position, pos2: Position) -> float:
    return calc_distance(pos1.lon, pos1.lat, pos2.lon, pos2.lat)


def calc_distance_pos_3d(pos1: Position, pos2: Position) -> float:
    """Calculates the distance including an approximation for the vertical distance."""
    dist = calc_distance(pos1.lon, pos1.lat, pos2.lon, pos2.lat)
    return sqrt(dist**2 + (pos1.alt_m - pos2.alt_m)**2)


def calc_distance_pos_wp(pos: Position, wp: WayPoint) -> float:
    return calc_distance(pos.lon, pos.lat, wp.lon, wp.lat)


def calc_distance_wp_wp(wp_1: WayPoint, wp_2: WayPoint) -> float:
    return calc_distance(wp_1.lon, wp_1.lat, wp_2.lon, wp_2.lat)


def calc_distance_tuple_wp(lon_lat: Tuple[float, float], wp: WayPoint) -> float:
    return calc_distance(lon_lat[0], lon_lat[1], wp.lon, wp.lat)


def calc_magnetic_heading(geographic_north: float, magnetic_declination: float) -> float:
    return normalize_degrees(geographic_north - magnetic_declination)


def normalize_degrees(raw_degrees: float) -> float:
    """Make sure an angle in degrees is between 0 and 360."""
    if raw_degrees < 0:
        raw_degrees += 360
    elif raw_degrees > 360:
        raw_degrees -= 360
    return raw_degrees


def norm_deg_180(angle: float) -> float:
    """Normalizes degrees based on geo.nas->normdeg180."""
    while angle <= -180:
        angle += 360
    while angle > 180:
        angle -= 360
    return angle


def calc_bearing(lon1: float, lat1: float, lon2: float, lat2: float, magnetic_declination: float = 0.) -> float:
    """This formula is for the initial bearing (sometimes referred to as forward azimuth).
    But for short distances it is close enough.
    The magnetic_variation corrects from true bearing to magnetic heading (close enough for short distances).
    """
    if lon1 == lon2 and lat1 == lat2:
        return 0.
    phi1 = radians(lat1)
    phi2 = radians(lat2)
    lambda1 = radians(lon1)
    lambda2 = radians(lon2)
    y = sin(lambda2 - lambda1) * cos(phi2)
    x = cos(phi1) * sin(phi2) - sin(phi1) * cos(phi2) * cos(lambda2 - lambda1)
    bearing = degrees(atan2(y, x))
    return normalize_degrees(calc_magnetic_heading(bearing, magnetic_declination))


def calc_bearing_pos(pos1: Position, pos2: Position, magnetic_variation: float = 0.) -> float:
    return calc_bearing(pos1.lon, pos1.lat, pos2.lon, pos2.lat, magnetic_variation)


def calc_bearing_pos_wp(pos: Position, wp: WayPoint, magnetic_variation: float = 0.) -> float:
    return calc_bearing(pos.lon, pos.lat, wp.lon, wp.lat, magnetic_variation)


def calc_bearing_wp(wp1: WayPoint, wp2: WayPoint, magnetic_variation: float = 0.) -> float:
    return calc_bearing(wp1.lon, wp1.lat, wp2.lon, wp2.lat, magnetic_variation)


def calc_bearing_tuple_wp(lon_lat: Tuple[float, float], wp: WayPoint, magnetic_variation: float = 0.) -> float:
    return calc_bearing(lon_lat[0], lon_lat[1], wp.lon, wp.lat, magnetic_variation)


def calc_destination(lon1: float, lat1: float, distance: float, bearing: float) -> Tuple[float, float]:
    """Given an initial point, distance and bearing: what is the destination point on Earth."""
    theta = radians(bearing)
    phi1 = radians(lat1)
    phi2 = asin(sin(phi1) * cos(distance / EQURAD) + cos(phi1) * sin(distance / EQURAD) * cos(theta))
    lat2 = degrees(phi2)
    lambda1 = radians(lon1)

    lambda2 = lambda1 + atan2(sin(theta) * sin(distance / EQURAD) * cos(phi1),
                              cos(distance / EQURAD) - sin(phi1) * sin(phi2))
    lon2 = degrees(lambda2)
    lon2 = (lon2 + 540) % 360 - 180
    return lon2, lat2


def calc_destination_wp(wp: WayPoint, distance: float, bearing: float) -> Tuple[float, float]:
    return calc_destination(wp.lon, wp.lat, distance, bearing)


def calc_destination_pos(pos: Position, distance: float, bearing: float) -> Tuple[float, float]:
    return calc_destination(pos.lon, pos.lat, distance, bearing)


def calc_destination_pos_3d(pos: Position, distance: float, bearing: float, pitch: float) -> Position:
    distance_horizontal = distance * cos(radians(pitch))
    distance_vertical = distance * sin(radians(pitch))
    lon, lat = calc_destination_pos(pos, distance_horizontal, bearing)
    return Position(lon, lat, pos.alt_m + distance_vertical)


def calc_altitude_angle(pos_1: Position, pos_2: Position) -> float:
    """The altitude in degrees over the horizon (or below if negative) position 2 is in respect to position 1.

    See https://de.wikipedia.org/wiki/Azimut why it is called altitude.
    """
    if pos_1.lon == pos_2.lon and pos_1.lat == pos_2.lat:
        return 90.
    distance = calc_distance_pos(pos_1, pos_2)
    alt_delta = pos_2.alt_m - pos_1.alt_m
    altitude = degrees(atan(fabs(alt_delta) / distance))
    if alt_delta < 0:
        altitude *= -1
    return altitude


def calc_angle_of_corner(prev_wp: WayPoint, corner_wp: WayPoint, next_wp: WayPoint) -> float:
    """Calculates the angle between 3 points (e.g. way) as a positive number between 0 and 180."""
    first_angle = calc_bearing_wp(prev_wp, corner_wp)
    second_angle = calc_bearing_wp(corner_wp, next_wp)
    angle = fabs(first_angle - second_angle)
    if angle > 180:
        angle = 360 - angle
    return angle


def calc_delta_bearing(bearing1: float, bearing2: float) -> float:
    """Calculates the difference between two bearings. If positive with clock, if negative against the clock sense.

    I.e. from bearing1 to bearing2 you need to turn delta with or against the clock."""
    if bearing1 == 360.:
        bearing1 = 0.
    if bearing2 == 360.:
        bearing2 = 0.
    delta = bearing2 - bearing1

    if delta > 180:
        delta = delta - 360
    elif delta < -180:
        delta = 360 + delta

    return delta


def calc_limited_bearing(current_bearing: float, target_bearing: float, max_change: float) -> float:
    """Calculates a new bearing based on a limited turn rate."""
    delta = calc_delta_bearing(current_bearing, target_bearing)
    if fabs(delta) > max_change:
        if delta > 0:
            delta = max_change
        else:
            delta = -1 * max_change
    new_bearing = current_bearing + delta
    return normalize_degrees(new_bearing)


def calc_limited_pitch(current_pitch: float, target_pitch: float, max_change: float) -> float:
    """Calculates a new pitch based on a limited turn rate."""
    delta = calc_delta_bearing(current_pitch, target_pitch)
    if fabs(delta) > max_change:
        if delta > 0:
            delta = max_change
        else:
            delta = -1 * max_change
    return current_pitch + delta  # no need to normalize vs. calc_limited_bearing


def calc_orientation(prev_pos: Position, next_pos: Position) -> Orientation:
    delta_vertical = next_pos.alt_m - prev_pos.alt_m
    delta_horizontal = calc_distance_pos(prev_pos, next_pos)
    if delta_horizontal == 0.:
        pitch = 0.
    else:
        pitch = atan(delta_vertical / delta_horizontal)
    orientation_pitch = degrees(pitch)
    bearing = calc_bearing_pos(prev_pos, next_pos)
    return Orientation(bearing, pitch=orientation_pitch)


INTERCEPT_MIN_SPEED = 25  # ca. 50 kt
INTERCEPT_ASSUMED_SPEED = 200  # ca. 400 kt
INTERCEPT_TIME_PENALTY = 30


def calc_intercept(chaser_pos: Position, chaser_speed: float,
                   target_pos: Position, target_speed: float, target_heading: float,
                   simplified_distance: int = 0) -> Tuple[float, float]:
    """Calculates the vector and ETA for the chaser to intercept the target. ETA is 0 if interception not possible.
    Approximation good enough for targets flying much slower than interceptor.

    If the chaser's speed indicates that it is not airborne, then apply time penalty and assume average speed.

    If the distance to the target is less than simplified distance, then the calculation is only based on current
    positions and the chaser's speed and does not take into account, that the target moves during ETA.
    """
    new_pos_target = target_pos
    eta = 0.
    vector = 0.
    chasing_speed = chaser_speed
    assumed_speed = False
    if chasing_speed < INTERCEPT_MIN_SPEED:  # not yet airborne
        chasing_speed = INTERCEPT_ASSUMED_SPEED
        assumed_speed = True
    for i in range(5):
        dist = calc_distance_pos_3d(chaser_pos, new_pos_target)
        eta = dist / chasing_speed
        if i == 0 and dist < simplified_distance:
            vector = calc_bearing_pos(chaser_pos, new_pos_target)
            break
        lon, lat = calc_destination_pos(target_pos, eta * target_speed, target_heading)
        new_pos_target = Position(lon, lat, new_pos_target.alt_m)
        vector = calc_bearing_pos(chaser_pos, new_pos_target)
    if assumed_speed:
        eta += INTERCEPT_TIME_PENALTY
    return vector, eta


def _convert_wgs_to_utm(lon, lat):
    """Get the EPSG code for a UTM projection based on lon/lat.

    From: https://gis.stackexchange.com/questions/269518/auto-select-suitable-utm-zone-based-on-grid-intersection
    """
    utm_band = str((floor((lon + 180) / 6) % 60) + 1)
    if len(utm_band) == 1:
        utm_band = '0'+utm_band
    if lat >= 0:
        epsg_code = '326' + utm_band
    else:
        epsg_code = '327' + utm_band
    return epsg_code


def make_utm_transformer(lon: float, lat: float) -> pyproj.Transformer:
    utm_code = _convert_wgs_to_utm(lon, lat)
    return pyproj.Transformer.from_crs('EPSG:4326', 'EPSG:{0}'.format(utm_code), always_xy=True)


def local_line_from_lon_lat(lon_1: float, lat_1: float, lon_2: float, lat_2: float,
                            transformer: pyproj.Transformer) -> shg.LineString:
    x_y_1 = transformer.transform(lon_1, lat_1, radians=False)
    x_y_2 = transformer.transform(lon_2, lat_2, radians=False)
    return shg.LineString([x_y_1, x_y_2])
