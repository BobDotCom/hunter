"""
Module to interact with a running FlightGear instance through the Telnet protocol.
See http://wiki.flightgear.org/Telnet_usage

README.commands

The code below is an updated copy from https://sourceforge.net/p/flightgear/flightgear/ci/next/tree/scripts/python/.
The main changes are adaptation to Python 3.7.

"""
import logging
from telnetlib import Telnet
import socket
import re

__all__ = ["FlightGear"]


CRLF = '\r\n'


class FGTelnet(Telnet):
    def __init__(self, host, port) -> None:
        Telnet.__init__(self, host, port)
        self.prompt = []
        self.prompt.append(re.compile(b'/[^>]*> '))
        self.timeout = 5
        # Telnet.set_debuglevel(self,2)
        logging.info('Initialized FGTelnet on %s:%i', host, port)

    def help(self) -> None:
        return

    def ls(self, my_dir=None) -> str:
        """
        Returns a list of properties.
        """
        if my_dir is None:
            self._put_cmd('ls')
        else:
            self._put_cmd('ls %s' % my_dir)
        return self._get_resp()

    def dump(self) -> str:
        """Dump current state as XML."""
        self._put_cmd('dump')
        return self._get_resp()

    def cd(self, my_dir) -> None:
        """Change directory."""
        self._put_cmd('cd ' + my_dir)
        self._get_resp()
        return

    def pwd(self) -> str:
        """Display current path."""
        self._put_cmd('pwd')
        return self._get_resp()

    def get(self, var) -> str:
        """Retrieve the value of a parameter."""
        self._put_cmd('get %s' % var)
        return self._get_resp()

    def set(self, var, value) -> None:
        """Set variable to a new value"""
        self._put_cmd('set %s %s' % (var, value))
        self._get_resp()  # Discard response

    def quit(self) -> None:
        """Terminate connection"""
        self._put_cmd('quit')
        self.close()
        return

    def nasal(self, nasal_code: str) -> None:
        """Run a nasal script.
        See http://wiki.flightgear.org/Telnet_usage#nasal
        """
        self._put_cmd('nasal')
        self._put_cmd(nasal_code)
        self._put_cmd('##EOF##')
        return

    def run(self, fg_command: str) -> None:
        """Run a fgcommand.
        See http://wiki.flightgear.org/Telnet_usage#run
        """
        self._put_cmd('run {}'.format(fg_command))

    # Internal: send one command to FlightGear
    def _put_cmd(self, cmd) -> None:
        cmd = cmd + CRLF
        Telnet.write(self, cmd.encode('ascii'))
        return

    # Internal: get a response from FlightGear
    def _get_resp(self) -> str:
        (i, match, resp) = Telnet.expect(self, self.prompt, self.timeout)
        # Remove the terminating prompt.
        # Everything preceding it is the response.
        piece = resp.decode('utf8').split('\n')[:-1]
        return piece


class FlightGear:
    """FlightGear interface class.

    An instance of this class represents a connection to a FlightGear telnet
    server.

    Properties are accessed using a dictionary style interface:
    For example:

    # Connect to FlightGear telnet server.
    fg = FlightGear('myhost', 5500)
    # parking brake on
    fg['/controls/gear/brake-parking'] = 1
    # Get current heading
    heading = fg['/orientation/heading-deg']

    Other non-property related methods
    """

    def __init__(self, host='localhost', port=5500) -> None:
        try:
            self.telnet = FGTelnet(host, port)
        except socket.error as msg:
            self.telnet = None
            raise socket.error(msg)

    def __del__(self) -> None:
        # Ensure telnet connection is closed cleanly.
        self.quit()

    def __getitem__(self, key):
        """Get a FlightGear property value.
        Where possible the value is converted to the equivalent Python type.
        """
        s = self.telnet.get(key)[0]
        match = re.compile('[^=]*=\s*\'([^\']*)\'\s*([^\r]*)\r').match(s)
        if not match:
            return None
        value, type_ = match.groups()
        if value == '':
            return None

        if type_ == '(double)':
            return float(value)
        elif type_ == '(int)':
            return int(value)
        elif type_ == '(bool)':
            if value == 'true':
                return 1
            else:
                return 0
        else:
            return value

    def __setitem__(self, key, value):
        """Set a FlightGear property value."""
        self.telnet.set(key, value)

    def quit(self) -> None:
        """Close the telnet connection to FlightGear."""
        if self.telnet:
            self.telnet.quit()
            self.telnet = None

    def exit_sim(self) -> None:
        """Exits FlightGear. Will automatically lead to loosing the connection."""
        self.run_fg_command('exit')

    def reset_sim(self) -> None:
        """Resets FlightGear. Will automatically lead to loosing the connection."""
        self.run_fg_command('reset')

    def view_next(self) -> None:
        # move to next view
        self.telnet.set("/command/view/next", "true")

    def view_prev(self) -> None:
        # move to next view
        self.telnet.set("/command/view/prev", "true")

    def run_nasal(self, code: str) -> None:
        self.telnet.nasal(code)
        
    def run_fg_command(self, fg_command: str) -> None:
        self.telnet.run(fg_command)
