import os

from flask import Flask, render_template

import hunter.utils as u
import hunter.web_ui.tools as t
import hunter.web_ui.ongoing_session as ose

# If `entrypoint` is not defined in app.yaml, App Engine will look for an app
# called `app` in `main.py`.
app = Flask(__name__)

# imports to other modules need to be done AFTER app has been defined
# otherwise app will not be available in the module and the import will fail

u.configure_logging('INFO', False, 'web-ui', True)

app.register_blueprint(t.bp_tools)
app.register_blueprint(ose.bp_ongoing_session)

TITLE_HOME = 'Home'


@app.route('/')
def root():
    gae = os.getenv('GAE_ENV')
    gcp_id = os.getenv('GOOGLE_CLOUD_PROJECT')

    return render_template('index.html', title=TITLE_HOME, gae_env=gae, gcp_id=gcp_id)
