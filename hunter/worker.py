"""Controls fg_targets (instances of targets running in FlightGear processes) over Telnet and web-socket.

Gets most control instructions from Controller and updates Controller with position/health information.
"""
from collections import deque
import logging
import multiprocessing as mp
import random
import socket
import sys
import time

import hunter.fg_targets as fgt
import hunter.gcp_ps_io as gps
import hunter.gcp_utils as gu
import hunter.messages as m
import hunter.scenarios as sc
import hunter.utils as u


MIN_TIME_BETWEEN_INSTANCES = 60  # minimum time passed between creating FGFS instances (very CPU intensive)


def get_local_ip_address() -> str:
    """Getting the IP address of the first network card.
    Maybe not unique in the world, but good enough on a local LAN, where most probably other worker nodes are sitting.

    Deliberately not catching a network error because that would be a sign that the worker is not on the network
    and then there is no point in starting the rest of the worker."""
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    foo = s.getsockname()[0]
    s.close()
    return foo


class FGFSWorker(mp.Process):
    """Worker controller of FG instances. Running it makes only sense, if a controller has placed requests."""
    def __init__(self, fg_instances: str, scenario_path: str, show_world: bool, testing: bool,
                 parent_conn: mp.Pipe) -> None:
        super().__init__()

        self.mp_server_host = None
        self.parent_conn = parent_conn  # The Pipe to the parent process (for command line CTRL-C only)

        self.worker_id = get_local_ip_address()  # should be pretty unique - at least in a local network incl. GCP
        self.index = 0

        self.scenario_path = scenario_path
        self.show_world = show_world

        self.fg_targets = None  # dict of callsign, FGMPConnected
        self.carrier = None  # need a direct pointer to be able to send specific messages
        self.session_id = None  # will be set when invitation from controller to register is accepted
        self.scenario = None
        self.fg_instances = fg_instances
        self.publisher_client = None
        self.subscriber_client = None
        self.testing = testing

        self.gcp_ps_runner = None

        if not self._accept_controller_invitation():
            self._exit_session()
            sys.exit(1)

        self.run_cycles = 0

        self.random_ship_waypoints = None  # populated in run() method

        # queueing for new instances
        self.instance_queue = deque()
        self.last_instance_queue_poll = 0

    # noinspection PyBroadException
    def _accept_controller_invitation(self) -> bool:
        """Waits for a controller to ask for registration and confirms back with the worker_id.
        If no reply within time limit or an exception occurred, then return False."""
        publisher_client = gu.construct_publisher_client()
        subscriber_client = gu.construct_subscriber_client()
        start_time = time.time()
        try:
            gps.handle_ctrl_to_workers_subscription(subscriber_client, self.worker_id, True)
            gps.handle_captain_to_carrier_subscription(subscriber_client, True)
            if self.testing:
                self.scenario = sc.load_scenario('croatia', self.scenario_path)  # FIXME
                self.session_id = '0'
                self.mp_server_host = u.MP_SERVER_HOST_DEFAULT
            else:
                while time.time() - start_time < u.GCP_MAX_WAIT_FOR_CTRL_REQUEST:
                    logging.info('Pulling ctrl registration requests')
                    self.session_id, scenario_name, self.mp_server_host \
                        = gps.pull_ctlr_registration_requests(subscriber_client, self.worker_id)
                    if self.session_id:
                        self.scenario = sc.load_scenario(scenario_name, self.scenario_path)
                        gps.send_action_register_worker(publisher_client, self.session_id,
                                                        self.worker_id, self.fg_instances)
                        logging.info('Worker %s has successfully registered with session %s',
                                     self.worker_id, self.session_id)
                        break
                    time.sleep(u.WORKER_POLLING_REGISTRATION_REQUEST)
        except Exception:
            logging.exception('Something went wrong in handling ctrl invitation. Collaboration with ctrl stopped')
            return False
        if self.session_id is None:
            logging.info('Waited %i seconds for the controller to place a request, but nothing received.',
                         u.GCP_MAX_WAIT_FOR_CTRL_REQUEST)
            return False
        return True

    # noinspection PyBroadException
    def run(self) -> None:
        """Overrides the method in Process and runs the endless loop"""
        logging.info('Starting FGFSWorker processing')

        self.gcp_ps_runner = gps.GCPPSRunner(self.session_id, self.worker_id)
        self.gcp_ps_runner.start()

        self.fg_targets = dict()

        try:
            if self.testing:
                data = m.GCPSubData({gps.ACTION: gps.ACTION_INSTANCE_CARRIER, gps.CALLSIGN: 'MP_029X',
                                     gps.ASSET_NAME: fgt.CarrierType.vinson.name}, '0')
                # data = m.GCPSubData({gps.ACTION: gps.ACTION_INSTANCE_TANKER, gps.CALLSIGN: 'TANKERX'}, '0')
                self.gcp_ps_runner.outgoing_queue.put_nowait(data)
            while True:
                self.run_cycles += 1
                if self.run_cycles % 100 == 0:
                    logging.info('Cycle %i in Worker', self.run_cycles)
                if self.parent_conn.poll():
                    message = self.parent_conn.recv()
                    logging.info('FGFSWorker got message from parent process: %s', message)
                    if isinstance(message, m.ExitSession):
                        self._exit_session()
                        return
                # messaging from Runners through Pipes
                to_remove = list()  # cannot remove during iteration
                for fg_runner in self.fg_targets.values():
                    while True:
                        message = fg_runner.receive_message()
                        if message is None:
                            break  # we are done polling data from GCP right now
                        if isinstance(message, m.FGTargetDestroyed):
                            self.gcp_ps_runner.post_message(message)
                        elif isinstance(message, m.RemoveDeadTarget):
                            self.gcp_ps_runner.post_message(message)
                            to_remove.append(message.callsign)
                for callsign in to_remove:
                    del self.fg_targets[callsign]
                # messaging through GCP from Controller or carrier Captain
                while True:
                    data = self.gcp_ps_runner.receive_message()
                    if data is None:
                        break  # we are done polling data from GCP right now
                    logging.info('Receiving stuff from GCPPSRunner through queue: %s', str(data))
                    if data and isinstance(data, m.GCPSubData):
                        if data.content_dict[gps.ACTION] == gps.ACTION_STOP:
                            logging.info('Controller requested stop serving')
                            self._exit_session()
                            return
                        elif data.content_dict[gps.ACTION] == gps.ACTION_CAPTAIN_ORDER:
                            logging.info('Received a captain order for the carrier')
                            if self.carrier:
                                self.carrier.post_message(data)
                        else:
                            logging.info('Controller asked for an instance - adding to queue')
                            self.instance_queue.appendleft(data)

                # check whether it is time to start an instance
                self._handle_queued_instance_requests()

                time.sleep(1)  # just shortly pause the endless loop

        except Exception:
            logging.exception('Some exception during worker.run() occurred. Continuing and see.')

    def _handle_queued_instance_requests(self):
        if len(self.instance_queue) == 0:
            return
        now = time.time()
        if (now - self.last_instance_queue_poll) > MIN_TIME_BETWEEN_INSTANCES:
            self.last_instance_queue_poll = now
            logging.info('Popping a new instance from the queue')
            data = self.instance_queue.pop()
            if data.content_dict[gps.ACTION] == gps.ACTION_INSTANCE_SAM:
                logging.info('Controller requested a SAM: %s', data.content_dict[gps.ASSET_NAME])
                self.index += 1
                sam = fgt.SurfaceAirMissile(self.index, self.mp_server_host,
                                            data.content_dict[gps.CALLSIGN], data.content_dict[gps.ASSET_NAME],
                                            gps.position_from_content_dict(data.content_dict),
                                            data.content_dict[gps.HEADING],
                                            self.show_world)
                self._add_fg_runner(sam, sam.callsign, sam.aero)
            elif data.content_dict[gps.ACTION] == gps.ACTION_INSTANCE_SHIP:
                logging.info('Controller requested a ship: %s', data.content_dict[gps.ASSET_NAME])
                if self.random_ship_waypoints is None:
                    my_fg_instances = u.parse_fg_instances(self.fg_instances)
                    self.random_ship_waypoints = random.sample(list(self.scenario.ships_network.nodes),
                                                               my_fg_instances.total_instances())
                start_node = self.random_ship_waypoints.pop()
                next_node = random.choice(list(self.scenario.ships_network.neighbors(start_node)))
                self.index += 1
                ship = fgt.MissileFrigate(self.index, self.mp_server_host,
                                          data.content_dict[gps.CALLSIGN], self.scenario.ships_network,
                                          start_node, next_node,
                                          self.show_world)
                self._add_fg_runner(ship, ship.callsign, ship.aero)
            elif data.content_dict[gps.ACTION] == gps.ACTION_INSTANCE_AUTOMAT:
                logging.info('Controller requested an automat: %s', data.content_dict[gps.ASSET_NAME])
                self.index += 1
                automat = fgt.AutomatAsset(self.index, self.mp_server_host,
                                           data.content_dict[gps.CALLSIGN],
                                           data.content_dict[gps.ICAO],
                                           data.content_dict[gps.NUMBER_OF_LIVES],
                                           data.content_dict[gps.PROPS_STRING],
                                           self.show_world)
                self._add_fg_runner(automat, automat.callsign, data.content_dict[gps.ASSET_NAME])
            elif data.content_dict[gps.ACTION] == gps.ACTION_INSTANCE_CARRIER:
                logging.info('Controller requested a carrier: %s', data.content_dict[gps.ASSET_NAME])
                carrier_type = fgt.parse_carrier_type_name(data.content_dict[gps.ASSET_NAME])
                if carrier_type is None:
                    logging.warning('Cannot interpret carrier type from name; %s', gps.ASSET_NAME)
                else:
                    self.index += 1
                    carrier = fgt.Carrier(self.index, self.mp_server_host,
                                          data.content_dict[gps.CALLSIGN],
                                          carrier_type,
                                          self.scenario.carrier_definition.sail_area,
                                          self.scenario.carrier_definition.loiter_centre,
                                          self.show_world)
                    self._add_fg_runner(carrier, carrier.callsign, data.content_dict[gps.ASSET_NAME])
                    self.carrier = carrier
            else:
                logging.warning('Request cannot be interpreted - programming error? %s', str(data.content_dict))

    def _exit_session(self):
        logging.info("FGFSWorker got exit command: shutting down dependencies and itself")
        for fg_instance_runner in self.fg_targets.values():
            fg_instance_runner.post_message(m.TearDownTarget())

        if self.gcp_ps_runner:
            self.gcp_ps_runner.post_message(m.ExitSession())

        gps.handle_ctrl_to_workers_subscription(gu.construct_subscriber_client(), self.worker_id, False)
        gps.handle_captain_to_carrier_subscription(gu.construct_subscriber_client(), False)

        # give all threads a bit time to finish up and asynchronously execute exit commands
        time.sleep(20)

        logging.info('FGFS worker %s stopped', self.worker_id)

    def _add_fg_runner(self, fg_runner: fgt.FGMPConnected, callsign: str, aero: str):
        self.fg_targets[callsign] = fg_runner
        fg_runner.start()
        logging.info('Adding and starting new "%s" with callsign %s', aero, callsign)
        time.sleep(5.)  # a bit of startup interval time if several new assets at once
