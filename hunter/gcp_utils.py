"""Module with common utilities to interact with GCP no matter the used service"""
import logging
import os
import sys
from typing import Any, Dict

from google.cloud import pubsub
import google.cloud.datastore as ds
import google.cloud.logging
from google.cloud.logging.handlers import CloudLoggingHandler, setup_logging

from google.oauth2 import service_account

import hunter.utils as u

ENV_VARIABLE_GOOGLE_CLOUD_PROJECT = 'GOOGLE_CLOUD_PROJECT'  # what most GCP services expect -> project ID

# environment variable e.g. pointing to /home/hello/gpc_credentials/hunter_pubsub.json
ENV_VARIABLE_CREDENTIAL = 'HUNTER_GCP_SA_INTEGRATED'

# running in GCP Compute Engine VM Instance (must be 'y' or 'yes' as value)
ENV_VARIABLE_CE_VM_INSTANCE = 'CE_VM_INSTANCE'


def is_gae_standard_env() -> bool:
    return os.getenv('GAE_ENV', '').startswith('standard')


def is_compute_engine_vm_instance() -> bool:
    return os.getenv(ENV_VARIABLE_CE_VM_INSTANCE, '').startswith('y')


def is_in_cloud() -> bool:
    return is_gae_standard_env() or is_compute_engine_vm_instance()


def check_minimal_gcp_env_variables() -> None:
    """Checks the availability of the minimal set of environment variables and exits if not fulfilled.
    """
    u.print_os_environ()

    missing = list()
    if os.getenv(ENV_VARIABLE_GOOGLE_CLOUD_PROJECT, None) is None:
        missing.append(ENV_VARIABLE_GOOGLE_CLOUD_PROJECT)
    if not (is_gae_standard_env() or is_compute_engine_vm_instance()):
        if len(os.getenv(ENV_VARIABLE_CREDENTIAL, '')) == 0:
            missing.append(ENV_VARIABLE_CREDENTIAL)

    if missing:
        print('The following environment variables are missing -> aborting: {}'.format(', '.join(missing)))
        sys.exit(1)


def get_project_id() -> str:
    """Gets the Google project ID either from built in environment variables for
    app engine/virtual machine or from real environment setup.

    Will raise an error if project ID is not available. If we do not have it, then nothing else will work.

    Cf. https://cloud.google.com/appengine/docs/standard/python3/runtime#environment_variables
    """
    project_id = os.getenv(ENV_VARIABLE_GOOGLE_CLOUD_PROJECT)
    if project_id is None:
        raise ValueError('The environment does not have a correctly set up GOOGLE_CLOUD_PROJECT variable.')
    return project_id


def create_datastore_client() -> ds.Client:
    if is_in_cloud():
        return ds.Client(project=get_project_id())
    return ds.Client(credentials=get_credentials_from_environment(), project=get_project_id())


def get_credentials_from_environment() -> Dict[Any, Any]:
    """Read credentials from local file, which is pointed to by env variable"""
    logging.info('Credentials points to: %s', os.getenv(ENV_VARIABLE_CREDENTIAL))
    return service_account.Credentials.from_service_account_file(os.getenv(ENV_VARIABLE_CREDENTIAL))


def construct_publisher_client() -> pubsub.PublisherClient:
    if is_in_cloud():
        return pubsub.PublisherClient()
    return pubsub.PublisherClient(credentials=get_credentials_from_environment())


def construct_subscriber_client() -> pubsub.SubscriberClient:
    if is_in_cloud():
        return pubsub.SubscriberClient()
    return pubsub.SubscriberClient(credentials=get_credentials_from_environment())


def configure_gcp_logging(log_level: str, main_process_name: str) -> bool:
    """Use GCP cloud native logging - if this is actually running in GCP.

    Return False if not running in GCP

    Cf. https://googleapis.dev/python/logging/latest/stdlib-usage.html
    """
    if not is_in_cloud():
        print('GCP cloud check returned False')
        return False

    print('GCP cloud check returned true. Configuring for level', log_level, 'and process', main_process_name)
    client = google.cloud.logging.Client()
    print('GCP logging client', str(client))

    handler = CloudLoggingHandler(client, name=main_process_name)
    logging.getLogger().setLevel(log_level)
    setup_logging(handler)
    logging.info('Logging setup as GCP native logging')
    return True
