"""
Module to interact with Google Cloud Firestore in Datastore mode.


Cf. Cloud Datastore:
    * https://cloud.google.com/datastore
    * https://googleapis.dev/python/datastore/latest/client.html
"""
from datetime import datetime as dt
from datetime import timezone as tz
import logging
import os.path as osp
import pickle
import queue
import time
from typing import Dict, List, Optional, Tuple

import google.cloud.datastore as ds
from google.cloud.datastore.query import PropertyFilter

import hunter.gcp_utils as gu
import hunter.geometry as g
import hunter.messages as m
import hunter.scenarios as s
import hunter.utils as u

PROP_CALLSIGN = 'callsign'
PROP_HEALTH = 'health'  # the value, not the name

KIND_HUNTER_SESSION = 'HunterSession'
# is a str due to pubsub attributes limitations. Must be in sync with SESSION_ID in web_ui/ongoing_session.py
PROP_SESSION_ID = 'session_id'
PROP_SCENARIO_IDENT = 'scenario_ident'
PROP_SCENARIO_NAME = 'scenario_name'
PROP_SCENARIO_DESCRIPTION = 'scenario_description'
PROP_SCENARIO_ICAO = 'scenario_icao'
PROP_SCENARIO_WEST_LON = 'scenario_west_lon'
PROP_SCENARIO_SOUTH_LAT = 'scenario_south_lat'
PROP_SCENARIO_EAST_LON = 'scenario_east_lon'
PROP_SCENARIO_NORTH_LAT = 'scenario_north_lat'
PROP_STARTED_DT = 'started_dt'  # timestamp
PROP_STARTED_BY = 'started_by'
PROP_STOPPED_DT = 'stopped_dt'
PROP_STOPPED_BY = 'stopped_by'
PROP_MP_SERVER_HOST = 'mp_server_host'
PROP_NUMBER_OF_WORKERS = 'number_of_workers'
PROP_IDENTIFIER = 'identifier'
PROP_GCI = 'gci'
PROP_HELI_SHOOT = 'heli_shoot'
PROP_DRONE_SHOOT = 'drone_shoot'
PROP_SHIP_SHOOT = 'ship_shoot'
PROP_SHILKA_SHOOT = 'shilka_shoot'
PROP_SAM_SHOOT = 'sam_shoot'
PROP_AWACS = 'awacs'
PROP_TANKER = 'tanker'
PROP_OVERRIDDEN = 'overridden'

PROP_SHOOTING = 'shooting'
KIND_DAMAGE_RESULT = 'DamageResult'
PROP_DAMAGE_TIME = 'damage_time'
PROP_WEAPON_TYPE = 'weapon_type'
PROP_ATTACKER = 'attacker'  # MP callsign
PROP_ATTACKER_KIND = 'attacker_kind'
PROP_TARGET = 'target'  # MP callsign
PROP_TARGET_KIND = 'target_kind'
PROP_CIVILIAN = 'is_civilian'
PROP_FG_TARGET = 'is_fg_target'

KIND_POSITION_UPDATE = 'PositionUpdate'
PROP_KIND = 'kind'  # the type of vehicle, aircraft, building etc.
PROP_POSITION_LON = 'lon'
PROP_POSITION_LAT = 'lat'
PROP_POSITION_ALT_M = 'alt_m'
PROP_HEADING = 'heading'
PROP_PITCH = 'pitch'
PROP_SPEED = 'speed'

MAX_BATCH_SIZE = 400

DEFAULT_USER = "unknown user"

MAX_AGE_SESSION = 20  # days
MAX_AGE_NOT_STOPPED_SESSION = 24  # hours

has_been_cleaned = False  # global variable to check whether clean-up in datastore has been done


# ===================== Hunter Session and Stuff =======================================================================


def _query_not_terminated_sessions(ds_client: ds.Client, mp_server_host: str) -> List[ds.Entity]:
    """Checks whether there are existing sessions based on existence of timestamp in field 'stopped'.

    This must be done before a new session is created."""
    query = ds_client.query(kind=KIND_HUNTER_SESSION)
    query.add_filter(filter=PropertyFilter(PROP_STOPPED_DT, '=', None))
    query.add_filter(filter=PropertyFilter(PROP_MP_SERVER_HOST, '=', mp_server_host))
    return list(query.fetch())


def _query_session_not_terminated(ds_client: ds.Client, override_session_id: str) -> List[ds.Entity]:
    """check whether we can find not terminated session(s) based on a session id."""
    query = ds_client.query(kind=KIND_HUNTER_SESSION)
    query.add_filter(filter=PropertyFilter(PROP_STOPPED_DT, '=', None))
    query.add_filter(filter=PropertyFilter(PROP_SESSION_ID, '=', override_session_id))
    return list(query.fetch())


def register_new_session(ds_client: ds.Client, scenario: s.ScenarioContainer, mp_server_host: str, gci: bool,
                         heli_shoot, drone_shoot, ship_shoot, shilka_shoot, sam_shoot,
                         has_awacs: bool, has_tanker: bool,
                         ctrl_callsign: str, identifier: str,
                         override_prev_session_id: int) -> ds.Entity:
    logging.info('Registering new session for scenario %s on %s', scenario.ident, mp_server_host)
    if override_prev_session_id > 0:
        _override_session(ds_client, str(override_prev_session_id))
    existing_sessions = _query_not_terminated_sessions(ds_client, mp_server_host)
    if existing_sessions:
        message = '''There are {} not stopped sessions on {}. The ID of the first one is {}.
                     You can use this ID to override a stopped session as a parameter at startup of the Controller.
                  '''.format(len(existing_sessions), mp_server_host, existing_sessions[0][PROP_SESSION_ID])
        raise ValueError(message)
    session_id = u.create_session_id()

    session = ds.Entity(ds_client.key(KIND_HUNTER_SESSION), exclude_from_indexes=(PROP_SCENARIO_IDENT,
                                                                                  PROP_SCENARIO_NAME,
                                                                                  PROP_SCENARIO_DESCRIPTION,
                                                                                  PROP_SCENARIO_ICAO,
                                                                                  PROP_SCENARIO_WEST_LON,
                                                                                  PROP_SCENARIO_SOUTH_LAT,
                                                                                  PROP_SCENARIO_EAST_LON,
                                                                                  PROP_SCENARIO_NORTH_LAT,
                                                                                  PROP_GCI,
                                                                                  PROP_HELI_SHOOT,
                                                                                  PROP_DRONE_SHOOT,
                                                                                  PROP_SHIP_SHOOT,
                                                                                  PROP_SHILKA_SHOOT,
                                                                                  PROP_SAM_SHOOT,
                                                                                  PROP_AWACS,
                                                                                  PROP_TANKER,
                                                                                  PROP_CALLSIGN,
                                                                                  PROP_IDENTIFIER,
                                                                                  PROP_OVERRIDDEN))
    session.update(
        {
            PROP_SESSION_ID: session_id,
            PROP_SCENARIO_IDENT: scenario.ident,
            PROP_STARTED_DT: dt.utcnow(),
            PROP_STOPPED_DT: None,  # must be set explicitly
            PROP_SCENARIO_NAME: scenario.name,
            PROP_SCENARIO_DESCRIPTION: scenario.description,
            PROP_SCENARIO_ICAO: scenario.icao,
            PROP_SCENARIO_WEST_LON: scenario.south_west[0],
            PROP_SCENARIO_SOUTH_LAT: scenario.south_west[1],
            PROP_SCENARIO_EAST_LON: scenario.north_east[0],
            PROP_SCENARIO_NORTH_LAT: scenario.north_east[1],
            PROP_MP_SERVER_HOST: mp_server_host,
            PROP_GCI: gci,
            PROP_HELI_SHOOT: heli_shoot,
            PROP_DRONE_SHOOT: drone_shoot,
            PROP_SHIP_SHOOT: ship_shoot,
            PROP_SHILKA_SHOOT: shilka_shoot,
            PROP_SAM_SHOOT: sam_shoot,
            PROP_AWACS: has_awacs,
            PROP_TANKER: has_tanker,
            PROP_CALLSIGN: ctrl_callsign,
            PROP_IDENTIFIER: identifier,
            PROP_OVERRIDDEN: False
        }
    )
    ds_client.put(session)
    logging.info('Registered new session with id %s', session_id)
    return session


class HunterSession:
    def __init__(self, session_id: str, started_dt: dt, stopped_dt: dt,
                 name: str, description: str, icao: str,
                 south_west: Tuple[float, float], north_east: Tuple[float, float],
                 mp_server_host: str, gci: bool,
                 heli_shoot: bool, drone_shoot: bool, ship_shoot: bool, shilka_shoot: bool, sam_shoot: bool,
                 awacs: bool, tanker: bool,
                 ctrl_callsign: str, identifier: str,
                 overridden: bool) -> None:
        self.session_id = session_id
        self.url_param = '?{}={}'.format(PROP_SESSION_ID, self.session_id)
        self.started_dt = started_dt
        self.stopped_dt = stopped_dt
        self.name = name
        self.description = description
        self.icao = icao
        self.south_west = south_west
        self.north_east = north_east
        self.mp_server_host = mp_server_host
        self.gci = gci
        self.heli_shoot = heli_shoot
        self.drone_shoot = drone_shoot
        self.ship_shoot = ship_shoot
        self.shilka_shoot = shilka_shoot
        self.sam_shoot = sam_shoot
        self.awacs = awacs
        self.tanker = tanker
        self.ctrl_callsign = ctrl_callsign
        self.identifier = identifier
        self.overridden = overridden
        self.airports = list()

    def load_apt_dat_airports(self) -> None:
        # noinspection PyBroadException
        try:
            full_path = osp.join(u.get_scenario_resources_dir(), s.APT_DAT_AIRPORT_FILE)
            with open(full_path, 'rb') as file_pickle:
                all_airports = pickle.load(file_pickle)

            airports_in_boundary = list()
            for apt in all_airports:
                if self.south_west[0] <= apt.lon <= self.north_east[0] and \
                        self.south_west[1] <= apt.lat <= self.north_east[1]:
                    airports_in_boundary.append(apt)
            self.airports = airports_in_boundary
        except Exception:  # Pickle can throw pretty much any exception
            logging.exception('Could not read airports information from file')

    @classmethod
    def map_session_from_entity(cls, entity: ds.Entity) -> 'HunterSession':
        my_session = HunterSession(entity[PROP_SESSION_ID], entity[PROP_STARTED_DT], entity[PROP_STOPPED_DT],
                                   entity[PROP_SCENARIO_NAME],
                                   entity[PROP_SCENARIO_DESCRIPTION],
                                   entity[PROP_SCENARIO_ICAO],
                                   (entity[PROP_SCENARIO_WEST_LON], entity[PROP_SCENARIO_SOUTH_LAT]),
                                   (entity[PROP_SCENARIO_EAST_LON], entity[PROP_SCENARIO_NORTH_LAT]),
                                   entity[PROP_MP_SERVER_HOST],
                                   entity[PROP_GCI],
                                   entity[PROP_HELI_SHOOT], entity[PROP_DRONE_SHOOT], entity[PROP_SHIP_SHOOT],
                                   entity[PROP_SHILKA_SHOOT], entity[PROP_SAM_SHOOT],
                                   entity[PROP_AWACS], entity[PROP_TANKER],
                                   entity[PROP_CALLSIGN], entity[PROP_IDENTIFIER],
                                   entity[PROP_OVERRIDDEN])
        return my_session


def query_session(ds_client: ds.Client, session_id: Optional[str],
                  load_airports: bool = False) -> Optional[HunterSession]:
    if session_id is None:
        return None
    query = ds_client.query(kind=KIND_HUNTER_SESSION)
    query.add_filter(filter=PropertyFilter(PROP_SESSION_ID, '=', session_id))
    results = list(query.fetch())
    if len(results) == 1:
        my_session = HunterSession.map_session_from_entity(results[0])
        if load_airports:
            my_session.load_apt_dat_airports()
        return my_session
    else:
        if len(results) > 1:
            logging.warning('There is more than one session with the same ID. Should never happen ?!')
        return None


def query_available_sessions(ds_client: ds.Client) -> List[HunterSession]:
    _cleanup_sessions(ds_client)

    query = ds_client.query(kind=KIND_HUNTER_SESSION)
    query.order = ['-' + PROP_STARTED_DT]
    results = list(query.fetch(limit=200))  # basically all

    sessions = list()
    for entity in results:
        sessions.append(HunterSession.map_session_from_entity(entity))
    return sessions


def _cleanup_sessions(ds_client: ds.Client) -> None:
    """Do regular clean-up of sessions for ease of use and limit amount of data.

    For performance reasons this method is only executed when a web-ui user gets a list of sessions. Additionally,
    this method is only called if global variable 'has_been_cleaned' is false.
    This is a very crude method and assumes that the Hunter web-app regularly is re-initialised such that
    this method is called. This assumption should be true as long as GAE is used as a host.
    Alternatively, the web-app can be run from command line on a developer workstation to do this periodically."""
    global has_been_cleaned
    if has_been_cleaned:
        return
    has_been_cleaned = True
    query = ds_client.query(kind=KIND_HUNTER_SESSION)
    available_sessions = list(query.fetch())
    now = dt.now(tz.utc)
    to_be_deleted_sessions_ids = list()
    for entity in available_sessions:
        started_dt = entity[PROP_STARTED_DT]
        delta = now - started_dt
        stopped_dt = None
        if PROP_STOPPED_DT in entity:
            stopped_dt = entity[PROP_STOPPED_DT]
        # make sure old not stopped sessions get overridden
        if stopped_dt is None:
            if delta.total_seconds() > 3600 * MAX_AGE_NOT_STOPPED_SESSION:
                entity.update(
                    {
                        PROP_STOPPED_DT: dt.utcnow(),
                        PROP_OVERRIDDEN: True
                    }
                )
                ds_client.put(entity)
        # delete old sessions
        if delta.total_seconds() > 3600 * 24 * MAX_AGE_SESSION:
            to_be_deleted_sessions_ids.append(entity[PROP_SESSION_ID])
            ds_client.delete(entity)
    # delete stuff related to old sessions
    for session_id in to_be_deleted_sessions_ids:
        # damage
        query = ds_client.query(kind=KIND_DAMAGE_RESULT)
        query.add_filter(filter=PropertyFilter(PROP_SESSION_ID, '=', session_id))
        records = list(query.fetch())
        for entity in records:
            ds_client.delete(entity)
        # positions
        query = ds_client.query(kind=KIND_POSITION_UPDATE)
        query.add_filter(filter=PropertyFilter(PROP_SESSION_ID, '=', session_id))
        records = list(query.fetch())
        for entity in records:
            ds_client.delete(entity)


def _terminate_session(ds_client: ds.Client, session_id: str) -> None:
    logging.info('Terminating session')
    results = _query_session_not_terminated(ds_client, session_id)
    if len(results) == 0:
        raise ValueError('Current session ID {} cannot be found as ongoing.', session_id)

    results[0].update(
        {
            PROP_STOPPED_DT: dt.utcnow(),
        }
    )
    ds_client.put(results[0])


def _override_session(ds_client: ds.Client, override_session_id: str) -> None:
    logging.info('Overriding not stopped session %s', override_session_id)
    results = _query_session_not_terminated(ds_client, override_session_id)
    if len(results) == 0:
        raise ValueError('Not stopped session ID {} cannot be found'.format(override_session_id))
    else:
        for result in results:
            result.update(
                {
                    PROP_STOPPED_DT: dt.utcnow(),
                    PROP_OVERRIDDEN: True
                }
            )
            ds_client.put(result)


# ===================== Damage =========================================================================================

def _register_damage_result(ds_client: ds.Client, damage_result: m.DamageResult, session_id: str) -> None:
    record = ds.Entity(ds_client.key(KIND_DAMAGE_RESULT), exclude_from_indexes=(PROP_HEALTH,
                                                                                PROP_WEAPON_TYPE,
                                                                                PROP_ATTACKER_KIND,
                                                                                PROP_TARGET,
                                                                                PROP_TARGET_KIND,
                                                                                PROP_CIVILIAN,
                                                                                PROP_FG_TARGET,
                                                                                PROP_SHOOTING))
    record.update(
        {
            PROP_DAMAGE_TIME: damage_result.damage_time,
            PROP_HEALTH: damage_result.health.value,
            PROP_WEAPON_TYPE: damage_result.weapon_type.value,
            PROP_ATTACKER: damage_result.attacker,
            PROP_ATTACKER_KIND: damage_result.attacker_kind,
            PROP_TARGET: damage_result.target,
            PROP_TARGET_KIND: damage_result.target_kind,
            PROP_CIVILIAN: damage_result.civilian,
            PROP_FG_TARGET: damage_result.fg_target,
            PROP_SHOOTING: damage_result.shooting,
            PROP_SESSION_ID: session_id
         }
    )
    ds_client.put(record)
    logging.info('Registered a damage result for attacker %s on %s', damage_result.attacker, damage_result.target)


def query_damage_results(ds_client: ds.Client, session_id: str) -> List[m.DamageResult]:
    query = ds_client.query(kind=KIND_DAMAGE_RESULT)
    query.add_filter(filter=PropertyFilter(PROP_SESSION_ID, '=', session_id))
    query.order = ['-' + PROP_DAMAGE_TIME]
    results = list(query.fetch(limit=50))

    damages = list()
    for entity in results:
        health_type = m.map_health_type(entity[PROP_HEALTH], True)
        weapon_type = m.map_weapon_type(entity[PROP_WEAPON_TYPE])
        damage = m.DamageResult(health_type, weapon_type, entity[PROP_ATTACKER], entity[PROP_ATTACKER_KIND],
                                entity[PROP_TARGET], entity[PROP_TARGET_KIND], entity[PROP_CIVILIAN],
                                entity[PROP_FG_TARGET], entity[PROP_SHOOTING])
        damage.damage_time = entity[PROP_DAMAGE_TIME]
        damages.append(damage)
    return damages


# ===================== PositionUpdates ================================================================================

def _upsert_position_updates(ds_client: ds.Client, batch: m.PositionUpdatesBatch,
                             prev_records: Dict[str, ds.Entity], session_id: str) -> None:
    """Inserts or updates positions based on the newest values.
    Then compares the content with the saved information to delete those entities, which are not actual
    any more (e.g. dead).
    """
    # first do the upsert
    data_records = list()
    batch_callsigns = set()
    inserted = 0
    updated = 0
    with ds_client.transaction():  # need a transaction if multiple mutations to same entity
        for pos_update in batch.position_updates:
            batch_callsigns.add(pos_update.callsign)
            if pos_update.callsign in prev_records:
                record = prev_records[pos_update.callsign]
                if record[PROP_HEALTH] == pos_update.health and (
                        record[PROP_POSITION_LON] == pos_update.position.lon) and (
                        record[PROP_POSITION_LAT] == pos_update.position.lat) and (
                        record[PROP_POSITION_ALT_M] == pos_update.position.alt_m) and (
                        record[PROP_HEADING] == pos_update.heading):  # speed is not interesting here
                    continue  # no need to send something to datastore, which has not changed
                else:
                    updated += 1
            else:
                record = ds.Entity(ds_client.key(KIND_POSITION_UPDATE), exclude_from_indexes=(PROP_KIND,
                                                                                              PROP_HEALTH,
                                                                                              PROP_POSITION_LON,
                                                                                              PROP_POSITION_LAT,
                                                                                              PROP_POSITION_ALT_M,
                                                                                              PROP_HEADING,
                                                                                              PROP_PITCH,
                                                                                              PROP_SPEED))
                prev_records[pos_update.callsign] = record
                inserted += 1
            record.update(
                {
                    PROP_CALLSIGN: pos_update.callsign,
                    PROP_KIND: pos_update.kind,
                    PROP_HEALTH: pos_update.health.value,
                    PROP_POSITION_LON: pos_update.position.lon,
                    PROP_POSITION_LAT: pos_update.position.lat,
                    PROP_POSITION_ALT_M: pos_update.position.alt_m,
                    PROP_HEADING: pos_update.heading,
                    PROP_PITCH: pos_update.pitch,
                    PROP_SPEED: pos_update.speed,
                    PROP_SESSION_ID: session_id,
                }
            )
            data_records.append(record)
        ds_client.put_multi(data_records)
    logging.info('Upserted %i position updates (out of which %i inserts)', updated + inserted, inserted)
    # then do the deletions
    to_delete_callsigns = set()
    for callsign, record in prev_records.items():
        if callsign not in batch_callsigns:
            ds_client.delete(record.key)
            to_delete_callsigns.add(callsign)
    if to_delete_callsigns:
        for callsign in to_delete_callsigns:
            del prev_records[callsign]
        logging.info('Deleted %i callsigns from position updates', len(to_delete_callsigns))


def query_current_session_positions(ds_client: ds.Client, session_id: str) -> List[m.PositionUpdate]:
    """The actual positions for the current session - can be empty.
    """

    query = ds_client.query(kind=KIND_POSITION_UPDATE)
    query.add_filter(filter=PropertyFilter(PROP_SESSION_ID, '=', session_id))
    query.order = [PROP_CALLSIGN]
    results = list(query.fetch(limit=200))  # basically all

    positions = list()
    for entity in results:
        health_type = m.map_health_type(entity[PROP_HEALTH], False)
        position = g.Position(entity[PROP_POSITION_LON], entity[PROP_POSITION_LAT], entity[PROP_POSITION_ALT_M])
        position_update = m.PositionUpdate(entity[PROP_CALLSIGN], entity[PROP_KIND], health_type, position,
                                           entity[PROP_HEADING], entity[PROP_PITCH], entity[PROP_SPEED])
        positions.append(position_update)
    return positions


# ===================== Other Stuff ====================================================================================

class GCPDSRunner(u.QueuedRunner):
    """A thread interacting with GCP Datastore asynchronously, so the main process' run() method does not get halted.
    """
    __slots__ = ('position_updates_prev_records', 'session_id')

    def __init__(self, session_id: str) -> None:
        super().__init__('GCPDSRunner')
        self.session_id = session_id
        self.position_updates_prev_records = dict()  # key = callsign, value = datastore entity

    def run(self) -> None:
        ds_client = None
        logging.info('GCPDCRunner started')
        while True:
            # noinspection PyBroadException
            try:
                if not ds_client:
                    ds_client = gu.create_datastore_client()
                while True:
                    try:
                        message = self.incoming_queue.get_nowait()
                        if isinstance(message, m.ExitSession):
                            try:
                                _terminate_session(ds_client, self.session_id)
                            except ValueError as e:
                                logging.warning('Unable to update the session as stopped.', e)
                            logging.info('GCPDCRunner got exit command - exiting')
                            return
                        elif isinstance(message, m.DamageResult):
                            logging.debug('GCPDSRunner got a damage result')
                            _register_damage_result(ds_client, message, self.session_id)
                        elif isinstance(message, m.PositionUpdatesBatch):
                            logging.debug('GCPDSRunner got a a batch of position updates')
                            _upsert_position_updates(ds_client, message, self.position_updates_prev_records,
                                                     self.session_id)
                    except queue.Empty:
                        break  # nothing to do as this just signals an empty queue - and we are done polling for now
            except Exception:
                logging.exception('Some exception during GCPDSRunner.run() occurred. Continuing and see.')
            time.sleep(0.5)
