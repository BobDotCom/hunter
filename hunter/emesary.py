"""Partial implementation of the FG Multiplayer implementation of Emesary.

Documentation: Richard Harrison's development notes has several articles.
E.g. http://chateau-logic.com/content/emesary-multiplayer-bridge-flightgear
and http://wiki.flightgear.org/Emesary

FlightGear side implementation:
* In FG-DATA/Nasal:
    # emesary.nas (version d07492, 2020-08-31)
    # emesary_mp_bridge.nas (version 21675f, 2020-10-19)
    # notifications.nas

* F-14b testbed (https://github.com/Zaretto/fg-aircraft): branch origin/EmesaryMP-dev-F14-v2
    # f-14b/Nasal/fox2.nas: ca. line 1331: in-flight message of sidewinder using GeoEventNotification
    # (ditto): ca. line 2208: flares with ArmamentNotification
    # (ditto): ca. line 2266: hit message
    # f14-b/Nasal/weapons.nas: ca. line 603: cannon hit message
    # f-14b/Nasal/ArmamentNotification.nas: ArmamentNotification implementation (aircraft specific)
    # f-14b/Nasal/damage.nas: ca. line 406: apply damage from ArmamentNotification
    # f-14b/Nasal/f-14b.nas: ca. line 370: routing bridge
    # f-14b/Nasal/AircraftEventNotification.nas: transmit properties directly
    # f-14b/Systems/mp-network.nas: ca. line 23

Other random notes:
* There is limited bandwidth on the notifications because each notification will remain in the MP string property
  for 10 seconds to allow everyone to see it (hopefully).

Hunter implementation:
* Most of the code is split between emesary.py (this module) and emesary_notifications.py retaining as much of the
  naming conventions in Nasal as possible - and keeping some of the module locations.
* Receiving/transmitting over MP is in other modules, where it fits best.
* The documentation in Hunter is mostly regarding the Python implementation. The overall architecture,
  how to use etc. is very nicely described in Nasal and not replicated.


Inspiration for the base62 encoding in Python: https://stackoverflow.com/questions/1119722/base-62-conversion
"""
import logging
import math
import string as s
from typing import Any, Optional, Tuple

import hunter.geometry as g


NotificationAutoTypeId = 1


class Notification:
    __slots__ = ('ident', 'notification_type', 'is_distinct', 'from_incoming_bridge', 'callsign', 'type_id')

    def __init__(self, type_: str, ident: str, type_id: int) -> None:
        self.ident = ident  # Generic message identity. ID or for simple messages a value that needs transmitting
        self.notification_type = type_  # e.g. "AIM-9"
        self.is_distinct = 1  # non-zero if this message supersedes previous messages of this type
        self.from_incoming_bridge = 0  # keep for compatibility instead of using a bool
        self.callsign = None  # populated automatically by the incoming bridge when routed
        self.type_id = type_id
        if self.type_id < 0:
            global NotificationAutoTypeId
            NotificationAutoTypeId += 1  # FIXME: most probably this should be per FG instance (target)
            self.type_id = NotificationAutoTypeId

    @property
    def bridge_message_notification_key(self) -> str:
        return '{}.{}'.format(self.notification_type, self.ident)


STRING_ENCODING = 'latin-1'


class BinaryAsciiTransfer:
    ALPHABET = chr(1) + chr(2) + chr(3) + chr(4) + chr(5) + chr(6) + chr(7) + chr(8) + chr(9) + \
               chr(10) + chr(11) + chr(12) + chr(13) + chr(14) + chr(15) + chr(16) + chr(17) + chr(18) + chr(19) + \
               chr(20) + chr(21) + chr(22) + chr(23) + chr(24) + chr(25) + chr(26) + chr(27) + chr(28) + chr(29) + \
               chr(30) + chr(31) + chr(34) + "%&'()*+,-./" + s.digits + ":;<=>?@" + s.ascii_uppercase + \
               "[\\]^_`" + s.ascii_lowercase + "{|}" + \
               chr(128) + chr(129) + chr(130) + chr(131) + chr(132) + chr(133) + chr(134) + chr(135) + chr(136) + \
               chr(137) + chr(138) + chr(139) + chr(140) + chr(141) + chr(142) + chr(143) + chr(144) + chr(145) + \
               chr(146) + chr(147) + chr(148) + chr(149) + chr(150) + chr(151) + chr(152) + chr(153) + chr(154) + \
               chr(155) + chr(156) + chr(157) + chr(158) + chr(159) + chr(160) + chr(161) + chr(162) + chr(163) + \
               chr(164) + chr(165) + chr(166) + chr(167) + chr(168) + chr(169) + chr(170) + chr(171) + chr(172) + \
               chr(173) + chr(174) + chr(175) + chr(176) + chr(177) + chr(178) + chr(179) + chr(180) + chr(181) + \
               chr(182) + chr(183) + chr(184) + chr(185) + chr(186) + chr(187) + chr(188) + chr(189) + chr(190) + \
               chr(191) + chr(192) + chr(193) + chr(194) + chr(195) + chr(196) + chr(197) + chr(198) + chr(199) + \
               chr(200) + chr(201) + chr(202) + chr(203) + chr(204) + chr(205) + chr(206) + chr(207) + chr(208) + \
               chr(209) + chr(210) + chr(211) + chr(212) + chr(213) + chr(214) + chr(215) + chr(216) + chr(217) + \
               chr(218) + chr(219) + chr(220) + chr(221) + chr(222) + chr(223) + chr(224) + chr(225) + chr(226) + \
               chr(227) + chr(228) + chr(229) + chr(230) + chr(231) + chr(232) + chr(233) + chr(234) + chr(235) + \
               chr(236) + chr(237) + chr(238) + chr(239) + chr(240) + chr(241) + chr(242) + chr(243) + chr(244) + \
               chr(245) + chr(246) + chr(247) + chr(248) + chr(249) + chr(250) + chr(251) + chr(252) + chr(253) + \
               chr(254) + chr(255)
    ALPHABET_B = ALPHABET.encode(STRING_ENCODING)
    BASE = 248
    PO2 = [1, 124, 30752, 7626496, 1891371008, 469060009984, 116326882476032, 28849066854055936]
    SPACES = b'                                  '  # 34 spaces

    @staticmethod
    def spaces(length: int) -> bytes:
        spaces_bytes = b''
        for i in range(length):
            spaces_bytes = spaces_bytes + b' '
        return spaces_bytes

    @staticmethod
    def empty_encoding(length: int) -> bytes:
        empty = b''
        for i in range(length):
            empty = empty + b'chr(1)'
        return  empty

    @staticmethod
    def encode_numeric(input_number: float, length: int, factor: float):
        num = int(input_number / factor)
        irange = BinaryAsciiTransfer.PO2[length]

        if num < -irange:
            num = -irange
        elif num > irange:
            num = irange

        num = int(num + irange)

        if num == 0:
            return BinaryAsciiTransfer.empty_encoding(length)

        arr = b''

        while num > 0 and length > 0:
            num0 = num
            num = int(num / BinaryAsciiTransfer.BASE)
            rem = num0 - (num * BinaryAsciiTransfer.BASE)
            arr = BinaryAsciiTransfer.ALPHABET[rem:rem + 1].encode(STRING_ENCODING) + arr
            length -= 1

        if length > 0:
            arr = BinaryAsciiTransfer.spaces(length) + arr
        return arr

    @staticmethod
    def decode_numeric(input_value: bytes, length: int, factor: float, pos: int) -> Tuple[Any, int]:
        irange = BinaryAsciiTransfer.PO2[length]
        power = length - 1
        value = 0
        pos = pos

        while length > 0 and power > 0:
            c = input_value[pos:pos + 1]
            if c != b' ':
                break
            power -= 1
            length -= 1
            pos += 1

        while length >= 0 and power >= 0:
            c = input_value[pos:pos + 1]
            # spaces are used as padding so ignore them.
            if c != b' ':
                cc = BinaryAsciiTransfer.ALPHABET_B.find(c)
                if cc < 0:
                    logging.warning("Emesary: BinaryAsciiTransfer.decodeNumeric: Bad encoding : ", input_value)
                    return value, pos
                value += int(cc * math.exp(math.log(BinaryAsciiTransfer.BASE) * power))
                power -= 1

            length -= 1
            pos += 1

        value -= irange
        value = value * factor
        return value, pos

    @staticmethod
    def encode_int(input_number: int, length: int):
        return BinaryAsciiTransfer.encode_numeric(input_number, length, 1.0)

    @staticmethod
    def decode_int(input_value: bytes, length: int, pos: int) -> Tuple[Any, int]:
        return BinaryAsciiTransfer.decode_numeric(input_value, length, 1.0, pos)


class TransferString:
    MAX_LENGTH = 16

    @staticmethod
    def get_alphanumeric_char(value: str) -> Optional[str]:
        if BinaryAsciiTransfer.ALPHABET.find(value) > 0:
            return value
        return None

    @staticmethod
    def encode(input_value: str) -> bytes:
        if input_value is None:
            return b'0'
        length = len(input_value)
        if length > TransferString.MAX_LENGTH:
            length = TransferString.MAX_LENGTH
        rv = b''
        actual_length = 0
        for i in range(length):
            ev = TransferString.get_alphanumeric_char(input_value[i:i + 1])
            if ev is not None:
                rv += ev.encode(STRING_ENCODING)
                actual_length += 1

        rv = BinaryAsciiTransfer.encode_numeric(length, 1, 1.0) + rv
        return rv

    @staticmethod
    def decode(input_value: bytes, pos: int) -> Tuple[str, int]:
        value, pos = BinaryAsciiTransfer.decode_numeric(input_value, 1, 1.0, pos)
        length = int(round(value))
        if length == 0:
            return '', pos
        rv = input_value[pos:pos+length]
        pos = pos + length
        return rv.decode('ascii'), pos


class TransferInt:
    @staticmethod
    def encode(input_value: int, length: int) -> bytes:
        return BinaryAsciiTransfer.encode_numeric(input_value, length, 1.0)

    @staticmethod
    def decode(input_value: bytes, length: int, pos: int) -> Tuple[int, int]:
        return BinaryAsciiTransfer.decode_numeric(input_value, length, 1.0, pos)


class TransferFixedDouble:
    @staticmethod
    def encode(input_value: float, length: int, factor: float):
        return BinaryAsciiTransfer.encode_numeric(input_value, length, factor)

    @staticmethod
    def decode(input_value: bytes, length: int, factor: float, pos: int) -> Tuple[float, int]:
        return BinaryAsciiTransfer.decode_numeric(input_value, length, factor, pos)


class TransferByte:
    @staticmethod
    def encode(input_value: int) -> bytes:
        return BinaryAsciiTransfer.encode_numeric(input_value, 1, 1.0)

    @staticmethod
    def decode(input_value: bytes, pos: int) -> Tuple[int, int]:
        return BinaryAsciiTransfer.decode_numeric(input_value, 1, 1.0, pos)


class TransferCoord:
    LAT_LON_LENGTH = 4
    LAT_LON_FACTOR = 0.000001
    ALT_LENGTH = 3

    @staticmethod
    def encode(input_value: g.Position) -> bytes:
        return_value = BinaryAsciiTransfer.encode_numeric(input_value.lat, TransferCoord.LAT_LON_LENGTH,
                                                          TransferCoord.LAT_LON_FACTOR)
        return_value += BinaryAsciiTransfer.encode_numeric(input_value.lon, TransferCoord.LAT_LON_LENGTH,
                                                           TransferCoord.LAT_LON_FACTOR)
        return_value += TransferInt.encode(int(input_value.alt_m), TransferCoord.ALT_LENGTH)
        return return_value

    @staticmethod
    def decode(input_value: bytes, pos: int) -> Tuple[g.Position, int]:
        lat, new_pos = BinaryAsciiTransfer.decode_numeric(input_value, TransferCoord.LAT_LON_LENGTH,
                                                          TransferCoord.LAT_LON_FACTOR, pos)
        lon, new_pos = BinaryAsciiTransfer.decode_numeric(input_value, TransferCoord.LAT_LON_LENGTH,
                                                          TransferCoord.LAT_LON_FACTOR, new_pos)
        alt, new_pos = TransferInt.decode(input_value, TransferCoord.ALT_LENGTH, new_pos)
        return g.Position(lon, lat, alt), new_pos
