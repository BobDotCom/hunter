"""Targets using a real FlightGear instance to be represented over MP."""
from collections import deque, namedtuple
import logging
import time
from enum import Enum, IntEnum
import math
import multiprocessing as mp
import queue
from typing import List, Optional, Tuple

import networkx as nx
import requests

import hunter.gcp_ps_io as gps
import hunter.geometry as g
import hunter.messages as m
from hunter.mp_targets import DEATH_TIME_SHIP, DEATH_TIME_UNKNOWN, DEATH_TIME_VEHICLE, MISSILE_FRIGATE_NAME, \
    MPTargetType
import hunter.fg_instance_utils as fgiu
from hunter.fg_httpd_io import FGHttpd
from hunter.fg_telnet_io import FlightGear
from hunter.messages import HealthType
import hunter.utils as u


class FGMPConnected(u.QueuedRunner):
    __slots__ = ('index', 'mp_server_host', 'target_type', 'fg_httpd', 'fg_telnet', 'callsign', 'crash_as_carrier',
                 'cmd_args', 'destroyed_timestamp', 'death_time')

    def __init__(self, index: int, mp_server_host: str, callsign: str,
                 target_type: MPTargetType, crash_as_carrier: bool, cmd_args: List[str], death_time: int) -> None:
        u.QueuedRunner.__init__(self, callsign)
        self.index = index
        self.mp_server_host = mp_server_host
        self.target_type = target_type
        self.fg_httpd = None  # is faster for property get/set and more convenient
        self.fg_telnet = None  # provides possibility to run Nasal
        self.callsign = callsign  # to display in logging
        self.crash_as_carrier = crash_as_carrier
        self.cmd_args = cmd_args
        self.destroyed_timestamp = 0  # if > 0 then point in time when self.crashed()
        self.death_time = death_time  # how long it takes from being destroyed until being dead and get removed

    @staticmethod
    def start_fg_instance_process(cmd_args: List[str], callsign: str) -> mp.Process:
        logging.info('Starting process for FGMP connected asset %s', callsign)
        p = mp.Process(target=fgiu.run_instance, args=(cmd_args,))
        p.start()
        time.sleep(fgiu.WAIT_FG_READY)  # wait for FG to have loaded and be ready to accept a telnet connection
        return p

    def run(self) -> None:
        raise NotImplementedError

    def initialise_fg_interfaces(self) -> None:
        self._initialise_http_interface()
        self._initialise_telnet_interface()
        time.sleep(5.)

    def _initialise_http_interface(self) -> None:
        http_port = u.PORT_HTTPD_BASIS + self.index
        logging.info('Initializing FG interfaces for %s on HTTP port %i', self.callsign, http_port)
        self.fg_httpd = FGHttpd(http_port, fgiu.HOST_FGFS)

    def _initialise_telnet_interface(self) -> None:
        telnet_port = u.PORT_TELNET_BASIS + self.index
        logging.info('Initializing FG interfaces for %s on Telnet port %i', self.callsign, telnet_port)
        self.fg_telnet = FlightGear(fgiu.HOST_FGFS, telnet_port)
        time.sleep(10.0)  # give it a bit of time
        # Wait five seconds for simulator to settle down
        num_errors = 0
        while True:
            try:
                if self.fg_telnet['/sim/time/elapsed-sec'] > 5:
                    break
                logging.debug('Elapsed time %d', self.fg_telnet['/sim/time/elapsed-sec'])
            except IndexError:
                num_errors += 1
                time.sleep(num_errors)

    def initialise_multiplayer(self) -> None:
        """Connect to MP and tell the world that we are ready to accept commands."""
        port_in = u.PORT_MP_BASIS_CLIENT_WORKER + self.index
        nasal = 'fgcommand("multiplayer-connect", props.Node.new({"servername": "' + self.mp_server_host
        nasal += '", "rxport": {}, "txport": {}'.format(port_in, u.PORT_MP_BASIS)
        nasal += '}));'
        logging.info('Nasal for multi_player %s: %s', self.callsign, nasal)
        self.fg_telnet.run_nasal(nasal)
        logging.info('Connected to MP on %s', self.mp_server_host)

    def throttle_framerate(self, frame_rate: int):
        # not needed because not existing self.fg_httpd.set_single_property('/sim/gui/frame-rate-throttled', 1)
        self.fg_httpd.set_single_property('/sim/frame-rate-throttle-hz', frame_rate)
        logging.info('Throttled frame rate for %s to %i', self.callsign, frame_rate)

    def handle_run_exception(self, p: mp.Process) -> None:
        """Handle if there is an exception in the run method, which cannot be handled."""
        logging.exception('Some error occurred in FGMPConnected %s. Removing the target.', self.callsign)
        try:
            self.tear_fg_down(p)
        except ConnectionRefusedError:
            logging.exception('Cannot take missile asset %s down - probably already down. Nothing else to do.',
                              self.callsign)
        self.outgoing_queue.put_nowait(m.RemoveDeadTarget(self.callsign, True, True))
        return

    def tear_fg_down(self, process: mp.Process) -> None:
        logging.info('Tearing down a FGMPConnected resource. Callsign = %s', self.callsign)
        # noinspection PyBroadException
        try:
            self.fg_httpd.simulator_exit()
        except Exception:
            logging.exception('Unable to request exit for %s', self.callsign)
            if process is not None:
                process.kill()

    def crashed(self) -> bool:
        if self.crash_as_carrier:
            return self.fg_httpd.sunk()
        return self.fg_httpd.crashed()

    def check_health(self) -> HealthType:
        """Checks the health of this FG instance - will often be overridden by sub-classes"""
        # noinspection PyBroadException
        try:
            now = time.time()
            if self.destroyed_timestamp > 0:
                if (now - self.destroyed_timestamp) > self.death_time:
                    logging.info('%s of type %s has passed its death time', self.callsign, self.target_type.name)
                    return HealthType.dead
            elif self.crashed():
                logging.info('%s has crashed', self.callsign)
                self.destroyed_timestamp = now
                return HealthType.destroyed
        except Exception:
            logging.exception('Problem in check_alive for %s of type %s - will continue and return True', self.callsign,
                              self.target_type.name)
        return HealthType.fit_for_fight


MISSILE_RELOAD_TIME = 240  # seconds
MISSILE_RELOAD_SAM_TIME = 480  # seconds
IS_ALIVE_FREQ = 2  # seconds


class AutomatAsset(FGMPConnected):
    """Leto's automat"""
    __slots__ = 'number_of_lives_left'

    def __init__(self, index: int, mp_server_host: str, callsign: str,
                 icao: str, number_of_lives: int, props_string: str,
                 show_world: bool) -> None:
        FGMPConnected.__init__(self, index, mp_server_host, callsign, MPTargetType.plane, True,
                               fgiu.make_command_line_automat(index, callsign, icao, props_string, show_world),
                               DEATH_TIME_UNKNOWN)
        self.number_of_lives_left = 9999 if number_of_lives == 0 else number_of_lives

    def check_health(self) -> HealthType:  # overrides method from base class
        # noinspection PyBroadException
        try:
            if self.fg_httpd.get_float_property('/finish') < 0.9:  # cf. mig28.nas resetMP -> 1 if re-spawning
                return HealthType.fit_for_fight
            else:
                return HealthType.destroyed
        except Exception:
            logging.exception('Problem in check_health for automat - will continue and return True')
        return HealthType.fit_for_fight

    def run(self) -> None:
        p = None
        # noinspection PyBroadException
        try:
            p = self.start_fg_instance_process(self.cmd_args, self.callsign)

            self.initialise_fg_interfaces()

            # Automat makes it own connection to MP through Nasal - thing to do compared to MARunner
            # And we let automat decide on frame rate throttling

            while True:
                try:
                    message = self.incoming_queue.get_nowait()
                    if isinstance(message, m.TearDownTarget):
                        self.tear_fg_down(p)
                        return
                except queue.Empty:
                    pass  # nothing to do as this just signals an empty queue and we are done polling for now
                health = self.check_health()
                # Compared to MARunner no need to check for dead, as the automat re-spawns automatically
                if health is m.HealthType.destroyed:
                    logging.info('Health of automat %s is DESTROYED based on is_alive', self.callsign)
                    self.outgoing_queue.put_nowait(m.FGTargetDestroyed(self.callsign))
                    self.number_of_lives_left -= 1
                    if self.number_of_lives_left <= 0:
                        self.tear_fg_down(p)
                        logging.info('Automat %s has used all its lives. Tearing down.', self.callsign)

                    time.sleep(30)  # wait an extra amount of time until re-spawned
                time.sleep(8.)  # /finish = 1 when dead or similar is kept for ca. 15 seconds
        except Exception:
            self.handle_run_exception(p)
            return  # go out of run()


class MissileAsset(FGMPConnected):
    """A shooting OPRF admin asset"""
    __slots__ = ('aero', 'start_reloading', 'readiness_confirmed', 'reload_time', 'cmd_args')

    def __init__(self, index: int, mp_server_host: str, callsign: str, target_type: MPTargetType,
                 aero: str, position: g.Position, heading: float, death_time: int, show_world: bool) -> None:
        FGMPConnected.__init__(self, index, mp_server_host, callsign, target_type, True,
                               fgiu.make_command_line_sam(index,
                                                          position.lon, position.lat, position.alt_m,
                                                          heading,
                                                          aero, callsign, show_world, True),
                               death_time)
        self.aero = aero  # type name (e.g. 's-300', whereas target_type is 'vehicle')
        self.start_reloading = 0
        self.readiness_confirmed = None  # first time good to go after startup checked?
        self.reload_time = MISSILE_RELOAD_TIME

    def run(self) -> None:
        p = None
        # noinspection PyBroadException
        try:
            p = self.start_fg_instance_process(self.cmd_args, self.callsign)

            self.initialise_fg_interfaces()

            self.throttle_framerate(fgiu.FRAME_RATE_THROTTLE)

            self.initialise_multiplayer()

            while True:
                try:
                    message = self.incoming_queue.get_nowait()
                    if isinstance(message, m.TearDownTarget):
                        self.tear_fg_down(p)
                        return
                except queue.Empty:
                    pass  # nothing to do as this just signals an empty queue and we are done polling for now
                health = self.check_health()
                if health is m.HealthType.dead:
                    logging.info('Health of missile asset %s is DEAD based on check_health', self.callsign)
                    self.tear_fg_down(p)
                    self.outgoing_queue.put_nowait(m.RemoveDeadTarget(self.callsign, False, True))
                    return
                elif health is m.HealthType.destroyed:
                    logging.info('Health of missile asset %s is DESTROYED based on check_health', self.callsign)
                    self.outgoing_queue.put_nowait(m.FGTargetDestroyed(self.callsign))
                time.sleep(IS_ALIVE_FREQ)
        except Exception:
            self.handle_run_exception(p)
            return  # go out of run()

    def set_enemies(self) -> None:
        """Make sure not to shoot at OPFOR callsigns."""
        self.fg_httpd.set_single_property('/enemies/opfor-switch', '1')
        logging.info('Set enemies to non-OPFOR')

    def check_health(self) -> HealthType:  # overrides method from base class
        # noinspection PyBroadException
        try:
            now = time.time()
            self._process_other_state()
            if self.destroyed_timestamp > 0:
                if (now - self.destroyed_timestamp) > self.death_time:
                    logging.info('Missile asset of type %s has passed its death time', self.aero)
                    return HealthType.dead
            elif self.crashed():
                logging.info('Missile asset of type %s has crashed', self.aero)
                self.destroyed_timestamp = now
                return HealthType.destroyed
            else:
                if self.readiness_confirmed:
                    if self._is_out_of_missiles():
                        if self.start_reloading == 0:  # we just discovered it
                            self.start_reloading = time.time()
                            self._change_radar_active(False)
                            logging.info('%s out of missiles: reload in seconds = %i', self.callsign, self.reload_time)
                        elif time.time() > self.start_reloading + self.reload_time:
                            self._reload_missiles()
                            time.sleep(1)
                            self._change_radar_active(True)
                            self.start_reloading = 0
                            logging.info('%s reloaded missiles', self.callsign)
                else:
                    self._check_readiness()

        except Exception:
            logging.exception('Problem in check_health for missile asset - will continue and return True')
        return HealthType.fit_for_fight

    def _process_other_state(self) -> None:
        """Can be overridden in subclasses to do additional processing of state in the FGFS instance.

        Called in is_alive()"""
        pass

    def _reload_missiles(self) -> None:
        self.fg_telnet.run_nasal('fire_control.reload()')

    def _is_out_of_missiles(self) -> bool:
        remaining = self.fg_httpd.get_float_property('/sam/missiles')
        return remaining < 1.

    def _check_readiness(self) -> None:
        if self.readiness_confirmed is None:
            self.set_enemies()
            self._change_radar_active(False)
            self.readiness_confirmed = False
            return

        remaining = self.fg_httpd.get_float_property('/sam/timeleft')
        if remaining < 1.:
            self._change_radar_active(True)
            self.readiness_confirmed = True

    def _change_radar_active(self, is_active: bool) -> None:
        """Toggle the radar active/passive.
        Currently, logic is commented out, because in Nasal/fire-control.nas the variable radarOn gets
        set all the time in iteration and thereby overrides stuff.
        """
        # value = 0 if is_active else 1  # value 1 means silent, 0 means active (yes, so it is for OPRF)
        # self.fg_httpd.set_single_property('/sim/multiplay/generic/int[2]', value)
        pass


class SurfaceAirMissile(MissileAsset):
    """A static and shooting SAM utilising the OPRF admin assets"""
    def __init__(self, index: int, mp_server_host: str, callsign: str, aero: str,
                 position: g.Position, heading: float, show_world: bool) -> None:
        super().__init__(index, mp_server_host, callsign, MPTargetType.vehicle, aero, position, heading,
                         DEATH_TIME_VEHICLE, show_world)
        self.reload_time = MISSILE_RELOAD_SAM_TIME


class MissileFrigate(MissileAsset):
    """A missile frigate utilizing the OPRF admin assets.

    Quite a bit of the code and attributes are in sync with MPMovingTarget.
    """

    CRUISE_SPEED = 11  # ca. 21 kt
    CRUISE_THROTTLE = 0.7  # (ca. 21 kt)
    CRUISE_TURN_RATE = 1  # 1 deg per second, i.e. 6 minutes for turning 360 degrees
    CRUISE_RUDDER = 0.2
    SLOW_TURN_DEGREES = 10
    SLOW_TURN_RUDDER = 0.05  # Make sure we are not constantly overshooting for corrections due to delay
    PERFORMANCE_TURN_DEGREES = 45
    PERFORMANCE_THROTTLE = 0.4  # less speed when more rudder
    PERFORMANCE_RUDDER = 0.5
    NO_TURN_DEGREES = 3  #

    def __init__(self, index: int, mp_server_host: str, callsign: str, graph: nx.Graph, start_node: g.WayPoint,
                 next_node: g.WayPoint, show_world: bool) -> None:
        self.path = graph
        self.current_wp = next_node  # the way-point we are sailing towards
        self.next_wp = g.random_next_way_point(graph, self.current_wp, start_node)  # the way-point after current
        start_heading = g.calc_bearing_wp(start_node, next_node)
        self.alt_m = start_node.alt_m  # not entirely correct, but good enough, so we don't fetch it from the instance

        super().__init__(index, mp_server_host, callsign, MPTargetType.ship, MISSILE_FRIGATE_NAME,
                         start_node.make_position(), start_heading, DEATH_TIME_SHIP, show_world)

        self.current_rudder = 0
        self.current_throttle = 0

    def _process_other_state(self) -> None:  # overrides method from parent
        """Updates settings for throttle and rudder as a primitive auto-pilot."""
        # Get the current position and heading from the FG instance and set it in the instance variables
        # (periodic sending of position/orientation to controller through worker is based on instance variables)
        # noinspection PyBroadException
        try:
            pos_lon = self.fg_httpd.get_float_property('/position/longitude-deg')
            pos_lat = self.fg_httpd.get_float_property('/position/latitude-deg')
            pos_alt = g.feet_to_metres(self.fg_httpd.get_float_property('/position/altitude-ft'))
            position = g.Position(pos_lon, pos_lat, pos_alt)
            hdg = self.fg_httpd.get_float_property('/orientation/heading-deg')

            delta_horizontal = g.calc_distance_pos_wp(position, self.current_wp)
            next_bearing = g.calc_bearing_pos_wp(position, self.next_wp)
            delta_bearing = g.calc_delta_bearing(hdg, next_bearing)
            distance_to_turn = math.fabs(delta_bearing) / self.CRUISE_TURN_RATE * self.CRUISE_SPEED
            distance_to_turn += self.CRUISE_SPEED  # increase the distance a bit -> do not start turning too late

            if distance_to_turn > delta_horizontal:
                # change nodes and start travelling that way, but max with turn_rate
                prev_wp = self.current_wp
                self.current_wp = self.next_wp
                self.next_wp = g.random_next_way_point(self.path, self.current_wp, prev_wp)

            # recalculate after the way-points maybe have changed
            current_bearing = g.calc_bearing_pos_wp(position, self.current_wp)
            delta_bearing = g.calc_delta_bearing(hdg, current_bearing)

            # adapt throttle and rudder
            next_throttle = self.CRUISE_THROTTLE
            if math.fabs(delta_bearing) >= self.PERFORMANCE_TURN_DEGREES:
                next_throttle = self.PERFORMANCE_THROTTLE
                next_rudder = self.PERFORMANCE_RUDDER
            elif math.fabs(delta_bearing) >= self.SLOW_TURN_DEGREES:
                next_rudder = self.CRUISE_RUDDER
            elif math.fabs(delta_bearing) >= self.NO_TURN_DEGREES:
                next_rudder = self.SLOW_TURN_RUDDER
            else:
                next_rudder = 0
            if delta_bearing < 0:  # counter-clock
                next_rudder *= -1  # minus is to turn left

            if next_throttle != self.current_throttle:
                self.current_throttle = next_throttle
                self.fg_httpd.set_single_property('/controls/engines/engine/throttle', next_throttle)
            if next_rudder != self.current_rudder:
                self.current_rudder = next_rudder
                self.fg_httpd.set_single_property('/controls/flight/rudder', next_throttle)
        except Exception:
            logging.exception('Problem in _process_other_state() for missile frigate - will continue and return')


class CarrierType(Enum):
    clemenceau = '026Y'
    eisenhower = '030Y'
    nimitz = '029Y'
    vinson = '029X'


def make_tacan_callsign(carrier_type: CarrierType) -> str:
    return 'MP_' + str(carrier_type.value)


def parse_carrier_type_name(type_name: str) -> Optional[CarrierType]:
    for member in CarrierType:
        if type_name == member.name:
            return member
    return None


CarrierWind = namedtuple('CarrierWind', ['abs_wind_deg', 'abs_wind_kts', 'rel_wind_deg', 'rel_wind_kts'])


class CarrierCourseType(IntEnum):
    """Must be aligned with gcp_ps_io.py constants"""
    loiter = 0  # loitering around because nothing to do - navigating towards or near the loiter point
    navigate = 1  # the carrier is commanded into a specific direction
    evade = 2  # the carrier is at the border of the sail area and needs to get free

    recovery = 10  # same as for FG AI carrier -> aircraft landing
    launch = 20  # same as for FG AI carrier -> aircraft starting'

    unknown = 99


def _map_carrier_course_type(value: int) -> CarrierCourseType:
    for type_ in CarrierCourseType:
        if type_.value == value:
            return type_
    logging.warning('Possible programming error: could not map value = %i to CarrierCourseType', value)
    return CarrierCourseType.unknown


class Carrier(FGMPConnected):
    """An MP Carrier controlled programmatically.

    The sail area is an enclosed list of lon/lat tuples, where the carrier can move.
    Given that a carrier can sail 20 or more miles per hour, the area should be big.

    In vinson-set.xml -> common.xml keybindings ->
    "CarrierControl" is mapped to Aircraft/MPCarrier/Systems/commander.nas
    CarrierControl.toggleAIControl() -> -> MP_CONTROL must be true such that key commands can work
    Left: CarrierControl.incRudder(-0.5) -> TGT_SPEED_KTS (not rudder)
    PgDown: CarrierControl.incSpeed(-0.5) -> TGT_HEADING_DEGS

    See also: https://wiki.hoggitworld.com/view/Carrier_Air_Operations

    """
    MP_CONTROL = '/controls/mp-control'  # below ai model path
    LIGHTING = '/controls/lighting/'  # below ai model path
    DECK_LIGHTS = 'deck-lights'  # true or false
    FLOOD_LIGHTS_RED_NORM = 'flood-lights-red-norm'  # 1 or 0
    TGT_SPEED_KTS = '/controls/tgt-speed-kts'  # absolute path. Only available in manual mode
    TGT_HEADING_DEGS = '/controls/tgt-heading-degs'  # absolute path. Only available in manual mode
    ABS_WIND_DEG = '/environment/wind-from-heading-deg'  # absolute path
    ABS_WIND_KTS = '/environment/wind-speed-kt'  # absolute path

    SPEED_LOITER = 5  # kts - guess
    SPEED_OPS_MIN = 5  # kts - guess for minimal speed ahead during launch/recovery to keep the boat's rudder effective
    SPEED_NAVIGATE = 15  # a bit fast, but not too much
    SPEED_EVADE = 20  # want to be a bit fast to get in a good position soon

    WARNING_FLIGHT_OPS_TIME = 600  # seconds before evading manoeuvre is initiated (distance depends on speed)
    MIN_FLIGHT_OPS_TIME = 1200  # minimum seconds of free sailing to even initiate flight operations (incl. turn time)
    EVADE_DIST = 5000  # distance from intersecting sail area when evading manoeuvre is initiated
    LOITER_DIST = 10000

    SAILING_CHECKS = 60

    def __init__(self, index: int, mp_server_host: str, callsign: str, carrier_type: CarrierType,
                 sail_area: List[Tuple[float, float]], loiter_centre: Tuple[float, float],
                 show_world) -> None:
        FGMPConnected.__init__(self, index, mp_server_host, callsign, MPTargetType.ship, False,
                               fgiu.make_command_line_carrier(index,
                                                              loiter_centre[0], loiter_centre[1],
                                                              carrier_type.name,
                                                              callsign, show_world,
                                                              True),
                               DEATH_TIME_SHIP)
        self.aero = carrier_type.name
        self.sail_area = sail_area  # List of lon_lat tuples. Last tuple != first tuple
        self.loiter_centre = loiter_centre  # where the carrier starts from and returns to if nothing happens
        self.ai_model_path = None
        self.course_command = CarrierCourseType.launch  # FIXME should be loiter
        self.winds = deque()  # tuples of abs_wind_deg and abs_wind_kts

        self.avg_wind_deg = 0.
        self.avg_wind_kts = 0.
        self.asked_relative_wind_speed = 25.  # from captain command
        self.asked_navigation_course = 10.  # from captain command
        self.asked_deck_lights = None  # from captain command - can be None, True or False
        self.asked_flood_lights = None  # from captain command

        self.last_sailing_check = 0
        self.flight_ops_warning_sent = False

    def run(self) -> None:
        p = None
        # noinspection PyBroadException
        try:
            p = self.start_fg_instance_process(self.cmd_args, self.callsign)

            self.initialise_fg_interfaces()

            self.throttle_framerate(fgiu.FRAME_RATE_THROTTLE)

            success = self._find_ai_model_path()
            if not success:
                logging.info('Carrier could not be properly matched in property tree - tearing down')
                self.tear_fg_down(p)
                self.outgoing_queue.put_nowait(m.RemoveDeadTarget(self.callsign, True, True))
                return

            self.initialise_multiplayer()

            self._start_sailing()

            while True:
                now = time.time()
                if (now - self.last_sailing_check) > self.SAILING_CHECKS:
                    self.last_sailing_check = now
                    self._check_position_and_course()
                    self._check_lights()
                try:
                    message = self.incoming_queue.get_nowait()
                    if isinstance(message, m.TearDownTarget):
                        self.tear_fg_down(p)
                        return
                    elif isinstance(message, m.GCPSubData):
                        self.asked_navigation_course = message.content_dict[gps.CARRIER_NAVIGATE_COURSE_DEG]
                        self.asked_deck_lights = message.content_dict[gps.CARRIER_DECK_LIGHTS]
                        self.asked_flood_lights = message.content_dict[gps.CARRIER_FLOOD_LIGHTS]
                        course_type = _map_carrier_course_type(message.content_dict[gps.CARRIER_COURSE_TYPE])
                        if course_type is not CarrierCourseType.unknown:
                            logging.info('Got a command from the captain with course %s', course_type.name)
                            self._change_course_command(course_type)
                except queue.Empty:
                    pass  # nothing to do as this just signals an empty queue and we are done polling for now
                health = self.check_health()
                if health is m.HealthType.dead:
                    logging.info('Health of carrier %s is DEAD based on is_alive', self.callsign)
                    self.tear_fg_down(p)
                    self.outgoing_queue.put_nowait(m.RemoveDeadTarget(self.callsign, False, True))
                    return
                elif health is m.HealthType.destroyed:
                    logging.info('Health of carrier %s is DESTROYED based on is_alive', self.callsign)
                    self.outgoing_queue.put_nowait(m.FGTargetDestroyed(self.callsign))

                time.sleep(IS_ALIVE_FREQ)
        except Exception:
            self.handle_run_exception(p)
            return  # go out of run()

    def _get_lon_lat(self) -> Tuple[float, float]:
        pos_lon = self.fg_httpd.get_float_property('/position/longitude-deg')
        pos_lat = self.fg_httpd.get_float_property('/position/latitude-deg')
        return pos_lon, pos_lat

    def _get_tgt_speed(self) -> float:
        return self.fg_httpd.get_float_property(self.TGT_SPEED_KTS)

    def _get_tgt_deg(self) -> float:
        return self.fg_httpd.get_float_property(self.TGT_HEADING_DEGS)

    def _change_course_command(self, new_course_command: CarrierCourseType) -> None:
        """Makes sure that the carrier goes where the captain wants - within the sail area limits.
        In base course there are two possibilities: either loiter around the centre point in a rectangular of ca.
        4 nm size and slow speed - or travel back to the loiter point.
        For launch and recovery the course is based on generating a good relative headwind, which for the angled
        recovery deck is different from the sail direction.

        From https://www.quora.com/During-landing-operations-do-aircraft-carriers-increase-their-speed-in-order-to-keep-
        a-slower-relative-speed-to-landing-aircraft

        Approximately 30 knots of wind over the deck is common for flight operations. If there is much less than 30 kts.
        of natural wind, the carrier will likely speed up to gain more wind. If there is no natural wind, the carrier
        will speed up to 25-30 knots through the water to make all its own wind over the deck. This accomplishes a
        couple of things:
        * Slower relative approach speed making it easier to land aboard.
        * Slower trap speed, reducing wear and tear on the cables, arresting gear, and the aircraft.

        The highest desired speed is normally at the beginning of the cycle, when the heaviest planes are taking off.
        In my era that was normally fully-loaded A-6 Intruders needing 27 knots of wind, 10 degrees to port.
        Once they were gone the Boss would tell us to slow, typically to ~25 knots wind over deck.

        The speed of the carrier is entirely secondary to the relative speed of the wind over the deck (for the
        purposes of recovering aircraft). If there is a nice, 25-knot wind, you might see the carrier barely making
        steerage speed, with a light, trickling wake. The wind will be in that sweet spot, coming right down the angled
        deck (9° offset from the ship’s axis on the Nimitz-class boats). That reduces the burble (that wind that is
        upset from the island and the rest of the ship). When the winds are light, in order to find a suitable
        wind-over-deck (WOD), they’ll pull the rods and set whatever speed the Air Boss calls for. When there is no
        wind, you’ll see a massive wake, and know that the winds are axial and the burble is going to be nasty.

        ---
        A nuclear carrier needs ca. 1-1.5 min to accelerate from 10 to 20 kts and ca. 3 min from 10 to 30 kts.
        A conventional needs ca. 2.5 to 5 min to accelerate from 10 to 20 kts. and ca. 12.5 min from 10 to 30 kts
        if all 8 boilers are online. Otherwise much longer.
        """
        if new_course_command is not self.course_command:
            # make sure that there is enough time left to do flight operations if we are going into flight ops
            # if we are already in flight ops and just changing between recovery and launch, then only little
            # course corrections etc. -> continue
            # using launch params as approximation
            lack_of_time = False
            if new_course_command in [CarrierCourseType.recovery, CarrierCourseType.launch] and self.course_command \
                    not in [CarrierCourseType.recovery, CarrierCourseType.launch]:
                lon_lat = self._get_lon_lat()
                current_tgt_speed = self._get_tgt_speed()
                current_tgt_deg = self._get_tgt_deg()
                lack_of_time = self._check_violating_sail_area(lon_lat, self.MIN_FLIGHT_OPS_TIME, current_tgt_speed,
                                                               current_tgt_deg)
            if lack_of_time:
                logging.info('Overriding request for %s flight ops due running out of time in sail area',
                             new_course_command.name)
                logging.info('Keeping command %s', self.course_command.name)
                new_course_command = self.course_command
            else:
                logging.info('Got a new course commanded: %s', new_course_command.name)
                self._send_chat('Command I am following is {}'.format(new_course_command.name))

        if new_course_command is self.course_command and self.course_command not in [CarrierCourseType.recovery,
                                                                                     CarrierCourseType.launch]:
            return  # we do not make any course or speed corrections if command has not changed unless in flight ops

        if new_course_command not in [CarrierCourseType.recovery, CarrierCourseType.launch]:
            self.flight_ops_warning_sent = False

        # calculate speed and course given new commands or if in flight ops
        asked_speed = 0
        asked_course = 0
        self.course_command = new_course_command
        if self.course_command is CarrierCourseType.loiter:
            asked_speed = self.SPEED_LOITER
            lon, lat = self._get_lon_lat()
            asked_course = g.calc_bearing(lon, lat, self.loiter_centre[0], self.loiter_centre[1])
        elif self.course_command is CarrierCourseType.navigate:
            asked_speed = self.SPEED_NAVIGATE
            asked_course = self.asked_navigation_course
        elif self.course_command is CarrierCourseType.evade:
            asked_speed = self.SPEED_NAVIGATE
            lon, lat = self._get_lon_lat()
            asked_course = g.calc_bearing(lon, lat, self.loiter_centre[0], self.loiter_centre[1])
        elif self.course_command is CarrierCourseType.recovery:
            asked_speed, asked_course = self._calc_recovery_tgt_speed_course()
        elif self.course_command is CarrierCourseType.launch:
            asked_speed, asked_course = self._calc_launch_tgt_speed_course()

        # based on recalculated asked speed and course check whether the ships needs to change speed and course at all
        current_tgt_speed = self._get_tgt_speed()
        current_tgt_deg = self._get_tgt_deg()

        delta = asked_speed - current_tgt_speed
        if math.fabs(delta) > 1.:
            logging.info('New target speed: %d', asked_speed)
            self.fg_telnet.run_nasal('CarrierControl.incSpeed({})'.format(delta))
            time.sleep(2.)
        delta = g.calc_delta_bearing(current_tgt_deg, asked_course)
        if math.fabs(delta) > 2.:

            logging.info('New target course deg: %d - prev: %d', asked_course, current_tgt_deg)
            self._send_chat('Going for BRC {} - was {}'.format(int(asked_course), current_tgt_deg))
            self.fg_telnet.run_nasal('CarrierControl.incRudder({})'.format(delta))
            time.sleep(2.)
        # finally check whether flight ops should be warned
        lon_lat = self._get_lon_lat()
        if self._check_violating_sail_area(lon_lat, self.WARNING_FLIGHT_OPS_TIME, asked_speed, asked_course):
            if self.flight_ops_warning_sent is False:
                self._send_chat('{} seconds or less left for flight ops'.format(self.WARNING_FLIGHT_OPS_TIME))
                self.flight_ops_warning_sent = True

    def _calc_launch_tgt_speed_course(self) -> Tuple[float, float]:
        if self.avg_wind_kts <= 0.5:
            tgt_course = self._get_tgt_deg()
        else:
            tgt_course = self.avg_wind_deg  # wind is from so we just can use the same direction for heading
        if tgt_course > 360:
            tgt_course -= 360
        tgt_speed = self.asked_relative_wind_speed - self.avg_wind_kts
        if tgt_speed < self.SPEED_OPS_MIN:
            tgt_speed = self.SPEED_OPS_MIN
        return tgt_speed, tgt_course

    def _calc_recovery_tgt_speed_course(self) -> Tuple[float, float]:
        """Paper and pen based approximation based on a 25 kt wind speed and 9 deg deck.

        Table below is: wind, degrees clock-way course, kt carrier speed
        * 2, 92, 24
        * 5, 32, 21
        * 10, 23, 15,5
        * 15, 16, 10
        * 20, 10, 5
        * 25, 0, 0 (however ship never sails below 5 kts)
        """
        tgt_course = self.avg_wind_deg  # wind is from so we just can use the same direction for heading
        tgt_speed = 25
        if self.avg_wind_kts <= 0.5:
            tgt_speed += 0
            tgt_course = self._get_tgt_deg()
        elif self.avg_wind_kts <= 2:
            tgt_speed += 2 - self.avg_wind_kts
            tgt_course += 92
        elif self.avg_wind_kts <= 5:
            tgt_speed += 1 - self.avg_wind_kts
            tgt_course += 32 + (5 - self.avg_wind_kts) * (60. / 3.)
        elif self.avg_wind_kts <= 10:
            tgt_course += 0.5 - self.avg_wind_kts
            tgt_course += 23 + (10 - self.avg_wind_kts) * (9. / 5.)
        elif self.avg_wind_kts <= 15:
            tgt_speed += 0 - self.avg_wind_kts
            tgt_course += 16 + (15 - self.avg_wind_kts) * (7. / 5.)
        elif self.avg_wind_kts <= 20:
            tgt_speed += 0 - self.avg_wind_kts
            tgt_course += 10 + (20 - self.avg_wind_kts) * (6. / 5.)
        else:
            tgt_speed += 0 - self.avg_wind_kts
            tgt_course += 9

        if tgt_speed < self.SPEED_OPS_MIN:
            tgt_speed = self.SPEED_OPS_MIN
        if tgt_course > 360:
            tgt_course -= 360
        return tgt_speed, tgt_course

    def _fetch_wind_info(self) -> None:
        """Checks current wind direction and does some averaging - return True if average changed.

        Averaging done so the course is not swinging all the time just due to a bit change in wind.
        """
        # fetch current absolute wind from FG
        abs_wind_deg = self.fg_httpd.get_float_property(self.ABS_WIND_DEG)
        abs_wind_kts = self.fg_httpd.get_float_property(self.ABS_WIND_KTS)

        # do the averaging over the last 3 winds
        self.winds.appendleft((abs_wind_deg, abs_wind_kts))
        if len(self.winds) > 4:
            self.winds.pop()
        avg_abs_wind_deg = 0
        avg_abs_wind_kts = 0
        for wind_tuple in self.winds:
            avg_abs_wind_deg += wind_tuple[0]
            avg_abs_wind_kts += wind_tuple[1]
        avg_abs_wind_deg /= len(self.winds)
        if avg_abs_wind_deg < 0:
            avg_abs_wind_deg += 360
        avg_abs_wind_kts /= len(self.winds)

        # check deviation from saved average and do updates
        winds_changed = False
        if math.fabs(avg_abs_wind_deg - self.avg_wind_deg) > 5:
            self.avg_wind_deg = avg_abs_wind_deg
            winds_changed = True
        if math.fabs(avg_abs_wind_kts - self.avg_wind_kts) > 2:
            self.avg_wind_kts = avg_abs_wind_kts
            winds_changed = True
        if winds_changed:
            logging.info('Average wind has changed to %d deg and %d kts', self.avg_wind_deg, self.avg_wind_kts)

    def _check_position_and_course(self) -> None:
        # get our current position
        lon_lat = self._get_lon_lat()

        # get the wind updated
        self._fetch_wind_info()

        proposed_course_command = None

        # if we are evading and within x miles from loiter point -> change to loiter because no need to steam fast
        if self.course_command is CarrierCourseType.evade:
            dist = g.calc_distance(lon_lat[0], lon_lat[1], self.loiter_centre[0], self.loiter_centre[1])
            if dist < self.LOITER_DIST:  # just using the same distance
                proposed_course_command = CarrierCourseType.loiter
        else:
            # make sure we are not exiting the sail area -> evade
            current_tgt_speed = self._get_tgt_speed()
            current_tgt_deg = self._get_tgt_deg()
            if self._check_violating_sail_area(lon_lat, 0., current_tgt_speed, current_tgt_deg):
                proposed_course_command = CarrierCourseType.evade

        if proposed_course_command is None:
            proposed_course_command = self.course_command

        # make sure to do the course calculations - even if it is the same course (winds might have changed)
        self._change_course_command(proposed_course_command)

    def _check_violating_sail_area(self, lon_lat: Tuple[float, float], sail_time: float,
                                   tgt_speed: float, tgt_course: float) -> bool:
        """Checks whether carrier would cross sail area boundary with current tgt speed and course."""
        transformer = g.make_utm_transformer(lon_lat[0], lon_lat[1])
        dist = g.knots_to_ms(tgt_speed) * sail_time + self.EVADE_DIST
        dest_point = g.calc_destination(lon_lat[0], lon_lat[1], dist, tgt_course)
        line = g.local_line_from_lon_lat(lon_lat[0], lon_lat[1], dest_point[0], dest_point[1], transformer)
        for idx in range(0, len(self.sail_area)):
            idx_prev = len(self.sail_area) - 1 if idx == 0 else idx - 1
            cross_line = g.local_line_from_lon_lat(self.sail_area[idx_prev][0], self.sail_area[idx_prev][1],
                                                   self.sail_area[idx][0], self.sail_area[idx][1],
                                                   transformer)
            if line.intersects(cross_line):
                return True
        return False

    def _check_lights(self):
        sun_angle = self.fg_httpd.get_float_property('/sim/time/sun-angle-rad')
        # deck lights
        needed = False  # especially if we are not in recovery, then shut them down
        if self.course_command is CarrierCourseType.recovery:
            if self.asked_deck_lights is None and sun_angle > 1.6:
                needed = True
            elif self.asked_deck_lights is True:
                needed = True
        actual_status = self._get_deck_lights()
        if actual_status is not needed:
            self._set_deck_lights(needed)

        # red flood light
        needed = False
        if self.course_command in [CarrierCourseType.recovery, CarrierCourseType.launch]:
            if self.asked_flood_lights is None and sun_angle > 1.6:
                needed = True
            elif self.asked_flood_lights is True:
                needed = True
        actual_status = self._get_flood_lights()
        if actual_status is not needed:
            self._set_flood_lights(needed)

    def _find_ai_model_path(self) -> bool:
        """Find out which path to use in the property tree for the carrier."""
        base_path = '/ai/models/carrier'
        for i in range(20):
            if i == 0:
                postfix = ''
            else:
                postfix = '[{}]'.format(i)
            try:
                logging.info(base_path + postfix)
                name = self.fg_httpd.get_str_property(base_path + postfix + '/name')
                logging.debug(name)
                if name.lower() == self.aero:
                    self.ai_model_path = base_path + postfix
                    return True
            except requests.HTTPError as e:
                logging.info('Error during searching /ai/model/carrier: %s', e.response)
                return False
        return False

    def _start_sailing(self) -> None:
        logging.info('%s is starting to sail', self.callsign)
        # place ship at loiter point
        self.fg_httpd.set_single_property(self.ai_model_path + '/position/longitude-deg', self.loiter_centre[0])
        self.fg_httpd.set_single_property(self.ai_model_path + '/position/latitude-deg', self.loiter_centre[1])
        time.sleep(5.)

        # toggle the AI control to make us the commander
        self.fg_telnet.run_nasal('CarrierControl.toggleAIControl()')  # Ctrl-A in keymapping
        time.sleep(2.)

        self._send_chat('Ready to steam')
        time.sleep(2.)

        self.fg_telnet.view_next()  # show the ship from helicopter view

    def _set_deck_lights(self, on: bool) -> None:
        logging.info('Setting deck lights to on: %s', on)
        self.fg_httpd.set_bool_property(self.ai_model_path + self.LIGHTING + self.DECK_LIGHTS, on, False)
        time.sleep(1.)

    def _get_deck_lights(self) -> bool:
        return self.fg_httpd.get_bool_property(self.ai_model_path + self.LIGHTING + self.DECK_LIGHTS, False)

    def _set_flood_lights(self, on: bool) -> None:
        logging.info('Setting red flood lights to on: %s', on)
        self.fg_httpd.set_bool_property(self.ai_model_path + self.LIGHTING + self.FLOOD_LIGHTS_RED_NORM, on, True)
        time.sleep(1.)

    def _get_flood_lights(self) -> bool:
        return self.fg_httpd.get_bool_property(self.ai_model_path + self.LIGHTING + self.FLOOD_LIGHTS_RED_NORM, True)

    def _send_chat(self, message: str) -> None:
        self.fg_httpd.send_chat('Ahoy! ' + message)


# Clemenceau: 8 deg angle
    # Nimitz etc: 9 deg

    # /ai/models/carrier[2]
    # controls/ai-control = 'true' (bool)
    # controls/turn-radius-ft = 12000 (double)
    # controls/tgt-heading-degs = '285' (double)
    # controls/tgt-speed-kts = '10' (double)
    # controls/constants/rudder = 0.5 , speed = 0.5

    # /ai/models/carrier[i]/position/
    # /ai/models/carrier[i]/orientation

    # In vinson-set.xml "CarrierControl" is mapped to Aircraft/MPCarrier/Systems/commander.nas
    # CarrierControl.incRudder(-0.5) # P_controls_rudder is by default 0 and therefore not the rudder but course is set
    # CarrierControl.toggleAIControl()
    # CarrierControl.incSpeed(-0.5)  # max speed is 30 kt

    # "position/latitude-deg";
    # "position/longitude-deg";
    # "position/altitude-ft";
    # "orientation/true-heading-deg";
    # "velocities/speed-kts";

    # environment/surface-wind-from-true-degs = 150 (double)
    # environment/surface-wind-speed-true-kts = 3 (double)
    # environment/rel-wind-from-carrier-hdg-degs = 5.5 (double)
    # environment/rel-wind-speed--kts = 23.9 (double)
