import logging
import random
from abc import ABC, abstractmethod
from math import sqrt
from typing import Tuple

from hunter.messages import HealthType, WeaponType
import hunter.emesary as e
import hunter.emesary_notifications as en

BROKEN_RATE = 0.33  # ratio of max hit points when a target is broken, but not destroyed


def _deduct_cannon_weapon_type(part: str) -> WeaponType:
    """For a hit based on cannon_types, determine the weapon type cannon vs. rocket"""
    if 'rocket' in part or 'Hydra-70' in part or 'SNEB' in part:
        return WeaponType.rocket
    return WeaponType.cannon


def _deduct_ground_weapon_type(part: str) -> WeaponType:
    # Russian unguided
    if 'FAB-100' in part or 'FAB-250' in part or 'FAB-500' in part or 'KAB-500' in part or 'OFAB-100' in part or \
            'ZB-250' in part or 'ZB-500' in part:
        return WeaponType.unguided_ground
    # Russian nuclear
    elif 'RN-14T' in part or 'RN-18T' in part:
        return WeaponType.unguided_ground
    # Swedish unguided
    elif 'M71' in part:
        return WeaponType.unguided_ground
    # American unguided
    elif 'MK-8' in part:
        return WeaponType.unguided_ground
    elif 'HVAR' in part:
        return WeaponType.rocket
    elif 'LAU-68' in part:
        return WeaponType.rocket
    elif 'LAU-10' in part:
        return WeaponType.rocket
    return WeaponType.guided_ground


def _max_damage_dist_from_warhead(lbs: float) -> float:
    """Calculates the max distance for explosion to have impact based on detonation power.

    Based on maxDamageDistFromWarhead in damage.nas.
    """
    return 3 * sqrt(lbs)


class TargetWithDamage(ABC):
    INVULNERABLE_HP = -1  # used on max_hp makes it invulnerable
    STATIC_HP = 1000000  # just a very high number that leads to virtually never broken, but still getting hit

    __slots__ = ('_callsign', 'health', 'max_hp', 'full_damage_dist_m', 'use_hitpoints_instead_of_failure_modes_bool')

    def __init__(self, max_hit_points: int, full_damage_dist_m: int, use_hitpoints: bool = True) -> None:
        self._callsign = 'REPLACE'
        self.health = HealthType.fit_for_fight
        self.max_hp = max_hit_points  # if 0, then no damage enabled
        self.full_damage_dist_m = full_damage_dist_m
        self.use_hitpoints_instead_of_failure_modes_bool = use_hitpoints

    @abstractmethod
    def fail_systems(self, probability: float, factor: float = 100, by_cannon: bool = False) -> HealthType:
        return HealthType.fit_for_fight

    @property
    def callsign(self) -> str:
        return self._callsign

    @callsign.setter
    def callsign(self, name: str) -> None:
        if name is None or len(name) > 7:
            raise ValueError('A callsign must at least have one character and not more than 7')
        self._callsign = name

    @property
    def invulnerable(self) -> bool:
        return self.max_hp == self.INVULNERABLE_HP

    def make_invulnerable(self) -> None:
        self.max_hp = self.INVULNERABLE_HP

# damage.nas version 2023-05-15 (3c6d957)


shells_em = {
    # [id,damage,(name)]
    #
    # 0.20 means a direct hit will disable 20% of the failure modes on average.
    # or, 0.20 also means a direct hit can do 20 hitpoints damage.
    #
    # Damage roughly proportional to projectile weight.
    # If weight isn't listed here, it was estimated from dimensions (proportional to diameter^2 * length).
    # Approximate formulae for cannons:
    # damage ~ weight / 3.6 (in g)
    # or damage ~ diameter^2 * length / 1.6e6 (in mm)
    #
    "M70 rocket":        [0,0.500], # 135mm, ~5kg warhead
    "S-5 rocket":        [1,0.200], # 55mm, ~1-2kg warhead
    "M55 shell":         [2,0.060], # 30x113mm, 220g
    "KCA shell":         [3,0.100], # 30x173mm, 360g
    "GSh-30":            [4,0.095], # 30x165mm mig29/su27
    "GAU-8/A":           [5,0.100], # 30x173mm, 360g
    "Mk3Z":              [6,0.060], # 30x113mm Jaguar, 220g
    "BK27":              [7,0.070], # 27x145mm, 270g
    "GSh-23":            [8,0.040], # 23x115mm,
    "M61A1 shell":       [9,0.030], # 20x102mm F14, F15, F16, 100g
    "50 BMG":            [10,0.015], # 12.7mm (non-explosive)
    "7.62":              [11,0.005], # 7.62mm (non-explosive)
    "Hydra-70":          [12,0.500], # 70mm, F-16/A-6 LAU-68 and LAU-61, ~4-6kg warhead
    "SNEB":              [13,0.500], # 68mm, Jaguar
    "DEFA 554":          [14,0.060], # 30x113mm Mirage, 220g
    "20mm APDS":         [15,0.030], # CIWS
    "LAU-10":            [16,0.500], # 127mm, ~4-7kg warhead
}


# lbs of warheads is explosive+fragmentation+fuse, so total warhead mass.

warheads_em = {
    # [id,lbs,anti surface,cluster,(name)]
    "AGM-65B":           [ 0,  126.00,1,0],
    "AGM-84":            [ 1,  488.00,1,0],
    "AGM-88":            [ 2,  146.00,1,0],
    "MK-82SE":           [ 3,  192.00,1,0],# snake eye
    "AGM-119":           [ 4,  264.50,1,0],
    "AGM-154A":          [ 5,  493.00,1,0],
    "AGM-158":           [ 6, 1000.00,1,0],
    "ALARM":             [ 7,  450.00,1,0],
    "AM 39 Exocet":      [ 8,  364.00,1,0],
    "AS 37 Martel":      [ 9,  330.00,1,0],# Also : AJ 168 Martel
    "AS30L":             [10,  529.00,1,0],
    "BL755":             [11,  100.00,1,1],# 800lb bomblet warhead. Mix of armour piecing and HE. 100 due to need to be able to kill buk-m2.
    "CBU-87":            [12,  100.00,1,1],# bomblet warhead. Mix of armour piecing and HE. 100 due to need to be able to kill buk-m2.
    "CBU-105":           [13,  100.00,1,1],# bomblet warhead. Mix of armour piecing and HE. 100 due to need to be able to kill buk-m2.
    "AS 37 Armat":       [14,  330.00,1,0],
    "FAB-100":           [15,   92.59,1,0],
    "FAB-250":           [16,  202.85,1,0],
    "FAB-500":           [17,  564.38,1,0],
    "GBU-12":            [18,  190.00,1,0],
    "GBU-24":            [19,  945.00,1,0],
    "GBU-31":            [20,  945.00,1,0],
    "GBU-54":            [21,  190.00,1,0],
    "GBU-10":            [22, 945.00,1,0],
    "GBU-16":            [23,  450.00,1,0],
    "HVAR":              [24,    7.50,1,0],#P51
    "KAB-500":           [25,  564.38,1,0],
    "Kh-25MP":           [26,  197.53,1,0],
    "Kh-66":             [27,  244.71,1,0],
    "LAU-68":            [28,   10.00,1,0],
    "M71":               [29,  200.00,1,0],
    "M71R":              [30,  200.00,1,0],
    "M90":               [31,   10.00,1,1],# bomblet warhead. x3 of real mass.
    "MK-82":             [32,  192.00,1,0],
    "MK-83":             [33,  445.00,1,0],
    "MK-83HD":           [34,  445.00,1,0],
    "MK-84":             [35,  945.00,1,0],
    "OFAB-100":          [36,   92.59,1,0],
    "RB-04E":            [37,  661.00,1,0],
    "RB-15F":            [38,  440.92,1,0],
    "RB-75":             [39,  126.00,1,0],
    "RN-14T":            [40,  800.00,1,0], #fictional, thermobaeric replacement for the RN-24 nuclear bomb
    "RN-18T":            [41, 1200.00,1,0], #fictional, thermobaeric replacement for the RN-28 nuclear bomb
    "RS-2US":            [42,   28.66,1,0],
    "S-21":              [43,  245.00,1,0],
    "S-24":              [44,  271.00,1,0],
    "SCALP EG":          [45,  992.00,1,0],# aka. Storm Shadow
    "Sea Eagle":         [46,  505.00,1,0],
    "MK-82HD":           [47,  192.00,1,0],
    "MK-20":             [48,  100.00,1,1],#aka CBU-100 # bomblet warhead. 247 x 0.4lb
    "ZB-250":            [49,  236.99,1,0],
    "ZB-500":            [50,  473.99,1,0],
    "AGM-45":            [51,  149.00,1,0],#shrike
    "AIM-120B":          [52,   44.00,0,0],
    "AIM-54":            [53,  135.00,0,0],
    "AGM-78":            [54,  215.00,1,0],
    "AIM-7F":            [55,   88.00,0,0],
    "AGM-62":            [56, 2000.00,1,0],
    "AIM-9L":            [57,   20.80,0,0],
    "AGM-65D":           [58,  126.00,1,0],
    "AIM-132":           [59,   22.05,0,0],
    "Apache AP":         [60,  110.23,0,1],# Real mass of bomblet. (x 10). Anti runway.
    "KN-06":             [61,  315.00,0,0],
    "9M317":             [62,  145.00,0,0],
    "GEM":               [63,  185.00,0,0],#MIM-104D
    "R.550 Magic":       [64,   26.45,0,0],# also called majic
    "5Ya23":             [65,  414.00,0,0],#Volga-M
    "R.550 Magic 2":     [66,   27.00,0,0],
    "R.530":             [67,   55.00,0,0],
    "MK-82AIR":          [68,  192.00,1,0],
    "AIM-9M":            [69,   20.80,0,0],
    "R-73 RMD-1":        [70,   16.31,0,0],# automat Mig29/su27
    "Meteor":            [71,   55.00,0,0],
    "MICA-EM":           [72,   30.00,0,0],
    "MICA-IR":           [73,   30.00,0,0],
    "R-13M":             [74,   16.31,0,0],
    "R-27R1":            [75,   85.98,0,0],
    "R-27T1":            [76,   85.98,0,0],
    "R-3R":              [77,   16.31,0,0],
    "R-3S":              [78,   16.31,0,0],
    "R-55":              [79,   20.06,0,0],
    "R-60":              [80,    6.60,0,0],
    "R-60M":             [81,    7.70,0,0],
    "R-73E":             [82,   16.31,0,0],
    "R-77":              [83,   49.60,0,0],
    "R74":               [84,   16.00,0,0],
    "RB-05A":            [85,  353.00,1,0],
    "RB-24":             [86,   20.80,0,0],
    "RB-24J":            [87,   20.80,0,0],
    "RB-71":             [88,   88.00,0,0],
    "RB-74":             [89,   20.80,0,0],
    "RB-99":             [90,   44.00,0,0],
    "Super 530D":        [91,   66.00,0,0],
    "48N6":              [92,  330.00,0,0],# 48N6 from S-300pmu
    "pilot":             [93,    0.00,1,0],# ejected pilot
    "BETAB-500ShP":      [94, 1160.00,1,0],
    "Flare":             [95,    0.00,0,0],
    "3M9":               [96, 125.00,0,0],# 3M9M3 Missile used with 2K12/SA-6
    "5V28V":             [97, 478.00,0,0],# Missile used with S-200D/SA-5
    "AIM-9X":            [98, 20.80,0,0],# block I
}


class Shell:
    __slots__ = ('ident', 'damage', 'name')

    def __init__(self, ident: int, damage: float, name: str) -> None:
        self.ident = ident
        self.damage = damage
        self.name = name


class WarHead:
    __slots__ = ('ident', 'lbs', 'is_anti_surface', 'is_cluster', 'name')

    def __init__(self, ident: int, lbs: float, is_anti_surface: bool, is_cluster: bool, name: str) -> None:
        self.ident = ident
        self.lbs = lbs
        self.is_anti_surface = is_anti_surface
        self.is_cluster = is_cluster
        self.name = name


def map_shells_name_dict_to_id_dict():
    my_dict = dict()
    for key, value in shells_em.items():
        my_dict[value[0]] = Shell(value[0], value[1], key)
    return my_dict


shells_em_by_id = map_shells_name_dict_to_id_dict()


def map_warheads_name_dict_to_id_dict():
    my_dict = dict()
    for key, value in warheads_em.items():
        my_dict[value[0]] = WarHead(value[0], value[1], value[2] == 1, value[3] == 1, key)
    return my_dict


warheads_em_by_id = map_warheads_name_dict_to_id_dict()


MISSED_DAMAGE_TUPLE = (HealthType.missed, WeaponType.unknown)


def process_damage_notification(notification: en.ArmamentNotification,
                                mp_target: TargetWithDamage) -> Tuple[HealthType, WeaponType]:
    # late import due to circular imports
    from hunter.mp_targets import MPTargetMoving, MPTargetType

    if mp_target.invulnerable:
        return MISSED_DAMAGE_TUPLE

    hitable_by_cannon = True  # FIXME: requires implementation
    hitable_by_air_munitions = True  # FIXME ditto

    if notification.secondary_kind < 0 and hitable_by_cannon:  # cannon hit
        shell_id = -1 * notification.secondary_kind - 1
        if shell_id not in shells_em_by_id:
            logging.warning('Cannot find id=%i in shells from notification=%s', shell_id, notification)
            return MISSED_DAMAGE_TUPLE
        shell = shells_em_by_id[shell_id]
        probability = shell.damage
        weapon_type = _deduct_cannon_weapon_type(shell.name)
        hit_count = round(notification.distance)
        if hit_count > 0:
            for i in range(hit_count):
                mp_target.fail_systems(probability, by_cannon=True)
            return mp_target.health, weapon_type

    elif notification.secondary_kind > 20:  # its a warhead
        warhead_id = notification.secondary_kind - 21
        if warhead_id not in warheads_em_by_id:
            logging.warning('Cannot find id=%i in warheads from notification=%s', warhead_id, notification)
            return MISSED_DAMAGE_TUPLE
        warhead = warheads_em_by_id[warhead_id]
        distance = notification.distance
        if warhead.is_cluster:  # cluster munition
            lbs = warhead.lbs
            max_dist = _max_damage_dist_from_warhead(lbs)
            distance = max(0., random.random() * 5 - mp_target.full_damage_dist_m)
            diff = max(0., max_dist - distance)
            diff = diff * diff
            probability = diff / (max_dist * max_dist)
            if mp_target.use_hitpoints_instead_of_failure_modes_bool:
                hp_dist = _max_damage_dist_from_warhead(mp_target.max_hp)
                probability = (max_dist / hp_dist) * probability
            return mp_target.fail_systems(probability, mp_target.max_hp), WeaponType.cluster_ground
        else:
            distance = max(distance - mp_target.full_damage_dist_m, 0)
            weapon_type = WeaponType.guided_air
            if warhead.is_anti_surface:
                weapon_type = _deduct_ground_weapon_type(warhead.name)
                # special situation that Swedish 'RB-05A' also can be fired air-to-air
                if 'RB-05A' in warhead.name and isinstance(mp_target, MPTargetMoving):
                    if mp_target.target_type in [MPTargetType.plane, MPTargetType.helicopter, MPTargetType.drone]:
                        weapon_type = WeaponType.guided_air  # because MPTargetMoving not aircraft on ground
                # elif hitable_by_air_munitions:  FIXME when hitable_by_air_munitions is implemented
                #    logging.info('Anti-surface warhead not allowed against air-targets')
                #    return MISSED_DAMAGE_TUPLE
            lbs = warhead.lbs
            # some code from Nasal left out regarding warhead.is_anti_surface because mp_targets do not check hitable
            max_dist = _max_damage_dist_from_warhead(lbs)
            diff = max_dist - distance
            if diff < 0:
                diff = 0
            diff = diff * diff
            probability = diff / (max_dist * max_dist)
            if mp_target.use_hitpoints_instead_of_failure_modes_bool:
                hp_dist = _max_damage_dist_from_warhead(mp_target.max_hp)
                probability = (max_dist / hp_dist) * probability
            return mp_target.fail_systems(probability, mp_target.max_hp), weapon_type

    return MISSED_DAMAGE_TUPLE


def weapon_type_from_notification(notification: en.ArmamentNotification, target_type) -> WeaponType:
    # late import due to circular imports
    from hunter.mp_targets import MPTargetType

    if notification.secondary_kind < 0:  # cannon hit
        shell_id = -1 * notification.secondary_kind - 1
        if shell_id not in shells_em_by_id:
            logging.warning('Cannot find id=%i in shells from notification=%s', shell_id, notification)
            return WeaponType.unknown
        shell = shells_em_by_id[shell_id]
        return _deduct_cannon_weapon_type(shell.name)

    elif notification.secondary_kind > 20:  # it is a warhead
        warhead_id = notification.secondary_kind - 21
        if warhead_id not in warheads_em_by_id:
            logging.warning('Cannot find id=%i in warheads from notification=%s', warhead_id, notification)
            return WeaponType.unknown
        warhead = warheads_em_by_id[warhead_id]
        if warhead.is_cluster:  # cluster munition
            return WeaponType.cluster_ground
        else:
            weapon_type = WeaponType.guided_air
            if warhead.is_anti_surface:
                weapon_type = _deduct_ground_weapon_type(warhead.name)
                # special situation that Swedish 'RB-05A' also can be fired air-to-air
                if 'RB-05A' in warhead.name and target_type in [MPTargetType.plane, MPTargetType.helicopter,
                                                                MPTargetType.drone]:
                    weapon_type = WeaponType.guided_air
            return weapon_type


def transform_to_emesary_callsign(callsign: str) -> str:
    """Converts a callsign from sim/multiplay/callsign to a callsign encoded in Emesary.
    Emesary does not have the following chars, which are replaced by '': $, #, backtick, ~, (space)

    Directly taken from method processCallsign in damage.nas.
    """
    callsign = callsign[0:7]
    emesary_callsign = ''
    for element in callsign:
        replaced = e.TransferString.get_alphanumeric_char(element)
        if replaced:
            emesary_callsign += replaced
    return emesary_callsign
