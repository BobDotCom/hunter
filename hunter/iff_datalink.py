"""Handles IFF and datalink stuff.
Modelled after iff.nas and datalink.nas in the F-16 starting of February 2023
"""
from enum import IntEnum, unique
import hashlib
import math
import time
from typing import Dict, Optional

import hunter.damage as d
import hunter.emesary as e


def _calculate_channel_hash_incl_time(time_diff: int, callsign: str, code: int, hash_length: int) -> str:
    to_hash = '{}{}{}'.format(time_diff, d.transform_to_emesary_callsign(callsign), code).encode()
    hashed = hashlib.md5(to_hash).hexdigest()
    return hashed[:hash_length]


# ----- IFF part

IFF_REFRESH_RATE = 120  # default as per iff.nas
IFF_HASH_LENGTH = 3  # default as per iff.nas


def calculate_iff_hash(callsign: str, iff_code: Optional[int], previous: bool = False) -> Optional[str]:
    """Given a callsign and an iff_code calculate the iff_hash which gets exchanged over MP."""
    if iff_code is None:
        return None
    int_systime = int(time.time())
    update_time = int(int_systime % IFF_REFRESH_RATE)
    time_diff = int_systime - update_time
    if previous:
        time_diff -= IFF_REFRESH_RATE
    return _calculate_channel_hash_incl_time(time_diff, callsign, iff_code, IFF_HASH_LENGTH)


@unique
class IFFType(IntEnum):
    iff_unknown = 0
    iff_hostile = 1
    iff_friendly = 2


def interrogate(callsign: str, iff_hash, good_iff_code: int) -> IFFType:
    """Given a callsign and an iff_hash check whether this is a plane using the expected iff_code."""
    if iff_hash is None:
        return IFFType.iff_unknown
    hash1 = calculate_iff_hash(callsign, good_iff_code)
    hash2 = calculate_iff_hash(callsign, good_iff_code, True)
    if hash1 == iff_hash or hash2 == iff_hash:
        return IFFType.iff_friendly
    return IFFType.iff_hostile


# ----- Datalink part
DATALINK_STRING_ENCODING = 'utf8'
DATALINK_DATA_SEPARATOR = b'!'
DATALINK_EXTENSION_PREFIX_CONTACTS = b'C'
DATALINK_EXTENSION_PREFIX_IDENTIFIER = b'I'
DATALINK_EXTENSION_PREFIX_POINT = b'P'
DATALINK_CONTACTS_SEPARATOR = b'#'

DATALINK_HASH_LENGTH = 5
DATALINK_REFRESH_PERIOD = 600  # channel_hash_period


def _get_time() -> int:
    return int(math.floor(time.time() / DATALINK_REFRESH_PERIOD) * DATALINK_REFRESH_PERIOD)


def _parse_hexadecimal(input_str: str) -> int:
    res = 0
    for i in range(len(input_str)):
        res *= 10
        c = ord(input_str[i])
        if 48 <= c < 58:
            res += c - 48
        elif 65 <= c < 71:
            res += c - 55
        elif 97 <= c < 103:
            res += c - 87
    return res


def _encode_datalink_channel(callsign: str, datalink_channel: int) -> bytes:
    hash_incl_time = _calculate_channel_hash_incl_time(_get_time(), callsign, datalink_channel, DATALINK_HASH_LENGTH)
    hexa_hash = _parse_hexadecimal(hash_incl_time)
    return e.TransferInt.encode(hexa_hash, 3)


def _encode_contact(iff_type: IFFType, callsign) -> bytes:
    return e.TransferString.encode(d.transform_to_emesary_callsign(callsign)) + e.TransferByte.encode(iff_type.value)


def _encode_contacts(contacts: Dict[str, IFFType]) -> bytes:
    encoded = bytes('', DATALINK_STRING_ENCODING)
    for callsign, iff_type in contacts.items():
        encoded += _encode_contact(iff_type, callsign) + DATALINK_CONTACTS_SEPARATOR
    return encoded


def encode_datalink_data(callsign: str, datalink_channel, contacts: Dict[str, IFFType]) -> bytes:
    # First encode channel
    encoded_data = _encode_datalink_channel(callsign, datalink_channel)

    # Then all extensions
    # Contacts extension
    encoded_contacts = _encode_contacts(contacts)
    encoded_data += DATALINK_DATA_SEPARATOR + DATALINK_EXTENSION_PREFIX_CONTACTS + encoded_contacts
    return encoded_data
