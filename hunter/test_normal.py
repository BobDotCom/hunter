"""Unit tests which can execute within seconds"""
import unittest

import hunter.damage as d
import hunter.emesary as e
import hunter.emesary_notifications as en
import hunter.geometry as g


class TestDamage(unittest.TestCase):
    def test_transform_to_emesary_callsign(self):
        self.assertEqual('OPFOR11', d.transform_to_emesary_callsign('OPFOR11'), 'Regular callsign OPFOR11')
        self.assertEqual('OPFOR11', d.transform_to_emesary_callsign('OPFOR118'), 'Too long callsign OPFOR118')
        self.assertEqual('OPFOR1', d.transform_to_emesary_callsign('OPFOR#1'), 'Has not supported char OPFOR#1')


"""
    print('========= F-14 old before bullets ===========')
    data = b'FGFS\x00\x01\x00\x01\x00\x00\x00\x07\x00\x00\x01\x14d\x00\x00\x00\x00\x00\x00\x00HB-VANO\x00Aircraft/f-14b/Models/f-14b.xml\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\xe86\xfa\x8b\x9e\x06\x04?\xb9\x99\x99\x99\x99\x99\x9aA>+)\xd4\x18CeA,\x1c\xf5\xd3\xe0\x8e:AV\xc9{\xf1G\x8dd\xbd\xb69\xd9\xc05\xe9\xb9>w\xa8\x8b5+M\xcc\xb5\x03\x91\xb2\xb5s2\x89\xb4z\xea^\xb4\xa5\xc7E1mw\xb1\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1f\xac\xe0\x02\x05\xdc\x04\xb0\x00\x00\x05\xdd\xff\xff\xd8\xf1\x05\xde\x00\x00\x05\xdf\x00\x01\x05\xe0\x00\x00\x05\xe1\xd8\xf1.\xd6\x00\x01\x00\x00.\xf2\x00\x00\x00\x002\xc8\x02\x02'
    values_f14_without = decode_fgms_data_packet(data)
    emesary_dict = values_f14_without[-1]
    decoded_notifications = dict()
    for value in emesary_dict.values():
        if value != '':
            decoded_notifications.update(en.process_incoming_message(value))

    data = b'FGFS\x00\x01\x00\x01\x00\x00\x00\x07\x00\x00\x01*d\x00\x00\x00\x00\x00\x00\x00HB-VANO\x00Aircraft/f-14b/Models/f-14b.xml\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00@\xe87@\xf2\x04l\xa8?\xb9\x99\x99\x99\x99\x99\x9aA>+)\xd4\x181\x99A,\x1c\xf5\xd3\xe0\x8d\xbdAV\xc9{\xf1G\x9a\x1e\xbd\xb69\xd7\xc05\xe9\xbb>w\xa8\x7f5\x14/\xf2\xb3\xb1\x0b\xa7\xb5\xd5\xf7\x96\xb3$ 7\xb4\x93a10Lf\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1f\xac\xe0\x02\x05\xdc\x04\xb0\x00\x00\x05\xdd\xff\xff\xd8\xf1\x05\xde\x00\x00\x05\xdf\x00\x01\x05\xe0\x00\x00\x05\xe1\xd8\xf1.\xd6\x00\x01.\xf2\x00\x1a!   \x17!\x14!\x05\x15- , \xba V\x08xbuil39~2\xc8\x02\x02'
    values_f14_bullets = decode_fgms_data_packet(data)

    emesary_dict = values_f14_bullets[-1]
    decoded_notifications = dict()
    for value in emesary_dict.values():
        if value != '':
            decoded_notifications.update(en.process_incoming_message(value))

{12018: b'!   \x17!\x14!\x05\x15- , \xba V\x08xbuil39~'}

msg_idx:
b'   \x17'
-1891370986.0

msg_type_id:
b'\x14'
-105.0

========
F-16
========

encoded notification: b'!\x83\x01\x01\r!\x96!\x87\xcc\x83\x01\x83\x02\x97\x8aOPFOR53'
encoded parts: [b'', b'\x83\x01\x01\r', b'\x96', b'\x87\xcc\x83\x01\x83\x02\x97\x8aOPFOR53']
msg idx encoded: b'\x83\x01\x01\r'
msg idx decoded: 12.0
msg_type_id encoded: b'\x96'
msg_type_id decoded: 19.0
ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=73.0, distance=0.1, bearing=30.8
Controller-1 MainThread -- 00:18:08 - INFO     : Processing armament notification for MP target OPFOR53: ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=73.0, distance=0.1, bearing=30.8


encoded notification: b'!\x83\x01\x01\x0e!\x96!\x87w\x83\x01\x83\x93\x83\x8aOPFOR50'
encoded parts: [b'', b'\x83\x01\x01\x0e', b'\x96', b'\x87w\x83\x01\x83\x93\x83\x8aOPFOR50']
msg idx encoded: b'\x83\x01\x01\x0e'
msg idx decoded: 13.0
msg_type_id encoded: b'\x96'
msg_type_id decoded: 19.0
ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=14.0, bearing=0.0
Controller-1 MainThread -- 00:18:30 - INFO     : Processing armament notification for MP target OPFOR50: ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=14.0, bearing=0.0


encoded notification: b'!\x83\x01\x01\x0f!\x96!\x87w\x83\x01\x83}\x83\x8aOPFOR50'
encoded parts: [b'', b'\x83\x01\x01\x0f', b'\x96', b'\x87w\x83\x01\x83}\x83\x8aOPFOR50']
msg idx encoded: b'\x83\x01\x01\x0f'
msg idx decoded: 14.0
msg_type_id encoded: b'\x96'
msg_type_id decoded: 19.0
ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=12.0, bearing=0.0
Controller-1 MainThread -- 00:18:32 - INFO     : Processing armament notification for MP target OPFOR50: ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=12.0, bearing=0.0


encoded notification: b'!\x83\x01\x01&!\x98!\x87\x9b\xc9\xc3\x84\x9af\x9f\x83\x06;\x85\xcc\x02\xef\x80\x83\x83\x1f'
encoded parts: [b'', b'\x83\x01\x01&', b'\x98', b'\x87\x9b\xc9\xc3\x84\x9af\x9f\x83\x06;\x85\xcc\x02\xef\x80\x83\x83\x1f']
msg idx encoded: b'\x83\x01\x01&'
msg idx decoded: 33.0
msg_type_id encoded: b'\x98'
msg_type_id decoded: 21.0
Controller-1 MainThread -- 00:20:04 - INFO     : Processing armament notification for MP target OPFOR54: ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=73.0, distance=0.0, bearing=93.94


encoded notification: b'!\x83\x01\x01\x11!\x96!\x87w\x83\x01\x84\x8b\x83\x8aOPFOR51'
encoded parts: [b'', b'\x83\x01\x01\x11', b'\x96', b'\x87w\x83\x01\x84\x8b\x83\x8aOPFOR51']
msg idx encoded: b'\x83\x01\x01\x11'
msg idx decoded: 16.0
msg_type_id encoded: b'\x96'
msg_type_id decoded: 19.0
ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=38.0, bearing=0.0
Controller-1 MainThread -- 00:20:54 - INFO     : Processing armament notification for MP target OPFOR51: ArmamentNotification: ident=mp-bridge, kind=4.0, secondary kind=-10.0, distance=38.0, bearing=0.0

"""


class TestEmesary(unittest.TestCase):
    def test_numeric_decode_encode(self):
        # int values
        test_tuples = [  # int_value, bytes value, message, length
            (-105, b'\x14', 'F-14 type_id -105', 1),
            (19, b'\x96', 'F-16 decode type_id 19', 1),
            (21, b'\x98', 'F-16 decode type_id 21', 1),
            (-1891370986, b'   \x17', 'F-14 decode index', 4),
            (14, b'\x83\x01\x01\x0f', 'F-16 decode index 14', 4),
            (16, b'\x83\x01\x01\x11', 'F-16 decode index 16', 4),
            (33, b'\x83\x01\x01&', 'F-16 decode index 33', 4)
        ]
        for test_tuple in test_tuples:
            encoded = e.BinaryAsciiTransfer.encode_int(test_tuple[0], test_tuple[3])
            self.assertEqual(test_tuple[1], encoded, test_tuple[2])
            decoded, _ = e.BinaryAsciiTransfer.decode_int(test_tuple[1], test_tuple[3], 0)
            self.assertAlmostEqual(test_tuple[0], decoded, 1, test_tuple[2])

        self.assertAlmostEqual(en.KIND_COLLISION, e.TransferByte.decode(e.TransferByte.encode(en.KIND_COLLISION), 0)[0],
                               1, 'Collision type encode-decode')

        orig_message_body = b'\x87\xcc\x83\x01\x83\x02\x97\x8aOPFOR53'
        notification = en.ArmamentNotification.decode_from_message_body(orig_message_body)
        self.assertAlmostEqual(en.KIND_COLLISION, notification.kind, 1, 'F-16 missile kind')
        self.assertAlmostEqual(73, notification.secondary_kind, 1, 'F-16 missile secondary kind')
        self.assertTrue(notification.secondary_kind > 20, 'F-16 it is a warhead')
        self.assertEqual('OPFOR53', notification.remote_callsign, 'F-16 missile remote callsign')

        message_body = notification.encode_to_message_body()
        self.assertEqual(orig_message_body, message_body, 'F-16 missile encoded back')

        orig_message_body = b'\x87w\x83\x01\x83\x93\x83\x8aOPFOR50'
        notification = en.ArmamentNotification.decode_from_message_body(orig_message_body)
        self.assertTrue(notification.secondary_kind < 0, 'F-16 it is a cannon hit')
        message_body = notification.encode_to_message_body()
        self.assertEqual(orig_message_body, message_body, 'F-16 cannon encoded back')

        # ArmamentInFlightNotification
        unique_id = 99
        lat = 30
        geo_position = g.Position(20, lat, 40)
        notification_orig = en.ArmamentInFlightNotification(unique_identity=unique_id)
        notification_orig.position = geo_position
        notification_orig.remote_callsign = 'HB_VANO'
        encoded_value = notification_orig.encode_to_message_body()
        notification_decoded = en.ArmamentInFlightNotification.decode_from_message_body(encoded_value)
        self.assertEqual(unique_id, notification_decoded.unique_identity, 'ArmamentInFlightNotification unique_id')
        self.assertAlmostEqual(lat, notification_decoded.position.lat, 1, 'ArmamentInFlightNotification lat')

        # ObjectInFlightNotification
        notification_orig = en.ObjectInFlightNotification(unique_identity=unique_id)
        notification_orig.position = geo_position
        encoded_value = notification_orig.encode_to_message_body()
        notification_decoded = en.ObjectInFlightNotification.decode_from_message_body(encoded_value)
        self.assertEqual(unique_id, notification_decoded.unique_identity, 'ObjectInFlightNotification unique_id')
        self.assertAlmostEqual(lat, notification_decoded.position.lat, 1, 'ObjectInFlightNotification lat')


class TestGeometry(unittest.TestCase):
    def test_calcs(self):
        lon2 = 8.42
        lat2 = 46.643
        lon1 = 8.125
        lat1 = 46.741
        a_bearing = g.calc_bearing(lon1, lat1, lon2, lat2)
        a_distance = g.calc_distance(lon1, lat1, lon2, lat2)
        lon3, lat3 = g.calc_destination(lon1, lat1, a_distance, a_bearing)
        self.assertAlmostEqual(lon2, lon3, delta=0.01)

        lon1 = 8.59
        lat1 = 46.636
        a_bearing = g.calc_bearing(lon1, lat1, lon2, lat2)
        a_distance = g.calc_distance(lon1, lat1, lon2, lat2)
        lon3, lat3 = g.calc_destination(lon1, lat1, a_distance, a_bearing)
        self.assertAlmostEqual(lon2, lon3, delta=0.01)

        lon1 = 8.442
        lat1 = 46.726
        a_bearing = g.calc_bearing(lon1, lat1, lon2, lat2)
        a_distance = g.calc_distance(lon1, lat1, lon2, lat2)
        lon3, lat3 = g.calc_destination(lon1, lat1, a_distance, a_bearing)
        self.assertAlmostEqual(lon2, lon3, delta=0.01)

        # same high up north near Hammerfest
        lon2 = 24.5
        lat2 = 70.
        lon1 = 25.
        lat1 = 70.125
        a_bearing = g.calc_bearing(lon1, lat1, lon2, lat2)
        a_distance = g.calc_distance(lon1, lat1, lon2, lat2)
        lon3, lat3 = g.calc_destination(lon1, lat1, a_distance, a_bearing)
        self.assertAlmostEqual(lon2, lon3, delta=0.01)

        lon1 = 25.
        lat1 = 70.
        lon2, lat2 = g.calc_destination(lon1, lat1, 1000, 90)
        self.assertGreater(lon2, lon1, 'Longitude gets larger flying East')
        lon2, lat2 = g.calc_destination(lon1, lat1, 1000, 270)
        self.assertGreater(lon1, lon2, 'Longitude gets smaller flying West')
        lon2, lat2 = g.calc_destination(lon1, lat1, 1000, 180)
        self.assertGreater(lat1, lat2, 'Latitude gets smaller flying South')

    def test_calc_delta_bearing(self):
        bearing1 = 0.
        bearing2 = 0.
        self.assertEqual(0., g.calc_delta_bearing(bearing1, bearing2), 'No difference 0')

        bearing1 = 0.
        bearing2 = 360.
        self.assertEqual(0., g.calc_delta_bearing(bearing1, bearing2), 'No difference 0/360')

        bearing1 = 360.
        bearing2 = 0.
        self.assertEqual(0., g.calc_delta_bearing(bearing1, bearing2), 'No difference 360/0')

        bearing1 = 20
        bearing2 = 200
        self.assertEqual(180., g.fabs(g.calc_delta_bearing(bearing1, bearing2)), '180 degrees')

        bearing1 = 10
        bearing2 = 20
        self.assertEqual(10., g.calc_delta_bearing(bearing1, bearing2), '10 with clock')

        bearing1 = 20
        bearing2 = 10
        self.assertEqual(-10., g.calc_delta_bearing(bearing1, bearing2), '10 against clock')

        bearing1 = 350
        bearing2 = 10
        self.assertEqual(20., g.calc_delta_bearing(bearing1, bearing2), 'Through 0: 20 with clock')

        bearing1 = 10
        bearing2 = 350
        self.assertEqual(-20., g.calc_delta_bearing(bearing1, bearing2), 'Through 0: 20 against clock')

    def test_calc_limited_bearing(self):
        bearing1 = 0.
        bearing2 = 0.
        self.assertEqual(0., g.calc_limited_bearing(bearing1, bearing2, 30), 'No difference 0')

        bearing2 = 270.
        self.assertEqual(330., g.calc_limited_bearing(bearing1, bearing2, 30), 'Against clock below 0')

        bearing2 = 90.
        self.assertEqual(30., g.calc_limited_bearing(bearing1, bearing2, 30), 'With clock above 0')

        bearing1 = 350.
        self.assertEqual(20., g.calc_limited_bearing(bearing1, bearing2, 30), 'With clock below 0')

    def test_calc_intercept(self):
        distance = 100000
        chaser_position = g.Position(24, 70, 1000)
        chaser_speed = 200

        target_lon_lat = g.calc_destination(24, 70, distance, 0)
        target_position = g.Position(target_lon_lat[0], target_lon_lat[1], 2000)
        target_heading = 180  # flying towards chaser
        target_speed = 100

        vector, eta = g.calc_intercept(chaser_position, chaser_speed, target_position, target_speed, target_heading)
        self.assertAlmostEqual(eta, distance / (chaser_speed + target_speed), delta=eta*0.1)

        target_heading = 0  # flying away from chaser
        vector, eta = g.calc_intercept(chaser_position, chaser_speed, target_position, target_speed, target_heading)
        self.assertAlmostEqual(eta, distance / (chaser_speed - target_speed), delta=eta*0.1)
