"""Archive of previously used code, that is not yet suited to be thrown away."""
from enum import Enum
import logging
from math import fabs
import multiprocessing as mp
import queue
import random
import time
from typing import Optional, Tuple

import networkx as nx

from hunter import geometry as g, scenarios as sc
import hunter.fg_instance_utils as fgiu
import hunter.fg_targets as fgt
import hunter.messages as m
import hunter.mp_targets as mpt


class FlightStage(Enum):
    """In which stage a drone is during flying.
    See https://docs.python.org/3/library/enum.html#orderedenum"""
    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.value > other.value
        return NotImplemented

    cold = 0

    connected = 1
    start_up = 10
    take_off_roll = 20
    take_off_rotate = 21
    climbing_free = 30
    climbing_autopilot = 31
    find_pattern = 40
    in_pattern = 41

    crashed = 99


class Aircraft(fgt.FGMPConnected):
    """Model of an aircraft in FlightGear suitable to be controlled through Telnet and httpd.

    Unless stated otherwise: all measurements in kt and ft. Heading is magnetic (not true heading)."""
    def __init__(self, index: int, mp_server_host: str, callsign: str,
                 aero: str, livery: Optional[str], engines: int,
                 rotate_speed: int, rotate_elevator_pitch: float,
                 climb_speed: int, climb_elevator_pitch: float,
                 cruise_speed: int, pattern_speed: int,
                 min_height_autopilot: int,
                 turn_bank_angle: float, turn_aileron: float, turn_elevator: float, performance_turn: bool,
                 airport: sc.Airport, way_points: nx.Graph, show_world: bool) -> None:
        fgt.FGMPConnected.__init__(self, index, mp_server_host, callsign, mpt.MPTargetType.plane, False,
                                   fgiu.make_command_line_aircraft(index, airport.icao, airport.runway, aero,
                                                                   callsign, show_world, True),
                                   mpt.DEATH_TIME_UNKNOWN)
        self.aero = aero
        self.livery = livery
        self.engines = engines
        self.rotate_speed = rotate_speed  # needs to be lower than actual Vr because we slowly increase pitch
        self.rotate_elevator_pitch = rotate_elevator_pitch  # max pitch angle to force rotation
        self.elevator_pitch_current = 0.
        self.climb_speed = climb_speed
        self.climb_elevator_pitch = climb_elevator_pitch  # correct a bit nose down after rotation
        self.cruise_speed = cruise_speed
        self.pattern_speed = pattern_speed
        self.min_height_autopilot = min_height_autopilot
        self.stored_autopilot_modes = list()

        # variables for making fast turns
        self.turn_bank_angle = turn_bank_angle  # how much the plane should be banked before pulling the elevator
        self.turn_aileron = turn_aileron  # stick movement left/right
        self.turn_elevator = turn_elevator  # stick movement pull/push
        self.performance_turn = performance_turn  # False if using default autopilot instead of aggressive Nasal

        self.flight_stage = FlightStage.cold
        self._magnetic_variation_deg = -999

        # way_points
        self.airport = airport
        self.way_points = way_points
        self.current_way_point = None  # WayPoint
        self.prev_way_point = None  # WayPoint. Will only be present if passed first node in graph

    def _check_status(self, p: mp.Process) -> bool:
        """Processes incoming and outgoing messages. Returns False if this process should stop."""
        logging.debug('Start of _check_status for %s', self.callsign)
        try:
            message = self.incoming_queue.get_nowait()
            if isinstance(message, m.TearDownTarget):
                self.tear_fg_down(p)
                return False
        except queue.Empty:
            pass  # nothing to do as this just signals an empty queue - and we are done polling for now
        health = self.check_health()
        if health is m.HealthType.dead:
            logging.info('Health of aircraft %s is DEAD based on is_alive', self.callsign)
            self.tear_fg_down(p)
            self.outgoing_queue.put_nowait(m.RemoveDeadTarget(self.callsign, False, True))
            return False
        elif health is m.HealthType.destroyed:
            logging.info('Health of aircraft %s is DESTROYED based on check_health', self.callsign)
            self.outgoing_queue.put_nowait(m.FGTargetDestroyed(self.callsign))
        logging.debug('Result of _check_status for %s is ok', self.callsign)
        return True

    def run(self) -> None:
        p = None
        # noinspection PyBroadException
        try:
            p = self.start_fg_instance_process(self.cmd_args, self.callsign)

            self.initialise_fg_interfaces()

            self.throttle_framerate(fgiu.FRAME_RATE_THROTTLE)

            # Before we go into loop, the plane goes through a set of flight stages
            self.flight_stage = FlightStage.connected

            if self._check_status(p) is False:
                return
            self._fly_start_up(False)
            if self._check_status(p) is False:
                return
            self._fly_take_off()
            if self._check_status(p) is False:
                return
            self._fly_climb()
            self._fly_to_pattern()
            if self._check_status(p) is False:
                return
            current_ap_heading = self.orientation_heading_magnetic_deg

            while True:
                if self._check_status(p) is False:
                    return
                current_ap_heading = self._fly_in_pattern(current_ap_heading)
                # fly_in_pattern has its own time.sleep(), so no need here
        except Exception:
            self.handle_run_exception(p)
            return  # go out of run()

    def _calc_target_alt_ft(self) -> int:
        for wp in list(self.way_points.nodes):
            logging.debug('altitude in ft: %f', wp.alt_ft)
        return int(list(self.way_points.nodes)[0].alt_ft)

    def _set_current_wp_to_closest_wp(self, lon_lat: Tuple[float, float]) -> None:
        min_distance = 99999999
        for wp in self.way_points.nodes:
            wp_distance = g.calc_distance(lon_lat[0], lon_lat[1], wp.lon, wp.lat)
            if wp_distance < min_distance:
                min_distance = wp_distance
                self.current_way_point = wp
        logging.info('First chosen %s', self.current_way_point)

    def _set_next_current_wp(self, current_heading: float) -> float:
        """Returns the new bearing based on a random selection of possible next WayPoints.
        Returning to the previous is prevented.
        As far as possible the angle between the old and the new bearing is larger than 60 degrees.
        If not then the largest possible point is used."""
        suitable_wps = list()
        max_angle_alternative_wp = None
        max_angle = -99
        neighbors = list(self.way_points.neighbors(self.current_way_point))
        if len(neighbors) == 1:
            suitable_wps.append(neighbors[0])
        else:
            for wp in self.way_points.neighbors(self.current_way_point):
                if self.prev_way_point:
                    if self.prev_way_point == wp:
                        continue  # we do not want to make a 180 degree turn and fly back
                new_bearing = g.calc_bearing_tuple_wp((self.current_way_point.lon, self.current_way_point.lat),
                                                      wp, self.magnetic_variation_deg)
                angle = fabs(new_bearing - current_heading)
                if angle < 60:
                    if angle > max_angle:
                        max_angle = angle
                        max_angle_alternative_wp = wp
                else:
                    suitable_wps.append(wp)
            if len(suitable_wps) == 0:
                suitable_wps.append(max_angle_alternative_wp)
        self.prev_way_point = self.current_way_point
        self.current_way_point = random.choice(suitable_wps)
        new_bearing = g.calc_bearing_tuple_wp((self.prev_way_point.lon, self.prev_way_point.lat),
                                              self.current_way_point, self.magnetic_variation_deg)
        logging.info('Next chosen %s with angle %d', self.current_way_point, fabs(new_bearing - current_heading))
        return new_bearing

    def _fly_start_up(self, damage: bool) -> None:
        self.flight_stage = FlightStage.start_up
        self._select_livery()
        self._set_brake_parking(True)
        self._start_up()
        if damage:
            self._enable_damage()

    def _fly_take_off(self) -> None:
        self.flight_stage = FlightStage.take_off_roll
        self._trim_takeoff(True)
        self._start_rolling()

        while True:
            if self.flight_stage is FlightStage.take_off_roll:
                ias = self.airspeed
                logging.debug('IAS: %f', ias)
                if ias > self.rotate_speed:  # start to climb
                    self.flight_stage = FlightStage.take_off_rotate
                    self._increase_nose_up()
            else:
                agl = self.position_altitude_agl_ft
                logging.debug('Above ground: %f', agl)
                if agl < 30:
                    self._increase_nose_up()
                else:
                    self._start_climbing()
                    return
            time.sleep(0.5)

    def _fly_climb(self) -> None:
        current_heading = self.airport.heading_magnetic_deg
        b_idx = 0  # index of departure beacon
        target_alt = self._calc_target_alt_ft()

        self.flight_stage = FlightStage.climbing_free
        while True:
            if self.flight_stage is FlightStage.climbing_free:
                agl = self.position_altitude_agl_ft
                if self._autopilot_allowed(agl):
                    self._set_autopilot_heading_bug(current_heading, True)
                    self._set_autopilot_speed(self.climb_speed)
                    self._set_autopilot_target_altitude_ft(target_alt)
                    self.flight_stage = FlightStage.climbing_autopilot

                    # late initialisation of multiplayer, so requested position is gotten from simulator
                    self.initialise_multiplayer()
            else:
                # check transit to climb to target
                alt = self.position_altitude_ft
                if alt > target_alt - 500:
                    self._set_autopilot_target_altitude_ft(target_alt)
                    self._set_autopilot_speed(self.cruise_speed)
                    return
                # follow beacons to climb out (must be in autopilot already)
                lon_lat = self.position_lon_lat
                distance_beacon = g.calc_distance_tuple_wp(lon_lat, self.airport.beacons[b_idx])
                if distance_beacon < 2000:
                    if len(self.airport.beacons) > b_idx + 1:
                        b_idx += 1
                        new_bearing = g.calc_bearing_tuple_wp(lon_lat, self.airport.beacons[b_idx],
                                                              self.magnetic_variation_deg)
                        logging.info('Changing direction to next beacon with bearing %f', new_bearing)
                        self._set_autopilot_heading_bug(new_bearing)

            time.sleep(1.)

    def _fly_to_pattern(self) -> None:
        self.flight_stage = FlightStage.find_pattern
        lon_lat = self.position_lon_lat

        # find entry point
        self._set_current_wp_to_closest_wp(lon_lat)
        bearing = g.calc_bearing_tuple_wp(lon_lat, self.current_way_point, self.magnetic_variation_deg)
        current_heading = bearing
        self._set_autopilot_heading_bug(current_heading)
        while True:
            lon_lat = self.position_lon_lat
            dist_to_wp = g.calc_distance_tuple_wp(lon_lat, self.current_way_point)
            logging.debug('Distance to first waypoint of pattern: %d', dist_to_wp)
            if dist_to_wp < 10000:  # need some space to turn into circle
                logging.info('Getting into pattern at distance %d', dist_to_wp)
                self._set_autopilot_target_altitude_ft(self.current_way_point.alt_ft)
                self._set_autopilot_speed(self.pattern_speed)
                self.flight_stage = FlightStage.in_pattern
                break
            else:
                bearing = g.calc_bearing_tuple_wp(lon_lat, self.current_way_point, self.magnetic_variation_deg)
                if fabs(bearing - current_heading) > 2.:
                    current_heading = bearing
                    self._set_autopilot_heading_bug(current_heading)
                    logging.info('Correcting heading to %d', current_heading)
            time.sleep(10.)
        self._prepare_pattern_flying()

    def _fly_in_pattern(self, heading: float) -> float:
        current_heading = heading
        # get the next point to steer at in the circle
        lon_lat = self.position_lon_lat
        dist_to_wp = g.calc_distance_tuple_wp(lon_lat, self.current_way_point)
        if dist_to_wp < self.pattern_speed * DISTANCE_FACTOR_WP:
            new_bearing = self._set_next_current_wp(current_heading)
            current_heading = new_bearing
            self._set_autopilot_target_altitude_ft(self.current_way_point.alt_ft)
            self._set_autopilot_heading_bug(current_heading)
        elif dist_to_wp > self.pattern_speed * DISTANCE_FACTOR_BEFORE_WP:
            bearing = g.calc_bearing_tuple_wp(lon_lat, self.current_way_point, self.magnetic_variation_deg)
            if fabs(bearing - current_heading) > 2.:
                current_heading = bearing
                self._set_autopilot_heading_bug(current_heading)
                logging.info('Correcting heading to %d', current_heading)
            time.sleep(5.)
        else:
            time.sleep(1.)
        return current_heading

    def _start_up(self) -> None:
        time.sleep(5.0)

    def _trim_takeoff(self, wow: bool) -> None:
        pass

    def _increase_nose_up(self) -> None:
        if self.rotate_elevator_pitch < self.elevator_pitch_current:
            self.elevator_pitch_current -= 0.1
            self._set_elevator_pitch(self.elevator_pitch_current)
            logging.debug('New elevator pitch: %d with max being %f', self.elevator_pitch_current,
                          self.rotate_elevator_pitch)

    def _start_rolling(self) -> None:
        self._set_brake_parking(False)

    def _start_climbing(self) -> None:
        self._toggle_gears(False)
        if self.climb_elevator_pitch != self.rotate_elevator_pitch:
            self._set_elevator_pitch(self.climb_elevator_pitch)
            time.sleep(2.)
            self._set_elevator_pitch(0.)
        self._trim_takeoff(False)

    def _prepare_pattern_flying(self) -> None:
        pass

    def _toggle_gears(self, down: bool) -> None:
        self.fg_httpd.set_bool_property('/controls/gear/gear-down', down)

    def _enable_damage(self) -> None:
        pass

    def _select_livery(self) -> None:
        if self.livery:
            self.fg_telnet.run_nasal('aircraft.livery.select("{}");'.format(self.livery))

    def _autopilot_allowed(self, agl: float) -> bool:
        is_allowed = agl > self.min_height_autopilot
        logging.debug('Enabling the autopilot is currently allowed: %s', str(is_allowed))
        return is_allowed

    def _set_autopilot_heading_bug(self, heading: float, enforce_autopilot: bool = False) -> None:
        if self.performance_turn and not enforce_autopilot:
            self.fg_telnet.run_nasal('hunter.performance_turn({}, {}, {}, {})'.format(heading, self.turn_bank_angle,
                                                                                      self.turn_aileron,
                                                                                      self.turn_elevator))
            time.sleep(10)
        else:
            self.fg_httpd.set_single_property('/autopilot/settings/heading-bug-deg', heading)
            self.fg_httpd.set_single_property('/autopilot/locks/heading', 'dg-heading-hold')

    def _set_autopilot_speed(self, speed: int) -> None:
        self.fg_httpd.set_single_property('/autopilot/settings/target-speed-kt', speed)
        self.fg_httpd.set_single_property('/autopilot/locks/speed', 'speed-with-throttle')

    def _set_autopilot_target_altitude_ft(self, altitude: int) -> None:
        self.fg_httpd.set_single_property('/autopilot/settings/target-altitude-ft', altitude)
        self.fg_httpd.set_single_property('/autopilot/locks/altitude', 'altitude-hold')

    def _set_elevator_pitch(self, pitch: float) -> None:
        self.fg_httpd.set_single_property('/controls/flight/elevator', pitch)
        logging.debug('Elevator pitch: %f', pitch)

    @property
    def position_lon_lat(self) -> Tuple[float, float]:  # TODO: try reading 2 props in http at once
        pos_lon = self.fg_httpd.get_float_property('/position/longitude-deg')
        pos_lat = self.fg_httpd.get_float_property('/position/latitude-deg')
        return pos_lon, pos_lat

    @property
    def orientation_heading_deg(self) -> float:
        return self.fg_httpd.get_float_property('/orientation/heading-deg')

    @property
    def orientation_heading_magnetic_deg(self) -> float:
        return self.fg_httpd.get_float_property('/orientation/heading-magnetic-deg')

    @property
    def magnetic_variation_deg(self) -> float:
        """Magnetic variation based on FlightGear internal calculation.
        In order to keep network IO low, only calculated once"""
        if self._magnetic_variation_deg < -360:
            self._magnetic_variation_deg = self.fg_httpd.get_float_property('/environment/magnetic-variation-deg')
            logging.info('Magnetic variation %f', self._magnetic_variation_deg)
        return self._magnetic_variation_deg

    @property
    def position_altitude_agl_ft(self) -> float:
        return self.fg_httpd.get_float_property('/position/altitude-agl-ft')

    @property
    def position_altitude_ft(self) -> float:
        return self.fg_httpd.get_float_property('/position/altitude-ft')

    def _set_brake_parking(self, on: bool) -> None:
        logging.debug('Setting parking brake to %s', str(on))
        self.fg_httpd.set_bool_property('/controls/gear/brake-parking', on, True)

    def _set_flaps(self, value: float) -> None:
        self.fg_httpd.set_single_property('/controls/flight/flaps/', value)

    def _set_aileron_trim(self, value: float) -> None:
        self.fg_httpd.set_single_property('/controls/flight/aileron-trim', value)

    def _set_rudder_trim(self, value: float) -> None:
        self.fg_httpd.set_single_property('/controls/flight/rudder-trim', value)

    def _set_throttle(self, position: float) -> None:
        for index in range(self.engines):
            logging.debug('Setting throttle to %f for engine %i', position, index)
            if index == 0:
                self.fg_httpd.set_single_property('/controls/engines/engine/throttle', position)
            else:
                self.fg_httpd.set_single_property('/controls/engines/engine[{}]/throttle'.format(index), position)

    @property
    def airspeed(self) -> float:
        return self.fg_httpd.get_float_property('/velocities/airspeed-kt')


class Pilatus9M(Aircraft):
    def __init__(self, callsign: str, way_points: nx.Graph) -> None:
        super().__init__(callsign, 'PC-9M', 'Swiss Air Force "Patrouille Suisse"', 1,
                         100, -0.4,
                         190, -0.4,
                         200, 200,
                         200,
                         70, 0.6, 0.6, True,
                         way_points)

    def _start_up(self) -> None:
        self.fg_httpd.set_bool_property('/controls/engines/start1', True)
        time.sleep(30.)

    def _start_rolling(self) -> None:
        self._set_throttle(0.4)
        self._set_brake_parking(False)
        time.sleep(2.0)
        self._set_throttle(0.6)
        time.sleep(4.0)
        self._set_throttle(0.8)

    def _trim_takeoff(self, wow: bool) -> None:
        if wow:
            self._set_flaps(.5)
            self._set_aileron_trim(0.25)
        else:
            self._set_rudder_trim(0.)
            self._set_aileron_trim(0.)
            self._set_flaps(0.)


class Viggen(Aircraft):
    def __init__(self, callsign: str, way_points: nx.Graph) -> None:
        super().__init__(callsign, 'Ja37-Viggen', 'Swedish Air Force 57 Red Ghost', 1,
                         120, -0.3,
                         300, 0.2,
                         350, 250,
                         DEFAULT_MIN_HEIGHT_AUTOPILOT,
                         70, 0.6, 0.6, True,
                         way_points)

    def _start_up(self) -> None:
        self.fg_telnet.run_nasal('ja37.autostarttimer()')
        time.sleep(40.0)

    def _start_rolling(self) -> None:
        self._set_throttle(0.7)
        time.sleep(5.0)  # thrust needs to build up
        self._set_brake_parking(False)

    def _enable_damage(self) -> None:
        self.fg_httpd.set_bool_property('/payload/armament/msg', True)


class Bourrasque(Aircraft):
    def __init__(self, callsign: str, way_points: nx.Graph) -> None:
        super().__init__(callsign, 'bourrasque', 'flight gear', 2,
                         150, -0.6,
                         300, -0.6,
                         400, 300,
                         DEFAULT_MIN_HEIGHT_AUTOPILOT,
                         70, 0.6, 0.6, True,
                         way_points)

    def _start_up(self) -> None:
        self.fg_telnet.run_nasal('core.fast_start()')
        time.sleep(40.0)

    def _start_rolling(self) -> None:
        self._set_throttle(0.7)
        time.sleep(5.0)  # thrust needs to build up
        self._set_brake_parking(False)


class M2000(Aircraft):
    def __init__(self, callsign: str, way_points: nx.Graph) -> None:
        super().__init__(callsign, 'm2000-5', None, 1,
                         170, -0.3,
                         300, -0.3,
                         400, 300,
                         DEFAULT_MIN_HEIGHT_AUTOPILOT,
                         70, 0.6, 0.6, True,
                         way_points)
        # ALT, PTCH, HDG

        # AP1, <MINIMUM

    def _start_up(self) -> None:
        self.fg_telnet.run_nasal('mirage2000.engine1.autostart();')
        time.sleep(40.0)
        # close canopy
        self.fg_telnet.run_nasal('doors.move_canopy();')
        time.sleep(10.0)
        self.fg_telnet.run_nasal('doors.move_canopy();')

    def _start_rolling(self) -> None:
        self._set_throttle(0.7)
        time.sleep(5.0)  # thrust needs to build up
        self._set_brake_parking(False)


DEFAULT_MIN_HEIGHT_AUTOPILOT = 200
DISTANCE_FACTOR_WP = 2  # this * pattern_speed -> min distance to wp
DISTANCE_FACTOR_PATTERN = 20  # this * pattern_speed -> distance before pattern
DISTANCE_FACTOR_BEFORE_WP = 10  # this * pattern_speed -> distance before WP to make corrections
