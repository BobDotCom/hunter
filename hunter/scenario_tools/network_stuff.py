import argparse
import logging
import random
from typing import Tuple

import networkx as nx

import hunter.geometry as g


POSSIBLE_NODE_COMBINATIONS_2_RECTANGLES = [  # sequence from tuple out of function _envelop_from_graph()
    (0, 1, 0, 1),
    (0, 1, 0, 3),
    (0, 1, 2, 1),
    (0, 1, 2, 3),
    (0, 3, 0, 1),
    (0, 3, 0, 3),
    (0, 3, 2, 1),
    (0, 3, 2, 3),
    (2, 1, 0, 1),
    (2, 1, 0, 3),
    (2, 1, 2, 1),
    (2, 1, 2, 3),
    (2, 3, 0, 1),
    (2, 3, 0, 3),
    (2, 3, 2, 1),
    (2, 3, 2, 3),
]


def _envelope_from_graph_points(sub_graph_nodes, buffer: float) -> Tuple[float, float, float, float]:
    """The smallest rectangle along lon/lat axis, which contains all nodes of a sub-graph."""
    smallest_lon = 999
    largest_lon = -999
    smallest_lat = 999
    largest_lat = -999
    for sub_node in sub_graph_nodes:
        smallest_lon = min(sub_node.lon, smallest_lon)
        largest_lon = max(sub_node.lon, largest_lon)
        smallest_lat = min(sub_node.lat, smallest_lat)
        largest_lat = max(sub_node.lat, largest_lat)
    return smallest_lon - buffer, smallest_lat - buffer, largest_lon + buffer, largest_lat + buffer


def _has_points_in_envelope(envelope: Tuple[float, float, float, float], graph_nodes) -> bool:
    for node in graph_nodes:
        if envelope[0] <= node.lon <= envelope[2] and envelope[1] <= node.lat <= envelope[3]:
            return True
    return False


def _closest_node_to_point(sub_graph_nodes, other_lon: float, other_lat: float):
    closest_dist = 999999
    closest_sub_node = None
    for sub_node in sub_graph_nodes:
        dist = g.calc_distance(sub_node.lon, sub_node.lat, other_lon, other_lat)
        if dist < closest_dist:
            closest_dist = dist
            closest_sub_node = sub_node
    return closest_sub_node


def info_about_network(network: nx.Graph) -> str:
    return 'Nodes {}, edges {}'.format(len(network.nodes), len(network.edges))


def remove_dangling_nodes(network: nx.Graph) -> int:
    """Remove the remaining dangling iteratively (if we remove one edge - then next edge behind might be dangling)."""
    total_removed = 0
    found_dangling = 99
    while found_dangling > 0:
        found_dangling = 0
        for dangling in list(network.nodes):
            if dangling.is_point_of_interest:  # do not remove points of interest
                continue
            neighbors = list(nx.neighbors(network, dangling))
            if len(neighbors) <= 1:
                found_dangling += 1
                total_removed += 1
                network.remove_node(dangling)
    return total_removed


def connect_disjoint_subgraphs(network: nx.Graph, acceptable_link_distance: int) -> None:
    """Connect disjoint sub-graphs if any included in network using envelop as proxy.
    Using while-loop instead of for-loop because when connecting the number of sub-graphs changes
    https://networkx.org/documentation/stable/reference/algorithms/component.html
    https://networkx.org/documentation/stable/reference/algorithms/generated/networkx.algorithms.components.connected_components.html#networkx.algorithms.components.connected_components
    """
    max_acceptable_link_distance = acceptable_link_distance
    loops = 0
    while nx.number_connected_components(network) > 1:
        sub_graphs = [network.subgraph(c) for c in nx.connected_components(network)]
        # make sure that we do not just expand the first all the time - might make a too big envelope
        chosen_index = random.randint(0, len(sub_graphs) - 1)
        current_graph = sub_graphs[chosen_index]
        closest_dist = 999999
        closest_combination = None
        closest_other = None
        current_envelope = _envelope_from_graph_points(list(current_graph.nodes), 0)
        for i in range(len(sub_graphs)):
            if i == chosen_index:  # may not test against self
                continue
            other_graph = sub_graphs[i]
            other_envelope = _envelope_from_graph_points(list(other_graph.nodes), 0)
            for combination in POSSIBLE_NODE_COMBINATIONS_2_RECTANGLES:
                dist = g.calc_distance(current_envelope[combination[0]], current_envelope[combination[1]],
                                       other_envelope[combination[2]], other_envelope[combination[3]])
                if dist < closest_dist:
                    closest_dist = dist
                    closest_combination = (current_envelope[combination[0]], current_envelope[combination[1]],
                                           other_envelope[combination[2]], other_envelope[combination[3]])
                    closest_other = i
                    # if there are many nodes it could take a very long time
                    if closest_dist < max_acceptable_link_distance:
                        break
            if closest_dist < max_acceptable_link_distance:
                break
        if closest_dist < max_acceptable_link_distance:
            sub_node_1 = _closest_node_to_point(list(current_graph.nodes), closest_combination[0],
                                                closest_combination[1])
            other_graph = sub_graphs[closest_other]
            sub_node_2 = _closest_node_to_point(list(other_graph.nodes), closest_combination[2], closest_combination[3])
            network.add_edge(sub_node_1, sub_node_2)
            logging.info('Reduced to %i sub-graphs with distance %d', nx.number_connected_components(network),
                         closest_dist)
            loops = 0
        else:
            loops += 1
            if loops > nx.number_connected_components(network):
                max_acceptable_link_distance *= 1.5
                logging.info('Matching is getting difficult - increasing link dist to %d', max_acceptable_link_distance)
                loops = 0


def connect_disjoint_subgraphs_by_node(network: nx.Graph, acceptable_link_distance: int) -> None:
    """Connect disjoint sub-graphs if any included in network by adjacent nodes.
    Using while-loop instead of for-loop because when connecting the number of sub-graphs changes.
    """
    while nx.number_connected_components(network) > 1:
        sub_graphs = [network.subgraph(c) for c in nx.connected_components(network)]
        chosen_index = 0
        largest_number = 0
        # use the largest subgraph
        for index, subgraph in enumerate(sub_graphs):
            if len(subgraph.nodes) > largest_number:
                largest_number = len(subgraph.nodes)
                chosen_index = index
        current_subgraph = sub_graphs[chosen_index]
        closest_dist = 999999
        closest_current = None
        closest_other = None
        current_envelope = _envelope_from_graph_points(list(current_subgraph.nodes), 0.1)
        for other_index, other_subgraph in enumerate(sub_graphs):
            if other_index == chosen_index:  # may not test against self
                continue
            if _has_points_in_envelope(current_envelope, list(other_subgraph.nodes)):
                for current_node in list(current_subgraph.nodes):
                    for other_node in list(other_subgraph.nodes):
                        dist = g.calc_distance(current_node.lon, current_node.lat, other_node.lon, other_node.lat)
                        if dist < closest_dist:
                            closest_dist = dist
                            closest_current = current_node
                            closest_other = other_node
                if closest_dist < acceptable_link_distance:
                    break
        if closest_dist < acceptable_link_distance:
            network.add_edge(closest_current, closest_other)
            logging.info('Reduced to %i sub-graphs with distance %d', nx.number_connected_components(network),
                         closest_dist)
        else:
            logging.info('Stopping at %i sub-graphs', nx.number_connected_components(network))
            break
        # Now remove everything not connected to the largest network
        for other_index, other_subgraph in enumerate(sub_graphs):
            if other_index == chosen_index:  # may not test against self
                continue
            for other_node in list(other_subgraph.nodes):
                network.remove_node(other_node)


def create_argument_parser_default(description: str, include_boundary: bool = True) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("-f", "--file", dest="filename",
                        help="read parameters from fILE (e.g. params.py)",
                        type=str, required=True)
    parser.add_argument("-o", "--output", dest="output",
                        help="write network to file (e.g. foo_network.pkl)",
                        type=str, required=True)
    if include_boundary:
        parser.add_argument("-b", "--boundary", dest="boundary",
                            help="set the boundary as WEST_SOUTH_EAST_NORTH like *9_47.0_11_48.5 (. as decimal)",
                            type=str, required=True)
    parser.add_argument('-d', dest='scenario_path', type=str,
                        help='the path to the directory where the scenarios live',
                        required=True)
    return parser
