""" Creates plausible networks for ships based on land vs. sea.

The goal is to generate a network for ships automatically based on mapping data (from OSM) instead of creating
a network manually. Also, the network should be suitable for larger ships (e.g. frigates) vs. smaller coastal
ships. The former stay away from the coast due to steering a threat - while the others stay close to the shore due
to visual navigation etc. For path planning a global view of a static environment is assumed.

References:
A] Yiquan Du, 2021: An Optimized Path Planning Method for Coastal Ships Based on Improved DDPG and DP
B] S. Russel, P. Norvig: Artificial Intelligence; A modern approach; Third edition, chapter 24.4
C] Yuanyuan Qiao et al., 2023: Survey of Deep Learning for Autonomous Surface Vehicles in Marine Environments
D] Junfeng Yuan et al., 2022: A second‐order dynamic and static ship path planning model based on
   reinforcement learning and heuristic search algorithms
E] Siyu Guo, 2021: Path Planning of Coastal Ships Based on Optimized DQN Reward Function
F] Yuanyuan Qiao, 2023: Survey of Deep Learning for Autonomous Surface Vehicles in Marine Environments
G] https://blog.habrador.com/2015/11/explaining-hybrid-star-pathfinding.html
H] Dmitri Dolgov et al., ????: Practical Search Techniques in Path Planning for Autonomous Driving.
   https://ai.stanford.edu/~ddolgov/papers/dolgov_gpp_stair08.pdf

A roadmap approach for static global path planning has been used, because:
* It is easy to understand and implement.
* Traditional shortest path can then be used (e.g. Dijkstra or A*) - maybe with some smoothing e.g. based on a
  Douglas-Peucher algorithm.
* Computationally ok - plus network generation can be done once and saved for each scenario.
* This is a simulation for fun - it does not have to be an optimal solution and the solution does not have to be
  smooth. Just when a pilot looks at it, then the navigation should look probable.
* While it does not allow for dynamic path planning, local obstacle avoidance e.g. for other ships can still be
  done by temporarily hop out of the current path. That might give collisions sometimes - but that is acceptable.

Alternatives would have been (see e.g. ref[A], which has good comparison of advantages/disadvantages incl. direct
tabular and visual comparison of path length, geometry as well as computation time)
* Artificial potential fields
* Different approaches using reinforcement learning with different types of neural networks

A custom network generation has been used based on grid. Alternatives would have been:
* Roadmap based path planning: visibility or Voronoi graphs (e.g. https://www.youtube.com/watch?v=Y5_aHsqX22s)
* Probabilistic roadmap (PRM, PRM* - e.g. https://theclassytim.medium.com/robotic-path-planning-prm-prm-b4c64b1f5acb)
* Rapidly-exploring random trees (RRT, RRT* - e.g.
                                  https://theclassytim.medium.com/robotic-path-planning-rrt-and-rrt-212319121378)

The network is created as follows:
* Vector data is based on land polygons from https://osmdata.openstreetmap.de/data/land-polygons.html, which again is
derived from OSM based on natural=coastline.
* A simplistic artificial potential field is added along the coast - one for close shore (to push the sips a bit
  away where possible) as well as a belt of a few 100 metres to have a preferred sailing area for small coastal
  ships respectively an area close to the shore which large ships avoid. Shortest path algorithm will just have
  increased or decreased cost
* Cell decomposition from vector data to raster data. Using approximate quadtree composition - see e.g.
  https://cs.stanford.edu/people/eroberts/courses/soco/projects/1998-99/robotics/basicmotion.html.
  Using composite / button-up clustering.
* To reduce network size and shortest path calculation time, clusters of cells will be combined
  (e.g. from 50m raster to 100m->200m->400m)
"""
from enum import IntEnum, unique
import logging
from typing import Dict, Optional, Tuple

import geopandas as gpd
import networkx as nx
from shapely.geometry import box

import hunter.scenario_tools.network_stuff as ns
import hunter.scenarios as sc
import hunter.utils as u

NEARSHORE_BUFFER_FACTOR = 20  # relative to scenario.lon_dem_grid_size

LEVELS = [5, 4, 3, 2]

GEOMETRY_FEATURE = 'geometry'


@unique
class ShoreType(IntEnum):
    """The type of content in the cell. All but 'coast' are navigable.
        Cf. https://en.wikipedia.org/wiki/Littoral_zone for terms.
    """
    coast = 0
    # used to keep a safety distance from the beach, but can be penetrated to reach a port
    # however is not used, because the blockyness of the coast vs. nearshore plus using the middle of the cell as
    # the waypoint already gives a safety margin
    foreshore = 1
    nearshore = 2  # in this zone most small vessels are - and large vessels keep away
    offshore = 3  # good for frigates etc.


class CompassPoint(IntEnum):
    n = 0
    ne = 1
    e = 2
    se = 3
    s = 4
    sw = 5
    w = 6
    nw = 7
    c = 8  # centre -> if the grid cell is not broken down more


class ShipNetwork:
    __slots__ = ('offshore_network', 'offshore_poi', 'nearshore_network', 'nearshore_poi')

    def __init__(self):
        self.offshore_network = None
        self.offshore_poi = None
        self.nearshore_network = None
        self.nearshore_poi = None


class GridCell:
    """The raster underlying the network.

    Level 0 is the most detailed quadtree - and an edge has the same length as the scenario's dem_grid_size.

    Level is the hierarchy in the quadtree.
    The size of a level is based on the scenario's dem_grid_size -> dem_grid_size * 2^level
    Level 2 (smallest) is ca. 50m * 2^2 = 200m, level 5 (largest) ca. 50m * 2^5 = 1600m

    lon_idx is the index for longitude and starts in West -> towards East
    lat_idx is the index for latitude and starts in South -> towards North
    """
    __slots__ = ('lon', 'lat', 'lon_idx', 'lat_idx', 'level', 'shore_type', 'final_level', 'compass_point',
                 'parent', 'children')

    def __init__(self, lon: float, lat: float, lon_idx: int, lat_idx: int, level: int, compass_point: CompassPoint,
                 parent: Optional['GridCell']):
        self.lon = lon
        self.lat = lat
        self.lon_idx = lon_idx
        self.lat_idx = lat_idx
        self.level = level
        self.shore_type = ShoreType.offshore
        self.final_level = False  # if True then no further break down
        self.compass_point = compass_point
        self.parent = parent
        self.children = list()

    @property
    def grid_id(self) -> Tuple[int, int]:
        return self.lon_idx, self.lat_idx

    def add_child(self, child: 'GridCell') -> None:
        self.children.append(child)

    def has_children(self) -> bool:
        return len(self.children) > 0

    def connect_inside(self, network: nx.Graph, nearshore_network: bool) -> None:
        """Connects all waypoints inside this grid with each other and adds WPs and edges to the graph."""
        pass  # FIXME

    def connect_outside(self, level_5_cells: Dict[Tuple[int, int], 'GridCell'], network: nx.Graph,
                        nearshore_network: bool) -> None:
        """Connects all waypoints on East and North side of this GridCell with waypoints in grid to East and North
         of this GridCell."""
        pass  # FIXME


def _create_geo_data_frame_box(boundary_tuple: Tuple[float, float, float, float],
                               buffer_deg: float = 0.) -> gpd.GeoDataFrame:
    boundary_box = box(boundary_tuple[0] - buffer_deg, boundary_tuple[1] - buffer_deg,
                       boundary_tuple[2] + buffer_deg, boundary_tuple[3] + buffer_deg)
    d = {GEOMETRY_FEATURE: [boundary_box]}
    return gpd.GeoDataFrame(d, crs='EPSG:4326')


def _read_land_data(boundary_tuple: Tuple[float, float, float, float]) -> gpd.GeoDataFrame:
    geo_box = _create_geo_data_frame_box(boundary_tuple, 0.1)  # make sure no residuals at borders
    # https://osmdata.openstreetmap.de/data/land-polygons.html
    land_polygons = gpd.read_file('/home/vanosten/custom_scenery/land-polygons-complete-4326/land_polygons.shp')
    # Clip the data using GeoPandas clip
    land_clip = gpd.clip(land_polygons, geo_box, keep_geom_type=False)
    return land_clip


def my_colourmap(value):  # scalar value defined in 'column'
    if value == ShoreType.coast:
        return "grey"
    elif value == ShoreType.offshore:
        return "blue"
    return "yellow"


def calculate_grid_cells(land_data: gpd.GeoDataFrame, lon_steps: int, lat_steps: int,
                         scenario: sc.ScenarioContainer) -> Dict[int, Dict[Tuple[int, int]]]:
    """Generates GridCells at different levels depending on need for break-down into smaller cells.
    Test first intersect - and then within, because if within land, then no further breakdown needed
    => only those which intersect need to be broken down to find out what is what (again first intersect, then within)
    """
    level_cells_dict = dict()
    this_level_cells = dict()
    for idx_lon in range(round(lon_steps / pow(2, LEVELS[0]))):
        for idx_lat in range(round(lat_steps / pow(2, LEVELS[0]))):
            lon = scenario.south_west[0] + (idx_lon + 0.5) * scenario.lon_dem_grid_size * pow(2, LEVELS[0])
            lat = scenario.south_west[1] + (idx_lat + 0.5) * scenario.lat_dem_grid_size * pow(2, LEVELS[0])
            cell = GridCell(lon, lat, idx_lon, idx_lat, LEVELS[0], CompassPoint.c, None)
            this_level_cells[cell.grid_id] = cell
    for level in LEVELS:
        geometry_col = list()
        shore_type_col = list()
        if level != LEVELS[0]:
            this_level_cells = dict()
            parent_level_cells = level_cells_dict[level + 1]
            for parent_cell in parent_level_cells.values():
                if parent_cell.final_level is False:
                    start_lon_idx = parent_cell.lon_idx * 2
                    start_lat_idx = parent_cell.lat_idx * 2
                    for idx_lon in range(2):
                        for idx_lat in range(2):
                            c_point = CompassPoint.sw if idx_lat == 0 else CompassPoint.nw
                            if idx_lon == 1:
                                c_point = CompassPoint.se if idx_lat == 0 else CompassPoint.ne
                            lon = scenario.south_west[0] + (
                                    start_lon_idx + idx_lon + 0.5) * scenario.lon_dem_grid_size * pow(2, LEVELS[0])
                            lat = scenario.south_west[1] + (
                                    start_lat_idx + idx_lat + 0.5) * scenario.lat_dem_grid_size * pow(2, LEVELS[0])
                            cell = GridCell(lon, lat, start_lon_idx + idx_lon, start_lat_idx + idx_lat, level, c_point,
                                            parent_cell)
                            this_level_cells[cell.grid_id] = cell
                            parent_cell.add_child(cell)

        count_coast_cells = 0
        count_coast_cells_final = 0
        count_offshore_cells_final = 0
        total_level_cells = len(this_level_cells)
        counter = 0
        for cell in this_level_cells.values():
            if counter % 1000 == 0:
                logging.info('%i out of %i cells tested at level %i', counter, total_level_cells, level)
            counter += 1
            cell_box = box(scenario.south_west[0] + cell.lon_idx * scenario.lon_dem_grid_size * pow(2, level),
                           scenario.south_west[1] + cell.lat_idx * scenario.lat_dem_grid_size * pow(2, level),
                           scenario.south_west[0] + (cell.lon_idx + 1) * scenario.lon_dem_grid_size * pow(2, level),
                           scenario.south_west[1] + (cell.lat_idx + 1) * scenario.lat_dem_grid_size * pow(2, level)
                           )
            has_intersected = False
            for index, row in land_data.iterrows():  # Looping over all polygons
                if cell_box.intersects(row[GEOMETRY_FEATURE]):
                    cell.shore_type = ShoreType.coast
                    count_coast_cells += 1
                    has_intersected = True
                    if level != LEVELS[-1] and cell_box.within(row[GEOMETRY_FEATURE]):
                        cell.final_level = True
                        count_coast_cells_final += 1
                    break
            if has_intersected is False:
                cell.final_level = True
                count_offshore_cells_final += 1
            if level == LEVELS[-1]:
                cell.final_level = True
            geometry_col.append(cell_box)
            shore_type_col.append(cell.shore_type.value)

        level_cells_dict[level] = this_level_cells
        logging.info('Out of %i grid cells %i are coast', total_level_cells, count_coast_cells)
        logging.info('Out of %i grid cells %i are coast final', total_level_cells, count_coast_cells_final)
        logging.info('Out of %i grid cells %i are offshore final', total_level_cells, count_offshore_cells_final)
        d = {GEOMETRY_FEATURE: geometry_col, 'ShoreType': shore_type_col}
        map_gdf = gpd.GeoDataFrame(d, crs='EPSG:4326')
        m = map_gdf.explore(column='ShoreType',
                            tooltip="ShoreType",
                            popup=True,
                            cmap='Set1',  # use "Set1" matplotlib colormap
                            # cmap=lambda value: my_colourmap(value),
                            # legend=False,  # needed -> https://github.com/geopandas/geopandas/pull/2590
                            style_kwds=dict(color="black")
                            )
        m.save('level_data_{}.html'.format(level))

    # For all those cells, which still are .offshore: test against nearshore buffer, but do not break down
    # into smaller cells -> just one pass
    nearshore_geoms = list()
    for index, row in land_data.iterrows():  # Looping over all polygons
        nearshore_geoms.append(row[GEOMETRY_FEATURE].buffer(scenario.lon_dem_grid_size * NEARSHORE_BUFFER_FACTOR))

    count_coast_cells = 0
    count_offshore_cells = 0
    count_nearshore_cells = 0
    geometry_col = list()
    shore_type_col = list()
    for level in LEVELS:
        for cell in level_cells_dict[level].values():
            if cell.final_level:
                intersection_found = False
                cell_box = box(scenario.south_west[0] + cell.lon_idx * scenario.lon_dem_grid_size * pow(2, level),
                               scenario.south_west[1] + cell.lat_idx * scenario.lat_dem_grid_size * pow(2, level),
                               scenario.south_west[0] + (cell.lon_idx + 1) * scenario.lon_dem_grid_size * pow(2, level),
                               scenario.south_west[1] + (cell.lat_idx + 1) * scenario.lat_dem_grid_size * pow(2, level)
                               )
                if cell.shore_type is ShoreType.offshore:
                    for nearshore in nearshore_geoms:
                        if cell_box.intersects(nearshore):
                            cell.shore_type = ShoreType.nearshore
                            count_nearshore_cells += 1
                            intersection_found = True
                            break
                    if intersection_found is False:
                        count_offshore_cells += 1
                else:
                    count_coast_cells += 1
                geometry_col.append(cell_box)
                shore_type_col.append(cell.shore_type.value)

    logging.info('There are %i coast, %i nearshore and %i offshore grid cells', count_coast_cells,
                 count_nearshore_cells, count_offshore_cells)
    d = {GEOMETRY_FEATURE: geometry_col, 'ShoreType': shore_type_col}
    map_gdf = gpd.GeoDataFrame(d, crs='EPSG:4326')
    m = map_gdf.explore(column='ShoreType',
                        tooltip="ShoreType",
                        popup=True,
                        cmap='Set1',  # use "Set1" matplotlib colormap
                        # cmap=lambda value: my_colourmap(value),
                        # legend=False,  # needed -> https://github.com/geopandas/geopandas/pull/2590
                        style_kwds=dict(color="black"),
                        )
    m.save('final_data.html')

    return level_cells_dict


def _main(scenario_path: str, scenario_name: str) -> None:
    """Read a scenario file and use the contained information to generate a DEM.
    The DEM is written to the scenario files with a file name matching a convention."""
    scenario = sc.load_scenario(scenario_name, scenario_path)  # raises ValueError if it does not exist

    lon_steps = int((scenario.north_east[0] - scenario.south_west[0]) / scenario.lon_dem_grid_size)
    lat_steps = int((scenario.north_east[1] - scenario.south_west[1]) / scenario.lat_dem_grid_size)
    lon_steps /= 2  # FIXME remove - speed-up for testing
    lat_steps /= 2  # FIXME remove

    logging.info('Staring to read land data from Shapefile')
    land_data = _read_land_data((scenario.south_west[0], scenario.south_west[1],
                                 scenario.north_east[0], scenario.north_east[1]))
    logging.info('Done reading land data from Shapefile')

    m = land_data.explore()
    m.save('land_data.html')

    level_cells_dict = calculate_grid_cells(land_data, lon_steps, lat_steps, scenario)

    # FIXME make clusters at different levels
    # Create the 2 networks: one for offshore vessels and one for nearshore vessels

    # --- keep only the largest subnetwork (number of WPs) in case there are several subnetworks

    # Save both networks and points of interest to pickle_file
    # FIXME loop through all edges and calculate cost based on distance plus penalty for large / small ships


if __name__ == '__main__':
    parser = ns.create_argument_parser_default('Create helicopter/drone network for Hunter using OSM data \
    based on a lon/lat defined area', False)
    parser.add_argument('-s', dest='scenario', type=str,
                        help='the scenario to use',
                        required=True)
    u.configure_logging('INFO', True, 'ship_network')
    args = parser.parse_args()

    _main(args.scenario_path, args.scenario)
