import logging
from math import fabs
import os.path as osp
import pickle
import sys

import networkx as nx

from osm2city import parameters
import osm2city.roads as r
import osm2city.static_types.osmstrings as s
import osm2city.static_types.enumerations as e
import osm2city.utils.coordinates as co
from osm2city.utils.elev_probe import FGElev
import osm2city.utils.osmparser as op
from osm2city.utils.utilities import parse_boundary, BoundaryError

import hunter.geometry as g
import hunter.scenario_tools.network_stuff as ns
import hunter.utils as u


KEPT_HIGHWAY_TYPES = [e.HighwayType.roundabout,
                      e.HighwayType.one_way_large,
                      e.HighwayType.one_way_multi_lane,
                      e.HighwayType.one_way_normal]


def _process_osm_for_roads_network(file_name: str, scenario_path: str, min_highway_type: int,
                                   max_point_dist: int, max_link_dist: int,
                                   hover: float,
                                   transform: co.Transformation) -> None:
    network = nx.Graph()

    osm_way_result = op.fetch_osm_db_data_ways_keys([s.K_HIGHWAY])
    osm_nodes_dict = osm_way_result.nodes_dict
    osm_ways_dict = osm_way_result.ways_dict
    logging.info("Number of ways before basic processing: %i", len(osm_ways_dict))
    my_ways = list()
    used_nodes = set()
    for way in osm_ways_dict.values():
        highway_type = e.highway_type_from_osm_tags(way.tags)
        if highway_type is None:
            continue
        elif highway_type.value < min_highway_type and highway_type not in KEPT_HIGHWAY_TYPES:
            continue
        my_ways.append(way)
        for ref in way.refs:
            used_nodes.add(ref)
    logging.info("Number of ways after basic processing: %i", len(my_ways))
    logging.info('Number of nodes actually used: %i out of %i', len(used_nodes), len(osm_nodes_dict))
    total_nodes = set(osm_nodes_dict.keys())
    unused_nodes = total_nodes.difference(used_nodes)
    for key in unused_nodes:
        del osm_nodes_dict[key]

    r.check_points_on_line_distance(max_point_dist, my_ways, osm_nodes_dict, transform)
    logging.info('Added %i nodes due to points on line check', len(osm_nodes_dict) - len(used_nodes))

    # Find the elevation as per FG scenery
    parameters.FG_ELEV_CACHE = False
    fg_elev = FGElev(None, 0)
    fg_elev.close()

    # Add all nodes to the graph as WayPoint
    way_points_dict = dict()  # osm_id, WayPoint
    for key, node in osm_nodes_dict.items():
        wp = g.WayPoint(key, node.lon, node.lat)
        wp.alt_m = fg_elev.probe_elev((node.lon, node.lat), True)
        wp.alt_m += hover
        way_points_dict[key] = wp
        network.add_node(wp)
    # Add edges and correct the altitude of WayPoints in tunnels
    for way in my_ways:
        tunnel_alt_m = way_points_dict[way.refs[0]].alt_m
        for index, ref in enumerate(way.refs):
            if index == 0:
                continue
            prev_wp = way_points_dict[way.refs[index - 1]]
            this_wp = way_points_dict[way.refs[index]]
            if s.is_tunnel(way.tags):
                this_wp.alt_m = tunnel_alt_m
            dist = g.calc_distance_wp_wp(prev_wp, this_wp)
            edge_cost = dist + fabs(prev_wp.alt_m - this_wp.alt_m) * 2
            network.add_edge(prev_wp, this_wp, cost=edge_cost)

    logging.info('# of connected components (sub-graphs) before linking %i', nx.number_connected_components(network))
    ns.connect_disjoint_subgraphs_by_node(network, max_link_dist)
    logging.info('# of connected components (sub-graphs) after linking %i', nx.number_connected_components(network))
    logging.info('Network after linking sub-graphs: \n%s', ns.info_about_network(network))

    logging.info('Network from OSM has %i nodes', nx.number_of_nodes(network))
    total_removed = ns.remove_dangling_nodes(network)
    logging.info('Removed %i dangling nodes', total_removed)

    logging.info("Created network graph with {} nodes and {} edges".format(nx.number_of_nodes(network),
                                                                           nx.number_of_edges(network)))
    output_file = osp.join(scenario_path, file_name)
    with open(output_file, 'wb') as file_pickle:
        pickle.dump(network, file_pickle)


if __name__ == '__main__':
    u.read_dotenv()

    parser = ns.create_argument_parser_default('Create road network for Hunter using OSM data \
    based on a lon/lat defined area')
    parser.add_argument("-w", dest="max_dist_wp",
                        help="""The maximal distance between way points. If the terrain is very bumpy, then
                        choose a low value. In flat terrain a higher value.
                        This fills in waypoints if in the OSM data points are far apart. The consequence is that the 
                        altitude is more accurate - at the expense of a larger network graph.""",
                        type=int, default=50,
                        required=False)
    parser.add_argument("-l", dest="max_link_dist",
                        help="""The maximal distance allowed when linking sub-graphs.""",
                        type=int, default=50,
                        required=False)
    parser.add_argument("-m", dest="min_highway_type",
                        help="""The minimal highway type to use as the lowest level of roads. 
                        Secondary highway is default if not set.
                        https://gitlab.com/osm2city/osm2city/-/blob/master/osm2city/static_types/enumerations.py#L393
                        Secondary highway is e.g. 12.""",
                        type=int, default=e.HighwayType.secondary.value,
                        required=False)
    logging.basicConfig(level=logging.INFO)
    args = parser.parse_args()

    parameters.read_from_file(args.filename)
    try:
        boundary_floats = parse_boundary(args.boundary)
    except BoundaryError as be:
        logging.error(be.message)
        sys.exit(1)

    logging.info("Overall boundary {}, {}, {}, {}".format(boundary_floats[0], boundary_floats[1],
                                                          boundary_floats[2], boundary_floats[3]))
    parameters.set_boundary(boundary_floats[0], boundary_floats[1],
                            boundary_floats[2], boundary_floats[3])

    the_transformer = co.Transformation(parameters.get_center_global())

    _process_osm_for_roads_network(args.output, args.scenario_path, args.min_highway_type,
                                   args.max_dist_wp, args.max_link_dist,
                                   1., the_transformer)
