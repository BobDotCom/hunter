import logging
import math
import os.path as osp
import pickle
import sys
from typing import Dict, List, Tuple

import networkx as nx

# osm2city
from osm2city import parameters
from osm2city.utils.elev_probe import FGElev
import osm2city.utils.osmparser as op
import osm2city.static_types.osmstrings as s
from osm2city.utils.utilities import parse_boundary, BoundaryError

import hunter.geometry as g
import hunter.scenario_tools.network_stuff as ns
import hunter.utils as u


def _create_hstore_key_values(tag_tuples: List[Tuple[str, str]]) -> List[str]:
    key_value_pairs = list()
    for key_value in tag_tuples:
        key_value_pairs.append('{}=>{}'.format(key_value[0], key_value[1]))
    return key_value_pairs


HELI_LANDING_SPOTS_TAGS = [(s.K_AEROWAY, s.V_HELIPORT), (s.K_AEROWAY, s.V_HELIPAD),
                           (s.K_MILITARY, s.V_RANGE), (s.K_MILITARY, s.V_TRAINING_AREA)]


def _check_heli_landing(tags: Dict[str, str]) -> bool:
    for key_value in HELI_LANDING_SPOTS_TAGS:
        if key_value[0] in tags and tags[key_value[0]] == key_value[1]:
            return True
    return False


POV_TAGS_CIVIL = [
    (s.K_AEROWAY, s.V_HELIPORT), (s.K_AEROWAY, s.V_HELIPAD), (s.K_AEROWAY, s.V_AERODROME),
    (s.K_AMENITY, s.V_HOSPITAL),
    (s.K_LANDUSE, s.V_RAILWAY),
    (s.K_LEISURE, s.V_MARINA), (s.K_HARBOUR, s.V_YES),
    (s.K_MAN_MADE, s.V_COMMUNICATIONS_TOWER), (s.K_POWER, s.V_PLANT),
    (s.K_PLACE, s.V_CITY), (s.K_PLACE, s.V_BOROUGH), (s.K_PLACE, s.V_SUBURB),
    (s.K_PLACE, s.V_VILLAGE)
]

POV_TAGS_CIVIL_EXTRA = [
    (s.K_LEISURE, s.V_PARK), (s.K_MAN_MADE, s.V_BRIDGE), (s.K_MAN_MADE, s.V_DYKE), (s.K_TOURISM, s.V_ZOO),
    (s.K_WATERWAY, s.V_DAM), (s.K_PUBLIC_TRANSPORT, s.V_STATION), (s.K_PLACE, s.V_TOWER),
    (s.K_PLACE, s.V_HAMLET), (s.K_PLACE, s.V_ISOLATED_DWELLING),
    (s.K_LEISURE, s.V_STADIUM), (s.K_LEISURE, s.V_TRACK)
]

POV_TAGS_MILITARY = [
    (s.K_MILITARY, s.V_BUNKER), (s.K_MILITARY, s.V_CHECKPOINT), (s.K_MILITARY, s.V_DANGER_AREA),
    (s.K_MILITARY, s.V_NAVAL_BASE), (s.K_MILITARY, s.V_RANGE), (s.K_MILITARY, s.V_TRAINING_AREA)
]

POV_TAGS_NAVAL = [
    (s.K_MAN_MADE, s.V_LIGHTHOUSE), (s.K_MAN_MADE, s.V_OFFSHORE_PLATFORM), (s.K_MAN_MADE, s.V_PIER)
]

PASS_TAGS = [
    (s.K_MOUNTAIN_PASS, s.V_YES)
]

PASS_TAGS_EXTRA = [
    (s.K_NATURAL, s.V_SADDLE)
]

# Only use few linear features because otherwise too many links, e.g. railways and motorways can generate
# parallel lines; also topology of intersections is not preserved after Way.simplify()
# Shores of lakes are added in to processing below
# These linear tags are mostly used to make sure that in mountainous areas the helis probably follow a bit more
# along the valley
LINEAR_TAGS_CIVIL = [
    (s.K_WATERWAY, s.V_RIVER), (s.K_WATERWAY, s.V_CANAL),
    (s.K_RAILWAY, s.V_RAIL),
    (s.K_HIGHWAY, s.V_PRIMARY), (s.K_HIGHWAY, s.V_SECONDARY)
]

LINEAR_TAGS_NAVAL = [
    (s.K_ROUTE, s.V_FERRY)
]

LINEAR_TAGS_CIVIL_EXTRA = [
    (s.K_RAILWAY, s.V_SUBWAY), (s.K_WATERWAY, s.V_STREAM),
    (s.K_RAILWAY, s.V_RAIL), (s.K_RAILWAY, s.V_DISUSED), (s.K_RAILWAY, s.V_PRESERVED),
    (s.K_RAILWAY, s.V_NARROW_GAUGE)
]

LINK_DIST_OK_DANGLING = 10000
LINK_DIST_OK_PASS = 2000
# if a distance between in sub-graphs is smaller than this, then accepts immediately and stop searching (good enough)
LINK_DIST_ACCEPTABLE_SUB_GRAPHS = 2000

SIMPLIFY_TOLERANCE = 2.

HELI_CRUISE_ABOVE_GROUND = 100  # the basic value to add to the ground elevation (metres)
HELI_MIN_ABOVE_GROUND = 50  # tolerable distance from ground between nodes (metres)

MAX_STEEPNESS_DEG = 20
STEEPNESS_INCREASE = 0.5

MIN_DIST_BETWEEN_NODES = 20  # it does not make sense to have nodes which are too close

# if there are too few nodes to do elev probing, then we might fly through mountains
# but there is actually probing between each point. Therefore, the distance can be a bit larger
MAX_DIST_BETWEEN_NODES = 150

ELEV_PROBE_DIST = 50  # points with this distance from orig point will also be probed for elev


def _safe_ground_elevation(way_point: g.WayPoint, fg_elev: FGElev) -> float:
    """Probes the waypoint as well as a set og points in distance X and returns the highest elevation.
    This gives some kind of safety distance, which a pilot also would use."""
    elevations = list()
    elevations.append(fg_elev.probe_elev((way_point.lon, way_point.lat), True))
    for i in range(8):
        other_lon_lat = g.calc_destination_wp(way_point, ELEV_PROBE_DIST, i * 45)
        elevations.append(fg_elev.probe_elev(other_lon_lat, True))
    return max(elevations)


def _remove_close_nodes_in_network(network: nx.Graph) -> None:
    """Remove all nodes in network, which are very close another node.
    For the node removed the edges are transferred to another node and thereby preserved."""
    correction_found = True
    total_corrections = 0
    while correction_found:
        correction_found = False
        for candidate_node in list(network.nodes):
            neighbors = list(nx.neighbors(network, candidate_node))
            for other_node in neighbors:
                dist = g.calc_distance_wp_wp(candidate_node, other_node)
                if dist < MIN_DIST_BETWEEN_NODES:
                    correction_found = True
                    total_corrections += 1
                    other_neighbors = list(nx.neighbors(network, candidate_node))
                    for connected_node in other_neighbors:
                        if connected_node is not candidate_node:
                            network.add_edge(candidate_node, connected_node)
                    network.remove_node(other_node)  # also removes its edges
                    break
            if correction_found:
                break

    logging.info('Total corrections of nodes too close to each other: %i', total_corrections)


def _process_osm_for_heli(file_name: str, scenario_path: str,
                          use_naval_stuff: bool, use_pov_extra_civil: bool,
                          use_extra_mountain_stuff: bool) -> None:
    """
    The heuristics are a compromise between having a "complete" network in the sense of having covered all possible
    routes that a helicopter might take, the resulting size of the network and somewhat realism.

    [A] Find linear features which somewhat indicate where a heli might fly - especially in a mountainous area like
        the Alps we have to find a network, which follows the valleys (however there is no "valley" feature in OSM).
        Will simplify the geometry of these features - because helis fly mostly straight lines.
        Then make a first network
    [B] Use nodes for passes to find the way between ends of linear features (e.g. rivers) -> search for nodes
        in the network, which only have one edge linked and which are within a max distance
        (it must be dangling nodes because otherwise might be connected to same linear feature (e.g. river) several
        times as after having made the network the original topology of "same river = a set of segments" is lost).
    [C] Find points of interest (POI): between which points would a heli want to fly?
        Connect POI's to the nearest X points in the network -> additional ways (we want them before the next step)
    [D] Make sure that there are no disjoint sub-graphs
    [E] Find nodes in the network, which only have one edge.
        Connect them with the nearest node in the network - within some reasonable distance to "close" the network.
        If none is found, then remove the node and the edge - unless the lone point is a POI.
    [F] Find elevation for each node plus minimal flight height (e.g. 200 m).
    [G] For each edge test, whether points along (e.g. every 500 m) would scratch the surface. If yes, then
        add a new node and split the edge.
    [H] For each edge, check whether steepness is above X. If yes, then add Y metres to lower node to make it less
        steep. Use Z iterations to smooth the network (too many iterations would possibly make the helis fly to high
        over the terrain).
    [I] Create the network with weights on edges based on distance, elevation difference and a bonus if passing by a
        pass.
    """

    # -------- [A] linear features to fake air "ways"
    # linear features for ways
    linear_tags = LINEAR_TAGS_CIVIL
    if use_pov_extra_civil or use_extra_mountain_stuff:
        linear_tags.extend(LINEAR_TAGS_CIVIL_EXTRA)
    if use_naval_stuff:
        linear_tags.extend(LINEAR_TAGS_NAVAL)
    key_value_pairs = _create_hstore_key_values(linear_tags)
    osm_linear_result = op.fetch_osm_db_data_ways_key_values(key_value_pairs)
    linear_nodes_dict = osm_linear_result.nodes_dict
    linear_ways_dict = osm_linear_result.ways_dict

    # add borderline of lakes as linear feature
    osm_lake_result = op.fetch_osm_db_data_ways_key_values(['water=>lake'])
    linear_nodes_dict.update(osm_lake_result.nodes_dict)
    for key, value in osm_lake_result.ways_dict.items():
        # split in the middle to get two ways (roughly opposite)
        if len(value.refs) > 2:
            original_refs = value.refs
            middle = int(round(len(original_refs)))
            value.refs = original_refs[:middle]
            linear_ways_dict[key] = value
            new_way = op.Way(op.get_next_pseudo_osm_id(op.OSMFeatureType.generic_way))
            new_way.refs = original_refs[middle:]
            linear_ways_dict[new_way.osm_id] = new_way

    # remove ways which for some reason have too few point
    for key in list(linear_ways_dict.keys()):
        if len(linear_ways_dict[key].refs) < 2:
            del linear_ways_dict[key]
    logging.info("Number of linear features found: {}".format(len(linear_ways_dict)))

    # simplify ways - needs to be done before ways get connected
    prev_nodes = 0
    after_nodes = 0
    for way in linear_ways_dict.values():
        prev_nodes += len(way.refs)
        way.simplify(linear_nodes_dict, None, SIMPLIFY_TOLERANCE)
        after_nodes += len(way.refs)
    logging.info('Simplified %i ways from %i nodes to %i', len(linear_ways_dict), prev_nodes, after_nodes)

    # create the basis network graph
    network = nx.Graph()
    way_points_dict = dict()  # osm_id, WayPoint
    for key, node in linear_nodes_dict.items():
        wp = g.WayPoint(key, node.lon, node.lat)
        way_points_dict[key] = wp
        network.add_node(wp)
    # Add edges
    for way in linear_ways_dict.values():
        for index, ref in enumerate(way.refs):
            if index == 0:
                continue
            prev_wp = way_points_dict[way.refs[index - 1]]
            this_wp = way_points_dict[way.refs[index]]
            network.add_edge(prev_wp, this_wp)

    logging.info('Network after linear: \n%s', ns.info_about_network(network))

    # ------- [B] mountain passes
    pass_tags = PASS_TAGS
    if use_extra_mountain_stuff:
        pass_tags.extend(PASS_TAGS_EXTRA)
    key_value_pairs = _create_hstore_key_values(PASS_TAGS)
    pass_dict = op.fetch_db_nodes_isolated(list(), key_value_pairs)
    logging.info('Number of pass points: %i', len(pass_dict))
    for node in pass_dict.values():
        poi = g.WayPoint(node.osm_id, node.lon, node.lat, g.MISSING_ELEV)
        poi.is_point_of_interest = True
        poi.is_heli_landing = _check_heli_landing(node.tags)
        poi.is_bonus_point = True
        network.add_node(poi)

        # search the closest node, which has only 1 neighbor -> link to pass
        # do it twice to possibly find both sides of a pass
        for i in range(2):
            closest_node = None
            closest_dist = 99999999
            for candidate_node in list(network.nodes):
                neighbors = list(nx.neighbors(network, candidate_node))
                if len(neighbors) == 1:
                    dist = g.calc_distance(poi.lon, poi.lat, candidate_node.lon, candidate_node.lat)
                    if dist < closest_dist:
                        closest_dist = dist
                        closest_node = candidate_node
            if closest_node and closest_dist < LINK_DIST_OK_PASS:
                network.add_edge(poi, closest_node)

    logging.info('Network after pass points: \n%s', ns.info_about_network(network))

    # -------- [C] Points of Interests
    # points of interest from osm nodes
    points_of_interest = dict()  # key = osm_id, value = g.NetworkNode
    pov_tags = POV_TAGS_CIVIL
    pov_tags.extend(POV_TAGS_MILITARY)
    if use_pov_extra_civil:
        pov_tags.extend(POV_TAGS_CIVIL_EXTRA)
    if use_naval_stuff:
        pov_tags.extend(POV_TAGS_NAVAL)
    key_value_pairs = _create_hstore_key_values(pov_tags)
    poi_nodes_dict = op.fetch_db_nodes_isolated(list(), key_value_pairs)
    for node in poi_nodes_dict.values():
        poi = g.WayPoint(node.osm_id, node.lon, node.lat, g.MISSING_ELEV)
        poi.is_point_of_interest = True
        poi.is_heli_landing = _check_heli_landing(node.tags)
        points_of_interest[poi.index] = poi
        network.add_node(poi)

    # points of interest from osm areas
    osm_way_result = op.fetch_osm_db_data_ways_key_values(key_value_pairs)
    linear_nodes_dict = osm_way_result.nodes_dict
    linear_ways_dict = osm_way_result.ways_dict
    for way in linear_ways_dict.values():
        lon = 0.
        lat = 0.
        for i in range(len(way.refs) - 1):
            node = linear_nodes_dict[way.refs[i]]
            lon += node.lon
            lat += node.lat
        lon /= len(way.refs) - 1
        lat /= len(way.refs) - 1
        poi = g.WayPoint(op.get_next_pseudo_osm_id(op.OSMFeatureType.generic_node), lon, lat, g.MISSING_ELEV)
        poi.is_point_of_interest = True
        poi.is_heli_landing = _check_heli_landing(way.tags)
        # we live with the tiny chance that an area could have the same id as point in OSM
        points_of_interest[poi.index] = poi
        network.add_node(poi)

    logging.info("Number of points of interest found: {}".format(len(points_of_interest)))

    # search the closest node, which has at least 1 neighbor -> link to POI
    # do it twice, so we do not have a dangling node
    for poi in points_of_interest.values():
        prev_closest = None
        for i in range(2):
            closest_node = None
            closest_dist = 99999999
            for candidate_node in list(network.nodes):
                if candidate_node is poi:
                    continue
                if prev_closest and prev_closest is candidate_node:  # do not use same node twice
                    continue
                neighbors = list(nx.neighbors(network, candidate_node))
                if len(neighbors) >= 1:
                    dist = g.calc_distance(poi.lon, poi.lat, candidate_node.lon, candidate_node.lat)
                    if dist < closest_dist:
                        closest_dist = dist
                        closest_node = candidate_node
            if prev_closest:
                if closest_dist < LINK_DIST_OK_PASS:  # if this is the second link, then we are picky about the distance
                    network.add_edge(poi, closest_node)
            else:
                network.add_edge(poi, closest_node)
            network.add_edge(poi, closest_node)
            prev_closest = closest_node

    logging.info('Network after points of interest: \n%s', ns.info_about_network(network))

    # find nodes, which do not have edges and remove them
    for node in list(network.nodes):
        neighbors = list(nx.neighbors(network, node))
        if not neighbors:
            network.remove_node(node)
    logging.info('Network after nodes without edges: \n%s', ns.info_about_network(network))

    # make sure there is a minimal distance between the nodes
    _remove_close_nodes_in_network(network)
    logging.info('Network after corrections of too close nodes: \n%s', ns.info_about_network(network))

    # -------- [D] Connect disjoint sub-graphs if any
    logging.info('# of connected components (sub-graphs) before linking %i', nx.number_connected_components(network))
    ns.connect_disjoint_subgraphs(network, LINK_DIST_ACCEPTABLE_SUB_GRAPHS)
    logging.info('Network after linking sub-graphs: \n%s', ns.info_about_network(network))

    # -------- [E] Find nodes in the network, which only have one edge
    # first round: try to connect
    for dangling in list(network.nodes):
        neighbors = list(nx.neighbors(network, dangling))
        if len(neighbors) == 1:
            neighbor_node = neighbors[0]
            closest_node = None
            closest_dist = 99999999
            for candidate_node in list(network.nodes):
                if closest_node is neighbor_node:
                    continue  # we do not want to connect with the dangling nodes only neighbor
                neighbors = list(nx.neighbors(network, candidate_node))
                if len(neighbors) >= 1:
                    dist = g.calc_distance(dangling.lon, dangling.lat, candidate_node.lon, candidate_node.lat)
                    if dist < closest_dist:
                        closest_dist = dist
                        closest_node = candidate_node
            if closest_node and closest_dist < LINK_DIST_OK_DANGLING:  # we do not care how far away it might be
                network.add_edge(dangling, closest_node)
    # second round: remove the remaining dangling iteratively
    total_removed = ns.remove_dangling_nodes(network)
    logging.info('Removed %i dangling nodes', total_removed)

    logging.info('Network after second dangling: \n%s', ns.info_about_network(network))

    # -------- [F] Find elevation for each node plus minimal flight height
    parameters.FG_ELEV_CACHE = False
    fg_elev = FGElev(None, 0)
    for way_point in list(network.nodes):
        way_point.ground_m = _safe_ground_elevation(way_point, fg_elev)
        way_point.alt_m = way_point.ground_m + HELI_CRUISE_ABOVE_GROUND

    # -------- [G] For each edge test, whether points along would scratch the surface.
    # Iteratively go through all edges and test if a heli would scratch the surface in the middle.
    # Only tests for edges of a certain length
    new_points_added = 99
    while new_points_added > 0:
        new_points_added = 0
        edges_to_remove = list()  # list of tuple (start, end)
        for edge in list(network.edges):
            start_wp = edge[0]
            end_wp = edge[1]
            dist = g.calc_distance_wp_wp(start_wp, end_wp)
            bearing = g.calc_bearing_wp(start_wp, end_wp)
            lon_lat = g.calc_destination_wp(start_wp, dist / 2, bearing)
            ground_elev = fg_elev.probe_elev(lon_lat, True)
            line_elev = start_wp.alt_m - (start_wp.alt_m - end_wp.alt_m) / 2
            if (ground_elev + HELI_MIN_ABOVE_GROUND > line_elev) or (dist > MAX_DIST_BETWEEN_NODES):
                new_points_added += 1
                edges_to_remove.append((start_wp, end_wp))  # will remove at end of loop through graph
                new_wp = g.WayPoint(op.get_next_pseudo_osm_id(op.OSMFeatureType.generic_node),
                                    lon_lat[0], lon_lat[1])
                new_wp.ground_m = _safe_ground_elevation(new_wp, fg_elev)
                new_wp.alt_m = new_wp.ground_m + HELI_CRUISE_ABOVE_GROUND
                network.add_node(new_wp)
                network.add_edge(start_wp, new_wp)
                network.add_edge(new_wp, end_wp)
        # now remove edges after loop
        for wp_tuple in edges_to_remove:
            network.remove_edge(wp_tuple[0], wp_tuple[1])
        logging.info('Added %i new way_points due to bumpiness', new_points_added)
    fg_elev.close()

    # ------- [H] For each edge, check whether steepness is above X
    # Over the iterations we accept more and more steepness to make sure that the general cruise level does
    # not get too high just because of some anomalies between a few waypoints
    iteration = 0
    corrections_done = 99
    while corrections_done > 0:
        corrections_done = 0
        allowed_max_angle = MAX_STEEPNESS_DEG + iteration * STEEPNESS_INCREASE
        if allowed_max_angle >= 40:
            break
        for edge in list(network.edges):
            start_wp = edge[0]
            end_wp = edge[1]
            dist = g.calc_distance_wp_wp(start_wp, end_wp)
            if dist > 10.:
                delta = math.fabs(start_wp.alt_m - end_wp.alt_m)
                angle = math.degrees(math.atan(delta/dist))
                if angle > allowed_max_angle:
                    allowed_delta = math.tan(math.radians(allowed_max_angle)) * dist
                    if start_wp.alt_m > end_wp.alt_m:
                        end_wp.alt_m = start_wp.alt_m - allowed_delta
                    else:
                        start_wp.alt_m = end_wp.alt_m - allowed_delta
                    corrections_done += 1
        iteration += 1
        logging.info('%i corrections of flight alt done due to steepness', corrections_done)

    # -------- [I] Create the network with weights on edges based on distance, elevation difference and a bonus
    for edge in list(network.edges):
        start_wp = edge[0]
        end_wp = edge[1]
        dist = g.calc_distance_wp_wp(start_wp, end_wp)
        delta = math.fabs(start_wp.alt_m - end_wp.alt_m)
        cost = dist + 2 * delta
        if start_wp.is_bonus_point or end_wp.is_bonus_point:
            cost = 0.1  # a little bonus of taking the pass way
        network[start_wp][end_wp][g.EDGE_ATTR_COST] = cost

    logging.info("Created network graph with {} nodes and {} edges".format(nx.number_of_nodes(network),
                                                                           nx.number_of_edges(network)))
    number_poi = 0
    for node in list(network.nodes):
        if node.is_point_of_interest:
            number_poi += 1
    logging.info('Network has %i points of interest.', number_poi)

    output_file = osp.join(scenario_path, file_name)
    with open(output_file, 'wb') as file_pickle:
        pickle.dump(network, file_pickle)


if __name__ == '__main__':
    u.read_dotenv()

    parser = ns.create_argument_parser_default('Create helicopter/drone network for Hunter using OSM data \
    based on a lon/lat defined area')
    parser.add_argument('-n', dest='use_naval_stuff', action='store_true',
                        help='if there are areas between islands to cover - or large bays',
                        default=False, required=False)
    parser.add_argument('-c', dest='use_pov_extra_civil', action='store_true',
                        help='if this is a very sparsely populated area (remote)',
                        default=False, required=False)
    parser.add_argument('-m', dest='use_extra_mountain_stuff', action='store_true',
                        help='if there are many hills/mountains and not a lot of infrastructure (remote)',
                        default=False, required=False)
    logging.basicConfig(level=logging.INFO)
    args = parser.parse_args()
    parameters.read_from_file(args.filename)
    try:
        boundary_floats = parse_boundary(args.boundary)
    except BoundaryError as be:
        logging.error(be.message)
        sys.exit(1)

    logging.info("Overall boundary {}, {}, {}, {}".format(boundary_floats[0], boundary_floats[1],
                                                          boundary_floats[2], boundary_floats[3]))
    parameters.set_boundary(boundary_floats[0], boundary_floats[1],
                            boundary_floats[2], boundary_floats[3])

    _process_osm_for_heli(args.output, args.scenario_path, args.use_naval_stuff, args.use_pov_extra_civil,
                          args.use_extra_mountain_stuff)
