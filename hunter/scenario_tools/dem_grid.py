"""A digital elevation model (DEM) based on the original in FlightGear.
Using FGElev the FG elevation model is copied into a grid, which can be saved to disk and distributed
with Hunter scenarios. That way Hunter has some means of understanding the DEM and can do visibility
calculations and other things.

The grid uses the lower left corner as the index - the elevation is probed at the mid-point of a grid cell.

Calculations of line of sight follow the grid model with Pythagoras and not a greater circle. It is not so accurate,
but in the end the DEM is not so accurate neither.
See also https://en.wikipedia.org/wiki/Great-circle_navigation -> quite a bit of computation.

The elevations of the grid are kept as integers to save disk space - and because it is good enough.

Unittest timeing suggest that 4 visibility calculations of ca. 10 km distance take ca. 2 ms.
"""

# from scipy.interpolate import RegularGridInterpolator
#
# itp = RegularGridInterpolator( (lat, lon), data, method='nearest')
# res = itp(some_new_point)
#
# As a bonus, this function can also perform more precise linear interpolations if you set method='linear'.

# https://github.com/MHenderson1988/line-of-sight-tool-python

# 100 km by 100 km grid
# 50 metres
# 100 * 1000 / 50 = 2000
# 2k * 2k = 4 million
# 3 bytes per point in grid (2 bytes for int plus a bit of overhead)
# 12 million bytes -> 12 MB of storage or memory => should not be a problem at all
# 400 km by 400 km (Falklands) would then be 200 Mb
# As a first implementation it could be good enough to just have one in-memory calculation
# per simulated target - instead of trying to centralize and having to either (a) have many pipes to handle, or
# (b) entirely make a different interface (e.g. a container running the calculation with a REST/JSON interface).
# => using more memory and more file reads at start-up vs. complexity/error handling.

import argparse
import bz2
import logging
import math

import numpy as np
import pickle
import time
from typing import List, Optional, Tuple


import hunter.geometry as g
import hunter.scenarios as sc
import hunter.utils as u


def _calc_below_horizon(pos: g.Position, tangential_pos: g.Position) -> float:
    """Calculates the height below horizon for a position relative to a tangential position due to the
    curvature of the Earth."""
    angle = math.sqrt(math.pow(pos.lon - tangential_pos.lon, 2) + math.pow(pos.lat - tangential_pos.lat, 2))
    return (1 - math.cos(math.radians(angle))) * g.EQURAD


class DigitalElevationModel:
    __slots__ = ('dem_grid', 'south_west', 'north_east', 'lon_dem_grid_size', 'lat_dem_grid_size', 'spacing')

    def __init__(self, dem: np.ndarray, south_west: Tuple[float, float], north_east: Tuple[float, float],
                 lon_dem_grid_size: float, lat_dem_grid_size: float) -> None:
        self.dem_grid = dem  # dtype=np.short
        self.south_west = south_west
        self.north_east = north_east
        self.lon_dem_grid_size = lon_dem_grid_size
        self.lat_dem_grid_size = lat_dem_grid_size

        # the spacing of points within the line of sight. Often the lon/lat grid sizes are not the same.
        # therefore the diameter seems to be a good compromise.
        self.spacing = math.sqrt(math.pow(self.lon_dem_grid_size, 2) + math.pow(self.lat_dem_grid_size, 2))

    def look_up_elevation(self, lon_lat: Tuple[float, float]) -> int:
        """ Returns the elevation stored in the DEM based on a global coordinate.
        Returns 0 if the lon_lat is not within the DEM."""
        if not (self.south_west[0] <= lon_lat[0] <= self.north_east[0]):
            return 0
        if not (self.south_west[1] <= lon_lat[1] <= self.north_east[1]):
            return 0
        lon_index = int((lon_lat[0] - self.south_west[0]) / self.lon_dem_grid_size)
        lat_index = int((lon_lat[1] - self.south_west[1]) / self.lat_dem_grid_size)
        # if exactly on edge of North or East or due to rounding a bit larger, then we risk an index out of bounds error
        if lon_index >= self.shape[0] or lat_index >= self.shape[1]:
            return 0
        return int(self.dem_grid[lon_index, lat_index])

    @property
    def shape(self) -> Tuple[int, int]:
        """The shape of the DEM -> number of cells lon / number of cells lat"""
        shape_tuple = self.dem_grid.shape  # shape can have any dimension
        return shape_tuple[0], shape_tuple[1]  # but we know there are two

    def _create_list_of_positions_on_line(self, start_pos: g.Position, end_pos: g.Position) -> List[g.Position]:
        """Creates a list of points on a line - but not start and end point."""
        points_on_line = list()
        grid_dist = math.sqrt(math.pow(end_pos.lon - start_pos.lon, 2) + math.pow(end_pos.lat - start_pos.lat, 2))
        number_of_points = int(grid_dist / self.spacing)
        if number_of_points >= 1:
            lon_spacing = (end_pos.lon - start_pos.lon) / number_of_points
            lat_spacing = (end_pos.lat - start_pos.lat) / number_of_points
            for i in range(number_of_points):
                pos = g.Position(start_pos.lon + (i + 1) * lon_spacing,
                                 start_pos.lat + (i + 1) * lat_spacing)
                pos.alt_m = self.look_up_elevation((pos.lon, pos.lat))
                points_on_line.append(pos)

        return points_on_line

    def is_visible(self, start_pos: g.Position, end_pos: g.Position) -> bool:
        """Returns True if there is no topographical obstacle including earth curvature in the line of sight.
        If the DEM does not cover a position, then an elevation of 0.0 will be assumed for the given position."""
        if start_pos.lon == end_pos.lon and start_pos.lat == end_pos.lat:
            return True  # Start and end point on the same coordinate

        lon_mid = start_pos.lon + (end_pos.lon - start_pos.lon) / 2
        lat_mid = start_pos.lat + (end_pos.lat - start_pos.lat) / 2
        mid_pos = g.Position(lon_mid, lat_mid, self.look_up_elevation((lon_mid, lat_mid)))

        # mid_pos does not need reduction with _calc_below_horizon because it is at zero in the middle
        start_alt_m = start_pos.alt_m - _calc_below_horizon(start_pos, mid_pos)  # do not change the original object
        end_alt_m = end_pos.alt_m - _calc_below_horizon(end_pos, mid_pos)  # do not change the original object
        dist_edge_to_edge = math.sqrt(math.pow(end_pos.lon - start_pos.lon, 2) + math.pow(
            end_pos.lat - start_pos.lat, 2))
        edge_points_alt_diff = end_alt_m - start_alt_m

        # make a quick check of visibility with mid-point before we do lots of calculations
        # height_line_of_sight is the height at a specific point on the line between the edge points
        height_line_of_sight = start_alt_m + edge_points_alt_diff * 0.5
        # if the theoretical line along the view is below a point, then the other edge is not visible
        if height_line_of_sight < mid_pos.alt_m:
            return False

        # Now check for all the other points on the line, whether the height_line_of_sight is lower than the
        # altitude of the point
        positions = self._create_list_of_positions_on_line(start_pos, end_pos)
        for i in range(len(positions)):
            below_horizon = _calc_below_horizon(positions[i], mid_pos)
            positions[i].alt_m -= below_horizon
            dist_from_start_pos = math.sqrt(math.pow(positions[i].lon - start_pos.lon, 2) + math.pow(
                positions[i].lat - start_pos.lat, 2))
            height_line_of_sight = start_alt_m + (edge_points_alt_diff * dist_from_start_pos / dist_edge_to_edge)
            if height_line_of_sight < positions[i].alt_m:
                return False  # return as soon as possible to omit additional calculations
        return True


def _read_scenario_and_write_out(scenario_path: str, scenario_name: str) -> None:
    """Read a scenario file and use the contained information to generate a DEM.
    The DEM is written to the scenario files with a file name matching a convention."""
    start = time.time()
    scenario = sc.load_scenario(scenario_name, scenario_path)  # raises ValueError is it does not exist

    from osm2city.utils.elev_probe import FGElev

    fg_elev = FGElev(None, 0)

    lon_steps = int((scenario.dem_north_east[0] - scenario.dem_south_west[0]) / scenario.lon_dem_grid_size)
    lat_steps = int((scenario.dem_north_east[1] - scenario.dem_south_west[1]) / scenario.lat_dem_grid_size)

    size_in_metres = g.calc_distance(scenario.dem_south_west[0], scenario.dem_south_west[1],
                                     scenario.dem_south_west[0] + scenario.lon_dem_grid_size,
                                     scenario.dem_south_west[1])
    logging.info('Grid size in metres longitudinal: %f', size_in_metres)
    size_in_metres = g.calc_distance(scenario.dem_south_west[0], scenario.dem_south_west[1],
                                     scenario.dem_south_west[0],
                                     scenario.dem_south_west[1] + scenario.lat_dem_grid_size)
    logging.info('Grid size in metres latitude: %f', size_in_metres)

    dem_grid = np.ndarray(shape=(lon_steps, lat_steps), dtype=np.short)
    logging.info('The memory size of the resulting 2D array is %i MiB plus some overhead',
                 (dem_grid.size * dem_grid.itemsize) // 1000000)

    i = 0
    for lon in range(lon_steps):
        for lat in range(lat_steps):
            if i % 100000 == 0:
                logging.info("Probed {:,} out of {:,}".format(i, dem_grid.size))
            i += 1
            c_lon = scenario.dem_south_west[0] + (lon + 0.5) * scenario.lon_dem_grid_size
            c_lat = scenario.dem_south_west[1] + (lat + 0.5) * scenario.lat_dem_grid_size
            dem_grid[lon, lat] = int(fg_elev.probe_elev((c_lon, c_lat), True))

    logging.info('Probed elevation for %i points took %f seconds', dem_grid.size, time.time() - start)

    dem_file_name = scenario.build_path_and_file_name(sc.FILE_PICKLE_DEM)
    with bz2.BZ2File(dem_file_name, 'wb') as file_pickle:
        pickle.dump(dem_grid, file_pickle)

    fg_elev.close()


def read_scenario_and_load_in(scenario: sc.ScenarioContainer) -> Optional[DigitalElevationModel]:
    """Read a DEM for a specific scenario based on a convention for path and name.
    If the DEM is not found (not all scenarios have a DEM), then return None."""
    start = time.time()
    dem_file_name = scenario.build_path_and_file_name(sc.FILE_PICKLE_DEM)
    try:
        with bz2.BZ2File(dem_file_name, 'rb') as file_pickle:
            dem_grid = pickle.load(file_pickle)
    except Exception as e:
        logging.info('Processing DEM file %s failed and will not be available - %s',
                     dem_file_name, str(e))
        return None
    logging.info('Read elevation for %i points took %f seconds', dem_grid.size, time.time() - start)
    logging.info('The memory size of the resulting 2D array is %i MiB plus some overhead',
                 (dem_grid.size * dem_grid.itemsize) // 1000000)
    dem = DigitalElevationModel(dem_grid, scenario.dem_south_west, scenario.dem_north_east,
                                scenario.lon_dem_grid_size, scenario.lat_dem_grid_size)
    return dem


if __name__ == '__main__':
    u.read_dotenv()

    from osm2city import parameters
    parser = argparse.ArgumentParser(description="Create the DEM for Hunter from FG scenery for a specific scenario.")
    parser.add_argument("-f", "--file", dest="filename",
                        help="read parameters from FILE (e.g. params.py)", metavar="FILE", required=True)
    parser.add_argument('-s', dest='scenario', type=str,
                        help='the scenario to use',
                        required=True)
    parser.add_argument('-d', dest='scenario_path', type=str,
                        help='the path to the directory where the scenarios live',
                        required=True)
    logging.basicConfig(level=logging.INFO)
    args = parser.parse_args()
    parameters.read_from_file(args.filename)
    parameters.FG_ELEV_CACHE = False

    _read_scenario_and_write_out(args.scenario_path, args.scenario)
