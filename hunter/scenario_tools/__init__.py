"""Contains scripts to generate data resources for Hunter based on OpenStreetMap.

Often these scripts are based on osm2city (explicit dependency) and are only run once to prepare scenarios;
i.e. they are not run as part of the multiplayer processing of a scenario.
"""
