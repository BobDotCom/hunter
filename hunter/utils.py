"""Utility functions and constants shared between controller and worker."""
import abc
from dataclasses import dataclass
import datetime
from enum import IntEnum, unique
import hashlib
import logging
import logging.config
import multiprocessing as mp
import os
import os.path as osp
import queue
import sys
import threading
import time
from typing import Any, Optional


CTRL_CALLSIGN = 'xHunter'  # max 6 letters

MP_SERVER_HOST_DEFAULT = 'mpserver.opredflag.com'
PORT_TELNET_BASIS = 10000  # basic port for Telnet networking
PORT_HTTPD_BASIS = 9000  # basic port
PORT_MP_BASIS = 5000  # basic port for multiplayer networking

# Reserved positions in controller -> 5000 and 5001 can be used for local FG instance in parallel
PORT_MP_BASIS_CLIENT_CTRL = 5000  # basic port for multiplayer networking
INDEX_CONTROLLER = 2
INDEX_AWACS = INDEX_CONTROLLER + 1
INDEX_TANKER = INDEX_AWACS + 1
INDEX_DYNAMIC_START = INDEX_TANKER + 1
# Client ports in Worker are significantly higher to allow Controller's MP targets on same machine
PORT_MP_BASIS_CLIENT_WORKER = 5150

# Update frequencies
MP_TIMESTAMP_IGNORE_MAXDIFF = 10  # seconds (as specified in FGMS packets)

WORKER_POLLING_REGISTRATION_REQUEST = 10
WORKER_CTRL_POLLING = 10
GCP_MAX_WAIT_FOR_CTRL_REQUEST = 600  # seconds

MAX_DELTA_SHOOTABLE = 5  # seconds since last time MP data for this aircraft (attacker) was processed
POS_UPDATE_FREQ_ATTACKER = 1
POS_UPDATE_FREQ_FG_TARGET = 2  # must be much lower than ..._UPDATES_TO_DS
POS_UPDATE_FREQ_MOVING_TARGET = 5  # every x seconds an update for moving mp_targets, trip mp_targets
POS_UPDATE_FREQ_STATIC_TARGET = 5
POS_UPDATE_FREQ_ZOMBIE_TIME = 15  # max time we consider ok that something is not attached to network
SEND_POSITION_UPDATES_TO_DS = 5  # every x seconds send an update for the position and health to datastore

# mp properties hardcoded instead of efgms.FGMS_prop_code_by_name for performance and to get rid of dependency
# See http://wiki.flightgear.org/Multiplayer_protocol#Properties_Packet
MP_PROP_CHAT = 10002  # '/sim/multiplay/chat'
MP_PROP_LIVERY = 1101  # sim/model/livery/file
MP_PROP_TANKER = 1300
MP_PROP_GEAR_0_POSITION_NORM = 201
MP_PROP_GEAR_1_POSITION_NORM = 211
MP_PROP_GEAR_2_POSITION_NORM = 221
MP_PROP_ENGINE_0_N1 = 300
MP_PROP_ENGINE_0_N2 = 301
MP_PROP_ENGINE_0_RPM = 302
MP_PROP_ENGINE_1_N2 = 311
# MP_PROP_ENGINE_1_RPM = 302
MP_PROP_ROTOR_MAIN = 800
MP_PROP_ROTOR_TAIL = 801
MP_PROP_GENERIC_BOOL_BASE = 11000  # sim/multiplay/generic/bool[0]
MP_PROP_GENERIC_STRING_BASE = 10100  # sim/multiplay/generic/string[0]
MP_PROP_GENERIC_FLOAT_BASE = 10200  # sim/multiplay/generic/float[0]
MP_PROP_GENERIC_INT_BASE = 10300  # sim/multiplay/generic/int[0]
MP_PROP_OPRF_SMOKE = MP_PROP_GENERIC_INT_BASE  # /sim/multiplay/generic/int[0]
MP_PROP_OPRF_RADAR = MP_PROP_GENERIC_INT_BASE + 2  # int[2] value 1 means silent, 0 means active (yes, so it is)
# the radar lock is the first 4 characters of an MD5(callsign) - see e.g. method create_radar_hash_for_callsign(...)
MP_PROP_OPRF_RADAR_LOCK = MP_PROP_GENERIC_STRING_BASE + 6
MP_PROP_OPRF_SELECT = MP_PROP_GENERIC_INT_BASE + 17  # sim/multiplay/generic/int[17] - for depot, groundtarget, hunter

# rotors/main/blade[3]/flap-deg" - if value != 0 and value is different from previous one, then we have a new flare
MP_PROP_OPRF_FLARES = 823  # rotors/main/blade[3]/flap-deg
MP_PROP_OPRF_CHAFFS = 813  # rotors/main/blade[3]/position-deg

MP_PROP_OPFOR_IFF_HASH = MP_PROP_GENERIC_STRING_BASE + 4
MP_PROP_OPFOR_DATALINK = MP_PROP_GENERIC_STRING_BASE + 7  # not the channel, but for transmitting the data

MP_PROP_EMESARY_BRIDGE_BASE = 12000

MP_PROP_FGUK_CRASHED = MP_PROP_GENERIC_INT_BASE + 7
MP_PROP_FGUK_BEACON = MP_PROP_GENERIC_INT_BASE + 1
MP_PROP_FGUK_STROBE = MP_PROP_GENERIC_INT_BASE + 3


# added altitude angle to measured altitude angle to compensate for earth curvature and other disturbing stuff,
# such that a very low flying target cannot be detected very far out
# 0.06 degrees is ca. 1 metres per 1000 m distance, 1 degree is ca. 17 metres.
# Earth curvature is 2 metres at 5 km and 8 metres at 10 km.
EXTRA_ALTITUDE = 0.5  # degrees (above ground)


def create_radar_hash_for_callsign(callsign: str) -> str:
    hashed = hashlib.md5(callsign.encode()).hexdigest()
    return hashed[:4]


def get_mp_server_address() -> str:
    """Reads the environment variable based on a name.
    If it is not set, then exit.
    """
    my_mp_server = os.getenv('HUNTER_MP_SERVER')
    if my_mp_server is None or len(my_mp_server) <= 1:  # it is set to "x" in the Dockerfile
        return MP_SERVER_HOST_DEFAULT
    if 'flightgear.org' in my_mp_server:
        logging.error('mp_server may not be one of the FlightGear default MP servers: %s', my_mp_server)
        sys.exit(1)
    logging.debug('mp_server is set to value %s', my_mp_server)
    return my_mp_server


def get_hunter_directory() -> str:
    """Determines the absolute path of the hunter root directory."""
    my_file = osp.realpath(__file__)
    my_dir = osp.split(my_file)[0]  # now we are in the hunter/hunter directory
    my_dir = osp.split(my_dir)[0]  # now we are in the root hunter directory
    return my_dir


SCENARIO_RESOURCES_DIR = 'scenario_resources'


def get_scenario_resources_dir() -> str:
    return osp.join(get_hunter_directory(), SCENARIO_RESOURCES_DIR)


def date_time_now() -> str:
    """Date and time as of now formatted as a string incl. seconds."""
    today = datetime.datetime.now()
    return today.strftime("%Y-%m-%d_%H%M%S")


class RuntimeFormatter(logging.Formatter):
    """A logging formatter which includes the delta time since start.

    Cf. https://stackoverflow.com/questions/25194864/python-logging-time-since-start-of-program
    """
    def __init__(self, *the_args, **kwargs) -> None:
        super().__init__(*the_args, **kwargs)
        self.start_time = time.time()

    def formatTime(self, record, datefmt=None):
        duration = datetime.datetime.utcfromtimestamp(record.created - self.start_time)
        elapsed = duration.strftime('%H:%M:%S')
        return "{}".format(elapsed)


@dataclass
class FGInstances:
    num_automats: int
    num_carriers: int
    num_missile_ships: int
    num_missile_sams: int

    def total_instances(self) -> int:
        return self.num_automats + self.num_carriers + self.num_missile_ships + self.num_missile_sams

    def has_no_sams(self) -> bool:
        return self.num_missile_sams == 0


def parse_fg_instances(max_instances: str) -> FGInstances:
    using = 'Max instances must have exactly 4 ints separated by "_" (e.g. 0_1_4_2)'
    split = max_instances.split('_')
    if len(split) != 4:
        raise ValueError(using)
    try:
        num_automats = int(split[0])
        num_carriers = int(split[1])
        num_ships = int(split[2])
        num_sams = int(split[3])
    except ValueError:
        raise ValueError(using)
    return FGInstances(num_automats, num_carriers, num_ships, num_sams)


@dataclass(frozen=True)
class Hostility:
    heli_shoot: bool
    drone_shoot: bool
    ship_shoot: bool
    shilka_shoot: bool
    sam_shoot: bool

    @property
    def is_hostile(self) -> bool:
        return self.heli_shoot or self.drone_shoot or self.ship_shoot or self.shilka_shoot or self.sam_shoot


def parse_hostility(hostility: str) -> Hostility:
    using = 'Hostility must have exactly 5 ints separated by "_" (e.g. 0_1_4_2)'
    split = hostility.split('_')
    if len(split) != 5:
        raise ValueError(using)
    try:
        heli_shoot = int(split[0]) > 0
        drone_shoot = int(split[1]) > 0
        ship_shoot = int(split[2]) > 0
        shilka_shoot = int(split[3]) > 0
        sam_shoot = int(split[4]) > 0
    except ValueError:
        raise ValueError(using)
    return Hostility(heli_shoot, drone_shoot, ship_shoot, shilka_shoot, sam_shoot)


def configure_logging(log_level: str, log_to_file: bool, main_process_name: str, gcp_logging: bool = False) -> None:
    """Set the logging level and maybe write to file.

    See also accepted answer to https://stackoverflow.com/questions/29015958/how-can-i-prevent-the-inheritance-
    of-python-loggers-and-handlers-during-multipro?noredirect=1&lq=1.
    And: https://docs.python.org/3.5/howto/logging-cookbook.html#logging-to-a-single-file-from-multiple-processes
    """
    if gcp_logging:
        print('Asked for configuring GCP Logging')
        import hunter.gcp_utils as gu
        success = gu.configure_gcp_logging(log_level, main_process_name)
        if success:
            return
        # else we just fall back to native logging

    log_format = '%(processName)-10s %(threadName)s -- %(asctime)s - %(levelname)-9s: %(message)s'
    console_handler = logging.StreamHandler()
    fmt = RuntimeFormatter(log_format)
    console_handler.setFormatter(fmt)
    logging.getLogger().addHandler(console_handler)
    logging.getLogger().setLevel(log_level)
    if log_to_file:
        process_name = mp.current_process().name
        if process_name == 'MainProcess':
            file_name = '{}_main_{}.log'.format(main_process_name, date_time_now())
        else:
            file_name = '{}_process_{}_{}.log'.format(main_process_name, process_name, date_time_now())
        file_handler = logging.FileHandler(filename=file_name)
        file_handler.setFormatter(fmt)
        logging.getLogger().addHandler(file_handler)


def create_prop_for_string(key: str, value: str) -> str:
    return '--prop:string:/{}={}'.format(key, value)


def create_prop_for_int(key: str, value: int) -> str:
    return '--prop:int:/{}={}'.format(key, str(value))


def create_prop_for_double(key: str, value: float) -> str:
    return '--prop:double:/{}={}'.format(key, str(value))


def create_prop_for_float(key: str, value: float) -> str:
    return '--prop:float:/{}={}'.format(key, str(value))


def create_prop_for_bool(key: str, value: bool) -> str:
    return '--prop:bool:/{}={}'.format(key, str(value).lower())


def create_session_id() -> str:
    """The session id exchanged between controller and workers to makes sure they are in the same session.
    Nanoseconds since epoch should be pretty unique. Str due to PuBSub message attributes."""
    return str(time.time_ns())


@unique
class CloudIntegrationLevel(IntEnum):
    """The hierarchical level of cloud integration"""
    none = 0
    datastore = 1  # only store information in datastore
    pubsub = 2  # allow interaction with workers etc. through messaging


def parse_cloud_integration_level_as_int(int_level: int) -> CloudIntegrationLevel:
    for level in CloudIntegrationLevel:
        if int_level == level.value:
            return level
    raise ValueError('{} is not a recognized cloud integration level'.format(int_level))


def use_datastore(level: CloudIntegrationLevel) -> bool:
    return level is not CloudIntegrationLevel.none


def use_pubsub(level: CloudIntegrationLevel) -> bool:
    return level is CloudIntegrationLevel.pubsub


def read_dotenv() -> None:
    """Read the .env file through a wrapper to apply some defaults.

    Using override=True especially because LD_LIBRARY_PATH must be set to something enhanced.
    """
    from dotenv import load_dotenv
    load_dotenv(verbose=True, override=True)
    print_os_environ()


def print_os_environ() -> None:
    for k, v in os.environ.items():
        print(f'{k}={v}')


class ParticipantMessageInterface(metaclass=abc.ABCMeta):
    """Broker to handle messages (mostly as defined by messages.py).

    It uses a queueing mechanism, such that this can work in e.g. for multithreading or multiprocessing.
    The "client" here the single Controller interacting with "providers" - which here are
    FGMS Participants. These FGMS Participants used a specific implementation of this interface to send
    and receive messages to/from the Controller.
    NB: there is a 1:1 relationship. For each participant there is 1 implementation to interface with the Controller.

    See also https://realpython.com/python-interface/#using-abstract-method-declaration
    """
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'post_message') and
                callable(subclass.post_message) and
                hasattr(subclass, 'send_message_out') and
                callable(subclass.send_message_out) and
                hasattr(subclass, 'receive_message') and
                callable(subclass.receive_message) and
                hasattr(subclass, 'fetch_incoming_message') and
                callable(subclass.fetch_incoming_message) or
                NotImplemented)

    @abc.abstractmethod
    def post_message(self, message) -> None:
        """Allow the Controller to post a message to a participant."""
        raise NotImplementedError

    @abc.abstractmethod
    def send_message_out(self, message) -> None:
        """Allow the participant to send something out to the Controller."""
        raise NotImplementedError

    @abc.abstractmethod
    def receive_message(self) -> Optional[Any]:
        """Allow the Controller to get messages/results back from the participant.

        If there are no more messages in the queue, then None is returned.
        The caller must repeat calling this method until None is returned.
        """
        raise NotImplementedError

    @abc.abstractmethod
    def fetch_incoming_message(self) -> Optional[Any]:
        """Allow the participant to look for incoming messages from the Controller.

        If there are no more messages in the queue, then None is returned.
        The caller must repeat calling this method until None is returned.
        """
        raise NotImplementedError


class QueuedRunner(threading.Thread, ParticipantMessageInterface):
    """A thread basis class with incoming and outgoing queue, so the main process' run() method does not get halted.

    Adapted from chapter 'threaded Program Architecture' in chapter 14/page 420 of 'Python in a Nutshell'.
    """
    __slots__ = ('incoming_queue', 'outgoing_queue')

    def __init__(self, name: str) -> None:
        super().__init__(name=name, daemon=True)
        self.incoming_queue = queue.Queue()
        self.outgoing_queue = queue.Queue()

    def post_message(self, message) -> None:
        logging.debug('Controller posting a new message to a participant (threaded-mode): %s', message)
        self.incoming_queue.put_nowait(message)

    def send_message_out(self, message) -> None:
        logging.debug('Participant sending a new message out to the Controller (threaded-mode): %s', message)
        self.outgoing_queue.put_nowait(message)

    def receive_message(self) -> Optional[Any]:
        try:
            message = self.outgoing_queue.get_nowait()
            logging.debug('Controller is receiving a message from participant (threaded-mode): %s', message)
            return message
        except queue.Empty:
            pass  # nothing to do as expected to be empty once in a while
        return None

    def fetch_incoming_message(self) -> Optional[Any]:
        try:
            message = self.incoming_queue.get_nowait()
            logging.debug('Participant is receiving a message from the Controller (threaded-mode): %s', message)
            return message
        except queue.Empty:
            pass  # nothing to do as expected to be empty once in a while
        return None

    def run(self) -> None:
        raise NotImplementedError
