"""The message objects exchanged between the different process instances.

Typing is only done for standard library - not for other stuff in war-room in order to reduce module dependencies.
"""
import copy
from datetime import datetime as dt
from enum import IntEnum, unique
import logging
from typing import Any, Dict, Optional

import hunter.geometry as g
import hunter.emesary_notifications as en


@unique
class HealthType(IntEnum):
    """Type of damage an MPTarget has over time.
    The values below 10 are returned to attacker for statistics."""
    missed = -1  # not used for health, but returned if attacker did not hit
    fit_for_fight = 0
    hit = 1
    broken = 2  # so much damage that no movement and self-defense is possible
    destroyed = 3  # it is burning, falling from the sky etc.
    dead = 4  # should be removed from MP


def is_capable_of_fighting_health(health: HealthType) -> bool:
    return health in [HealthType.fit_for_fight, HealthType.hit]


def map_health_type(value: int, is_damage: bool) -> HealthType:
    for type_ in HealthType:
        if type_.value == value:
            return type_
    logging.warning('Possible programming error: could not map value = %i to HealthType (damage = %s)',
                    value, is_damage)
    if is_damage:
        return HealthType.missed
    else:
        return HealthType.fit_for_fight


@unique
class WeaponType(IntEnum):
    cannon = 0
    rocket = 1
    unguided_ground = 10
    guided_ground = 11
    cluster_ground = 12
    # unguided_air = 20 - not used yet (maybe never, because rockets and bullets are only unguided used in air)
    guided_air = 21
    unknown = 30


def map_weapon_type(value: int) -> WeaponType:
    for type_ in WeaponType:
        if type_.value == value:
            return type_
    logging.warning('Possible programming error: could not map value = %i to WeaponType', value)
    return WeaponType.unknown


class ExitSession:
    """Signal between CLI and Controller to exit the session."""
    def __str__(self):
        return 'Exit session'


class TearDownTarget:
    """Signal from Controller to target to tear itself down and release IO resources."""
    def __str__(self):
        return 'Tear down target'


class RemoveDeadTarget:
    """Signal from target to Controller that it should be removed"""
    __slots__ = ('callsign', 'error_situation', 'is_fg_target')

    def __init__(self, callsign: str, error_situation: bool, is_fg_target: bool = False) -> None:
        self.callsign = callsign
        self.error_situation = error_situation  # to distinguish whether the target is dead by logic or exception
        self.is_fg_target = is_fg_target  # if this is an FGFS instance

    def __str__(self):
        reason = ' according to logic'
        if self.error_situation:
            reason = ' due to exception situation'
        if self.is_fg_target:
            return 'Remove FGFS instance ' + self.callsign + reason
        return 'Remove dead target ' + self.callsign + reason


class ReceivedFGMSData:
    """Received byte data from socket connection to FGMS: received by Receiver and forwarded to Controller."""
    __slots__ = ('udp_package',)

    def __init__(self, udp_data) -> None:
        self.udp_package = udp_data

    def __str__(self):
        return 'Received raw data from FGMS'


class FGTargetDestroyed:
    """Sent from Runner to worker to signal that an FG instance is now destroyed due to damage (not dead)."""
    __slots__ = 'callsign'

    def __init__(self, callsign: str) -> None:
        self.callsign = callsign

    def __str__(self):
        return 'FGTarget {} is destroyed'.format(self.callsign)


class DamageResult:
    """Received by Controller from target to indicate the result of shooting at the target"""
    __slots__ = ('damage_time', 'health', 'weapon_type', 'attacker', 'attacker_kind', 'target', 'target_kind',
                 'civilian', 'fg_target', 'shooting')

    def __init__(self, health: HealthType, weapon_type: WeaponType, attacker: str, attacker_kind: str,
                 target: str, target_kind: str,
                 civilian: bool = False, fg_target: bool = False, shooting: bool = False) -> None:
        # comply with GCP datastore date/time props:
        # https://cloud.google.com/datastore/docs/concepts/entities#properties_and_value_types
        # https://googleapis.dev/python/datastore/latest/entities.html
        self.damage_time = dt.utcnow()
        self.health = health
        self.weapon_type = weapon_type
        self.attacker = attacker  # callsign of the attacker
        self.attacker_kind = attacker_kind
        self.target = target  # callsign of the target
        self.target_kind = target_kind  # basically a type
        self.civilian = civilian
        self.fg_target = fg_target  # mp_target is False, else fg_target
        self.shooting = shooting
        if not self.check_send_result(health):
            raise ValueError('DamageResult needs to be hit, broken or destroyed. {}'.format(self))

    def copy_with_health(self, new_health: HealthType) -> 'DamageResult':
        new_damage = copy.deepcopy(self)
        new_damage.health = new_health
        return new_damage

    @staticmethod
    def check_send_result(health_result: HealthType) -> bool:
        """Checks whether the damage result should be sent."""
        return health_result in (HealthType.hit, HealthType.broken, HealthType.destroyed)

    def _stringify_civilian(self) -> str:
        civilian_str = 'military'
        if self.civilian:
            civilian_str = 'civilian '
        return civilian_str

    def __str__(self):
        target_str = 'FG target' if self.fg_target else 'MP target'
        return 'Attacker {} reduced health of {} {}/{} to {} at {}: {}'.format(self.attacker,
                                                                               self._stringify_civilian(),
                                                                               self.target, self.target_kind,
                                                                               self.health.name,
                                                                               self.damage_time.strftime(
                                                                                   '%d %b %Y %H:%M:%S +0000'),
                                                                               target_str)


class HitNotification:
    """The ArmamentNotification from the attacker to the target before damage calculation through Emesary.
    The result will be sent as a DamageResult message.
    """
    __slots__ = ('notification', 'attacker_callsign', 'attacker_aircraft', 'target')

    def __init__(self, notification: en.ArmamentNotification, attacker_callsign: str, attacker_aircraft: str,
                 target: str) -> None:
        self.notification = notification
        self.attacker_callsign = attacker_callsign
        self.attacker_aircraft = attacker_aircraft
        self.target = target  # callsign of the target

    def __str__(self):
        return 'Hit notification from attacker {} using {} to target {}: {}'.format(self.attacker_callsign,
                                                                                    self.attacker_aircraft,
                                                                                    self.target, self.notification)


class AddMovingTarget:
    __slots__ = ('asset', 'path', 'start_node')

    def __init__(self, asset, path, start_node) -> None:
        self.asset = asset
        self.path = path  # nx.Graph
        self.start_node = start_node

    def __str__(self):
        return 'Add moving target {}'.format(self.asset.name)


class AddFGFSAircraft:
    __slots__ = ('aircraft', 'airport', 'target_alt_ft')

    def __init__(self, aircraft, airport, target_alt_ft: int) -> None:
        self.aircraft = aircraft  # wa.Aircraft
        self.airport = airport  # app.Airport
        self.target_alt_ft = target_alt_ft

    def __str__(self):
        return 'Add FGFS aircraft {} departing at {} with target altitude in ft {}'.format(self.aircraft.aero,
                                                                                           self.airport.icao,
                                                                                           self.target_alt_ft)


class AddFGFSCarrier:
    __slots__ = ('carrier',)

    def __init__(self, carrier) -> None:
        self.carrier = carrier  # wa.Carrier

    def __str__(self):
        return 'Add FGFS carrier {}'.format(self.carrier.aero)


class AttackerPosition:
    """The position of an attacker as read directly from the receiver - to be sent to a target shooter."""
    __slots__ = ('callsign', 'position', 'heading', 'pitch', 'radar_lock', 'radar_on', 'chaff_on', 'flares_on',
                 'timestamp', 'iff_hash')

    def __init__(self, callsign: str, position, heading: float, pitch: float,
                 radar_lock: Optional[str], radar_on: bool, chaff_on: bool, flares_on: bool,
                 timestamp: float, iff_hash: Optional[str]) -> None:
        self.callsign = callsign
        self.position = position
        self.heading = heading
        self.pitch = pitch
        self.radar_lock = radar_lock
        self.radar_on = radar_on
        self.chaff_on = chaff_on
        self.flares_on = flares_on
        # the original timestamp from MP - might be different from time.time() - incl. due to lag
        # therefore only suitable for relative calculations - diffs
        self.timestamp = timestamp
        self.iff_hash = iff_hash


class PositionUpdate:
    """Objects send their position once in a while, such that controller can display or assign targets."""
    __slots__ = ('callsign', 'kind', 'position', 'heading', 'pitch', 'speed', 'health')

    def __init__(self, callsign: str, kind: str, health: HealthType, position,  # g.Position
                 heading: float = -.01, pitch: float = -0.1, speed: float = -.01) -> None:
        self.callsign = callsign
        self.kind = kind
        self.health = health
        self.position = position
        self.heading = heading  # true heading
        self.pitch = pitch  # 0 is horizontal, negative is down, never exceeds 90 degs
        self.speed = speed  # not necessarily airspeed. If < 0 then not known or not relevant

    @property
    def unknown_speed(self) -> bool:
        return self.speed < 0.

    @property
    def unknown_heading(self) -> bool:
        return self.heading < 0.

    @property
    def unknown_pitch(self) -> bool:
        return self.pitch < 0.

    def __str__(self):
        return '{} \'s health is {}: has position: {} with heading: {} and speed: {}'.format(self.callsign, self.health,
                                                                                             self.position,
                                                                                             self.heading, self.speed)


class PositionUpdatesBatch:
    """A list of PositionUpdate messages to be processed in one batch."""
    __slots__ = 'position_updates'

    def __init__(self) -> None:
        self.position_updates = list()

    def add_position_update(self, position_update: PositionUpdate) -> None:
        self.position_updates.append(position_update)

    def __str__(self):
        return 'Position updates for {} items'.format(len(self.position_updates))


@unique
class GCIResponseType(IntEnum):
    all_sent = 0  # only used for picture
    no_info_avail = 1
    picture = 2  # returns 1 nearest ship instead of up to 10 full tactical picture
    bogey_dope = 3  # BRAA to vehicle instead of
    cutoff = 4  # to helicopter
    # popup is currently not used


MESSAGE_ID = 0


def _get_unique_message_id() -> int:
    global MESSAGE_ID
    MESSAGE_ID += 1
    return MESSAGE_ID


class GCIResponse:
    __slots__ = ('req_callsign', 'response_type', 'message_id', '_bearing', '_range_m', '_altitude_ft',
                 '_aspect', '_vector', '_eta_s')

    def __init__(self, response_type: GCIResponseType, req_callsign: str) -> None:
        self.req_callsign = req_callsign
        self.response_type = response_type
        self.message_id = _get_unique_message_id()
        self._bearing = 0
        self._range_m = 0
        self._altitude_ft = 0
        self._aspect = 0
        self._vector = 0
        self._eta_s = 0  # seconds

    def set_altitude_m(self, altitude_m) -> None:
        """Feet is used per default. change to m for metric aircraft is done in aircraft"""
        self._altitude_ft = int(g.metres_to_feet(altitude_m))

    def set_bearing(self, magnetic_bearing: float) -> None:
        self._bearing = int(magnetic_bearing)

    def set_range(self, range_m: float) -> None:
        self._range_m = int(range_m)

    def set_aspect(self, aspect: float) -> None:
        self._aspect = int(aspect)

    def set_vector(self, magnetic_vector: float) -> None:
        self._vector = int(magnetic_vector)

    def set_eta_s(self, eta_s: float) -> None:
        self._eta_s = int(eta_s)

    def __str__(self):
        """Formatted to be directly included in MP property.
        ALL SENT: requestor-callsign:unique-message-id:0:d:d:d:d
        NO INFO: requestor-callsign:unique-message-id:1:n:n:n:n
        PICTURE: requestor-callsign:unique-message-id:2:bearing:range:altitude:[BLUFOR=0|OPFOR=1] -> always 1 for OPFOR
        BOGEY DOPE: requestor-callsign:unique-message-id:3:bearing:range:altitude:aspect
        CUTOFF: requestor-callsign:unique-message-id:4:vector-heading:time:altitude:aspect
        """
        response = '{}:{}:'.format(self.req_callsign, self.message_id)
        if self.response_type is GCIResponseType.all_sent:
            response += '0:d:d:d:d'
        elif self.response_type is GCIResponseType.picture:
            response += '2:{}:{}:{}:1'.format(self._bearing, self._range_m, self._altitude_ft)
        elif self.response_type is GCIResponseType.bogey_dope:
            response += '3:{}:{}:{}:{}'.format(self._bearing, self._range_m, self._altitude_ft, self._aspect)
        elif self.response_type is GCIResponseType.cutoff:
            response += '4:{}:{}:{}:{}'.format(self._vector, self._eta_s, self._altitude_ft, self._aspect)
        else:  # self.response_type is GCIResponseType.no_info_avail:
            response += '1:n:n:n:n'
        return response


class DatalinkData:
    __slots__ = 'encoded_data'

    def __init__(self, encoded_data: bytes) -> None:
        self.encoded_data = encoded_data


class GCPSubData:
    """A data structure to move data from GCP pub_sub to Controller.

    We know it is a dictionary, because that is what we are using in gcp_ps_io.py to encode."""
    __slots__ = ('content_dict', 'worker_id')

    def __init__(self, content_dict: Dict[str, Any], worker_id: Optional[str]) -> None:
        self.content_dict = content_dict
        self.worker_id = worker_id

    def __str__(self):
        return 'Worker {} sent GCPSubData: {}'.format(self.worker_id, str(self.content_dict))


class HeartbeatReceiver:
    """A heartbeat from the receiver to check whether it is still working"""

    def __init__(self) -> None:
        pass

    def __str__(self):
        return 'Heartbeat for Receiver'
