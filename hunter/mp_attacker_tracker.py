"""Provides common code for mp_shooters and mp_dispensers for assessing attackers."""

from abc import ABC, abstractmethod

from typing import Dict, List, Optional

import hunter.geometry as g
import hunter.emesary_notifications as en
import hunter.iff_datalink as ifd
import hunter.messages as m
import hunter.utils as u


class AttackerPositionHistory:
    """Stores positions for an attacker aircraft over time in LIFO principle."""
    __slots__ = ('max_length', 'historic_positions')

    def __init__(self, max_length: int) -> None:
        self.max_length = max_length  # how much history is kept
        self.historic_positions = list()

    def add_position(self, newest_position: m.AttackerPosition):
        if len(self.historic_positions) > self.max_length:
            del self.historic_positions[0]
        self.historic_positions.append(newest_position)

    @property
    def latest_position(self) -> g.Position:
        return self.historic_positions[-1].position

    @property
    def latest_attacker_position(self) -> m.AttackerPosition:
        return self.historic_positions[-1]

    def guess_next_position(self) -> g.Position:
        """Very primitive guess of future position based on last 2 positions"""
        if len(self.historic_positions) == 1:
            return self.historic_positions[-1].position
        else:
            orientation = g.calc_orientation(self.historic_positions[-2].position, self.historic_positions[-1].position)
            dist = g.calc_distance_pos_3d(self.historic_positions[-2].position, self.historic_positions[-1].position)
            return g.calc_destination_pos_3d(self.historic_positions[-1].position, dist,
                                             orientation.hdg, orientation.pitch)

    def calc_speed(self) -> Optional[float]:
        """The calculated speed based on the last 2 positions and a time_diff of the updates.
        NB: the time_diff is not the same as send_freq, because it depends on the rate of MP packages
        received from the attacker.

        If the speed cannot be calculated, then None is returned
        """
        if len(self.historic_positions) <= 1:
            return None
        pos_prev = self.historic_positions[-2].position
        pos_last = self.historic_positions[-1].position
        distance = g.calc_distance_pos_3d(pos_prev, pos_last)
        return distance / (self.historic_positions[-1].timestamp - self.historic_positions[-2].timestamp)


TOO_SLOW_ATTACKER = 15  # m/s - slower than this and we must assume a parked or taxiing attacker


class AttackerTracker(ABC):
    __slots__ = ('parent', 'attackers_hist_positions',)

    def __init__(self, parent) -> None:  # mpt.MPTarget not imported due to potential circular reference
        self.parent = parent
        self.attackers_hist_positions = dict()  # key = callsign, value = g.AttackerPositionHistory

    def process_attacker_info(self, attacker_positions: Dict[str, m.AttackerPosition],
                              missiles_in_flight: List[en.ArmamentInFlightNotification]):
        """Updates the internal structure of attacker position history and then processes the attackers.
        """
        for attacker_callsign, attacker_position in attacker_positions.items():
            iff_type = ifd.interrogate(attacker_callsign, attacker_position.iff_hash, self.parent.iff_code)
            if iff_type is ifd.IFFType.iff_friendly:
                continue
            if attacker_callsign in self.attackers_hist_positions:
                position_history = self.attackers_hist_positions[attacker_callsign]
            else:
                position_history = AttackerPositionHistory(u.MAX_DELTA_SHOOTABLE / self.parent.send_freq + 1)
                self.attackers_hist_positions[attacker_callsign] = position_history
            position_history.add_position(attacker_position)

        # if since last call of this method the attacker disappeared -> remove history and attacker
        # assumes that a well-behaving attacker sends data at least every self.parent.send_freq second (0.5)
        to_remove = list()
        for callsign in self.attackers_hist_positions.keys():
            if callsign not in attacker_positions:
                to_remove.append(callsign)
            else:  # if attacker is very slow, then it is not considered a threat - probably on an airport
                position_history = self.attackers_hist_positions[callsign]
                attacker_speed = position_history.calc_speed()
                if attacker_speed is not None and attacker_speed < TOO_SLOW_ATTACKER:
                    to_remove.append(callsign)

        for callsign in to_remove:
            del self.attackers_hist_positions[callsign]

        self._process_attackers(missiles_in_flight)

    @abstractmethod
    def _process_attackers(self, missiles_in_flight: List[en.ArmamentInFlightNotification]) -> None:
        pass
