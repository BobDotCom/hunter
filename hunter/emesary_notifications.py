"""See documentation in emesary.py

Migrated from
 * FGData/Nasal/notification.nas (commit 21675f, 2020-10-19)
 * f-16/Nasal/ArmamentNotification.nas (commit 0009c70fdab67c3057d261a3ebcacbfeb8907216, 2022-11-25)
"""
import logging
from typing import Dict, Union

import hunter.emesary as e
import hunter.geometry as g

GeoEventNotification_Id = 18
ArmamentNotification_Id = 19  # according to F14b. According to notification.nas it would be 22
ArmamentInFlightNotification_Id = 21
ObjectInFlightNotification_Id = 22  # currently used for chaff/flare
StaticNotification_Id = 25  # not yet implemented in Hunter from f-14b/Nasal/ArmamentNotification.nas

# Defined kinds. Called kind to avoid confusion with notification type (notifications.nas line 237 ff)
KIND_CREATED = 1
KIND_MOVED = 2
KIND_DELETED = 3
KIND_COLLISION = 4

# Based on GeoBridgedTransmitter.nas in OPRF planes
OPRF_BRIDGE_CHANNEL_INFLIGHT = 18
OPRF_BRIDGE_CHANNEL_HIT = 19  # weapon hit og craters
OPRF_BRIDGE_FLARES = 17  # flares

IDENT_MP_BRIDGE = 'mp-bridge'

# Secondary kind (8 bits)
# using the first 4 bits as the classification and the second 4 bits as the sub-classification
# All secondary kinds are defined in notifications.nas line 246 ff.
# Here only those secondary kinds get defined, which are needed in Hunter


class KindNotification(e.Notification):
    __slots__ = ('kind', 'secondary_kind', 'remote_callsign', 'is_distinct')

    def __init__(self, type_: str, notification_id: int, ident: str = 'none',
                 kind: int = 0, secondary_kind: int = 0, is_distinct: int = 0) -> None:
        super().__init__(type_, ident, notification_id)
        self.kind = kind  # the activity that the notification represents (KIND_CREATED etc.)
        self.secondary_kind = secondary_kind  # the entity on which the activity is being performed.
        self.remote_callsign = ''
        self.is_distinct = is_distinct


class GeoEventNotification(KindNotification):
    __slots__ = ('name', 'position', 'unique_index', 'flags')

    def __init__(self, ident: str = 'none', name: str = '', kind: int = 0, secondary_kind: int = 0) -> None:
        super().__init__(GeoEventNotification.__name__, GeoEventNotification_Id, ident,
                         kind, secondary_kind, 0)
        self.name = name  # name of the notification, bridged.
        self.position = None  # geometry.py -> Position
        self.unique_index = 0
        self.flags = 0

    @property
    def bridge_message_notification_key(self) -> str:
        return '{}.{}.{}'.format(self.notification_type, self.ident, self.unique_index)

    # TODO: bridgeProperties


class ArmamentNotification(KindNotification):
    """ArmamentNotification from f-14/Nasal/ArmamentNotification.nas.

    The decoding code from Nasal is in method process_incoming_message()

    Example from F-14 for cannon:
    Kind=4 SecondaryKind=20 RelativeAltitude=-76.299 Distance=2 Bearing=307.5 RemoteCallsign=Richard

    Missile F-14 fox2.nas:
                    var msg = notifications.ArmamentNotification.new("mhit", 4, 21+me.ID);
                    msg.RelativeAltitude = explosion_coord.alt() - me.t_coord.alt();
                    msg.Bearing = explosion_coord.course_to(me.t_coord);
                    msg.Distance = direct_dist_m;
                    msg.RemoteCallsign = me.callsign;
                    f14.hitBridgedTransmitter.NotifyAll(msg);
    Cannon F-14 weapons.nas:
                  var msg = notifications.ArmamentNotification.new("mhit", 4, 151+armament.shells[typeOrd][0]);
                  msg.RelativeAltitude = 0;
                  msg.Bearing = 0;
                  msg.Distance = hits_count;
                  msg.RemoteCallsign = hit_callsign;
                  f14.hitBridgedTransmitter.NotifyAll(msg);

    """
    __slots__ = ('relative_altitude', 'distance', 'bearing')

    def __init__(self, ident: str = 'none', kind: int = 0, secondary_kind: int = 0) -> None:
        super().__init__(ArmamentNotification.__name__, ArmamentNotification_Id, ident,
                         kind, secondary_kind, 0)
        self.relative_altitude = 0.
        self.distance = 0.
        self.bearing = 0.

    def __str__(self):
        string = 'ArmamentNotification: ident={}, kind={}, secondary kind={}'.format(self.ident, self.kind,
                                                                                     self.secondary_kind)
        string += ', distance={}, bearing={}'.format(self.distance, self.bearing)
        return string

    @classmethod
    def decode_from_message_body(cls, msg_body: bytes) -> 'ArmamentNotification':
        kind, pos = e.TransferByte.decode(msg_body, 0)
        secondary_kind, pos = e.TransferByte.decode(msg_body, pos)
        relative_altitude, pos = e.TransferFixedDouble.decode(msg_body, 2, 1/10, pos)
        distance, pos = e.TransferFixedDouble.decode(msg_body, 2, 1/10, pos)
        bearing, pos = e.TransferFixedDouble.decode(msg_body, 1, 1.54, pos)
        bearing = g.norm_deg_180(bearing)
        remote_callsign, _ = e.TransferString.decode(msg_body, pos)

        notification = ArmamentNotification(ident=IDENT_MP_BRIDGE, kind=kind, secondary_kind=secondary_kind)
        notification.distance = distance
        notification.bearing = bearing
        notification.remote_callsign = remote_callsign
        return notification

    def encode_to_message_body(self) -> bytes:
        message_body = b''
        message_body += e.TransferByte.encode(self.kind)
        message_body += e.TransferByte.encode(self.secondary_kind)
        message_body += e.TransferFixedDouble.encode(self.relative_altitude, 2, 1/10)
        message_body += e.TransferFixedDouble.encode(self.distance, 2, 1/10)
        message_body += e.TransferFixedDouble.encode(g.norm_deg_180(self.bearing), 1, 1.54)
        message_body += e.TransferString.encode(self.remote_callsign)
        return message_body


class ArmamentInFlightNotification(KindNotification):
    __slots__ = ('position', 'heading', 'pitch', 'u_fps', 'callsign',
                 'flags', 'unique_identity')

    def __init__(self, ident: str = 'none', unique_identity: int = 0, kind: int = 0, secondary_kind: int = 0) -> None:
        super().__init__(ArmamentInFlightNotification.__name__, ArmamentInFlightNotification_Id, ident,
                         kind, secondary_kind, 0)
        self.position = None  # geometry.Position
        self.heading = 360.
        self.pitch = 90.
        self.u_fps = 0.  # speed in feet per second
        self.callsign = None
        self.flags = 0

        # this id is unique for each fired missile - i.e. remains same across different InFlightMessages for the
        # same fired missile. What changes is the index of the message sent.
        self.unique_identity = unique_identity

    def __str__(self):
        string = 'ArmamentInFlightNotification: ident={}, kind={}, secondary kind={}'.format(self.ident, self.kind,
                                                                                             self.secondary_kind)
        string += ', unique_identity={}, u_fps={}, flags={}'.format(self.unique_identity, self.u_fps, self.flags)
        return string

    def set_oprf_flags(self, radar_on: bool, thrust_on: bool) -> None:
        self.flags = 0
        if radar_on:
            self.flags += 1  # bit at position 0
        if thrust_on:
            self.flags += 2  # bit at position 1

    @property
    def radar_on(self) -> bool:
        return self.flags == 1 or self.flags == 3

    @property
    def thrust_on(self) -> bool:
        return self.flags == 2 or self.flags == 3

    @classmethod
    def decode_from_message_body(cls, msg_body: bytes) -> 'ArmamentInFlightNotification':
        geo_position, pos = e.TransferCoord.decode(msg_body, 0)
        kind, pos = e.TransferByte.decode(msg_body, pos)
        secondary_kind, pos = e.TransferByte.decode(msg_body, pos)
        u_fps, pos = e.TransferFixedDouble.decode(msg_body, 1, 1./0.03703, pos)
        u_fps += 3348
        heading, pos = e.TransferFixedDouble.decode(msg_body, 1, 1.54, pos)
        heading = g.norm_deg_180(heading)
        pitch, pos = e.TransferFixedDouble.decode(msg_body, 1, 1./1.38, pos)
        remote_callsign, pos = e.TransferString.decode(msg_body, pos)
        flags, pos = e.TransferByte.decode(msg_body, pos)
        unique_identity, _ = e.TransferByte.decode(msg_body, pos)

        notification = ArmamentInFlightNotification(ident=IDENT_MP_BRIDGE, unique_identity=unique_identity,
                                                    kind=kind, secondary_kind=secondary_kind)
        notification.position = geo_position
        notification.u_fps = u_fps
        notification.heading = heading
        notification.pitch = pitch
        notification.remote_callsign = remote_callsign
        notification.flags = flags
        return notification

    def encode_to_message_body(self) -> bytes:
        message_body = b''
        message_body += e.TransferCoord.encode(self.position)
        message_body += e.TransferByte.encode(self.kind)
        message_body += e.TransferByte.encode(self.secondary_kind)
        message_body += e.TransferFixedDouble.encode(self.u_fps - 3348, 1, 1./0.03703)
        message_body += e.TransferFixedDouble.encode(g.norm_deg_180(self.heading), 1, 1.54)
        message_body += e.TransferFixedDouble.encode(self.pitch, 1, 1./1.38)
        message_body += e.TransferString.encode(self.remote_callsign)
        message_body += e.TransferByte.encode(self.flags)
        message_body += e.TransferByte.encode(self.unique_identity)
        return message_body


class ObjectInFlightNotification(KindNotification):
    """Currently in OPRF only used to visualise flares/chaffs.

    Uses kind=2 when releasing and alive falling in air, kind=3 when burned-out
    """
    __slots__ = ('position', 'callsign', 'unique_identity')

    def __init__(self, ident: str = 'none', unique_identity: int = 0, kind: int = 0, secondary_kind: int = 0) -> None:
        super().__init__(ObjectInFlightNotification.__name__, ObjectInFlightNotification_Id, ident,
                         kind, secondary_kind, 1)
        self.position = None  # geometry.Position
        self.callsign = None  # currently not set by attackers - instead overwritten by Controller
        self.unique_identity = unique_identity

    def __str__(self):
        string = 'ObjectInFlightNotification: ident={}, kind={}, secondary kind={}'.format(self.ident, self.kind,
                                                                                           self.secondary_kind)
        string += ', position={}, callsign={}, unique_id={}'.format(self.position, self.callsign, self.unique_identity)
        return string

    @classmethod
    def decode_from_message_body(cls, msg_body: bytes) -> 'ObjectInFlightNotification':
        geo_position, pos = e.TransferCoord.decode(msg_body, 0)
        kind, pos = e.TransferByte.decode(msg_body, pos)
        secondary_kind, pos = e.TransferByte.decode(msg_body, pos)
        unique_identity, _ = e.TransferByte.decode(msg_body, pos)

        notification = ObjectInFlightNotification(ident=IDENT_MP_BRIDGE, unique_identity=unique_identity,
                                                  kind=kind, secondary_kind=secondary_kind)
        notification.position = geo_position
        return notification

    def encode_to_message_body(self) -> bytes:
        message_body = b''
        message_body += e.TransferCoord.encode(self.position)
        message_body += e.TransferByte.encode(self.kind)
        message_body += e.TransferByte.encode(self.secondary_kind)
        message_body += e.TransferByte.encode(self.unique_identity)
        return message_body


# ========= Stuff from emesary_mp_bridge.nas ======================

# See Notes section in documentation at the top of emesary_mp_bridge.nas
SEPARATOR_CHAR = b'!'

MESSAGE_END_CHAR = b'~'


def process_incoming_message(encoded_val: bytes) -> Dict[str, Union[ArmamentNotification, ArmamentInFlightNotification,
                                                                    ObjectInFlightNotification]]:
    """Processes an incoming Emesary bridge base value and extracts the encoded ArmamentNotifications.

    Mimics new_class.ProcessIncoming = func(encoded_val) ...

    A dict is returned with the message index as key and the notification as value.
    0, 1 or n notifications might be included.

    According to Discord-radar-mafia, 30.5.:
    The index should be per notification type per bridge.
    It is managed by the bridge rather than the notification (to ensure that notifications remain bridge agnostic).


    """
    notifications = dict()
    if encoded_val == b'':
        return notifications
    # print('raw:', encoded_val)
    encoded_notifications = encoded_val.split(MESSAGE_END_CHAR)
    # print('array:', encoded_notifications)
    for encoded_notification in encoded_notifications:
        if encoded_notification == b'':  # typical at end of encoded_notifications list due to MESSAGE_END_CHAR
            continue
        # get the message parts
        encoded_notification_parts = encoded_notification.split(SEPARATOR_CHAR)
        # print('encoded parts:', encoded_notification_parts)
        if len(encoded_notification_parts) < 4:
            logging.warning('Error: emesary.IncomingBridge.ProcessIncoming bad msg %s', encoded_notification)
        else:
            msg_idx = e.BinaryAsciiTransfer.decode_int(encoded_notification_parts[1], 4, 0)[0]
            # print('msg_idx', msg_idx)
            msg_type_id = e.BinaryAsciiTransfer.decode_int(encoded_notification_parts[2], 1, 0)[0]
            # print('msg_type_id', msg_type_id)
            msg_body = encoded_notification_parts[3]
            # print('msg_body', msg_body)
            # the original code in Nasal does reflection here. We go with specific code due to clarity
            notification = None
            if msg_type_id == ArmamentNotification_Id:
                try:
                    notification = ArmamentNotification.decode_from_message_body(msg_body)
                except ValueError:
                    logging.exception('Unable to process body of ArmamentNotification.')
            elif msg_type_id == ArmamentInFlightNotification_Id:
                try:
                    notification = ArmamentInFlightNotification.decode_from_message_body(msg_body)
                except ValueError:
                    logging.exception('Unable to process body of ArmamentInfFlightNotification.')
            elif msg_type_id == ObjectInFlightNotification_Id:
                try:
                    notification = ObjectInFlightNotification.decode_from_message_body(msg_body)
                except ValueError:
                    logging.exception('Unable to process body of ObjectInFlightNotification.')
            if notification:
                # print('msg_idx', msg_idx)
                # print(notification)
                # using combination of msg_idx and type to make sure the key never overlaps
                notifications['{}_{}'.format(msg_idx, msg_type_id)] = notification
    return notifications


