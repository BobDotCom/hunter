"""Code to simulate the flight path of guided missiles in Hunter.
This is a primitive good enough simulation for SAM and Air-to-air
launched missiles, so it gives some realism - but not more.
E.g. wind is not simulated, the coordinate system is primitive.
Aerodynamic coefficients are approximated by experiments and used
in simplified formulas etc.

Inspiration:
* Original code contributed by Nikolai V. Chr. (https://gitlab.com/emptyyetfull) from
  a private repo. See at the git-history to see the first version.
  Simulates the way missiles and other weapons are guided and handled in OPRF
  Nasal code (see below) divided into horizontal and vertical 2-D space.
* Missile code in Nasal code cf. https://github.com/NikolaiVChr/OpRedFlag.
  When referring to missile-code.nas then it is the file in the OPRF F-16.
  Many variable / function names are kept as is from Nasal to ease comparison
  despite the fact that they might not comply to Python naming.
* OpenRocket: https://openrocket.info/

According to https://en.wikipedia.org/wiki/Missile a missile has five system
components: targeting, guidance system, flight system, engine, and warhead.

This module calculates the flight path and some steering limitations given different
forces (e.g. thrust and drag) as well as outside input of guidance.
Targeting and guidance is done in module mp_shooters.py.

There are a set of phases in flight:
* Launch
* Powered flight in stages
* Flight without power
The flight is finished either at crash into target or when it does
not matter any more (too slow, fall en earth).

FPS: feet per second (not frames per second)
SLUG: British gravitational system (equivalent of 24.59 kg / 32.17 lbs)
"""

from enum import IntEnum, unique
import math
from typing import List, Tuple
import unittest

import hunter.geometry as g

FT2M = 0.3048
M2FT = 1 / FT2M
NM2M = 1852
M2NM = 1 / NM2M
G_FPS = 9.80665 * M2FT
SLUG2LB = 32.1740485564  # https://en.wikipedia.org/wiki/Slug_(unit)
FT2NM = FT2M * M2NM
NM2FT = NM2M * M2FT
D2R = math.pi / 180
R2D = 180 / math.pi


def minus(a, b):
    return [a[0] - b[0], a[1] - b[1]]


def plus(a, b):
    return [a[0] + b[0], a[1] + b[1]]


def product(scalar, vector):
    return [scalar * vector[0], scalar * vector[1]]


def extrapolate(x, x1, x2, y1, y2):
    return y1 + ((x - x1) / (x2 - x1)) * (y2 - y1)


def clamp(v, min, max):
    return min if v < min else max if v > max else v


def dotProduct(a, b):
    return a[0] * b[0] + a[1] * b[1]


def magnitudeVector(a):
    return math.sqrt(math.pow(a[0], 2) + math.pow(a[1], 2))


def projVectorOnPlane(planeNormal, vector):
    return minus(vector, product(
        dotProduct(vector, planeNormal) / math.pow(magnitudeVector(planeNormal), 2),
        planeNormal))


def normalize(v):
    mag = magnitudeVector(v)
    return [v[0] / mag, v[1] / mag]


def angleBetweenVectors(a, b):
    a = normalize(a)
    b = normalize(b)
    value = clamp((dotProduct(a, b) / magnitudeVector(a)) / magnitudeVector(b), -1,
                  1)  # just to be safe in case some floating point error makes it out of bounds
    return R2D * math.acos(value)


def _calc_rho_speed_of_sound(altitude: float) -> Tuple[float, float]:
    if altitude < 36152:
        # curve fits for the troposphere
        temperature = 59 - 0.00356 * altitude
        p = 2116 * math.pow(((temperature + 459.7) / 518.6), 5.256)
    elif 36152 < altitude < 82345:
        # lower stratosphere
        temperature = -70
        p = 473.1 * math.pow(math.e, 1.73 - (0.000048 * altitude))
    else:
        # upper stratosphere
        temperature = -205.05 + (0.00164 * altitude)
        p = 51.97 * math.pow(((temperature + 459.7) / 389.98), -11.388)

    rho = p / (1718 * (temperature + 459.7))
    snd_speed = math.sqrt(1.4 * 1716 * (temperature + 459.7))
    return rho, snd_speed


def _calc_max_g_at_rho(rho: float, max_g_sea_level: float,
                       has_vector_thrust: bool, current_thrust_lbf: float) -> float:
    if has_vector_thrust and current_thrust_lbf == 0:
        max_g_sea_level = max_g_sea_level * 0.666
    return clamp(max_g_sea_level + ((rho - 0.0023769) / (0.00036159 - 0.0023769)) * (
                max_g_sea_level * 0.5909 - max_g_sea_level), 0.25, 100)


def _calc_drag_coefficient(mach: float, current_thrust_lbf: float,
                           cd_base: float, cd_plume: float) -> float:
    if mach < 0.7:
        cd_0 = (0.0125 * mach + 0.20) * 5 * cd_base
    elif mach < 1.2:
        cd_0 = (0.3742 * math.pow(mach, 2) - 0.252 * mach + 0.0021 + 0.2) * 5 * cd_base
    else:
        cd_0 = (0.2965 * math.pow(mach, -1.1506) + 0.2) * 5 * cd_base
    # TODO: implement advanced from original: https://gitlab.com/vanosten/hunter/-/issues/141
    cd_0 = cd_0 * cd_plume if current_thrust_lbf > 0 else cd_0
    return cd_0


def _calc_speed_loss_from_energy_bleed(delta_time: float, vector_thrust: bool,
                                       current_g: float, current_thrust_lbf,
                                       altitude_ft: float) -> float:
    """Bleed energy and thereby loose speed from pulling Gs.
    Is a negative number.
    """
    # if self.advanced:
    #    return 0  https://gitlab.com/vanosten/hunter/-/issues/141

    # First we get the speed loss due to normal drag:
    b300 = _bleed32800at0g(delta_time)
    b000 = _bleed0at0g(delta_time)

    # We then subtract the normal drag from the loss due to G and normal drag.
    b325 = _bleed32800at25g(delta_time) - b300
    b025 = _bleed0at25g(delta_time) - b000
    b300 = 0
    b000 = 0

    # We now find what the speed loss will be at sea level respectively at 32800 ft.
    speed_loss_32800 = b300 + ((current_g - 0) / (25 - 0)) * (b325 - b300)
    speed_loss_0 = b000 + ((current_g - 0) / (25 - 0)) * (b025 - b000)

    # Now extrapolate that to the current density-altitude.
    speed_loss = speed_loss_0 + ((altitude_ft - 0) / (32800 - 0)) * (speed_loss_32800 - speed_loss_0)
    #
    # For good measure the result is clamped to below zero.
    speed_loss = clamp(speed_loss, -100000, 0)
    if vector_thrust:
        speed_loss = speed_loss - speed_loss * 0.66 * (
           0 if current_thrust_lbf == 0 else 1)  # vector thrust will only bleed 1/3 of the calculated loss.
    return speed_loss


def _calc_generic_bleed_for_altitude_g(factor_1: float, factor_2: int, delta_time: float) -> float:
    loss_fps = 0 + ((delta_time - 0) / (factor_1 - 0)) * (factor_2 - 0)
    return loss_fps * M2FT


def _bleed32800at0g(delta_time: float) -> float:
    return _calc_generic_bleed_for_altitude_g(15., -330, delta_time)


def _bleed32800at25g(delta_time: float) -> float:
    return _calc_generic_bleed_for_altitude_g(3.5, -240, delta_time)


def _bleed0at0g(delta_time: float) -> float:
    return _calc_generic_bleed_for_altitude_g(22., -950, delta_time)


def _bleed0at25g(delta_time: float) -> float:
    return _calc_generic_bleed_for_altitude_g(7., -750, delta_time)


def _calc_angular_speed_g(delta_time: float, delta_heading, delta_pitch,
                          current_speed_fps: float) -> float:
    """The g-force due to angular acceleration.
    https://en.wikipedia.org/wiki/Angular_acceleration
    https://en.wikipedia.org/wiki/Centrifugal_force
    """
    return 0.  # FIXME (in original: def steering_speed_G


def machToFPS(mach, alt):
    rs = _calc_rho_speed_of_sound(alt)
    sound_fps = rs[1]
    return mach * sound_fps


def scalarProj(head, pitch, magn, projHead, projPitch):
    head = head * D2R
    pitch = pitch * D2R
    projHead = projHead * D2R
    projPitch = projPitch * D2R

    # Convert the 2 polar vectors to cartesian:

    # velocity vector of target
    ax = magn * math.cos(pitch) * math.cos(-head)  # north
    ay = magn * math.cos(pitch) * math.sin(-head)  # west
    az = magn * math.sin(pitch)  # up

    # vector pointing from target perpendicular to LOS
    bx = 1 * math.cos(projPitch) * math.cos(-projHead)  # north
    by = 1 * math.cos(projPitch) * math.sin(-projHead)  # west
    bz = 1 * math.sin(projPitch)  # up

    # the dot product is the scalar projection. And represent the target velocity perpendicular to LOS
    result = (ax * bx + ay * by + az * bz) / 1
    return result


def steering_speed_G(steering_e_deg, steering_h_deg, s_fps, dt):
    # Get G number from steering (e, h) in deg, speed in ft/s.
    steer_deg = math.sqrt((steering_e_deg * steering_e_deg) + (steering_h_deg * steering_h_deg))

    # next speed vector
    vector_next_x = math.cos(steer_deg * D2R) * s_fps
    vector_next_y = math.sin(steer_deg * D2R) * s_fps

    # present speed vector
    vector_now_x = s_fps
    vector_now_y = 0

    # subtract the vectors from each other
    dv = math.sqrt((vector_now_x - vector_next_x) * (vector_now_x - vector_next_x) + (
            vector_now_y - vector_next_y) * (vector_now_y - vector_next_y))

    # calculate g-force
    # dv/dt=a
    g = (dv / dt) / G_FPS

    return g


def max_G_Rotation(steering_e_deg, steering_h_deg, s_fps, dt, gMax):
    guess = 1
    coef = 1
    lastgoodguess = 1

    for i in range(1, 25):
        coef = coef / 2
        new_g = steering_speed_G(steering_e_deg * guess, steering_h_deg * guess, s_fps, dt)
        if new_g < gMax:
            lastgoodguess = guess
            guess = guess + coef
        else:
            guess = guess - coef
    return lastgoodguess


class HunterMissile:
    """A missile suitable for simulation in Hunter.

    The missile can have up to three stages.

    By convention variables starting with i_ or _i_ are initialized once and never updated.
    """
    # FIXME: lbm vs. lbf vs. lbs etc. is not correctly applied in naming and comments
    __slots__ = ('_life_time', '_i_stage_duration',
                 '_orientation', '_position', '_current_speed_fps',
                 '_current_thrust_lbf', '_i_stage_thrust_lbf', '_i_stage_fuel_lbfs',
                 '_current_weight_lbs',
                 '_i_max_g_sea_level', '_current_g',
                 '_i_vector_thrust',
                 '_i_cd_base', '_i_cd_plume', '_i_cross_section_sqft')

    def __init__(self, launched_orientation: g.Orientation, launched_position: g.Position,
                 launched_speed_ms: float,
                 stage_duration: List[float], stage_thrust: List[float],
                 weight_total_launch_lbs: float, weight_fuel_launch_lbs: float,
                 max_g_sea_level: float, vector_thrust: bool,
                 cd_base: float,  # "drag-coeff"
                 cd_plume: float,  # "exhaust-plume-parasitic-drag-factor"
                 cross_section_sqft: float) -> None:
        self._life_time = 0  # time in seconds since launch

        self._orientation = launched_orientation
        self._position = launched_position
        self._current_speed_fps = g.metres_to_feet(launched_speed_ms)

        self._i_stage_duration = stage_duration
        self._i_stage_thrust_lbf = stage_thrust

        self._current_weight_lbs = weight_total_launch_lbs
        self._initialize_fuel_consumption_per_stage(weight_fuel_launch_lbs)

        self._i_max_g_sea_level = max_g_sea_level
        self._current_g = 1.

        self._i_vector_thrust = vector_thrust

        self._i_cd_base = cd_base
        self._i_cd_plume = cd_plume
        self._i_cross_section_sqft = cross_section_sqft

    @property
    def mass(self) -> float:
        return self._current_weight_lbs / SLUG2LB

    @classmethod
    def create_vympel_r_3s(cls, launched_orientation: g.Orientation,
                           launched_position: g.Position, launched_speed_ms: float) -> 'HunterMissile':
        """Parameters according to OPFOR Mig-21:
        https://github.com/l0k1/MiG-21bis/blob/master/MiG-21-set-common.xml"""
        instance = HunterMissile(launched_orientation, launched_position, launched_speed_ms,
                                 [3.5, 0., 0.], [3821., 0., 0],
                                 165., 55.,
                                 28., False,
                                 0.55, 1., 0.143)
        return instance

    def _initialize_fuel_consumption_per_stage(self, weight_fuel_launch_lbs: float) -> None:
        self._i_stage_fuel_lbfs = [0., 0., 0.]  # lbm/s
        impulse = [0., 0., 0.]
        impulse_total = 0.
        for i in range(3):
            if self._i_stage_thrust_lbf[i] > 0:
                assert self._i_stage_duration[i] > 0, f'In stage {i} there is thrust but no duration'
            impulse[i] = self._i_stage_thrust_lbf[i] * self._i_stage_duration[i]  # lbf*s
            impulse_total += impulse[i]
        fuel_per_impulse = weight_fuel_launch_lbs / impulse_total  # lbm/(lbf*s)
        for i in range(3):
            if impulse[i] > 0.:
                self._i_stage_fuel_lbfs[i] = fuel_per_impulse * self._i_stage_duration[i]

    def run_increment(self, delta_time: float) -> None:
        """Advance one delta_time in time."""
        self._life_time += delta_time
        self._calc_current_thrust()
        rho, speed_sound_fps = _calc_rho_speed_of_sound(self._position.alt_ft)
        mach = self._current_speed_fps / speed_sound_fps
        self._calc_current_guidance(rho)
        self._calc_current_speed(mach, rho, delta_time)
        self._calc_current_weight(delta_time)

    def _calc_current_thrust(self) -> None:
        """Based on current life_time calculate the thrust depending on stages"""
        self._current_thrust_lbf = 0
        if self._life_time > (self._i_stage_duration[0] + self._i_stage_duration[1] + self._i_stage_duration[2]):
            self._current_thrust_lbf = 0
        elif self._life_time > self._i_stage_duration[0] + self._i_stage_duration[1]:
            self._current_thrust_lbf = self._i_stage_thrust_lbf[2]
        elif self._life_time > self._i_stage_duration[0]:
            self._current_thrust_lbf = self._i_stage_thrust_lbf[1]
        else:
            self._current_thrust_lbf = self._i_stage_thrust_lbf[0]

    def _calc_current_guidance(self, rho: float) -> None:
        """Based on target position and limitations calculate heading, pitch etc."""
        current_max_allowed_g = _calc_max_g_at_rho(rho, self._i_max_g_sea_level,
                                                   self._i_vector_thrust, self._current_thrust_lbf)
        # FIXME: calculate pitch/heading, current G incl. limiting G (_guide() )

        # TODO: self._limit_g() -> final current_g plus final pitch/heading
        # In Nasal func limitG is using overload_limiter instead of max_G_Rotation()

    def _calc_current_speed(self, mach: float, rho: float, delta_time: float) -> None:
        # speed change due to thrust / weight ratio acceleration
        self._current_speed_fps += (self._current_thrust_lbf / self.mass) * delta_time

        # speed change due to drag
        current_cd = _calc_drag_coefficient(mach, self._current_thrust_lbf,
                                            self._i_cd_base, self._i_cd_plume)
        q = 0.5 * rho * self._current_speed_fps * self._current_speed_fps
        drag_acc = (current_cd * q * self._i_cross_section_sqft) / self.mass
        self._current_speed_fps -= drag_acc * delta_time

        # speed change due to energy bleed
        self._current_speed_fps += _calc_speed_loss_from_energy_bleed(delta_time,
                                                                      self._i_vector_thrust,
                                                                      self._current_g,
                                                                      self._current_thrust_lbf,
                                                                      self._position.alt_ft)

    def _calc_current_weight(self, delta_time: float) -> None:
        """Weight is reduced by consuming fuel."""
        if self._life_time > (self._i_stage_duration[0] + self._i_stage_duration[1]):
            self._current_weight_lbs -= self._i_stage_fuel_lbfs[2] * delta_time
        elif self._life_time > self._i_stage_duration[0]:
            self._current_weight_lbs -= self._i_stage_fuel_lbfs[1] * delta_time
        else:
            self._current_weight_lbs -= self._i_stage_fuel_lbfs[0] * delta_time


@unique
class MissileGuidanceLaw(IntEnum):
    """See explanation in missile-code.nas.
    xx is degrees to aim above target, yy is seconds it will do that)
    """
    direct = 1
    other = 2
    apn = 3
    opn = 4
    pn = 5
    pn_2030 = 6


class Missile:
    """Is not necessarily a missile - can also be the launcher"""
    __slots__ = ('life_time', 'dt',
                 'name', 'pitch',
                 'ref_area_sqft', 'Cd_base', 'Cd_plume',
                 'weight_launch_lbm', 'weight_fuel_lbm',
                 'stage_1_duration', 'stage_2_duration', 'stage_3_duration',
                 'force_lbf_1', 'force_lbf_2', 'force_lbf_3',
                 'giveUpMach', 'gSea', 'seeker_fov',
                 'vector_thrust', 'guidanceLaw', 'pro_constant',
                 'advanced', 'a_ratio', 'wing_eff',
                 'finished', 'weight_current', 'mass', 'thrust_lbf',
                 'fuel_per_sec_1', 'fuel_per_sec_2', 'fuel_per_sec_3',
                 'myG', 'apn', 'rho', 'maxMach', 'speed_fps', 'fixed_aim', 'fixed_aim_time',
                 'speed_scalar_fps', 'max_g_current', 'track_signal_e',
                 'last_t_elev_deg', 'last_t_elev_norm_speed', 'dist_direct_last',
                 'exploded', 'lostFoV', 'hit', 't_rotate', 't_rotate2',
                 'distance_nm', 'horiz', 'target_g', 'altitude_ft', 'startMach',
                 'travel_x_ft', 'travel_y_ft', 'overrideAPN',
                 'target_altitude_ft', 'target_startMach', 'target_heading',
                 'target_travel_x_ft', 'target_travel_y_ft', 't_speed_fps', 't_pitch',
                 'target_g2', 'target_g_time', 'target_speed_fps',
                 'target_dist_x_ft', 'target_dist_y_ft')

    def __init__(self, dt: float, name: str,
                 ref_area_sqft: float, Cd_base: float, Cd_plume: int,
                 weight_launch_lbm: int, weight_fuel_lbm: int,
                 stage_1_duration: int, stage_2_duration: int, stage_3_duration: int,
                 force_lbf_1: int, force_lbf_2: int, force_lbf_3: int,
                 giveUpMach: float, gSea: int, seeker_fov: float,
                 vector_thrust: bool, guidanceLaw: MissileGuidanceLaw, pro_constant: int,
                 advanced: bool, a_ratio: float, wing_eff: float) -> None:
        self.life_time = 0.
        self.dt = dt  # the time elapsed between the calls to loop
        self.name = name
        self.ref_area_sqft = ref_area_sqft
        self.Cd_base = Cd_base
        self.Cd_plume = Cd_plume
        self.weight_launch_lbm = weight_launch_lbm
        self.weight_fuel_lbm = weight_fuel_lbm
        self.stage_1_duration = stage_1_duration
        self.stage_2_duration = stage_2_duration
        self.stage_3_duration = stage_3_duration
        self.force_lbf_1 = force_lbf_1
        self.force_lbf_2 = force_lbf_2
        self.force_lbf_3 = force_lbf_3
        self.giveUpMach = giveUpMach
        self.gSea = gSea
        self.seeker_fov = seeker_fov
        self.vector_thrust = vector_thrust
        self.guidanceLaw = guidanceLaw
        self.pro_constant = pro_constant
        self.advanced = advanced
        self.a_ratio = a_ratio
        self.wing_eff = wing_eff

        # dynamically set - e.g. in initialize_missile
        self.finished = False
        self.weight_current = 0.
        self.mass = 0.
        self.thrust_lbf = 0.
        self.fuel_per_sec_1 = 0.
        self.fuel_per_sec_2 = 0.
        self.fuel_per_sec_3 = 0.
        self.apn = 0
        self.speed_fps = [0., 0.]
        self.speed_scalar_fps = 0.
        self.max_g_current = 0.
        self.track_signal_e = 0.
        self.last_t_elev_deg = None
        self.last_t_elev_norm_speed = None
        self.dist_direct_last = None
        self.exploded = False
        self.lostFoV = False
        self.hit = False  # FIXME: most probably not used outside of performance testing
        self.t_rotate = 0.
        self.t_rotate2 = 0.

    @classmethod
    def create_buk_m2(cls) -> 'Missile':
        instance = Missile(0.10, "BUK-M2",
                           1.312336, 0.25, 1,  # drag stuff
                           1576, 900,  # weight stuff
                           15, 0, 0, 16500, 0, 0,  # stage stuff
                           0.5, 24, 220 * 0.5,
                           False, MissileGuidanceLaw.apn, 3,
                           False, 1.72, 0.872)
        return instance

    def run(self):
        self.finished = False
        self._reset()

        while not self.finished:
            self._loop()

    def _reset(self):
        # and these are the rest:
        self.travel_x_ft = 0
        self.travel_y_ft = 0
        self.last_t_elev_deg = None
        self.last_t_elev_norm_speed = None
        self.dist_direct_last = None
        self.overrideAPN = False
        self.myG = 1
        self.maxMach = 0
        self.exploded = False
        self.lostFoV = False
        self.hit = 0
        self.life_time = 0

    def initialize_missile(self, distance_nm: int, horiz: bool, pitch: float, target_g: float,
                           altitude_ft: int, startMach: float,
                           target_altitude_ft: int, target_startMach: float, target_heading: int):
        # right now faking position of me and target aircraft -> "scenario"
        self.horiz = horiz
        self.distance_nm = distance_nm
        self.target_g = target_g
        self.pitch = pitch  # initially set to the launch pitch
        self.altitude_ft = altitude_ft
        self.startMach = startMach


        self.target_altitude_ft = target_altitude_ft
        self.target_startMach = target_startMach
        self.target_heading = target_heading

        self.target_g_time = -1
        self.target_g2 = 0

        # the real initialization calculations start here
        self.weight_current = self.weight_launch_lbm
        self.mass = self.weight_launch_lbm / SLUG2LB
        impulse1 = self.force_lbf_1 * self.stage_1_duration  # lbf*s
        impulse2 = self.force_lbf_2 * self.stage_2_duration  # lbf*s
        impulse3 = self.force_lbf_3 * self.stage_3_duration  # lbf*s
        impulseT = impulse1 + impulse2 + impulse3  # lbf*s
        fuel_per_impulse = self.weight_fuel_lbm / impulseT  # lbm/(lbf*s)
        self.fuel_per_sec_1 = (fuel_per_impulse * impulse1) / self.stage_1_duration  # lbm/s
        if impulse2 != 0:
            self.fuel_per_sec_2 = (fuel_per_impulse * impulse2) / self.stage_2_duration  # lbm/s
        else:
            self.fuel_per_sec_2 = 0
        if impulse3 != 0:
            self.fuel_per_sec_3 = (fuel_per_impulse * impulse3) / self.stage_3_duration  # lbm/s
        else:
            self.fuel_per_sec_3 = 0

        self.target_travel_x_ft = self.distance_nm * NM2M * M2FT
        self.target_travel_y_ft = (
                    self.target_altitude_ft - self.altitude_ft) if not self.horiz else self.target_altitude_ft

        startFPS = machToFPS(self.startMach, self.altitude_ft)
        self.speed_fps = [startFPS * math.cos(self.pitch * D2R), startFPS * math.sin(self.pitch * D2R), 0]
        self.t_speed_fps = abs(
            machToFPS(self.target_startMach, self.target_altitude_ft if not self.horiz else self.altitude_ft))

        if self.pitch == 90.:
            self.pitch = min(90., math.atan2(self.target_travel_y_ft, self.target_travel_x_ft) * R2D + 20.0)
        if self.guidanceLaw is MissileGuidanceLaw.apn:
            self.apn = 2
        elif self.guidanceLaw is MissileGuidanceLaw.opn:
            self.apn = 0
        elif self.guidanceLaw in (MissileGuidanceLaw.pn, MissileGuidanceLaw.pn_2030):
            self.apn = 1
        else:
            self.apn = -1

        self.fixed_aim = None
        self.fixed_aim_time = None
        if self.guidanceLaw is MissileGuidanceLaw.pn_2030:
            self.fixed_aim = int(MissileGuidanceLaw.pn_2030.name[-4:-2])
            self.fixed_aim_time = int(MissileGuidanceLaw.pn_2030.name[-2:])

        self.t_pitch = self.target_heading
        # self.t_pitch  = 180 if self.target_startMach > 0 else 0

        if self.target_g > 1:
            # self.t_rotate = -10 if self.target_startMach>0 else 10 # make sure the target moves up in Y axis in graph
            self.t_rotate = 10
            self._limitTG()
        elif self.target_g < -1:
            self.t_rotate = 10
            self.target_g *= -1
            self._limitTG()
            self.t_rotate *= -1
        else:
            self.t_rotate = 0

        if self.target_g_time != -1:
            if self.target_g2 > 1:
                # self.t_rotate = -10 if self.target_startMach>0 else 10 # make sure the target moves up in Y axis in graph
                self.t_rotate2 = 10
                self._limitTG2()
            elif self.target_g2 < -1:
                self.t_rotate2 = 10
                self.target_g2 *= -1
                self._limitTG2()
                self.t_rotate2 *= -1
            else:
                self.t_rotate2 = 0
        else:
            self.target_g_time = 100000

        self.target_speed_fps = [math.cos(self.t_pitch * D2R) * self.t_speed_fps,
                                 math.sin(self.t_pitch * D2R) * self.t_speed_fps]

    def _loop(self):
        self.life_time += self.dt
        self.thrust_lbf = self._thrust()
        if not self.horiz:
            rs = _calc_rho_speed_of_sound(self.altitude_ft + self.travel_y_ft)
        else:
            rs = _calc_rho_speed_of_sound(self.altitude_ft)

        self.rho = rs[0]
        sound_fps = rs[1]
        self.speed_scalar_fps = math.sqrt(self.speed_fps[0] * self.speed_fps[0] + self.speed_fps[1] * self.speed_fps[1])
        mach = self.speed_scalar_fps / sound_fps

        if mach > self.maxMach:
            self.maxMach = mach

        guideCommand = self._guide()
        self.pitch += guideCommand

        Cd = self._drag(mach, self.myG, self.speed_scalar_fps)

        speed_change_fps = self._speedChange(self.rho, Cd, self.speed_scalar_fps)
        if not self.horiz:
            self.speed_scalar_fps += speed_change_fps + self._energyBleed(self.myG,
                                                                          self.altitude_ft + self.travel_y_ft)
        else:
            self.speed_scalar_fps += speed_change_fps + self._energyBleed(self.myG, self.altitude_ft)

        self.speed_fps = [math.cos(self.pitch * D2R) * self.speed_scalar_fps,
                          math.sin(self.pitch * D2R) * self.speed_scalar_fps]
        if not self.horiz:
            self.speed_fps[1] -= G_FPS * self.dt

            self.pitch = math.atan2(self.speed_fps[1], self.speed_fps[0]) * R2D

        dist_x_ft = self.speed_fps[0] * self.dt
        dist_y_ft = self.speed_fps[1] * self.dt
        self.travel_x_ft += dist_x_ft
        self.travel_y_ft += dist_y_ft

        if self.life_time < self.target_g_time:
            self.t_pitch += self.t_rotate
        else:
            self.t_pitch += self.t_rotate2
        self.target_speed_fps = [math.cos(self.t_pitch * D2R) * self.t_speed_fps,
                                 math.sin(self.t_pitch * D2R) * self.t_speed_fps]
        self.target_dist_x_ft = self.target_speed_fps[0] * self.dt
        self.target_dist_y_ft = self.target_speed_fps[1] * self.dt
        self.target_travel_x_ft += self.target_dist_x_ft
        self.target_travel_y_ft += self.target_dist_y_ft

        # consume fuel
        if self.life_time > (self.stage_1_duration + self.stage_2_duration + self.stage_3_duration):
            self.weight_current = self.weight_launch_lbm - self.weight_fuel_lbm
        elif self.life_time > (self.stage_1_duration + self.stage_2_duration):
            self.weight_current = self.weight_current - self.fuel_per_sec_3 * self.dt
        elif self.life_time > (self.stage_1_duration):
            self.weight_current = self.weight_current - self.fuel_per_sec_2 * self.dt
        else:
            self.weight_current = self.weight_current - self.fuel_per_sec_1 * self.dt

        self.mass = self.weight_current / SLUG2LB

        fov = angleBetweenVectors(self.speed_fps, [self.target_travel_x_ft - self.travel_x_ft,
                                                   self.target_travel_y_ft - self.travel_y_ft])

        if (self.travel_x_ft > self.target_travel_x_ft and self.distance_nm > 0) or (
                self.travel_x_ft < self.target_travel_x_ft and self.distance_nm < 0):
            self.exploded = True
            self._finish()
        elif mach < self.giveUpMach < self.maxMach:
            self._finish()
        elif self.seeker_fov < fov:
            self.lostFoV = True
            self._finish()
        elif self.life_time > 10 * 60:
            # for missiles too weak to reach minimum speed, to prevent loop from going forever.
            self._finish()

    def _guide(self):
        self.max_g_current = self._maxG(self.rho, self.gSea)
        t_elev_deg = math.atan2(self.target_travel_y_ft - self.travel_y_ft,
                                self.target_travel_x_ft - self.travel_x_ft) * R2D
        t_pitch_fg = self.t_pitch

        # lets convert program pitch to 3D FG pitch:
        while t_pitch_fg > 180:
            t_pitch_fg = t_pitch_fg - 360

        while t_pitch_fg < -180:
            t_pitch_fg = t_pitch_fg + 360

        h = 180 if abs(t_pitch_fg) > 90 else 0
        if t_pitch_fg > 90:
            t_pitch_fg = 180 - t_pitch_fg
        elif t_pitch_fg < -90:
            t_pitch_fg = -180 - t_pitch_fg

        t_LOS_elev_norm_speed = scalarProj(h, t_pitch_fg, self.t_speed_fps, 180, t_elev_deg * -1 + 90)

        dist_curr_direct = math.sqrt(
            math.pow(self.travel_x_ft - self.target_travel_x_ft, 2) + math.pow(
                self.travel_y_ft - self.target_travel_y_ft, 2))
        curr_deviation_e = t_elev_deg - self.pitch
        if self.last_t_elev_deg is None:
            self.last_t_elev_deg = t_elev_deg
            self.last_t_elev_norm_speed = t_LOS_elev_norm_speed
            self.dist_direct_last = dist_curr_direct
            return 0

        raw_steer_signal_elev = 0.
        if self.guidanceLaw is MissileGuidanceLaw.direct:
            raw_steer_signal_elev = curr_deviation_e
            if not self.horiz:
                attitudePN = math.atan2(-(-self.speed_fps[1] + G_FPS * self.dt), self.speed_fps[0]) * R2D
                gravComp = self.pitch - attitudePN
                raw_steer_signal_elev += gravComp

            self.track_signal_e = raw_steer_signal_elev

            self._limitG()
            return self.track_signal_e

        line_of_sight_rate_up_rps = (D2R * (t_elev_deg - self.last_t_elev_deg)) / self.dt
        self.last_t_elev_deg = t_elev_deg
        vert_closing_rate_fps = math.fabs((self.dist_direct_last - dist_curr_direct) / self.dt)
        self.dist_direct_last = dist_curr_direct

        if not self.horiz and self.fixed_aim and self.life_time < self.fixed_aim_time:
            raw_steer_signal_elev = curr_deviation_e + self.fixed_aim
        elif self.apn == 0:
            # Original Proportional navigation.
            radians_up_per_sec = self.pro_constant * line_of_sight_rate_up_rps
            raw_steer_signal_elev = self.dt * radians_up_per_sec * R2D
        elif self.apn == 1 or self.overrideAPN:
            # Proportional navigation.
            toBody = math.cos(curr_deviation_e * D2R)
            if toBody == 0:
                toBody = 0.00001
            acc_upwards_fps2 = self.pro_constant * line_of_sight_rate_up_rps * vert_closing_rate_fps
            acc_upwards_fps2 /= toBody  # body test
            velocity_vector_length_fps = clamp(self.speed_scalar_fps, 0.0001, 1000000)
            commanded_upwards_vector_length_fps = acc_upwards_fps2 * self.dt
            raw_steer_signal_elev = R2D * commanded_upwards_vector_length_fps / velocity_vector_length_fps
        elif self.apn == 2:
            # Augmented proportional navigation.
            t_LOS_elev_norm_acc = (t_LOS_elev_norm_speed - self.last_t_elev_norm_speed) / self.dt
            self.last_t_elev_norm_speed = t_LOS_elev_norm_speed
            toBody = math.cos(curr_deviation_e * D2R)
            if toBody == 0:
                toBody = 0.00001
            acc_upwards_fps2 = self.pro_constant * line_of_sight_rate_up_rps * vert_closing_rate_fps + t_LOS_elev_norm_acc
            acc_upwards_fps2 /= toBody
            velocity_vector_length_fps = clamp(self.speed_scalar_fps, 0.0001, 1000000)
            commanded_upwards_vector_length_fps = acc_upwards_fps2 * self.dt
            raw_steer_signal_elev = R2D * commanded_upwards_vector_length_fps / velocity_vector_length_fps

        if not self.horiz:
            attitudePN = math.atan2(-(-self.speed_fps[1] + G_FPS * self.dt), self.speed_fps[0]) * R2D
            gravComp = self.pitch - attitudePN

            raw_steer_signal_elev += gravComp

        self.track_signal_e = raw_steer_signal_elev

        self._limitG()
        return self.track_signal_e

    def _finish(self):
        self.hit = FT2M * math.sqrt(
            math.pow(self.travel_x_ft - self.target_travel_x_ft, 2) + math.pow(
                self.travel_y_ft - self.target_travel_y_ft,
                2)) < self.dt * 2000
        self.finished = True

    def _drag(self, mach, N, speed_fps):
        if mach < 0.7:
            Cd0 = (0.0125 * mach + 0.20) * 5 * self.Cd_base
        elif mach < 1.2:
            Cd0 = (0.3742 * math.pow(mach, 2) - 0.252 * mach + 0.0021 + 0.2) * 5 * self.Cd_base
        else:
            Cd0 = (0.2965 * math.pow(mach, -1.1506) + 0.2) * 5 * self.Cd_base
        if self.advanced:
            Gs = N
            Cd0 = Cd0 * self.Cd_plume if self.thrust_lbf is not None and self.thrust_lbf > 0 else Cd0
            if N is None:
                Gs = 0
            if self.vector_thrust and self.thrust_lbf > 0:
                Gs = Gs * 0.35
            if mach < 1.1:
                Cdi = self.Cd_base * Gs
            else:
                FN = Gs * self.mass * G_FPS
                CN = 2 * FN / (self.rho * speed_fps * speed_fps * self.ref_area_sqft)
                CL = CN * math.cos(Gs * 1.5 * D2R) - Cd0 * math.sin(1.5 * Gs * D2R)
                Cdi = (CL * CL) / (math.pi * self.wing_eff * self.a_ratio)

        else:
            Cdi = 0
            Cd0 = Cd0 * self.Cd_plume if self.thrust_lbf is not None and self.thrust_lbf > 0 else Cd0
        return Cd0 + Cdi

    def _thrust(self):  # CONVERTED
        self.thrust_lbf = 0
        if self.life_time > (self.stage_1_duration + self.stage_2_duration + self.stage_3_duration):
            self.thrust_lbf = 0
        elif self.life_time > self.stage_1_duration + self.stage_2_duration:
            self.thrust_lbf = self.force_lbf_3
        elif self.life_time > self.stage_1_duration:
            self.thrust_lbf = self.force_lbf_2
        else:
            self.thrust_lbf = self.force_lbf_1
        return self.thrust_lbf

    def _energyBleed(self, gForce, altitude):
        if self.advanced:
            return 0
        # Bleed of energy from pulling Gs.
        # This is very inaccurate, but better than nothing.
        #
        # First we get the speedloss due to normal drag:
        b300 = self._bleed32800at0g()
        b000 = self._bleed0at0g()
        #
        # We then subtract the normal drag from the loss due to G and normal drag.
        b325 = self._bleed32800at25g() - b300
        b025 = self._bleed0at25g() - b000
        b300 = 0
        b000 = 0
        #
        # We now find what the speedloss will be at sealevel and 32800 ft.
        speedLoss32800 = b300 + ((gForce - 0) / (25 - 0)) * (b325 - b300)
        speedLoss0 = b000 + ((gForce - 0) / (25 - 0)) * (b025 - b000)
        #
        # We then inter/extra-polate that to the currect density-altitude.
        speedLoss = speedLoss0 + ((altitude - 0) / (32800 - 0)) * (speedLoss32800 - speedLoss0)
        #
        # For good measure the result is clamped to below zero.
        speedLoss = clamp(speedLoss, -100000, 0)
        # self.energyBleedKt += self.speedLoss * FPS2KT
        speedLoss = speedLoss - self.vector_thrust * speedLoss * 0.66 * (
            0 if self.thrust_lbf == 0 else 1)  # vector thrust will only bleed 1/3 of the calculated loss.
        return speedLoss

    def _bleed32800at0g(self):
        loss_fps = 0 + ((self.dt - 0) / (15 - 0)) * (-330 - 0)
        return loss_fps * M2FT

    def _bleed32800at25g(self):
        loss_fps = 0 + ((self.dt - 0) / (3.5 - 0)) * (-240 - 0)
        return loss_fps * M2FT

    def _bleed0at0g(self):
        loss_fps = 0 + ((self.dt - 0) / (22 - 0)) * (-950 - 0)
        return loss_fps * M2FT

    def _bleed0at25g(self):
        loss_fps = 0 + ((self.dt - 0) / (7 - 0)) * (-750 - 0)
        return loss_fps * M2FT

    def _limitG(self):
        self.myG = steering_speed_G(self.track_signal_e, 0, self.speed_scalar_fps, self.dt)
        # print("want "+str(self.myG)+" G")
        if self.max_g_current < self.myG:
            MyCoef = max_G_Rotation(self.track_signal_e, 0, self.speed_scalar_fps, self.dt, self.max_g_current)
            self.track_signal_e = self.track_signal_e * MyCoef
            self.myG = steering_speed_G(self.track_signal_e, 0, self.speed_scalar_fps, self.dt)
            # print("given " + str(self.myG) + " G")
        if self.horiz:
            self.myG = max(1, self.myG)

    def _limitTG(self):
        MyCoef = max_G_Rotation(self.t_rotate, 0, self.t_speed_fps, self.dt, self.target_g)
        self.t_rotate = self.t_rotate * MyCoef

    def _limitTG2(self):
        MyCoef = max_G_Rotation(self.t_rotate2, 0, self.t_speed_fps, self.dt, self.target_g2)
        self.t_rotate2 = self.t_rotate2 * MyCoef

    def _maxG(self, rho, max_g_sealevel):
        if self.vector_thrust and self.thrust_lbf == 0:
            max_g_sealevel = max_g_sealevel * 0.666
        return clamp(max_g_sealevel + ((rho - 0.0023769) / (0.00036159 - 0.0023769)) * (
                    max_g_sealevel * 0.5909 - max_g_sealevel),
                     0.25, 100)

    def _speedChange(self, rho, Cd, speed_fps):
        acc = self.thrust_lbf / self.mass
        q = 0.5 * rho * speed_fps * speed_fps
        drag_acc = (Cd * q * self.ref_area_sqft) / self.mass
        return acc * self.dt - drag_acc * self.dt


class TestMissile(unittest.TestCase):
    """This class only tests whether the code can run. Parameters are fine-tuned else-where (not disclosed)"""
    def test_original_missile(self):
        missile = Missile.create_buk_m2()
        missile.initialize_missile(25, False, 24., 1, 10, 0., 40000, 1.5, 170)  # scenario # 1
        missile.run()
        print(missile.exploded, missile.lostFoV, missile.hit)
        self.assertTrue(missile.hit)

        missile_3 = Missile.create_buk_m2()
        missile_3.initialize_missile(10, False, 24., 1, 10, 0., 20000, 0.9, 0)  # scenario # 3
        missile_3.run()
        print(missile_3.exploded, missile_3.lostFoV, missile_3.hit)
        self.assertTrue(missile_3.hit)

        missile_6 = Missile.create_buk_m2()
        missile_6.initialize_missile(27, False, 24., 1, 10, 0., 20000, 0.9, 130)  # scenario # 6
        missile_6.run()
        print(missile_6.exploded, missile_6.lostFoV, missile_6.hit)
        self.assertFalse(missile_6.hit)

    def test_hunter_missile(self):
        missile = HunterMissile.create_vympel_r_3s(g.Orientation(0., 0.),
                                                   g.Position(0., 0., 1000.),
                                                   0.)
        delta_time = 0.5
        for i in range(20):
            missile.run_increment(delta_time)
        pass
