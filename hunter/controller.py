"""
Controls all aspects of a scenario and runs the mp_targets. fg_targets (instances of FlightGear) run in workers,
but are also controlled by the Controller.

https://docs.python.org/3/library/subprocess.html
https://docs.python.org/3.7/library/multiprocessing.html

"""

import logging
import multiprocessing as mp
import random
import sys
import time
from typing import Any, Dict, List, Optional

from google.cloud import pubsub

import networkx as nx

import hunter.damage as d
import hunter.scenario_tools.dem_grid as dg
import hunter.iff_datalink as ifd
import hunter.mp_dispensers as mpd
import hunter.mp_shooters as mps
import hunter.mp_targets as mpt
import hunter.fg_targets as fgt
import hunter.emesary_notifications as en
import hunter.fgms_io as fio
import hunter.gcp_ds_io as gds
import hunter.gcp_ps_io as gps
import hunter.gcp_utils as gu
import hunter.geometry as g
import hunter.messages as m
import hunter.scenarios as sc
import hunter.utils as u


# http://flightgear.sourceforge.net/getstart-en/getstart-enpa2.html

GCI_TIME_LIMIT = 10  # if request is newer than this, then it is supposed to be a resend and therefore ignored
GCI_SAME_TARGET_LIMIT = 120  # if a request is newer than this, then info for the same target might be displayed
GCI_SIMPLIFIED_INTERCEPT = 5000  # if below value, then intercept vector will be calculated in a simplified way

FGMS_CLIENT_PORT = 5010


class ObjectContainer:
    __slots__ = ('_last_position_update', '_last_position_processed', 'iff_hash', 'callsign')

    def __init__(self, callsign: str) -> None:
        # need to have a fake default position so properties can be accessed immediately
        self._last_position_update = m.PositionUpdate('', '', m.HealthType.fit_for_fight, g.Position(0., 0., 0.))
        self._last_position_processed = time.time()
        self.iff_hash = None
        self.callsign = callsign

    @property
    def health(self) -> m.HealthType:
        return self._last_position_update.health

    @health.setter
    def health(self, new_health: m.HealthType) -> None:
        self._last_position_update.health = new_health

    @property
    def position(self) -> g.Position:
        return self._last_position_update.position

    @property
    def last_position_update(self) -> m.PositionUpdate:
        return self._last_position_update

    @last_position_update.setter
    def last_position_update(self, position_update: m.PositionUpdate) -> None:
        self._last_position_update = position_update

    @property
    def heading(self) -> float:
        return self._last_position_update.heading

    @property
    def speed(self) -> float:
        return self._last_position_update.speed

    def process_position(self, current_pos: g.Position, callsign: str, kind: str) -> None:
        """Position object is from ATCpie and is tuple of EarthCoord and alt_ft"""
        current_time = time.time()
        if self._last_position_update is not None:
            # now we can also get speed and bearing
            bearing = g.calc_bearing_pos(self._last_position_update.position, current_pos)
            distance = g.calc_distance_pos(self._last_position_update.position, current_pos)
            speed = distance / (current_time - self._last_position_processed)
            pitch = g.calc_altitude_angle(self._last_position_update.position, current_pos)
            self._last_position_update = m.PositionUpdate(callsign, kind, self.health, current_pos,
                                                          bearing, pitch, speed)
        else:
            self._last_position_update = m.PositionUpdate(callsign, kind, self.health, current_pos)
        self._last_position_processed = current_time

    def is_zombie(self) -> bool:
        callsign = self._last_position_update.callsign
        zombie = True if time.time() - self._last_position_processed > u.POS_UPDATE_FREQ_ZOMBIE_TIME else False
        if zombie:
            logging.info('%s is zombie at time %i and last_position_processed %i', callsign, time.time(),
                         self._last_position_processed)
        return zombie


class TargetContainer(ObjectContainer):
    __slots__ = 'emesary_callsign'

    def __init__(self, callsign: str):
        super().__init__(callsign)
        self.emesary_callsign = d.transform_to_emesary_callsign(callsign)  # cached for faster processing in loops


class MPTargetContainer(TargetContainer):
    """Container for targets represented by simulated MP instances served by the controller"""
    __slots__ = ('broker', '_mp_target')

    def __init__(self, broker: u.ParticipantMessageInterface, mp_target: mpt.MPTarget) -> None:
        super().__init__(mp_target.callsign)
        self.broker = broker  # e.g. fio.FGMSThreadedSender
        self._mp_target = mp_target

    @property
    def mp_target(self) -> Optional[mpt.MPTarget]:
        """Returns None if this is not a simulated target, else a pointer to the object."""
        if isinstance(self._mp_target, mpt.MPTarget):
            return self._mp_target
        return None

    @property
    def mp_target_shooter(self) -> Optional[mps.Shooter]:  # can return a ShooterImpl
        """Returns None if this is not a simulated shooting target, else a pointer to the object."""
        if isinstance(self._mp_target, mpt.MPTarget) and self._mp_target.is_shooting:
            return self._mp_target.shooter
        return None

    @property
    def mp_target_dispenser(self) -> Optional[mpd.Dispenser]:  # can return a DispenserImpl
        """Returns None if this is not a simulated dispensing target, else a pointer to the object."""
        if isinstance(self._mp_target, mpt.MPTarget) and self._mp_target.is_dispensing:
            return self._mp_target.dispenser
        return None

    @property
    def mp_moving_target(self) -> Optional[mpt.MPTargetMoving]:
        """Returns None if this is not a simulated moving target, else a pointer to the object."""
        target = self._mp_target
        if target and isinstance(target, mpt.MPTargetMoving):
            return target
        return None


class FGTargetContainer(TargetContainer):
    """Container for targets represented by FGFS instances served by a worker over GCP pub/sub"""
    __slots__ = ('worker_id', 'target_type', 'name', 'last_damage_result', 'shooting')

    def __init__(self, worker_id: str, callsign: str) -> None:
        super().__init__(callsign)
        self.worker_id = worker_id
        self.target_type = None  # if it is None, then this container is just a placeholder - else mpt.MPTargetType
        self.name = None
        self.last_damage_result = None  # used when real FG instance is destroyed -> result resent
        self.shooting = False

    def last_damage_result_as_destroyed(self) -> Optional[m.DamageResult]:
        """If there has been a damage result registered at all, then resend the last to give the last
        pilot hitting the reward of having destroyed the target."""
        if self.last_damage_result:
            res = self.last_damage_result.copy_with_health(m.HealthType.destroyed)
            self.last_damage_result = None  # in case proxy is reused - e.g. for automat
            return res
        return None

    @property
    def placeholder(self) -> bool:
        return self.target_type is None

    def reset(self) -> None:
        """Set back to being a placeholder and not an FG target."""
        self.target_type = None
        self.name = None
        self.shooting = False

    def make_sam(self, name: str) -> None:
        self.target_type = mpt.MPTargetType.vehicle
        self.name = name
        self.shooting = True

    def make_frigate(self, name: str) -> None:
        self.target_type = mpt.MPTargetType.ship
        self.name = name
        self.shooting = True

    def make_automat(self, name: str) -> None:
        self.target_type = mpt.MPTargetType.plane
        self.name = name
        self.shooting = True

    def make_carrier(self, name: str) -> None:
        self.target_type = mpt.MPTargetType.ship
        self.name = name


class AttackerContainer(ObjectContainer):
    __slots__ = ('mp_time_stamp',
                 '_last_gci_request', '_last_gci_target', '_last_gci_response_type', '_processed_emesary_indices',
                 'fgfs_model', 'newest_position')

    def __init__(self, fgfs_model: str, callsign: str) -> None:
        super().__init__(callsign)
        self.mp_time_stamp = 0  # MP time is not the same as local computer time!
        self._last_gci_request = 0  # timestamp based on time.time()
        self._last_gci_target = None  # callsign string
        self._last_gci_response_type = None  # m.GCIResponseType
        self._processed_emesary_indices = list()
        self.fgfs_model = fgfs_model

        # this position is constantly updated, last_position is not -> datastore
        self.newest_position = None  # g.Position

    def check_existing_emesary_index(self, msg_idx_type_id: str) -> bool:
        """Checks whether an emesary index (msg_idx plus type_id) has been seen already.
        Returns True if it has been seen already"""
        if msg_idx_type_id in self._processed_emesary_indices:
            return True
        if len(self._processed_emesary_indices) > 20:  # we do not need extensive history
            self._processed_emesary_indices.pop(0)
        self._processed_emesary_indices.append(msg_idx_type_id)
        return False

    def process_gci_request(self, gci_requests: List[Optional[bool]]) -> Optional[m.GCIResponseType]:
        """Checks request for what type of response is required and returns it if outside time limit - else None. """
        current_time = time.time()
        last_request = -1  # 0, 1, or 2 if a request was made outside time limit and is the newest one
        for idx in range(3):
            if gci_requests[idx] is True:
                if current_time - self._last_gci_request > GCI_TIME_LIMIT:
                    last_request = idx
                    self._last_gci_request = current_time
                break  # only one gci request type can be processed at a time
        if last_request == 0:
            return m.GCIResponseType.picture
        elif last_request == 1:
            return m.GCIResponseType.bogey_dope
        elif last_request == 2:
            return m.GCIResponseType.cutoff
        return None

    def get_last_requested_target(self, response_type: m.GCIResponseType) -> Optional[str]:
        """Returns the callsign of the last target's callsign if within time limit and same response type"""
        current_time = time.time()
        if self._last_gci_target is None:
            return None
        if (current_time - self._last_gci_request < GCI_SAME_TARGET_LIMIT) and \
                self._last_gci_response_type is response_type:
            return self._last_gci_target
        return None

    def set_last_requested_target(self, callsign: str, response_type: m.GCIResponseType) -> None:
        self._last_gci_target = callsign
        self._last_gci_response_type = response_type


def number_part_of_callsign(callsign: str) -> int:
    """Extracts the last two letters the callsign and converts them to a number."""
    number_part = callsign[-2:]
    logging.debug('Extracted %s as number from callsign % s', number_part, callsign)
    return int(number_part)


class Controller(mp.Process):
    """The heart of Hunter controlling all targets and having much of the main logic"""
    def __init__(self, parent_conn: mp.Pipe,
                 ctrl_callsign: str, identifier: str, gci_responses: bool,
                 cloud_integration_level: u.CloudIntegrationLevel,
                 scenario_path: str, scenario_name: str, hostility: u.Hostility,
                 defender_callsigns: List[str], override_prev_session_id: int) -> None:
        super().__init__()

        if ctrl_callsign is None or len(ctrl_callsign) > 7:
            raise ValueError('The callsign for the controller must at least have one character and not more than 7')
        if identifier is None or len(identifier) > 5:
            raise ValueError('The identifier must have between 1 and 5 letters')

        self.scenario = sc.load_scenario(scenario_name, scenario_path)  # raises ValueError is it does not exist
        self.identifier = identifier
        self.callsign = ctrl_callsign
        self.mp_server_host = u.get_mp_server_address()
        self.hostility = hostility
        if self.hostility.is_hostile:
            self.scenario.adapt_scenario_to_hostile_environment()
        # there is no network port parameter because there are many assumptions on port ranges -> u.PORT_MP_BASIS

        self.parent_conn = parent_conn  # The Pipe to the parent process (for command line CTRL-C only)
        self.attackers = dict()  # key = callsign, value = AttackerContainer (actually attackers and defenders)

        self.defender_callsigns = defender_callsigns
        self.mp_targets = dict()  # key = target_callsign, value = MPTargetContainer
        self.awacs_callsign = None
        self.last_datalink_processed = 0
        self.tanker_callsign = None
        self.fg_targets = dict()  # key = target_callsign, value = FGTargetContainer (can be None)
        self.fg_instances = u.FGInstances(0, 0, 0, 0)

        # following handled during set_up() and tear_down()
        self.fgms_receiver = None
        self.number_of_mp_messages_received = 0  # used to do a bit of heart beat control
        self.gci_responses = gci_responses

        self.cloud_integration_level = cloud_integration_level
        self.has_workers = False

        publisher_client = None  # publisher and subscriber are local to init, as run() is another process
        if u.use_datastore(self.cloud_integration_level):
            ds_client = gu.create_datastore_client()
            try:
                session = gds.register_new_session(ds_client, self.scenario, self.mp_server_host,
                                                   self.gci_responses,
                                                   self.hostility.heli_shoot,
                                                   self.hostility.drone_shoot,
                                                   self.hostility.ship_shoot,
                                                   self.hostility.shilka_shoot,
                                                   self.hostility.sam_shoot,
                                                   self.scenario.awacs_definition is not None,
                                                   self.scenario.tanker_definition is not None,
                                                   self.callsign, self.identifier,
                                                   override_prev_session_id)
                self.session_id = session[gds.PROP_SESSION_ID]
            except ValueError:
                logging.exception('Cannot start controller because interaction with Cloud is not working as expected')
                sys.exit(1)
        if u.use_pubsub(self.cloud_integration_level):
            publisher_client = gu.construct_publisher_client()
            subscriber_client = gu.construct_subscriber_client()
            self._request_workers(subscriber_client, publisher_client, scenario_name)
        self.gcp_ps_runner = None  # will be set in run(method) if needed
        self.gcp_ds_runner = None  # (ditto)

        # Populate assets
        random.shuffle(self.scenario.static_targets)  # make sure that there is randomness in callsigns etc
        self._populate_assets(publisher_client)

        self.last_next_moving_poll = 0  # when has there last time been checked to add a new moving target?
        self.last_position_processed = 0  # based on u.POS_UPDATE_FREQ_MOVING_TARGET
        self.last_send_position_update_ds = 0  # based on u.SEND_POSITION_UPDATES_TO_DS
        self.last_one_second_poll = 0  # reset every second to be able to make checks every second (run cycle is faster)

        self.run_cycles = 0
        self.last_receiver_heartbeat = 0

        self.local_damage_results = list()  # local list of DamageResult messages (vs. those sent to GCP)

    # noinspection PyBroadException
    def _request_workers(self, subscriber: pubsub.SubscriberClient, publisher: pubsub.PublisherClient,
                         scenario_name: str) -> None:
        """Sends out requests to workers to register.
        If no reply, then self.has_workers is set to False, such that the interaction is not done."""
        try:
            gps.handle_workers_to_ctrl_subscription(subscriber, True)
            logging.info('Requesting workers for scenario %s and session ID=%s', scenario_name, self.session_id)
            gps.send_action_request_workers_to_register(publisher, self.session_id, scenario_name,
                                                        self.mp_server_host)
            time.sleep(u.WORKER_POLLING_REGISTRATION_REQUEST * 3)  # give it a bit of time
            logging.info('Checking for answers from workers')
            available_workers = gps.pull_workers_serving(subscriber, self.session_id)
            if available_workers:
                self.has_workers = True
                self._pre_allocate_callsigns_fg_targets(available_workers)
                logging.info('Workers serving: %s', str(available_workers))
            else:
                logging.info('No workers available. Serving only MP targets, no FG instances.')
                self.has_workers = False
        except Exception:
            logging.exception('Something went wrong in requesting workers. Collaboration with workers stopped')
            self.has_workers = False

    def _populate_assets(self, publisher: pubsub.PublisherClient) -> None:
        """Populates all missile assets and all non-moving assets - both FG targets and MP targets"""
        if self.has_workers:
            self._populate_carrier(publisher)
            self._populate_automats(publisher)  # next priority, so first
            self._populate_missile_ships(publisher)  # must come before SAMs due to fixed number from scenario
            self._populate_missile_sams(publisher)  # number is dynamic based on remaining capacity in fg_targets

    def _populate_carrier(self, publisher: pubsub.PublisherClient) -> None:
        """Populates the world with at most one MP carrier."""
        if self.scenario.carrier_definition is None:
            return
        if self.fg_instances.num_carriers == 0:
            return
        callsign_to_remove = None
        for callsign, container in self.fg_targets.items():
            if container.placeholder:
                # noinspection PyBroadException
                try:
                    my_callsign = callsign
                    if self.scenario.carrier_definition.use_tacan_callsign:
                        my_callsign = fgt.make_tacan_callsign(self.scenario.carrier_definition.carrier_type)
                    carrier_name = self.scenario.carrier_definition.carrier_type.name
                    gps.send_action_add_instance_carrier(publisher, self.session_id, container.worker_id,
                                                         carrier_name, my_callsign)
                    logging.info('Returned from GCP send_action_add_instance_carrier')
                    container.make_carrier(carrier_name)
                    container.last_position_update = m.PositionUpdate(my_callsign, carrier_name,
                                                                      m.HealthType.fit_for_fight,
                                                                      self.scenario.centre_position, 0)
                    if callsign != my_callsign:
                        self.fg_targets[my_callsign] = container
                        callsign_to_remove = callsign

                    self.scenario.has_active_carrier = True

                except Exception:
                    logging.exception('Could not send action to request a carrier instance')
                break  # no matter whether we succeed below - if it is broken it is broken the first time
        if callsign_to_remove:
            del self.fg_targets[callsign_to_remove]

    def _populate_automats(self, publisher: pubsub.PublisherClient) -> None:
        """Populates the world with automats (shooting fighter aircraft) running in a worker.
        The number of automats is static from the scenario. Depending on capacity in fg_assets all or some
        automats get created."""
        if self.scenario.automats is None or len(self.scenario.automats) == 0:
            return
        remaining_requested = min(len(self.scenario.automats), self.fg_instances.num_automats)
        random.shuffle(self.scenario.automats)
        for callsign, container in self.fg_targets.items():
            if remaining_requested == 0:
                break
            if container.placeholder:
                # noinspection PyBroadException
                try:
                    automat = self.scenario.automats[remaining_requested - 1]
                    props_string = automat.create_properties_string(callsign)
                    gps.send_action_add_instance_automat(publisher, self.session_id, container.worker_id,
                                                         automat.craft.name, callsign,
                                                         self.scenario.icao, automat.number_of_lives, props_string)
                    logging.info('Returned from GCP send_action_add_instance_automat')
                    container.make_automat(automat.craft.name)
                    container.last_position_update = m.PositionUpdate(callsign, automat.craft.name,
                                                                      m.HealthType.fit_for_fight,
                                                                      self.scenario.centre_position, 0)
                    remaining_requested -= 1
                except Exception:
                    logging.exception('Could not send action to request an automat instance')

    def _populate_missile_ships(self, publisher: pubsub.PublisherClient) -> None:
        """Populates the world with missile ships running in a worker.
        The number of ships is static from the scenario. Depending on capacity in fg_assets all or some
        ships get created."""
        remaining_requested = min(self.scenario.ships_missile, self.fg_instances.num_missile_ships)
        for callsign, container in self.fg_targets.items():
            if remaining_requested == 0:
                break
            if container.placeholder:
                # noinspection PyBroadException
                try:
                    gps.send_action_add_instance_ship(publisher, self.session_id, container.worker_id,
                                                      mpt.MISSILE_FRIGATE_NAME, callsign)
                    logging.info('Returned from GCP send_action_add_instance_ship')
                    container.make_frigate(mpt.MISSILE_FRIGATE_NAME)
                    # this is pure random and will get adjusted as soon as the ship sails
                    way_point = random.choice(list(self.scenario.ships_network.nodes))
                    container.last_position_update = m.PositionUpdate(callsign, mpt.MISSILE_FRIGATE_NAME,
                                                                      m.HealthType.fit_for_fight,
                                                                      way_point.make_position(), random.randint(0, 360))
                    remaining_requested -= 1
                except Exception:
                    logging.exception('Could not send action to request a missile ship instance')

    def _populate_missile_sams(self, publisher: pubsub.PublisherClient) -> None:
        """Populates the world with missile SAMs running in a worker.
        The SAMs in a scenario have a priority assigned: the higher the priority (1 is highest) the more likely
        a static asset will be transformed into a shooting SAM - as long as there is capacity in fg_targets.
        """

        remaining_requested = self.fg_instances.num_missile_sams
        prio_1 = list()
        prio_2 = list()
        prio_3 = list()
        for static_target in self.scenario.static_targets:
            if mpt.MPTarget.is_sam(static_target.target_type):
                if static_target.missile_priority == 0:
                    continue
                elif static_target.missile_priority == 1:
                    prio_1.append(static_target)
                elif static_target.missile_priority == 2:
                    prio_2.append(static_target)
                else:
                    prio_3.append(static_target)

        for callsign, container in self.fg_targets.items():
            if remaining_requested == 0:
                return
            if container.placeholder:
                if prio_1:
                    chosen_target = prio_1.pop()
                elif prio_2:
                    chosen_target = prio_2.pop()
                elif prio_3:
                    chosen_target = prio_3.pop()
                else:
                    break  # no point in looping if no priorities left

                remaining_requested -= 1

                # noinspection PyBroadException
                try:
                    gps.send_action_add_instance_sam(publisher, self.session_id, container.worker_id,
                                                     chosen_target.target_type, callsign,
                                                     chosen_target.position.lon, chosen_target.position.lat,
                                                     chosen_target.position.alt_m, chosen_target.heading)
                    logging.info('Returned from GCP send_action_add_instance_sam')
                    container.make_sam(chosen_target.target_type)
                    container.last_position_update = m.PositionUpdate(callsign, chosen_target.target_type,
                                                                      m.HealthType.fit_for_fight,
                                                                      chosen_target.position, chosen_target.heading)
                    self.scenario.static_targets.remove(chosen_target)
                except Exception:
                    logging.exception('Could not send action to request a SAM instance: %s', str(chosen_target))

    # noinspection PyBroadException
    def run(self) -> None:
        """Overrides the method in Process and runs the endless loop"""
        try:
            # for some reason configuring GCP logging is also needed within run()
            _ = gu.configure_gcp_logging('INFO', 'Controller')

            logging.info('Starting Controller processing')

            logging.info('Setting up receiver and start accepting messages')
            self._set_up_receiver()

            if u.use_datastore(self.cloud_integration_level):
                self.gcp_ds_runner = gds.GCPDSRunner(self.session_id)
                self.gcp_ds_runner.start()

            if u.use_pubsub(self.cloud_integration_level):
                self.gcp_ps_runner = gps.GCPPSRunner(self.session_id, None)
                self.gcp_ps_runner.start()

            # add static and towed mp_targets - moving will be added dynamically
            # need to be added in run-methods, such that daemon for FGMSRunner is not stopped
            for static_target in self.scenario.static_targets:
                name = static_target.target_type
                if self.hostility.sam_shoot and self.fg_instances.has_no_sams():
                    if mpt.MPTarget.is_sam(name):
                        name = mpt.MPTarget.SHILKA_IR
                self._add_static_target(name, static_target.position, static_target.heading)
            self._add_towed_targets()
            self._add_awacs()
            self._add_tanker()

            self.scenario.stamp_scenario_as_started()
        except:
            logging.exception('Something bad happened during start of Controller - most probably programming error')
            self._exit_session()

        try:
            while True:
                now = time.time()
                self.run_cycles += 1
                if self.run_cycles % 1000 == 0:
                    logging.info('Cycle %i in Controller', self.run_cycles)
                if self.parent_conn.poll():
                    message = self.parent_conn.recv()
                    logging.info('Controller got message from parent (start_controller): %s', message)
                    if isinstance(message, m.ExitSession):
                        self._exit_session()
                        return
                if self.fgms_receiver:
                    # potentially a lot since last main loop, especially because GCP can take time
                    while True:
                        message = self.fgms_receiver.receive_message()
                        if message is None:
                            break  # we are done polling data from GCP right now
                        if isinstance(message, m.ReceivedFGMSData):
                            self._receive_fgms_data(message.udp_package)
                        elif isinstance(message, m.RemoveDeadTarget):
                            logging.warning('FGMS for Receiver shot down. No point to continue. Exit.')
                            self._exit_session()
                            return
                        elif isinstance(message, m.HeartbeatReceiver):
                            logging.info('Received heartbeat from Receiver')
                            self.last_receiver_heartbeat = time.time()

                    if self.last_receiver_heartbeat > 0 and \
                            (now - self.last_receiver_heartbeat) > mpt.MP_RECEIVER_MAX_HEARTBEAT_WAIT:
                        logging.warning('No heartbeat received from Receiver - re-starting Receiver')
                        self._tear_down_receiver()
                        time.sleep(10)  # this is ca. the zombie time of MP
                        self._set_up_receiver()
                else:
                    logging.warning('No receiver object. Something is wrong in the logic.')
                # messaging from targets' FGMSSenders through Pipes
                to_remove = list()  # cannot remove during iteration
                for target_container in self.mp_targets.values():
                    while True:
                        message = target_container.broker.receive_message()
                        if message is None:
                            break  # we are done polling data from GCP right now
                        if isinstance(message, m.DamageResult):
                            self._register_damage(message)
                        elif isinstance(message, m.PositionUpdate):
                            target_container.last_position_update = message
                        elif isinstance(message, m.RemoveDeadTarget):
                            to_remove.append(message.callsign)

                for callsign in to_remove:
                    del self.mp_targets[callsign]
                if u.use_pubsub(self.cloud_integration_level):
                    while True:
                        data = self.gcp_ps_runner.receive_message()
                        if data is None:
                            break  # we are done polling data from GCP right now
                        logging.info('Receiving stuff from GCPPSRunner through queue: %s', str(data))
                        if data and isinstance(data, m.GCPSubData):
                            if data.content_dict[gps.ACTION] in [gps.ACTION_DEAD, gps.ACTION_CRASH]:
                                worker_id = data.worker_id
                                the_callsign = data.content_dict[gps.CALLSIGN]
                                self.fg_targets[the_callsign] = FGTargetContainer(worker_id, the_callsign)
                            elif data.content_dict[gps.ACTION] == gps.ACTION_DESTROYED:
                                target_container = self.fg_targets[data.content_dict[gps.CALLSIGN]]
                                if not target_container.placeholder:  # this message could arrive after dead
                                    target_container.health = m.HealthType.destroyed
                                    destroyed_damage = target_container.last_damage_result_as_destroyed()
                                    if destroyed_damage:
                                        self._register_damage(destroyed_damage)
                            else:
                                logging.warning('Programming error: action %s not known here',
                                                data.content_dict[gps.ACTION])

                if u.use_datastore(self.cloud_integration_level):
                    if (now - self.last_send_position_update_ds) > u.SEND_POSITION_UPDATES_TO_DS:
                        self.last_send_position_update_ds = now
                        position_updates = m.PositionUpdatesBatch()
                        for attacker in self.attackers.values():
                            if attacker and attacker.is_zombie() is False:
                                position_updates.add_position_update(attacker.last_position_update)
                        for target in self.mp_targets.values():
                            if target:  # not checking for zombie
                                position_updates.add_position_update(target.last_position_update)
                        for target in self.fg_targets.values():
                            if target and target.placeholder is False and target.is_zombie() is False:
                                position_updates.add_position_update(target.last_position_update)
                        self.gcp_ds_runner.post_message(position_updates)

                # check whether we should dynamically add another moving target
                if (now - self.last_next_moving_poll) > self.scenario.polling_freq:
                    self.last_next_moving_poll = now
                    self._poll_next_moving_targets()

                # datalink stuff
                self._process_datalink_data()
                # just shortly pause the endless loop; must be a low number due to maybe many MP items
                time.sleep(0.01)
                if (now - self.last_one_second_poll) > 1:
                    self.last_one_second_poll = now
                    self._poll_next_trip_targets()
                    # check whether an attacker has not been seen for a while
                    to_remove_attacker_callsigns = list()
                    for callsign, container in self.attackers.items():
                        if container.is_zombie():
                            to_remove_attacker_callsigns.append(callsign)
                    for callsign in to_remove_attacker_callsigns:
                        self._process_unregister_attacker(callsign, True)

        except Exception:
            logging.exception('Some exception during Controller.run() occurred. Exiting the session.')
            # FIXME should use more clear Exception to distinguish what to let go and what to catch
            self._exit_session()

    def _exit_session(self):
        """This should be the last thing happening. We close down stuff - even logging."""
        logging.info("Controller got exit command: shutting down dependencies and itself (takes at least 20 secs)")
        self._tear_down_receiver()
        # MP Targets
        for key, target_container in self.mp_targets.items():
            target_container.broker.post_message(m.TearDownTarget())
        if self.gcp_ps_runner:
            self.gcp_ps_runner.post_message(m.ExitSession())
        if self.gcp_ds_runner:
            self.gcp_ds_runner.post_message(m.ExitSession())
        # FG Targets and workers
        # No need to tear each single FG target down - that is done on worker side
        if self.has_workers:
            publisher_client = gu.construct_publisher_client()  # need to
            subscriber_client = gu.construct_subscriber_client()
            workers = set()
            for container in self.fg_targets.values():
                if container is not None:
                    workers.add(container.worker_id)
            for worker_id in workers:
                gps.send_action_worker_stop(publisher_client, self.session_id, worker_id)
                logging.info('Returned from GCP send_action_worker_stop')
            gps.handle_workers_to_ctrl_subscription(subscriber_client, False)
        # finish up
        self._print_total_stats()
        self.mp_targets.clear()
        self.fg_targets.clear()
        self.attackers.clear()

        # give all threads a bit time to finish up and asynchronously execute exit commands
        time.sleep(20)

        logging.info('FGMS interaction stopped')
        logging.shutdown()

    def _print_total_stats(self) -> None:
        """Prints the damage results to sys.out"""
        print('######## DAMAGE STATS ########')
        if len(self.local_damage_results) == 0:
            print('No damage registered')
        for damage_result in self.local_damage_results:
            print(damage_result)

        print('\n\n######## REMAINING STATS ########')
        remaining_ships = 0
        remaining_helis = 0
        remaining_drones = 0
        remaining_planes = 0
        remaining_towed = 0
        remaining_static = 0
        remaining_vehicles = 0
        for target_container in self.mp_targets.values():
            tgt = target_container.mp_target
            if tgt and tgt.target_type is mpt.MPTargetType.ship:
                remaining_ships += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.helicopter:
                remaining_helis += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.drone:
                remaining_drones += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.plane:
                remaining_planes += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.towed:
                remaining_towed += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.static:
                remaining_static += 1
            elif tgt and tgt.target_type is mpt.MPTargetType.vehicle:
                remaining_vehicles += 1
        print('Remaining ships: {}'.format(remaining_ships))
        print('Remaining helicopters: {}'.format(remaining_helis))
        print('Remaining drones: {}'.format(remaining_drones))
        print('Remaining planes: {}'.format(remaining_planes))
        print('Remaining towed: {}'.format(remaining_towed))
        print('Remaining static: {}'.format(remaining_static))
        print('Remaining vehicles: {}'.format(remaining_vehicles))

    def _poll_next_moving_targets(self) -> None:
        remaining_gas = 0
        remaining_ships = 0
        remaining_helis = 0
        remaining_drones = 0
        for target_container in self.mp_targets.values():
            tgt = target_container.mp_moving_target
            if tgt:
                if tgt.civilian:  # needs to be checked first because target_type can be many things
                    remaining_gas += 1
                elif tgt.target_type is mpt.MPTargetType.ship:
                    remaining_ships += 1
                elif tgt.target_type is mpt.MPTargetType.helicopter:
                    remaining_helis += 1
                elif tgt.target_type is mpt.MPTargetType.drone:
                    remaining_drones += 1

        add_message = self.scenario.next_moving_ship(remaining_ships, self.fg_instances.num_missile_ships > 0)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)
        add_message = self.scenario.next_moving_heli(remaining_helis)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)
        add_message = self.scenario.next_moving_drone(remaining_drones)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)
        add_message = self.scenario.next_moving_ga(remaining_gas)
        if add_message:
            self._add_moving_target(add_message.asset, add_message.path, add_message.start_node)

    def _poll_next_trip_targets(self) -> None:
        activated_trip_targets = self.scenario.next_trip_targets()
        if activated_trip_targets:
            for trip_target in activated_trip_targets:
                self._add_trip_target(trip_target)

    def _add_static_target(self, name: str, pos: g.Position, hdg: float) -> None:
        mp_target = mpt.MPTarget.create_target(name, pos, hdg)
        mp_target.callsign = self._create_callsign(mp_target.target_type.name)
        mp_target.iff_code = self.scenario.iff_opfor
        if mp_target.name == mpt.MPTarget.SHILKA:
            mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
            mp_target.shooter = mps.Shooter.create_shilka_shooter(mp_target, self.hostility.shilka_shoot)
        elif mp_target.name == mpt.MPTarget.SHILKA_IR:
            mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
            mp_target.shooter = mps.Shooter.create_missile_ir_sa_shooter(mp_target, self.hostility.sam_shoot)
        elif mpt.MPTarget.is_sam(mp_target.name):
            mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
            mp_target.shooter = mps.Shooter.create_missile_fake_shooter(mp_target)

        return self._add_target(mp_target)

    def _add_moving_target(self, asset: mpt.MPTargetAsset, path: nx.Graph, start_node: g.WayPoint,
                           override_speed: int = 0) -> None:
        mp_target = mpt.MPTargetMoving.create_moving_target(asset, path, start_node,
                                                            towing_speed=override_speed,
                                                            iff_code=self.scenario.iff_opfor)
        mp_target.callsign = self._create_callsign(mp_target.target_type.name)
        if asset.ttype is mpt.MPTargetType.helicopter:
            mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
            mp_target.dispenser = mpd.Dispenser(mp_target)
            if mp_target.is_missile_heli:
                mp_target.shooter = mps.Shooter.create_missile_ir_aa_shooter(mp_target, self.hostility.heli_shoot)
        elif asset.ttype == mpt.MPTargetType.drone:
            mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
            mp_target.dispenser = mpd.Dispenser(mp_target)
            mp_target.shooter = mps.Shooter.create_missile_ir_aa_shooter(mp_target, self.hostility.drone_shoot)
        elif asset.ttype == mpt.MPTargetType.ship:
            mp_target.attach_dem(dg.read_scenario_and_load_in(self.scenario))
            mp_target.shooter = mps.Shooter.create_kashtan_shooter(mp_target, self.hostility.ship_shoot)
        self._add_target(mp_target)

    def _add_awacs(self) -> None:
        if self.scenario.awacs_definition:
            start_node = random.choice(list(self.scenario.awacs_definition.awacs_network.nodes))
            asset = mpt.awacs_ec_137r
            mp_target = mpt.MPTargetMoving.create_moving_target(asset, self.scenario.awacs_definition.awacs_network,
                                                                start_node, iff_code=self.scenario.iff_attackers)
            mp_target.callsign = self.scenario.awacs_definition.callsign
            self.awacs_callsign = mp_target.callsign
            mp_target.dispenser = mpd.Dispenser(mp_target)
            self._add_target(mp_target, u.INDEX_AWACS)

    def _add_tanker(self) -> None:
        if self.scenario.tanker_definition:
            start_node = random.choice(list(self.scenario.tanker_definition.tanker_network.nodes))
            asset = mpt.tanker_kc_137r
            mp_target = mpt.MPTargetMoving.create_moving_target(asset, self.scenario.tanker_definition.tanker_network,
                                                                start_node, iff_code=self.scenario.iff_attackers)
            mp_target.callsign = self.scenario.tanker_definition.callsign
            self.tanker_callsign = mp_target.callsign
            mp_target.dispenser = mpd.Dispenser(mp_target)
            self._add_target(mp_target, u.INDEX_TANKER)

    def _add_trip_target(self, mp_target: mpt.MPTargetTrips) -> None:
        mp_target.callsign = self._create_callsign(mp_target.target_type.name)
        self._add_target(mp_target)
        logging.info('Added new trip target with activation delay = %i', mp_target.activation_delay)

    def _add_towed_target(self, asset: mpt.MPTargetAsset, path: nx.DiGraph, start_node: g.WayPoint,
                          override_speed: int = 0) -> None:
        towed_target = mpt.MPTargetMoving.create_moving_towed_target(asset, path, start_node, override_speed)
        towed_target.callsign = self._create_callsign(mpt.MPTargetType.towed.name)
        self._add_target(towed_target)

    def _add_towed_targets(self) -> None:
        """Add a series of towed targets.
        Towed targets are just moving targets with a lag, common speed, common start_node and common network.
        The network is directed and simple, such that the tractor and towed target always choose the same.
        There can be more than one tractor/towed combination - they just do not start at the same node.
        """
        if self.scenario.towed_number > 0:
            interval = int(len(self.scenario.towed_network.nodes)/self.scenario.towed_number)
            towed_lag = int(self.scenario.towed_dist / g.knots_to_ms(self.scenario.towed_speed))
            # add tractor towing the asset
            for i in range(self.scenario.towed_number):
                start_node = list(self.scenario.towed_network.nodes)[i * interval]
                self._add_moving_target(self.scenario.towing_asset, self.scenario.towed_network,
                                        start_node, self.scenario.towed_speed)
            # add distance between tractor and towed by using lag * constant speed
            time.sleep(towed_lag)
            # add the towed target
            for i in range(self.scenario.towed_number):
                start_node = list(self.scenario.towed_network.nodes)[i * interval]
                self._add_towed_target(self.scenario.towing_asset, self.scenario.towed_network,
                                       start_node, self.scenario.towed_speed)

    def _format_callsign(self, type_string: str, index: int) -> str:
        """Target callsign is always 7 letters and either hiding or revealing the type of the target plus 2 digits.
        If revealing: a combination of identifier (min 1 letter) + type (max 4 letters) + index (2 digits)
        If hiding: a 5-letter code + index (2 digits)
        Because the number of targets is limited, but targets are respawned, index is re-used."""
        callsign = self.identifier
        max_type = 5 - len(self.identifier)
        if type_string:
            if len(type_string) <= max_type:
                callsign += type_string
            else:
                callsign += type_string[:max_type]
        else:
            callsign += mpt.MPTargetType.unknown.name[:max_type]
        return callsign + '{:02d}'.format(index)

    def _pre_allocate_callsigns_fg_targets(self, available_workers: Dict[str, str]) -> None:
        """Pre-allocates callsigns for fgfs instance targets, so they are not taken by mp_targets at any time."""
        number = 0
        for capacity in available_workers.values():
            fg_instances = u.parse_fg_instances(capacity)
            number += fg_instances.total_instances()
        self.fg_targets = dict()
        pre_allocated_indexes = set()
        for pos in range(number):
            continue_search = True
            while continue_search:
                index = random.randint(u.INDEX_DYNAMIC_START, 99)
                if index not in pre_allocated_indexes:
                    pre_allocated_indexes.add(index)
                    continue_search = False

        for worker_id, capacity in available_workers.items():
            fg_instances = u.parse_fg_instances(capacity)
            for i in range(fg_instances.total_instances()):
                if pre_allocated_indexes:
                    callsign = self._format_callsign('fgfs', pre_allocated_indexes.pop())
                    self.fg_targets[callsign] = FGTargetContainer(worker_id, callsign)
            self.fg_instances.num_automats += fg_instances.num_automats
            self.fg_instances.num_carriers += fg_instances.num_carriers
            self.fg_instances.num_missile_ships += fg_instances.num_missile_ships
            self.fg_instances.num_missile_sams += fg_instances.num_missile_sams

    def _create_callsign(self, type_string: str) -> str:
        """Finds and allocates the index number of the callsign and then formats it to a valid callsign."""
        next_index = -1
        for digit in range(u.INDEX_DYNAMIC_START, 100):
            no_existing_found = True
            # search in targets' callsigns
            for key in self.fg_targets.keys():
                try:
                    my_digits = number_part_of_callsign(key)
                    if digit == my_digits:
                        no_existing_found = False
                        break
                except ValueError:
                    pass  # happens if there is a carrier or tanker, which does not have 2 digits at the end of callsign
            for key in self.mp_targets.keys():
                if self.tanker_callsign and key == self.tanker_callsign:
                    continue
                if self.awacs_callsign and key == self.awacs_callsign:
                    continue
                my_digits = number_part_of_callsign(key)
                if digit == my_digits:
                    no_existing_found = False
                    break

            if no_existing_found:
                # search in defender's callsign
                for callsign in self.defender_callsigns:
                    my_digits = number_part_of_callsign(callsign)
                    if digit == my_digits:
                        no_existing_found = False
                        break

            if no_existing_found:
                next_index = digit
                break

        if next_index < 0:
            raise ValueError('No valid index position found - too many targets')

        return self._format_callsign(type_string, next_index)

    def _add_target(self, mp_target: mpt.MPTarget, special_index: int = 0) -> None:
        index = special_index if special_index > 0 else number_part_of_callsign(mp_target.callsign)
        participant = fio.FGMSParticipant(mp_target, self.mp_server_host, u.PORT_MP_BASIS,
                                          u.PORT_MP_BASIS_CLIENT_CTRL + index)
        logging.info('%s has send_freq %04.2f with name %s', mp_target.callsign, mp_target.send_freq, mp_target.name)
        if mp_target.uses_multi_processing:
            logging.info('%s is using multiprocessing', mp_target.callsign)
            fgms_sender = fio.FGMSMultiProcessingBroker(participant)
        else:
            logging.info('%s is using threading', mp_target.callsign)
            fgms_sender = fio.FGMSThreadedBroker(participant)
            fgms_sender.start()
        self.mp_targets[mp_target.callsign] = MPTargetContainer(fgms_sender, mp_target)

    def _set_up_receiver(self) -> None:
        logging.info('Starting Receiver')
        receiver = mpt.MPTarget.create_receiver(self.callsign, self.scenario.centre_position, self.gci_responses,
                                                self.scenario.mp_visibility_range)
        participant = fio.FGMSParticipant(receiver, self.mp_server_host, u.PORT_MP_BASIS,
                                          u.PORT_MP_BASIS_CLIENT_CTRL + u.INDEX_CONTROLLER)
        self.fgms_receiver = fio.FGMSThreadedBroker(participant)
        self.fgms_receiver.start()
        self.last_receiver_heartbeat = 0

    def _tear_down_receiver(self) -> None:
        if self.fgms_receiver:
            self.fgms_receiver.post_message(m.TearDownTarget())
            self.fgms_receiver = None
        logging.info('Stopped Receiver')

    def _register_damage(self, message: m.DamageResult) -> None:
        logging.info('Message from target: %s', message)
        self.local_damage_results.append(message)
        if u.use_datastore(self.cloud_integration_level):
            self.gcp_ds_runner.post_message(message)

    def _receive_fgms_data(self, udp_packet) -> None:
        """Interpret data from FGMS. We are only interested in attackers, because they might shoot at targets.

        Callback function used in FGMSSender
        Adapted from ATCpie ext.fgms.update_FgmsAircraft_list and
        session.flightgearMP.FlightGearMultiPlayerSessionManager.receive_fgms_data."""
        self.number_of_mp_messages_received += 1
        if self.number_of_mp_messages_received % 2000 == 0:
            logging.info('Received %i MP messages until now', self.number_of_mp_messages_received)
        try:
            fgms_unpacked_data = fio.decode_fgms_data_packet(udp_packet, self.mp_targets)
        except ValueError as err:
            logging.warning('Ignoring packet: %s' % err)
            return

        if fgms_unpacked_data is None:
            return

        if fgms_unpacked_data.callsign in self.fg_targets:
            attacker_and_found = False
        elif fgms_unpacked_data.callsign in self.attackers:
            attacker_and_found = True
            if self.attackers[fgms_unpacked_data.callsign].fgfs_model != fgms_unpacked_data.fgfs_model:
                # flown aeroplane has changed - need to unregister and register again
                # this is more safe and accurate to real life than just updating the model in the container
                self._process_unregister_attacker(fgms_unpacked_data.callsign, False)
                self._process_register_attacker(fgms_unpacked_data.callsign, fgms_unpacked_data.fgfs_model)
        else:
            self._process_register_attacker(fgms_unpacked_data.callsign, fgms_unpacked_data.fgfs_model)
            attacker_and_found = True

        now = time.time()

        # do attacker specific stuff
        if attacker_and_found:  # process attackers all the time
            container = self.attackers[fgms_unpacked_data.callsign]
            if container.mp_time_stamp > 0 and (
                    fgms_unpacked_data.time_stamp < container.mp_time_stamp):
                logging.info('Dropping unordered package for %s due to negative time difference %f',
                             fgms_unpacked_data.callsign,
                             fgms_unpacked_data.time_stamp - container.mp_time_stamp)
                return  # Drop unordered UDP packet
            # make updates
            container.mp_time_stamp = fgms_unpacked_data.time_stamp
            container.fgfs_model = fgms_unpacked_data.fgfs_model  # pilot could have changed the plane
            self._process_attacker_gci_requests(fgms_unpacked_data.callsign, fgms_unpacked_data.gci_requests)
            self._process_emesary_notifications(fgms_unpacked_data.callsign, fgms_unpacked_data.encoded_notifications)
            container.newest_position = fgms_unpacked_data.position
            container.iff_hash = fgms_unpacked_data.iff_hash

            if (now - self.last_position_processed) > u.POS_UPDATE_FREQ_ATTACKER:
                self.last_position_processed = now
                container.process_position(fgms_unpacked_data.position, fgms_unpacked_data.callsign,
                                           container.fgfs_model)

            for mp_target_container in self.mp_targets.values():
                shooter = mp_target_container.mp_target_shooter
                dispenser = mp_target_container.mp_target_dispenser
                if shooter or dispenser:  # dispenser could also be a shooter - we do not want to send stuff twice
                    lpd = container.last_position_update
                    mp_target_container.broker.post_message(m.AttackerPosition(fgms_unpacked_data.callsign,
                                                                               fgms_unpacked_data.position,
                                                                               lpd.heading, lpd.pitch,
                                                                               fgms_unpacked_data.radar_lock,
                                                                               fgms_unpacked_data.radar_on,
                                                                               fgms_unpacked_data.chaff_on,
                                                                               fgms_unpacked_data.flares_on,
                                                                               fgms_unpacked_data.time_stamp,
                                                                               fgms_unpacked_data.iff_hash))

        # do the position updates (the ones used for data store etc.)
        if (now - self.last_position_processed) > u.POS_UPDATE_FREQ_FG_TARGET:
            self.last_position_processed = now
            if fgms_unpacked_data.callsign in self.fg_targets:
                container = self.fg_targets[fgms_unpacked_data.callsign]
                kind = container.name
                container.process_position(fgms_unpacked_data.position, fgms_unpacked_data.callsign, kind)

    def _process_register_attacker(self, callsign: str, fgfs_model: str) -> None:
        self.attackers[callsign] = AttackerContainer(fgfs_model, callsign)
        logging.info('Registered attacker %s flying %s', callsign, fgfs_model)

    def _process_unregister_attacker(self, callsign: str, disappeared: bool) -> None:
        if disappeared:
            logging.info('Unregistering %s flying %s: not seen for at least %i seconds', callsign,
                         self.attackers[callsign].fgfs_model, u.POS_UPDATE_FREQ_ZOMBIE_TIME)
        else:
            logging.info('Unregistering %s flying %s: changed aircraft?', callsign, self.attackers[callsign].fgfs_model)
        del self.attackers[callsign]

    def _process_emesary_notifications(self, req_callsign: str, encoded_notifications: Dict[int, Any]) -> None:
        """Process incoming Emesary notifications - mostly hit/collision messages.
        """
        if not encoded_notifications:
            return
        attacker_container = self.attackers[req_callsign]
        decoded_notifications = dict()
        for value in encoded_notifications.values():
            if value != '':
                # noinspection PyBroadException
                try:
                    decoded_notifications.update(en.process_incoming_message(value))
                except Exception:
                    logging.error('Cannot process emesary notification for %s', req_callsign)
        for idx, notification in decoded_notifications.items():
            if attacker_container.check_existing_emesary_index(idx):
                continue
            logging.info('New msg_idx_type_id=%s -> msg=%s', idx, notification)
            if notification.type_id == en.ArmamentNotification_Id and notification.kind == en.KIND_COLLISION:
                target_found = False
                # first test the MP targets
                for key, mp_container in self.mp_targets.items():
                    if mp_container.emesary_callsign == notification.remote_callsign:
                        logging.info('Processing armament notification from %s for MP target %s: %s',
                                     req_callsign, key, notification)
                        mp_container.broker.post_message(m.HitNotification(notification, req_callsign,
                                                                           attacker_container.fgfs_model, key))
                        target_found = True
                        break
                # next test the FG targets
                if not target_found:
                    for key, fg_container in self.fg_targets.items():
                        if fg_container.emesary_callsign == notification.remote_callsign:
                            logging.info('Processing armament notification for FG target %s: %s', key, notification)
                            weapon_type = d.weapon_type_from_notification(notification, fg_container.target_type)
                            health = m.HealthType.hit  # we do not know better
                            fg_container.health = health
                            damage = m.DamageResult(health, weapon_type, req_callsign, attacker_container.fgfs_model,
                                                    key, fg_container.name, False, True, fg_container.shooting)
                            fg_container.last_damage_result = damage
                            self._register_damage(damage)
            elif notification.type_id == en.ArmamentInFlightNotification_Id:
                for key, mp_container in self.mp_targets.items():
                    if mp_container.emesary_callsign == notification.remote_callsign:
                        dispenser = mp_container.mp_target_dispenser
                        if dispenser:
                            mp_container.broker.post_message(notification)
                            logging.info('Processing armament in flight notification from %s for MP target %s: %s',
                                         req_callsign, key, notification)
                        break
            elif notification.type_id == en.ObjectInFlightNotification_Id:
                pass  # currently we do nothing - Chaff/Flare is only visual info - shooters process another property

    def _process_attacker_gci_requests(self, req_callsign: str, gci_requests: List[Optional[bool]]) -> None:
        """Some OPRF planes can request information from GCI. Process the request and reply via MP.

        Documentation: MiG-21bis/Nasal/gci-listeners.nas; oprf-assets/gic-radar/Nasal/gci.nas

        In oprf_assets/GCI-radar/Nasal/gci.nas:
        * func check_requests : message format plus calculation of bearing, range, altitude
            * line 292 ff. PICTURE

        In MiG-21bis:
            * MiG-21-set-common.xml -> bool[0] alias /instrumentation/gci/picture
            * Systems/keyboard.xml: key n=251 (F5) sets /instrumentation/gci/picture

        """
        if not self.gci_responses:
            return
        attacker_container = self.attackers[req_callsign]
        response = attacker_container.process_gci_request(gci_requests)
        if response:
            prev_target_callsign = attacker_container.get_last_requested_target(response)
            next_target_callsign = None
            closest_distance = 9999999
            # mp targets
            for callsign, mpt_container in self.mp_targets.items():
                tgt = mpt_container.mp_target
                if tgt is not None:  # static target is None
                    if mpt_container.health in [m.HealthType.destroyed, m.HealthType.dead]:
                        continue
                    if tgt.civilian:
                        continue
                    elif prev_target_callsign is not None and callsign == prev_target_callsign:
                        next_target_callsign = callsign
                        break
                    elif response is m.GCIResponseType.picture and tgt.target_type is mpt.MPTargetType.ship:
                        distance = g.calc_distance_pos(attacker_container.position, mpt_container.position)
                        if distance < closest_distance:
                            closest_distance = distance
                            next_target_callsign = callsign
                    elif response is m.GCIResponseType.bogey_dope and tgt.target_type is mpt.MPTargetType.vehicle and (
                            self.scenario.gci_allowed_vehicles is None or
                            tgt.name in self.scenario.gci_allowed_vehicles):
                        distance = g.calc_distance_pos(attacker_container.position, mpt_container.position)
                        if distance < closest_distance:
                            closest_distance = distance
                            next_target_callsign = callsign
                    elif response is m.GCIResponseType.cutoff and tgt.target_type in [mpt.MPTargetType.helicopter,
                                                                                      mpt.MPTargetType.drone]:
                        _, eta = g.calc_intercept(attacker_container.position, attacker_container.speed,
                                                  mpt_container.position, mpt_container.speed, mpt_container.heading,
                                                  GCI_SIMPLIFIED_INTERCEPT)
                        if 0 < eta < closest_distance:  # if eta = 0 then cannot be reached by interceptor
                            closest_distance = eta
                            next_target_callsign = callsign

            # fg targets
            if next_target_callsign != prev_target_callsign:
                for callsign, fgt_container in self.fg_targets.items():
                    if not fgt_container.placeholder:
                        if fgt_container.health in [m.HealthType.destroyed, m.HealthType.dead]:
                            continue
                        elif prev_target_callsign is not None and callsign == prev_target_callsign:
                            next_target_callsign = callsign
                            break
                        elif response is m.GCIResponseType.picture and \
                                fgt_container.target_type is mpt.MPTargetType.ship:
                            distance = g.calc_distance_pos(attacker_container.position, fgt_container.position)
                            if distance < closest_distance:
                                closest_distance = distance
                                next_target_callsign = callsign
                        elif response is m.GCIResponseType.bogey_dope and \
                                fgt_container.target_type is mpt.MPTargetType.vehicle:  # we know it is a SAM
                            distance = g.calc_distance_pos(attacker_container.position, fgt_container.position)
                            if distance < closest_distance:
                                closest_distance = distance
                                next_target_callsign = callsign

            if next_target_callsign is None:
                self.fgms_receiver.post_message(m.GCIResponse(m.GCIResponseType.no_info_avail, req_callsign))
            else:
                if next_target_callsign in self.mp_targets:
                    container = self.mp_targets[next_target_callsign]
                else:
                    container = self.fg_targets[next_target_callsign]
                attacker_container.set_last_requested_target(next_target_callsign, response)
                gci_resp = m.GCIResponse(response, req_callsign)
                dist = g.calc_distance_pos(attacker_container.position, container.position)
                gci_resp.set_range(dist)
                altitude = container.position.alt_m
                gci_resp.set_altitude_m(altitude)
                bearing = g.calc_bearing_pos(attacker_container.position, container.position)
                gci_resp.set_bearing(bearing)
                aspect = g.calc_aspect(bearing, container.heading)
                gci_resp.set_aspect(aspect)
                if response is m.GCIResponseType.cutoff:
                    vector, eta = g.calc_intercept(attacker_container.position,
                                                   attacker_container.speed,
                                                   container.position, container.speed, container.heading,
                                                   GCI_SIMPLIFIED_INTERCEPT)
                    gci_resp.set_vector(vector)
                    gci_resp.set_eta_s(eta)
                self.fgms_receiver.post_message(gci_resp)
                if response is m.GCIResponseType.picture:
                    self.fgms_receiver.post_message(m.GCIResponse(m.GCIResponseType.all_sent, req_callsign))
        # else do nothing

    def _process_datalink_data(self) -> None:
        """Prepare the datalink data for AWACS - but only if there is an AWACS and not all the time."""
        now = time.time()
        if self.awacs_callsign and (now - self.last_datalink_processed) > 10:
            self.last_datalink_processed = now
            data = dict()
            for callsign, container in self.attackers.items():
                data[callsign] = ifd.interrogate(callsign, container.iff_hash, self.scenario.iff_attackers)
            for callsign, container in self.mp_targets.items():
                if container.mp_target and container.mp_target.iff_code:
                    # we only show flying stuff, e.g. no shooting ships or shooting SAMs
                    if container.mp_target.target_type in [mpt.MPTargetType.helicopter,
                                                           mpt.MPTargetType.drone,
                                                           mpt.MPTargetType.plane]:
                        if container.mp_target.iff_code == self.scenario.iff_attackers:
                            data[callsign] = ifd.IFFType.iff_friendly
                        else:
                            data[callsign] = ifd.IFFType.iff_hostile
            # no processing of fg_targets because SAMs/ships do not have IFF and
            # automats are not yet implemented
            encoded_data = ifd.encode_datalink_data(self.awacs_callsign, self.scenario.datalink_attackers, data)

            for callsign, container in self.mp_targets.items():
                if callsign == self.awacs_callsign:
                    container.broker.post_message(m.DatalinkData(encoded_data))
