"""Unit tests which can not execute within seconds and might take several minutes"""
import os
import unittest

import hunter.scenario_tools.dem_grid as dg
import hunter.geometry as g
import hunter.scenarios as sc
import hunter.utils as u


class TestDEMCalculations(unittest.TestCase):
    def setUp(self):
        """Uses the test scenario near ENNA. Some tests are based on knowledge of the topography.
        Grid size in metres longitudinal: 39.893338
        Grid size in metres latitude: 55.659745
        The memory size of the resulting 2D array is 16 MiB plus some overhead
        8,000,000 data points
        """
        u.read_dotenv()
        scenario = sc.load_scenario(os.getenv('HUNTER_SCENARIO_NAME'), os.getenv('HUNTER_SCENARIOS_PATH'))
        self.dem = dg.read_scenario_and_load_in(scenario)

    def test_dem_available(self):
        """Make sure we actually have a valid DEM, as some tests will depend on it."""
        self.assertIsNotNone(self.dem, "The DEM has not been set up correctly.")

    def test_read_scenario_and_load_in(self):
        shape_tuple = self.dem.shape
        self.assertEqual(shape_tuple[0], 2000, 'Shape size longitudinal')
        self.assertEqual(shape_tuple[1], 2000, 'Shape size latitudinal')

    def test_look_up_elevation(self):
        self.assertEquals(0, self.dem.look_up_elevation((23, 70)), 'To the left of the DEM')
        self.assertEquals(0, self.dem.look_up_elevation((25, 72)), 'To the North of the DEM')

        self.assertLess(self.dem.look_up_elevation((25., 70.08)), 1, 'Sea level ')
        self.assertGreater(self.dem.look_up_elevation((25., 70.05)), 0, 'Land area')
        self.assertGreater(self.dem.look_up_elevation((24.5, 70.)), 500, 'Mountain area')

    def test_create_list_of_points_on_line(self):
        positions = self.dem._create_list_of_positions_on_line(g.Position(25, 70),
                                                               g.Position(25 + .8 * self.dem.spacing,
                                                                          70))
        self.assertEqual(0, len(positions), 'Very short distance to right')
        positions = self.dem._create_list_of_positions_on_line(g.Position(25, 70),
                                                               g.Position(25 - .8 * self.dem.spacing,
                                                                          70))
        self.assertEqual(0, len(positions), 'Very short distance to left')
        positions = self.dem._create_list_of_positions_on_line(g.Position(25, 70),
                                                               g.Position(25 + 3.8 * self.dem.spacing,
                                                                          70))
        self.assertEqual(3, len(positions), 'Several points to the right')
        positions = self.dem._create_list_of_positions_on_line(g.Position(25, 70),
                                                               g.Position(25,
                                                                          70 - 3.8 * self.dem.spacing))
        self.assertEqual(3, len(positions), 'Several points downwards')

    def test_calc_below_horizon(self):
        self.assertEqual(0., dg._calc_below_horizon(g.Position(25., 70.), g.Position(25., 70.)), 'Same position')
        self.assertEqual(dg._calc_below_horizon(g.Position(24.9, 70.), g.Position(25., 70.)),
                         dg._calc_below_horizon(g.Position(25.1, 70.), g.Position(25., 70.)), 'Same position opposite')
        self.assertGreater(dg._calc_below_horizon(g.Position(25., 70.), g.Position(25., 70.1)), 0., 'Below zero')

    def test_is_visible(self):
        self.assertFalse(self.dem.is_visible(g.Position(23., 70.), g.Position(25., 70.)), 'Start outside of DEM')
        self.assertFalse(self.dem.is_visible(g.Position(23., 70.), g.Position(25., 71.)), 'End outside of DEM')
        self.assertTrue(self.dem.is_visible(g.Position(25., 70.), g.Position(25., 70.)), 'Same position')
        # at 10 km below horizon is ca. 7,85 metres
        self.assertFalse(self.dem.is_visible(g.Position(25.39, 70.375, 1.), g.Position(25.5, 70.46, 1.)),
                         'Ca. 10 km distance on water start at 1m and end at 1m alt')
        self.assertTrue(self.dem.is_visible(g.Position(25.39, 70.375, 5.), g.Position(25.5, 70.46, 5.)),
                        'Ca. 10 km distance on water start at 5m and end at 5m alt')
        self.assertTrue(self.dem.is_visible(g.Position(25.39, 70.375, 10.), g.Position(25.5, 70.46, 3.)),
                        'Ca. 10 km distance on water start at 10m and end at 3m alt')
        self.assertTrue(self.dem.is_visible(g.Position(25.39, 70.375, 2.), g.Position(25.5, 70.46, 10.)),
                        'Ca. 10 km distance on water start at 2m and end at 10m alt')
        self.assertTrue(self.dem.is_visible(g.Position(25.5, 70.46, 10.), g.Position(25.39, 70.375, 2.)),
                        'Ca. 10 km distance on water - switch start/end')

