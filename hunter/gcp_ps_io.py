"""
Module to interact with the Google Cloud pub/sub services.


Cf. Cloud Pub/Sub:
    * https://cloud.google.com/pubsub/docs/
    * https://googleapis.dev/python/pubsub/latest/index.html
"""
import datetime
import json
import logging
import queue
import sys
import time
from typing import Any, Dict, List, Optional, Tuple

from google import pubsub_v1
import google.api_core.exceptions as gex

import hunter.gcp_utils as gu
import hunter.geometry as g
import hunter.messages as m
import hunter.utils as u


# TOPIC: all topics must already exist - they are not dynamically creates
TOPIC_CTRL_TO_WORKERS = 'ctrl_to_workers'  # ctrl writes, each worker creates subscription and reads
TOPIC_WORKERS_TO_CTRL = 'workers_to_ctrl'  # each worker writes, ctrl has one subscription

TOPIC_CAPTAIN_TO_CARRIER = 'captain_to_carrier'

SUBSCRIBER_CONTROLLER_NAME = 'ctrl'
SUBSCRIBER_WORKER_NAME = 'worker'  # the specific name will have the worker's IP-address added
SUBSCRIBER_CARRIER_NAME = 'carrier'

ACTION = 'action'
ACTION_REGISTER = 'register'
ACTION_SERVING = 'serving'
ACTION_STOP = 'stop'
ACTION_INSTANCE_SAM = 'instance_sam'
ACTION_INSTANCE_SHIP = 'instance_ship'
ACTION_INSTANCE_AUTOMAT = 'instance_automat'
ACTION_INSTANCE_CARRIER = 'instance_carrier'
ACTION_CAPTAIN_ORDER = 'captain_order'
ACTION_CRASH = 'crash'
ACTION_DEAD = 'dead'
ACTION_DESTROYED = 'destroyed'
CALLSIGN = 'callsign'
ASSET_NAME = 'asset_name'
WORKER_ID = 'worker_id'
MAX_INSTANCES = 'max_instances'
LONGITUDE = 'lon'
LATITUDE = 'lat'
ALTITUDE_METRES = 'alt-m'
HEADING = 'heading'
SPEED = 'speed'
PROPS_STRING = 'props_string'
ICAO = 'icao'
SCENARIO_NAME = 'scenario_name'
NUMBER_OF_LIVES = 'number_of_lives'
MP_SERVER_HOST = 'mp_server_host'

ATTRIBUTE_WORKER_ID = 'worker_id'
ATTRIBUTE_SESSION_ID = 'session_id'
ATTRIBUTE_SCENARIO = 'scenario'

# Carrier stuff
CARRIER_COURSE_TYPE = 'carrier_course_type'
CARRIER_NAVIGATE_COURSE_DEG = 'carrier_navigate_course_deg'
CARRIER_DECK_LIGHTS = 'carrier_deck_lights'
CARRIER_FLOOD_LIGHTS = 'carrier_flood_lights'


UTF_8_ENCODING = 'utf-8'


def position_from_content_dict(data: Dict[str, Any]) -> g.Position:
    return g.Position(data[LONGITUDE], data[LATITUDE], data[ALTITUDE_METRES])


def _send_action(publisher: pubsub_v1.PublisherClient, topic_name: str, content: Dict[str, Any],
                 session_id: Optional[str], worker_id: Optional[str]) -> bool:
    """Sends an action between Controller and Worker(s). using GCP pub/sub.
    Because these types of messages are not so frequent, it is ok to wait for the result."""
    try:
        payload = json.dumps(content)
        topic_path = publisher.topic_path(gu.get_project_id(), topic_name)
        if worker_id:
            future = publisher.publish(topic_path, payload.encode(UTF_8_ENCODING),
                                       session_id=session_id, worker_id=worker_id)
        elif session_id:
            future = publisher.publish(topic_path, payload.encode(UTF_8_ENCODING),
                                       session_id=session_id)
        else:
            future = publisher.publish(topic_path, payload.encode(UTF_8_ENCODING))
        _ = future.result()
        return True
    except RuntimeError:  # because we will not do anything about it during runtime
        logging.exception('GCP publish action failed. Content: %s', str(content))
    return False


def send_action_request_workers_to_register(publisher: pubsub_v1.PublisherClient,
                                            session_id: str, scenario_name: str,
                                            mp_server_host: str) -> None:
    content_dict = {ACTION: ACTION_REGISTER, SCENARIO_NAME: scenario_name,
                    MP_SERVER_HOST: mp_server_host}
    _send_action(publisher, TOPIC_CTRL_TO_WORKERS, content_dict, session_id, None)


MAX_ALLOWED_SESSION_AGE = 60  # seconds: it should never take longer than that from the moment it was sent to received


def _create_ctrl_to_worker_subscription_path(subscriber: pubsub_v1.SubscriberClient, worker_id: str) -> str:
    subscription_name = _create_subscription_name(TOPIC_CTRL_TO_WORKERS, _create_worker_subscriber_name(worker_id))
    return subscriber.subscription_path(gu.get_project_id(), subscription_name)


def _create_default_pull_request_dict(subscription_path: str) -> Dict[str, Any]:
    return {
        "subscription": subscription_path,
        "max_messages": 10,
        "return_immediately": True}


def _create_default_acknowledge_dict(subscription_path: str, ack_ids) -> Dict[str, Any]:
    return {
        "subscription": subscription_path,
        "ack_ids": ack_ids
    }


def pull_ctlr_registration_requests(subscriber: pubsub_v1.SubscriberClient,
                                    worker_id: str) -> Tuple[Optional[str], Optional[str], Optional[str]]:
    """Pull for requests from controller to register and save the controller's session_id.
    Returns None if there was no request or the request was unlikely from a current session."""
    subscription_path = _create_ctrl_to_worker_subscription_path(subscriber, worker_id)
    pull_responses = subscriber.pull(_create_default_pull_request_dict(subscription_path))
    ack_ids = []
    the_session_id = None
    the_scenario_name = None
    the_mp_server_host = None
    for response in pull_responses.received_messages:
        ack_ids.append(response.ack_id)
        # make sure it is a valid session and nothing old. This time the worker has not yet stored the session
        session_id = response.message.attributes[ATTRIBUTE_SESSION_ID]
        difference = (time.time_ns() - int(session_id)) / 1000000000
        if difference < MAX_ALLOWED_SESSION_AGE:
            the_session_id = session_id
            data = json.loads(response.message.data)
            the_scenario_name = data[SCENARIO_NAME]
            the_mp_server_host = data[MP_SERVER_HOST]
    if ack_ids:
        subscriber.acknowledge(_create_default_acknowledge_dict(subscription_path, ack_ids))
    return the_session_id, the_scenario_name, the_mp_server_host


def send_action_register_worker(publisher: pubsub_v1.PublisherClient, session_id: str,
                                worker_id: str, max_instances: str) -> None:
    content_dict = {ACTION: ACTION_SERVING, MAX_INSTANCES: max_instances}
    _send_action(publisher, TOPIC_WORKERS_TO_CTRL, content_dict, session_id, worker_id)


WAIT_TIME_WORKERS_SERVING_RESPONSE = 20  # seconds


def _create_workers_to_ctrl_subscription_path(subscriber: pubsub_v1.SubscriberClient) -> str:
    subscription_name = _create_subscription_name(TOPIC_WORKERS_TO_CTRL, SUBSCRIBER_CONTROLLER_NAME)
    return subscriber.subscription_path(gu.get_project_id(), subscription_name)


def pull_workers_serving(subscriber: pubsub_v1.SubscriberClient, session_id: str) -> Dict[str, str]:
    """Check which workers are available and response within the time limit."""
    worker_ids = dict()
    time.sleep(WAIT_TIME_WORKERS_SERVING_RESPONSE)
    subscription_path = _create_workers_to_ctrl_subscription_path(subscriber)
    # pulling several times to make sure all messages received if different batches
    for _ in range(5):
        pull_responses = subscriber.pull(_create_default_pull_request_dict(subscription_path))
        ack_ids = []
        for response in pull_responses.received_messages:
            ack_ids.append(response.ack_id)
            logging.info('Worker response for session %s (my session %s)',
                         response.message.attributes[ATTRIBUTE_SESSION_ID], session_id)
            if response.message.attributes[ATTRIBUTE_SESSION_ID] == session_id:
                data = json.loads(response.message.data)
                if data[ACTION] == ACTION_SERVING:
                    worker_ids[response.message.attributes[ATTRIBUTE_WORKER_ID]] = data[MAX_INSTANCES]
        if ack_ids:
            subscriber.acknowledge(_create_default_acknowledge_dict(subscription_path, ack_ids))
        time.sleep(1)
    return worker_ids


def send_action_worker_stop(publisher: pubsub_v1.PublisherClient, session_id, worker_id: str) -> None:
    content_dict = {ACTION: ACTION_STOP}
    _send_action(publisher, TOPIC_CTRL_TO_WORKERS, content_dict, session_id, worker_id)


def send_action_add_instance_sam(publisher: pubsub_v1.PublisherClient, session_id: str, worker_id: str,
                                 asset_name: str, callsign: str,
                                 lon: float, lat: float, alt_m: float, heading: float) -> None:
    content_dict = {ACTION: ACTION_INSTANCE_SAM, ASSET_NAME: asset_name, CALLSIGN: callsign,
                    LONGITUDE: lon, LATITUDE: lat, ALTITUDE_METRES: alt_m, HEADING: heading}
    _send_action(publisher, TOPIC_CTRL_TO_WORKERS, content_dict, session_id, worker_id)


def send_action_add_instance_ship(publisher: pubsub_v1.PublisherClient, session_id: str, worker_id: str,
                                  asset_name: str, callsign: str) -> None:
    content_dict = {ACTION: ACTION_INSTANCE_SHIP, ASSET_NAME: asset_name, CALLSIGN: callsign}
    _send_action(publisher, TOPIC_CTRL_TO_WORKERS, content_dict, session_id, worker_id)


def send_action_add_instance_automat(publisher: pubsub_v1.PublisherClient, session_id: str, worker_id: str,
                                     asset_name: str, callsign: str, icao: str, number_of_lives: int,
                                     props_string: str) -> None:
    content_dict = {ACTION: ACTION_INSTANCE_AUTOMAT, ASSET_NAME: asset_name, CALLSIGN: callsign,
                    ICAO: icao, NUMBER_OF_LIVES: number_of_lives, PROPS_STRING: props_string}
    _send_action(publisher, TOPIC_CTRL_TO_WORKERS, content_dict, session_id, worker_id)


def send_action_add_instance_carrier(publisher: pubsub_v1.PublisherClient, session_id: str, worker_id: str,
                                     asset_name: str, callsign: str) -> None:
    content_dict = {ACTION: ACTION_INSTANCE_CARRIER, ASSET_NAME: asset_name, CALLSIGN: callsign}
    _send_action(publisher, TOPIC_CTRL_TO_WORKERS, content_dict, session_id, worker_id)


def pull_ctrl_other_action(subscriber: pubsub_v1.SubscriberClient,
                           session_id: str, worker_id: str) -> List[Dict[str, Any]]:
    subscription_path = _create_ctrl_to_worker_subscription_path(subscriber, worker_id)
    pull_responses = subscriber.pull(_create_default_pull_request_dict(subscription_path))
    ack_ids = []
    data_dicts = list()
    for response in pull_responses.received_messages:
        ack_ids.append(response.ack_id)
        a_session_id = response.message.attributes[ATTRIBUTE_SESSION_ID]
        a_worker_id = response.message.attributes[ATTRIBUTE_WORKER_ID]
        logging.info('Controller message for session %s (my session %s)',
                     a_session_id, session_id)
        if a_session_id == session_id and a_worker_id == worker_id:
            data_dicts.append(json.loads(response.message.data))
    if ack_ids:
        subscriber.acknowledge(_create_default_acknowledge_dict(subscription_path, ack_ids))
    return data_dicts


def send_action_instance_destroyed(publisher: pubsub_v1.PublisherClient, session_id: str, worker_id: str,
                                   callsign: str) -> None:
    """Worker asks to update the health of an instance in the controller to destroyed."""
    content_dict = {ACTION: ACTION_DESTROYED, CALLSIGN: callsign}
    _send_action(publisher, TOPIC_WORKERS_TO_CTRL, content_dict, session_id, worker_id)


def send_action_instance_down(publisher: pubsub_v1.PublisherClient, session_id: str, worker_id: str,
                              callsign: str, error_situation: bool = False) -> None:
    """Worker asks to get an instance removed in controller.
    Error situation True means that an exception occurred around an FG target. Otherwise, it was shot by an attacker."""
    my_action = ACTION_CRASH if error_situation else ACTION_DEAD
    content_dict = {ACTION: my_action, CALLSIGN: callsign}
    _send_action(publisher, TOPIC_WORKERS_TO_CTRL, content_dict, session_id, worker_id)


def _create_worker_subscriber_name(worker_id: str) -> str:
    return 'worker_{}'.format(worker_id)


def _create_subscription_name(topic: str, subscriber: str) -> str:
    """The subscription name is always TOPIC_NAME-SUBSCRIBER_NAME.
    This is a convention used in Hunter, not enforced by GCP.
    See also rules for resource names: https://cloud.google.com/pubsub/docs/admin#resource_names
    """
    return '{}-{}'.format(topic, subscriber)


# noinspection PyBroadException
def _handle_subscription(subscriber: pubsub_v1.SubscriberClient, subscription_name: str, topic_name: str,
                         create_action: bool) -> bool:
    """Creates or deletes a subscription for the WORKERS_TO_CTRL topic (always for the controller)."""
    project_id = gu.get_project_id()
    topic_path = 'projects/{project_id}/topics/{topic}'.format(
        project_id=project_id,
        topic=topic_name,  # Set this to something appropriate.
    )
    subscription_path = 'projects/{project_id}/subscriptions/{sub}'.format(
        project_id=project_id,
        sub=subscription_name,  # Set this to something appropriate.
    )

    if create_action:
        try:
            _ = subscriber.create_subscription(request={"name": subscription_path, "topic": topic_path})
            logging.info('Created subscription %s', subscription_path)
            return True
        except gex.AlreadyExists:
            logging.info('Subscription %s already exists and therefore not created', subscription_path)
            return True
        except Exception:
            logging.exception('Could not create subscription for %s', subscription_path)
            return False
    else:
        try:
            subscriber.delete_subscription(request={"subscription": subscription_path})
            logging.info('Deleted subscription %s', subscription_path)
            return True
        except gex.NotFound:
            logging.info('Subscription %s not found and therefore not deleted', subscription_path)
            return True
        except Exception:
            logging.exception('Could not delete subscription for %s', subscription_path)
            return False


def handle_workers_to_ctrl_subscription(subscriber: pubsub_v1.SubscriberClient, create_action: bool) -> bool:
    """Creates or deletes a subscription for the WORKERS_TO_CTRL topic (always for the controller)."""
    subscription_name = _create_subscription_name(TOPIC_WORKERS_TO_CTRL, SUBSCRIBER_CONTROLLER_NAME)
    return _handle_subscription(subscriber, subscription_name, TOPIC_WORKERS_TO_CTRL, create_action)


def handle_ctrl_to_workers_subscription(subscriber: pubsub_v1.SubscriberClient, worker_id: str,
                                        create_action: bool) -> bool:
    """Creates or deletes a subscription for the WORKERS_TO_CTRL topic (always for the controller)."""
    subscriber_name = _create_worker_subscriber_name(worker_id)
    subscription_name = _create_subscription_name(TOPIC_CTRL_TO_WORKERS, subscriber_name)
    return _handle_subscription(subscriber, subscription_name, TOPIC_CTRL_TO_WORKERS, create_action)


def handle_captain_to_carrier_subscription(subscriber: pubsub_v1.SubscriberClient, create_action: bool) -> bool:
    """Creates or deletes a subscription for the CAPTAIN_TO_CARRIER topic (always for the controller)."""
    subscription_name = _create_subscription_name(TOPIC_CAPTAIN_TO_CARRIER, SUBSCRIBER_CARRIER_NAME)
    return _handle_subscription(subscriber, subscription_name, TOPIC_CAPTAIN_TO_CARRIER, create_action)


def send_action_captain_order(publisher: pubsub_v1.PublisherClient, course_type: int, navigate_course_deg: int,
                              deck_lights: bool, flood_lights: bool) -> bool:
    content_dict = {ACTION: ACTION_CAPTAIN_ORDER, CARRIER_COURSE_TYPE: course_type,
                    CARRIER_NAVIGATE_COURSE_DEG: navigate_course_deg,
                    CARRIER_DECK_LIGHTS: deck_lights, CARRIER_FLOOD_LIGHTS: flood_lights}
    return _send_action(publisher, TOPIC_CAPTAIN_TO_CARRIER, content_dict, None, None)


def _pull_carrier_captain_orders(subscriber: pubsub_v1.SubscriberClient) -> Optional[Dict[str, Any]]:
    """Check whether we get a new course command from the captain for the carrier."""
    subscription_name = _create_subscription_name(TOPIC_CAPTAIN_TO_CARRIER, SUBSCRIBER_CARRIER_NAME)
    subscription_path = subscriber.subscription_path(gu.get_project_id(), subscription_name)
    pull_responses = subscriber.pull(_create_default_pull_request_dict(subscription_path))
    ack_ids = []
    utc_timezone = datetime.timezone.utc
    newest = datetime.datetime(datetime.MINYEAR, 1, 1, tzinfo=utc_timezone)  # very lon ago
    order = None
    for response in pull_responses.received_messages:
        ack_ids.append(response.ack_id)
        logging.info('Request from captain')
        publish_time = response.message.publish_time
        if publish_time > newest:
            newest = publish_time
            order = json.loads(response.message.data)
    if ack_ids:
        subscriber.acknowledge(_create_default_acknowledge_dict(subscription_path, ack_ids))
    return order


# noinspection PyBroadException
def create_all_topics_for_testing(publisher: pubsub_v1.PublisherClient) -> None:
    """Only used for testing because PubSub emulator forgets all history incl. configuration when restarted.
    As the topics are a pre-requisite for all other testing, exceptions are not handled."""
    topics = [TOPIC_CTRL_TO_WORKERS, TOPIC_WORKERS_TO_CTRL, TOPIC_CAPTAIN_TO_CARRIER]
    for topic_name in topics:
        topic_path = publisher.topic_path(gu.get_project_id(), topic_name)
        try:
            publisher.create_topic(request={"name": topic_path})
            logging.info('Topic %s created', topic_path)
        except gex.AlreadyExists:  # all other exceptions we want to get raised and not caught
            logging.info('Topic %s already exists', topic_path)
        except Exception:
            logging.exception('Non recoverable exception in creating topics. Shutting down.')
            sys.exit(1)


class GCPPSRunner(u.QueuedRunner):
    """A thread doing the interaction with GCP asynchronously, so the main process' run() method does not get halted.

    Depending on network connection etc. pulling from Pub/Sub can take seconds, which the main process might not be
    able to absorb ("real-time system")."""
    __slots__ = ('session_id', 'worker_id', 'last_poll')

    def __init__(self, session_id: str, worker_id: Optional[str]) -> None:
        super().__init__('GCPPSRunner')
        self.session_id = session_id
        self.worker_id = worker_id  # if None, then acting on behalf of a ctrl, else on behalf of a worker
        self.last_poll = 0

    def run(self) -> None:
        publisher_client = gu.construct_publisher_client()
        subscriber_client = gu.construct_subscriber_client()
        if self.worker_id:
            subscription_path = _create_ctrl_to_worker_subscription_path(subscriber_client, self.worker_id)
        else:
            subscription_path = _create_workers_to_ctrl_subscription_path(subscriber_client)
        logging.info('GCPPSRunner started for %s', 'controller' if self.worker_id is None else 'worker ' +
                                                                                               self.worker_id)
        while True:
            # noinspection PyBroadException
            try:
                while True:
                    try:
                        message = self.incoming_queue.get_nowait()
                        if isinstance(message, m.ExitSession):
                            logging.info('GCPPSRunner got exit command - exiting. Is controller: %s',
                                         self.worker_id is None)
                            return
                        # stuff from worker
                        elif isinstance(message, m.FGTargetDestroyed):
                            send_action_instance_destroyed(publisher_client, self.session_id, self.worker_id,
                                                           message.callsign)
                            logging.info('Returned from GCP send_action_instance_destroyed')
                        elif isinstance(message, m.RemoveDeadTarget):
                            send_action_instance_down(publisher_client, self.session_id, self.worker_id,
                                                      message.callsign, message.error_situation)
                            logging.info('Returned from GCP send_action_instance_down')
                        else:
                            logging.warning('Programming error: not handled message %s', message)
                    except queue.Empty:
                        break  # nothing to do as this just signals an empty queue and we are done polling for now

                # Cannot poll PubSub too often, as otherwise the outer loop gets too slow
                now = time.time()
                if (now - self.last_poll) > u.WORKER_CTRL_POLLING:
                    self.last_poll = now
                    # default stuff
                    pull_responses = subscriber_client.pull(_create_default_pull_request_dict(subscription_path))
                    ack_ids = []
                    for response in pull_responses.received_messages:
                        ack_ids.append(response.ack_id)
                        if response.message.attributes[ATTRIBUTE_SESSION_ID] == self.session_id:
                            content_dict = json.loads(response.message.data)
                            if ATTRIBUTE_WORKER_ID in response.message.attributes[ATTRIBUTE_WORKER_ID]:
                                worker_id = response.message.attributes[ATTRIBUTE_WORKER_ID]
                            else:
                                worker_id = None
                            self.outgoing_queue.put_nowait(m.GCPSubData(content_dict, worker_id))
                    if ack_ids:
                        subscriber_client.acknowledge(_create_default_acknowledge_dict(subscription_path, ack_ids))
                    # carrier stuff
                    if self.worker_id:
                        carrier_order = _pull_carrier_captain_orders(subscriber_client)
                        if carrier_order:
                            self.outgoing_queue.put_nowait(m.GCPSubData(carrier_order, None))
                    logging.info('Returned from polling GCP')
                time.sleep(2.)
            except Exception:
                logging.exception('Some exception during GCPPSRunner.run() occurred. Continuing and see.')
            time.sleep(0.5)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s -%(levelname)s - %(message)s', level=logging.INFO)
    my_ctrl_publisher = gu.construct_publisher_client()
    my_ctrl_subscriber = gu.construct_subscriber_client()
    my_worker_publisher = gu.construct_publisher_client()
    my_worker_subscriber = gu.construct_subscriber_client()
    my_carrier_subscriber = gu.construct_subscriber_client()
    my_project = gu.get_project_id()
    my_session_id = u.create_session_id()
    my_worker_id = '127.0.0.1'
    callsign_11 = 'OPFOR11'

    # create all topics
    create_all_topics_for_testing(my_ctrl_publisher)

    # worker subscribes first
    _ = handle_ctrl_to_workers_subscription(my_worker_subscriber, my_worker_id, True)
    _ = handle_workers_to_ctrl_subscription(my_ctrl_subscriber, True)

    # sending
    send_action_request_workers_to_register(my_ctrl_publisher, my_session_id, 'axalp',
                                            u.MP_SERVER_HOST_DEFAULT)
    ctrl_session_id, a_scenario_name, a_host = pull_ctlr_registration_requests(my_worker_subscriber,
                                                                               my_worker_id)
    logging.info('Session_id from controller request: %s -> original session_id: %s', ctrl_session_id, my_session_id)

    send_action_register_worker(my_worker_publisher, my_session_id, my_worker_id, '8_0_0_0_0')
    available_workers = pull_workers_serving(my_ctrl_subscriber, my_session_id)
    logging.info('Workers serving: %s', str(available_workers))

    send_action_add_instance_sam(my_ctrl_publisher, my_session_id, my_worker_id, 'BUK-M2', callsign_11,
                                 23.3497, 69.9797, 3, 0)  # ENAT

    # shut down
    send_action_instance_down(my_worker_publisher, my_session_id, my_worker_id, callsign_11)

    send_action_worker_stop(my_ctrl_publisher, my_session_id, my_worker_id)

    # worker subscribes first
    _ = handle_ctrl_to_workers_subscription(my_worker_subscriber, my_worker_id, False)
    _ = handle_workers_to_ctrl_subscription(my_ctrl_subscriber, False)

    print('Done GCP testing')
