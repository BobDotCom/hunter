"""Models a mp_target, which dispenses defensive countermeasures like chaff/flare.

Emesary notifications currently do not distinguish between chaff and flare - they are just for visuals.
The choice of flares/chaff is set in MP_PROP_OPRF_CHAFFS/MP_PROP_OPRF_FLARES - however, right now both are
released always.

The core logic through Dispenser.process_attacker_info(...) and Dispenser.extend_props_with_dispenser_stuff(...)
makes a hard assumption that the methods are called more than once per second
-> defined in MPTarget.send_freq

The overall logic is as per discussion with Leto.
The code / logic for dispensing flares incl. geometry is in damage.nas around the
string 'Flares over MP'
"""
import copy
from enum import IntEnum, unique
import logging
import math
import random
from typing import Any, Dict, List
import unittest

import hunter.emesary as e
import hunter.emesary_notifications as en
import hunter.geometry as g
import hunter.messages as m
import hunter.mp_attacker_tracker as mat
import hunter.utils as u

THREAT_DISTANCE_IR = g.nm_to_metres(5)
THREAT_DISTANCE_RADAR_LOCK = 3 * THREAT_DISTANCE_IR

# The assumed visibility from a heli is reduced because pilot is not aware all angles at all times
VISIBLE_BEARING = 110 - 30  # from pilot looking ahead to looking towards and over the shoulder
VISIBLE_ANGLE_UP = 60 - 10  # could probably look 80+ deg up, but then due to angle would not be a threat
VISIBLE_ANGLE_DOWN = 60 - 15

THREATENING_BEARING = 180 - 75  # 180 is nose towards nose, less than 90 would be from behind

COUNTER_MEASURE_TIME_TO_LIVE = 8  # number of seconds a flare/chaff is alive
COUNTER_MEASURE_TERMINAL_SPEED = 50  # m/s
COUNTER_MEASURE_NUMBER_DEFAULT = 300  # a high number because they use a lot


class Countermeasure:
    __slots__ = ('lived_time', 'position', 'heading', 'speed', 'unique_id')

    def __init__(self, position: g.Position, heading: float, speed: float, unique_id: int) -> None:
        self.position = copy.copy(position)  # otherwise we are using a reference on the parent position
        self.lived_time = 0
        self.heading = heading  # of aeroplane at dispense of countermeasure
        self.speed = speed  # ditto - only using horizontal speed - damage.nas also uses vertical speed
        self.unique_id = unique_id

    def __str__(self):
        return 'Countermeasure id={} has lived {} at {}'.format(self.unique_id, self.lived_time, self.position)

    def update_position(self) -> None:
        """Calculates a new position of the countermeasure as a function of lived time."""
        lost_altitude = 0
        gained_distance = 0
        for i in range(self.lived_time):
            downward_speed = 9.83 * self.lived_time * 0.5  # not accelerating too fast
            downward_speed = COUNTER_MEASURE_TERMINAL_SPEED if downward_speed > COUNTER_MEASURE_TERMINAL_SPEED \
                else downward_speed
            lost_altitude += downward_speed
            horizontal_speed = self.speed - self.lived_time * 10
            horizontal_speed = 0 if horizontal_speed < 0 else horizontal_speed
            gained_distance += horizontal_speed
        lon, lat = g.calc_destination_pos(self.position, gained_distance, self.heading)
        self.position.lon = lon
        self.position.lat = lat
        self.position.alt_m -= lost_altitude


def _process_outgoing_in_flight_notification(counter_measure: Countermeasure, msg_idx: int) -> bytes:
    """Number of hits is for bullets. Else it is the distance of the missile explosion to the plane."""
    encoded_notification = en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(msg_idx, 4)
    encoded_notification += en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(en.ObjectInFlightNotification_Id, 1)
    encoded_notification += en.SEPARATOR_CHAR
    kind = en.KIND_DELETED if counter_measure.lived_time == COUNTER_MEASURE_TIME_TO_LIVE else en.KIND_MOVED
    secondary_kind = 21 + 95  # 95 = flare warheads_em in damage.nas
    notification = en.ObjectInFlightNotification(en.IDENT_MP_BRIDGE, counter_measure.unique_id, kind, secondary_kind)
    if counter_measure.lived_time < COUNTER_MEASURE_TIME_TO_LIVE:
        counter_measure.update_position()
    notification.position = counter_measure.position
    encoded_notification += notification.encode_to_message_body()
    encoded_notification += en.MESSAGE_END_CHAR
    return encoded_notification


@unique
class ThreatLevel(IntEnum):
    """The values are used to decided probability of releasing a countermeasure.
    The higher the number the more probable."""
    none = 0
    low = 1
    medium = 2
    high = 3
    alert = 4


def max_threat_level(threat_level_1: ThreatLevel, threat_level_2: ThreatLevel) -> ThreatLevel:
    """Returns the highest threat level of two submitted."""
    if threat_level_1 > threat_level_2:
        return threat_level_1
    elif threat_level_1 < threat_level_2:
        return threat_level_2
    return threat_level_1


class Dispenser(mat.AttackerTracker):
    """Dispenses chaff/flare countermeasures if there is an assessed threat.
    NB: the whole logic depends on the parent mp_target having a send_freq of 0.5 seconds."""
    __slots__ = ('parent', 'available_countermeasures', 'hashed_callsign', 'counter_measure_unique_id',
                 'active_countermeasures', 'last_elapsed', 'tick')

    def __init__(self, parent,  # mpt.MPTarget not imported due to circular reference
                 available_countermeasures: int = COUNTER_MEASURE_NUMBER_DEFAULT) -> None:
        super().__init__(parent)

        self.available_countermeasures = available_countermeasures
        # keep a hash of the parent callsign in order not to have to recalculate it over and over again
        self.hashed_callsign = u.create_radar_hash_for_callsign(self.parent.callsign)
        self.counter_measure_unique_id = 0  # increased whenever a new flare/chaff is dispensed
        self.active_countermeasures = list()
        self.last_elapsed = 0.1
        self.tick = False  # switches all 0.5 seconds (send_freq) to allow some logic

    def _tick_tack(self) -> None:
        """Switch between tick .0 secs and tack .5 secs - assuming the send_freq is 0.5"""
        self.tick = not self.tick

    def _next_elapsed_for_counter_measure(self) -> float:
        """Calculates a new value, which makes it into the OPRF float field for flares/chaff.
        The value just needs to change - and it has to be below the TT_SHORT_FLOAT_3 value limit.
        """
        self.last_elapsed += 1.0
        if self.last_elapsed > 32.0:
            self.last_elapsed = 1.0
        return self.last_elapsed

    def next_counter_measure_unique_id(self) -> int:
        self.counter_measure_unique_id += 1
        return self.counter_measure_unique_id

    def _age_counter_measures(self) -> None:
        """Age active counter_measures and remove counter_measures having gone out of live."""
        for counter_measure in reversed(self.active_countermeasures):
            counter_measure.lived_time += 1
            if counter_measure.lived_time > COUNTER_MEASURE_TIME_TO_LIVE:
                self.active_countermeasures.remove(counter_measure)

    def _randomly_add_counter_measures(self, threat_level: ThreatLevel) -> None:
        """Release counter-measures but only with a certain randomness."""
        do_release = False
        if threat_level is ThreatLevel.alert:  # always = every second
            do_release = True
        elif threat_level is ThreatLevel.high and random.randint(0, 1) == 0:  # in average every 2nd second
            do_release = True
        elif threat_level is ThreatLevel.medium and random.randint(0, 3) == 0:  # in average every 4th second
            do_release = True
        else:
            pass  # we do nothing at ThreatLevel.low or ThreatLevel.none
        if do_release:
            logging.info('%s detected a threat at level %s, chaff/flare added', self.parent.callsign,
                         threat_level.name)
            counter_measure = Countermeasure(self.parent.position, self.parent.orientation.hdg,
                                             self.parent.cruise_speed, self.next_counter_measure_unique_id())
            self.active_countermeasures.append(counter_measure)
        elif threat_level > ThreatLevel.medium:
            logging.info('%s detected a threat at level %s, but no chaff/flare', self.parent.callsign,
                         threat_level.name)

    def _decide_threat_attacker_radar_on(self, attacker_position: m.AttackerPosition, distance: float) -> ThreatLevel:
        """If the attacker has the radar on, then check whether it is a threat"""
        threat_level = ThreatLevel.none
        if distance < THREAT_DISTANCE_RADAR_LOCK:
            # first check whether there is an active radar lock -> FOX-1 semi-active radar missile probable
            # assuming that the attacker behaves ok, i.e. not having a lock while the aspect to the dispenser
            # actually would disallow it (fly away)
            if self.hashed_callsign and self.hashed_callsign == attacker_position.radar_lock:
                threat_level = max_threat_level(threat_level, ThreatLevel.medium)
                if distance < THREAT_DISTANCE_RADAR_LOCK / 2.:
                    threat_level = max_threat_level(threat_level, ThreatLevel.high)
                if distance < THREAT_DISTANCE_IR:
                    threat_level = ThreatLevel.alert
            else:
                # check whether the aspect is such that the attacker is not flying away
                bearing_to_attacker = g.calc_bearing_pos(self.parent.position, attacker_position.position)
                delta = g.calc_delta_bearing(self.parent.orientation.hdg, bearing_to_attacker)
                in_front_of_me = math.fabs(delta) <= 90
                delta_headings = g.calc_delta_bearing(self.parent.orientation.hdg, attacker_position.heading)
                good_aspect = False
                if in_front_of_me and math.fabs(delta_headings) >= 90:
                    good_aspect = True
                elif not in_front_of_me and math.fabs(delta_headings) < 90:
                    good_aspect = True
                if good_aspect:
                    threat_level = max_threat_level(threat_level, ThreatLevel.low)
                    if distance < THREAT_DISTANCE_RADAR_LOCK / 2.:
                        threat_level = max_threat_level(threat_level, ThreatLevel.medium)
                    if distance < THREAT_DISTANCE_IR:
                        threat_level = max_threat_level(threat_level, ThreatLevel.high)
                    if distance < THREAT_DISTANCE_IR / 2.:
                        threat_level = ThreatLevel.alert
        if threat_level > ThreatLevel.medium:
            logging.info('%s assesses %s as a threat based on radar and/or radar lock', self.parent.callsign,
                         attacker_position.callsign)
        return threat_level

    def _decide_threat_attacker_pilots_eye(self, attacker_position: m.AttackerPosition,
                                           distance: float) -> ThreatLevel:
        """Check whether there is a visible threat from the dispenser pilot's perspective."""
        threat_level = ThreatLevel.none
        if distance < THREAT_DISTANCE_IR:
            # visible for pilot?
            bearing_to_attacker = g.calc_bearing_pos(self.parent.position, attacker_position.position)
            delta = g.calc_delta_bearing(self.parent.orientation.hdg, bearing_to_attacker)
            delta_headings = g.calc_delta_bearing(self.parent.orientation.hdg, attacker_position.heading)
            # make sure pilot can see attacker
            if math.fabs(delta) < VISIBLE_BEARING:
                angle = g.calc_altitude_angle(self.parent.position, attacker_position.position)
                # make sure pilot can see attacker
                if VISIBLE_ANGLE_DOWN < angle < VISIBLE_ANGLE_UP:
                    # pilot assesses the attacker as a threat if there is some nose on - we disregard the pitch
                    if math.fabs(delta_headings) > THREATENING_BEARING:
                        threat_level = max_threat_level(threat_level, ThreatLevel.high)
                        if distance < THREAT_DISTANCE_IR / 2.:
                            threat_level = ThreatLevel.alert
        if threat_level > ThreatLevel.medium:
            logging.info('%s\' pilot visually assesses %s as a threat', self.parent.callsign,
                         attacker_position.callsign)
        return threat_level

    def _decide_threat_missiles_in_flight(self, missiles: List[en.ArmamentInFlightNotification]) -> ThreatLevel:
        threat_level = ThreatLevel.none
        for missile in missiles:
            if missile.radar_on:
                distance = g.calc_distance_pos_3d(self.parent.position, missile.position)
                if distance < THREAT_DISTANCE_RADAR_LOCK:
                    threat_level = max_threat_level(threat_level, ThreatLevel.high)
                    if distance < THREAT_DISTANCE_RADAR_LOCK / 2.:
                        threat_level = ThreatLevel.alert
                        break
        if threat_level > ThreatLevel.medium:
            logging.info('%s sees at least one missile as a threat', self.parent.callsign)
        return threat_level

    # implements AttackerTracker._process_attackers()
    def _process_attackers(self, missiles_in_flight: List[en.ArmamentInFlightNotification]) -> None:
        """Based on information about attackers assess threats and dispense countermeasures."""
        self._tick_tack()
        if not self.tick:  # only process threats every .0 second
            return

        threat_level = ThreatLevel.none
        if m.is_capable_of_fighting_health(self.parent.health) and self.available_countermeasures > 0:
            # check threat from incoming active-radar missiles
            threat_level = max_threat_level(self._decide_threat_missiles_in_flight(missiles_in_flight), threat_level)
            # check threat based on position of attackers
            if threat_level is not ThreatLevel.alert:
                for position_history in self.attackers_hist_positions.values():
                    attacker_position = position_history.latest_attacker_position
                    distance = g.calc_distance_pos_3d(self.parent.position, attacker_position.position)
                    if distance < THREAT_DISTANCE_RADAR_LOCK:  # first we check whether visibility is ok
                        if self.parent.dem and not self.parent.dem.is_visible(self.parent.position,
                                                                              attacker_position.position):
                            continue
                        if attacker_position.radar_on:
                            threat_level = max_threat_level(self._decide_threat_attacker_radar_on(attacker_position,
                                                                                                  distance),
                                                            threat_level)
                        if threat_level is ThreatLevel.alert:
                            break
                        threat_level = max_threat_level(self._decide_threat_attacker_pilots_eye(attacker_position,
                                                                                                distance),
                                                        threat_level)

        # We have to keep doing this even if the parent's health is broken - otherwise the countermeasures stay forever.
        self._age_counter_measures()

        self._randomly_add_counter_measures(threat_level)

    def _extend_counter_measure_props(self, props: Dict[int, Any]) -> None:
        """OPRF FG instance way of doing it is as follows. Hunter cannot do it currently, because sends data only every
        0.5 second:
        1: Set the value to a random number when flare/chaff released. I normally use 0.0-1.0
        2: Wait 0.1 seconds to be sure its seen
        3: Set the value to 0.0 (important, else any new missiles will see it released again and again)
        4: Wait 0.9 seconds
        5: Repeat for any new flares/chaff
        """
        has_zero_life_time_counter_measure = False
        for counter_measure in self.active_countermeasures:
            if counter_measure.lived_time == 0:
                has_zero_life_time_counter_measure = True

        if self.tick:
            if has_zero_life_time_counter_measure:
                self.available_countermeasures -= 1
                if self.available_countermeasures == 0:
                    logging.info('%s just released its last chaff/flare - run out', self.parent.callsign)
                delta_time = self._next_elapsed_for_counter_measure()
                props[u.MP_PROP_OPRF_CHAFFS] = delta_time
                props[u.MP_PROP_OPRF_FLARES] = delta_time
            else:
                pass  # we just leave the props empty
        else:
            if has_zero_life_time_counter_measure:
                props[u.MP_PROP_OPRF_CHAFFS] = 0.0
                props[u.MP_PROP_OPRF_FLARES] = 0.0
            else:
                pass  # we just leave the props empty

    def _extend_emesary_props(self, props: Dict[int, Any]) -> None:
        if self.tick:
            encoded_message = None  # bytes
            for counter_measure in self.active_countermeasures:
                my_message = _process_outgoing_in_flight_notification(counter_measure,
                                                                      self.parent.next_emesary_msg_idx())
                if encoded_message:
                    encoded_message = encoded_message + my_message
                else:
                    encoded_message = my_message
            if encoded_message:
                props[u.MP_PROP_EMESARY_BRIDGE_BASE + en.OPRF_BRIDGE_FLARES] = encoded_message
        else:
            pass  # nothing to do

    def extend_props_with_dispenser_stuff(self, props: Dict[int, Any]) -> None:
        """Extend the properties to send to MP with shooting info - maybe including an Emesary notifications.
        Called from the FGMSParticipant - after calling process_attacker_info(...)
        """
        self._extend_counter_measure_props(props)
        self._extend_emesary_props(props)


class TestDispensing(unittest.TestCase):
    def setUp(self):
        dispenser_callsign = 'OPFOR99'
        self.dispenser_hashed_callsign = u.create_radar_hash_for_callsign(dispenser_callsign)
        heli_msl = 2000
        dispenser_position = g.Position(25., 70., heli_msl)  # south of ENNA
        import hunter.mp_targets as mpt
        test_heli_mp_target = mpt.MPTarget.create_target(mpt.MPTarget.TRUCK, dispenser_position, 90.)
        test_heli_mp_target.callsign = dispenser_callsign
        self.dispenser = Dispenser(test_heli_mp_target)

    def test_max_threat_level(self):
        threat_level_1 = ThreatLevel.alert
        threat_level_2 = ThreatLevel.low
        self.assertEqual(threat_level_1, max_threat_level(threat_level_1, threat_level_2), '1 and then 2')
        self.assertEqual(threat_level_1, max_threat_level(threat_level_2, threat_level_1), '2 and then 1')

    def test_decide_threat_attacker_radar_on(self):
        self.dispenser.parent.orientation.hdg = 90.  # make sure not changed anywhere else
        attacker_callsign = 'ATTACK1'
        attacker_msl = 2000
        lon_lat = g.calc_destination_pos(self.dispenser.parent.position, THREAT_DISTANCE_RADAR_LOCK * 1.2, 90.)
        position = g.Position(lon_lat[0], lon_lat[1], attacker_msl)
        attacker_position = m.AttackerPosition(attacker_callsign, position, 90., 0.,
                                               self.dispenser_hashed_callsign, True, False, False, 0., None)
        dist = g.calc_distance_pos_3d(self.dispenser.parent.position, attacker_position.position)
        self.assertEqual(ThreatLevel.none, self.dispenser._decide_threat_attacker_radar_on(attacker_position, dist),
                         'Radar lock and outside range')

        lon_lat = g.calc_destination_pos(self.dispenser.parent.position, THREAT_DISTANCE_RADAR_LOCK * 0.9, 90.)
        position = g.Position(lon_lat[0], lon_lat[1], attacker_msl)
        attacker_position.position = position
        dist = g.calc_distance_pos_3d(self.dispenser.parent.position, attacker_position.position)
        self.assertEqual(ThreatLevel.medium, self.dispenser._decide_threat_attacker_radar_on(attacker_position, dist),
                         'Radar lock and inside medium range')

        lon_lat = g.calc_destination_pos(self.dispenser.parent.position, THREAT_DISTANCE_IR * 0.9, 90.)
        position = g.Position(lon_lat[0], lon_lat[1], attacker_msl)
        attacker_position.position = position
        dist = g.calc_distance_pos_3d(self.dispenser.parent.position, attacker_position.position)
        self.assertEqual(ThreatLevel.alert, self.dispenser._decide_threat_attacker_radar_on(attacker_position, dist),
                         'Radar lock and inside alert range')

        # no radar lock
        attacker_position.radar_lock = None
        lon_lat = g.calc_destination_pos(self.dispenser.parent.position, THREAT_DISTANCE_RADAR_LOCK / 2. * 0.9, 90.)
        position = g.Position(lon_lat[0], lon_lat[1], attacker_msl)
        attacker_position.position = position
        dist = g.calc_distance_pos_3d(self.dispenser.parent.position, attacker_position.position)
        self.assertEqual(ThreatLevel.none, self.dispenser._decide_threat_attacker_radar_on(attacker_position, dist),
                         'Radar on without lock and inside medium range in front and flying away from dispenser')

        attacker_position.heading = 330
        self.assertEqual(ThreatLevel.medium, self.dispenser._decide_threat_attacker_radar_on(attacker_position, dist),
                         'Radar on without lock and inside medium range in front and flying towards dispenser')

        lon_lat = g.calc_destination_pos(self.dispenser.parent.position, THREAT_DISTANCE_RADAR_LOCK / 2. * 0.9, 270.)
        position = g.Position(lon_lat[0], lon_lat[1], attacker_msl)
        attacker_position.position = position
        dist = g.calc_distance_pos_3d(self.dispenser.parent.position, attacker_position.position)
        self.assertEqual(ThreatLevel.none, self.dispenser._decide_threat_attacker_radar_on(attacker_position, dist),
                         'Radar on without lock and inside medium range behind and flying away from dispenser')

        attacker_position.heading = 120
        self.assertEqual(ThreatLevel.medium, self.dispenser._decide_threat_attacker_radar_on(attacker_position, dist),
                         'Radar on without lock and inside medium range behind and flying towards dispenser')

        lon_lat = g.calc_destination_pos(self.dispenser.parent.position, THREAT_DISTANCE_IR / 2. * 0.9, 270.)
        position = g.Position(lon_lat[0], lon_lat[1], attacker_msl)
        attacker_position.position = position
        dist = g.calc_distance_pos_3d(self.dispenser.parent.position, attacker_position.position)
        self.assertEqual(ThreatLevel.alert, self.dispenser._decide_threat_attacker_radar_on(attacker_position, dist),
                         'Radar on without lock and inside alert range behind and flying towards dispenser')

    def test_decide_threat_attacker_pilots_eye(self):
        self.dispenser.parent.orientation.hdg = 90.  # make sure not changed anywhere else
        attacker_callsign = 'ATTACK1'
        attacker_msl = 2000
        lon_lat = g.calc_destination_pos(self.dispenser.parent.position, THREAT_DISTANCE_IR * 1.2, 90.)
        position = g.Position(lon_lat[0], lon_lat[1], attacker_msl)
        attacker_position = m.AttackerPosition(attacker_callsign, position, 90., 0.,
                                               None, False, False, False, 0., None)
        dist = g.calc_distance_pos_3d(self.dispenser.parent.position, attacker_position.position)
        self.assertEqual(ThreatLevel.none, self.dispenser._decide_threat_attacker_pilots_eye(attacker_position, dist),
                         'Pilot\'s eye outside range')

        lon_lat = g.calc_destination_pos(self.dispenser.parent.position, THREAT_DISTANCE_IR * 0.9, 90.)
        position = g.Position(lon_lat[0], lon_lat[1], attacker_msl)
        attacker_position.position = position
        dist = g.calc_distance_pos_3d(self.dispenser.parent.position, attacker_position.position)
        self.assertEqual(ThreatLevel.none, self.dispenser._decide_threat_attacker_pilots_eye(attacker_position, dist),
                         'Pilot\'s eye inside high range and visible and not threatening heading')

        attacker_position.heading = 210
        self.assertEqual(ThreatLevel.high, self.dispenser._decide_threat_attacker_pilots_eye(attacker_position, dist),
                         'Pilot\'s eye inside high range and visible and threatening heading 210')

        attacker_position.heading = 330
        self.assertEqual(ThreatLevel.high, self.dispenser._decide_threat_attacker_pilots_eye(attacker_position, dist),
                         'Pilot\'s eye inside high range and visible and threatening heading 330')

        position = g.calc_destination_pos_3d(self.dispenser.parent.position, THREAT_DISTANCE_IR * 0.51, 90.,
                                             VISIBLE_ANGLE_UP * 1.1)
        attacker_position.position = position
        dist = g.calc_distance_pos_3d(self.dispenser.parent.position, attacker_position.position)
        self.assertEqual(ThreatLevel.none, self.dispenser._decide_threat_attacker_pilots_eye(attacker_position, dist),
                         'Pilot\'s eye inside high range and visible and threatening heading 330 but too high')

        position = g.calc_destination_pos_3d(self.dispenser.parent.position, THREAT_DISTANCE_IR * 0.51, 90.,
                                             VISIBLE_ANGLE_DOWN * 1.1)
        attacker_position.position = position
        dist = g.calc_distance_pos_3d(self.dispenser.parent.position, attacker_position.position)
        self.assertEqual(ThreatLevel.none, self.dispenser._decide_threat_attacker_pilots_eye(attacker_position, dist),
                         'Pilot\'s eye inside high range and visible and threatening heading 330 but too low')

        position = g.calc_destination_pos_3d(self.dispenser.parent.position, THREAT_DISTANCE_IR * 0.51, 90.,
                                             VISIBLE_ANGLE_DOWN * 0.5)
        attacker_position.position = position
        dist = g.calc_distance_pos_3d(self.dispenser.parent.position, attacker_position.position)
        self.assertEqual(ThreatLevel.high, self.dispenser._decide_threat_attacker_pilots_eye(attacker_position, dist),
                         'Pilot\'s eye inside high range and visible and threatening heading 330 and not too low')

    def test_decide_threat_missiles_in_flight(self):
        missiles = list()  #: List[en.ArmamentInFlightNotification]
        self.assertEqual(ThreatLevel.none, self.dispenser._decide_threat_missiles_in_flight(missiles),
                         'No missiles')

        missile = en.ArmamentInFlightNotification()
        missile.flags = 0
        missile_position = g.calc_destination_pos_3d(self.dispenser.parent.position, THREAT_DISTANCE_RADAR_LOCK * 1.2,
                                                     90., 0)
        missile.position = missile_position
        missiles.append(missile)
        self.assertEqual(ThreatLevel.none, self.dispenser._decide_threat_missiles_in_flight(missiles),
                         '1 missile without radar and out of range')

        missile.flags = 1
        self.assertEqual(ThreatLevel.none, self.dispenser._decide_threat_missiles_in_flight(missiles),
                         '1 missile with radar and out of range')

        missile_position = g.calc_destination_pos_3d(self.dispenser.parent.position, THREAT_DISTANCE_RADAR_LOCK * 0.8,
                                                     90., 0)
        missile.position = missile_position
        self.assertEqual(ThreatLevel.high, self.dispenser._decide_threat_missiles_in_flight(missiles),
                         '1 missile with radar and within high threat range')

        missile_position = g.calc_destination_pos_3d(self.dispenser.parent.position, THREAT_DISTANCE_RADAR_LOCK * 0.4,
                                                     90., 0)
        missile.position = missile_position
        self.assertEqual(ThreatLevel.alert, self.dispenser._decide_threat_missiles_in_flight(missiles),
                         '1 missile with radar and within alert threat range')

        missile.flags = 0
        self.assertEqual(ThreatLevel.none, self.dispenser._decide_threat_missiles_in_flight(missiles),
                         '1 missile without radar and within alert threat range')

    def test_randomly_add_counter_measures(self):
        before_number = len(self.dispenser.active_countermeasures)
        self.dispenser._randomly_add_counter_measures(ThreatLevel.none)
        self.assertEqual(before_number, len(self.dispenser.active_countermeasures),
                         'Add counter measure for none')
        self.dispenser._randomly_add_counter_measures(ThreatLevel.alert)
        self.assertEqual(before_number + 1, len(self.dispenser.active_countermeasures),
                         'Add counter measure for alert')

    def test_emesary_and_props(self):
        self.dispenser.tick = False
        self.dispenser._tick_tack()  # loop 1 - tick
        self.dispenser._age_counter_measures()
        self.dispenser._randomly_add_counter_measures(ThreatLevel.alert)  # adds one new CounterMeasure
        self.assertEqual(1, len(self.dispenser.active_countermeasures), 'Loop 1 - tick: Has 1 active counter measure')

        props = dict()
        self.dispenser.extend_props_with_dispenser_stuff(props)
        notifications = None
        emesary_prop_code = u.MP_PROP_EMESARY_BRIDGE_BASE + en.OPRF_BRIDGE_FLARES
        if emesary_prop_code in props:
            notifications = en.process_incoming_message(props[emesary_prop_code])
        self.assertEqual(1, len(notifications), 'Loop 1 - tick: Has new emesary notification')
        self.assertTrue(u.MP_PROP_OPRF_CHAFFS in props, 'Loop 1 - tick: Has chaff prop')
        self.assertTrue(props[u.MP_PROP_OPRF_CHAFFS] > 0., 'Loop 1 - tick: Chaff prop > 0.')

        self.dispenser._tick_tack()  # loop 1 - tack
        props = dict()
        self.dispenser.extend_props_with_dispenser_stuff(props)
        self.assertFalse(emesary_prop_code in props, 'Loop 1 - tack: Has no emesary notifications')
        self.assertTrue(u.MP_PROP_OPRF_CHAFFS in props, 'Loop 1 - tack: Has chaff prop')
        self.assertTrue(props[u.MP_PROP_OPRF_CHAFFS] == 0., 'Loop 1 - tick: Chaff prop == 0.')

        self.dispenser._tick_tack()  # loop 2 -tick
        self.dispenser._age_counter_measures()
        self.dispenser._randomly_add_counter_measures(ThreatLevel.none)  # adds no new CounterMeasure

        props = dict()
        self.dispenser.extend_props_with_dispenser_stuff(props)
        notifications = None
        if emesary_prop_code in props:
            notifications = en.process_incoming_message(props[emesary_prop_code])
        self.assertEqual(1, len(notifications), 'Loop 2 - tick: Still has emesary notification')
        self.assertFalse(u.MP_PROP_OPRF_CHAFFS in props, 'Loop 2 - tick: Has no chaff prop')

        self.dispenser._tick_tack()  # loop 2 - tack
        props = dict()
        self.dispenser.extend_props_with_dispenser_stuff(props)
        self.assertFalse(emesary_prop_code in props, 'Loop 2 - tack: Has no emesary notifications')
        self.assertFalse(u.MP_PROP_OPRF_CHAFFS in props, 'Loop 2 - tack: Has no chaff prop')

        self.dispenser._tick_tack()
        for i in range(COUNTER_MEASURE_TIME_TO_LIVE - 1):
            self.dispenser._age_counter_measures()
        self.assertEqual(1, len(self.dispenser.active_countermeasures),
                         'Loop 1 almost end - tick: Still has 1 active counter measure')
        props = dict()
        self.dispenser.extend_props_with_dispenser_stuff(props)
        self.assertTrue(emesary_prop_code in props, 'Loop almost end - tick: Still has emesary notification')

        self.dispenser._age_counter_measures()
        self.assertEqual(0, len(self.dispenser.active_countermeasures),
                         'Loop at end - tick: No more active counter measure')
        props = dict()
        self.dispenser.extend_props_with_dispenser_stuff(props)
        self.assertFalse(emesary_prop_code in props, 'Loop at end - tick: No more emesary notification')
