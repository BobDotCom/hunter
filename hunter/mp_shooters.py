"""Models a shooting MP target.

======
SHILKA
======

Some resources (most of them actually not used in calculations):
A https://apps.dtic.mil/dtic/tr/fulltext/u2/a426717.pdf: Limitations of guns as defence against manoeuvring air
B https://apps.dtic.mil/dtic/tr/fulltext/u2/a166753.pdf: The Development of Soviet Air Defense Doctrine and Practice
C https://archive.org/details/DTIC_ADA392785/page/n3/mode/2up: Soviet ZSU-23-4: Capabilities and Countermeasures
D https://www.armedconflicts.com/CZE-SVK-ZU-23-2-M2-modernizace-t94983: smaller gun than Shilka
E https://www.alternatewars.com/BBOW/Weapons/Historical_Phits_Pkills.htm: kill probability of different arms

The core logic through Shooter.process_attacker_info(...) and Shooter.extend_props_with_shooting_stuff(...)
makes a hard assumption that the methods are called more than once per second
-> defined in MPTarget.send_freq

Python code for ballistic missiles: https://github.com/jlev/ballistic-missile-range

===========
HELICOPTERS
===========
Mi-24
* https://en.wikipedia.org/wiki/Mil_Mi-24
* https://weaponsystems.net/system/297-Mil+Mi-24D
* https://en.wikipedia.org/wiki/YakB-12.7_machine_gun
* https://weaponsystems.net/system/535-12.7mm%20Yakushev-Borzov%20Yak-B
* According to DCS guide: the -P version has R60M Aphid IR-missiles

Ka-50
* https://en.wikipedia.org/wiki/Kamov_Ka-50
* According to DCS guide: https://en.wikipedia.org/wiki/9K38_Igla
* According to DCS guide: 2A42 cannon: min target range 800m. max range 2000m

"""
import random
from dataclasses import dataclass
from enum import Enum
import math
import time
import logging
from typing import Any, Dict, List, Optional
import unittest

import hunter.emesary as e
import hunter.emesary_notifications as en
import hunter.geometry as g
import hunter.messages as m
import hunter.mp_attacker_tracker as mat
import hunter.scenarios as sc
import hunter.utils as u


# see missile-code.nas - look for checkForChaff: func ()
FLARES_RESISTANCE = 0.85
CHAFF_RESISTANCE = 0.85


@dataclass(frozen=True)
class EmesaryMessageAndBridge:
    encoded_message: bytes
    oprf_bridge: int


class EmesaryMessageContainer:
    __slots__ = 'message_dict'

    def __init__(self) -> None:
        self.message_dict = dict()

    def add_message(self, message_and_bridge: EmesaryMessageAndBridge) -> None:
        if message_and_bridge.oprf_bridge not in self.message_dict:
            self.message_dict[message_and_bridge.oprf_bridge] = message_and_bridge.encoded_message
        else:
            self.message_dict[message_and_bridge.oprf_bridge] = message_and_bridge.encoded_message + \
                                                                self.message_dict[message_and_bridge.oprf_bridge]

    def reset(self) -> None:
        """Should be called for each new iteration in shooter."""
        self.message_dict.clear()

    def add_messages_to_props(self, props: Dict[int, Any]) -> None:
        for bridge, encoded_messages in self.message_dict.items():
            props[u.MP_PROP_EMESARY_BRIDGE_BASE + bridge] = encoded_messages


def _process_outgoing_armament_hit_notification(index: int, shell_id: int, number_of_hits: int,
                                                remote_callsign: str, is_cannon: bool) -> EmesaryMessageAndBridge:
    """Number of hits is for bullets. Else it is the distance of the missile explosion to the plane."""
    encoded_notification = en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(index, 4)
    encoded_notification += en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(en.ArmamentNotification_Id, 1)
    encoded_notification += en.SEPARATOR_CHAR
    secondary_kind = shell_id * -1 - 1 if is_cannon else 21 + shell_id
    notification = en.ArmamentNotification(en.IDENT_MP_BRIDGE, en.KIND_COLLISION, secondary_kind)
    notification.distance = number_of_hits
    notification.remote_callsign = remote_callsign
    encoded_notification += notification.encode_to_message_body()
    encoded_notification += en.MESSAGE_END_CHAR
    return EmesaryMessageAndBridge(encoded_notification, en.OPRF_BRIDGE_CHANNEL_HIT)


def _process_outgoing_armament_in_flight_lost(index: int, shell_id: int, unique_identity: int,
                                              position: g.Position, heading: float, pitch: float,
                                              speed_ms: float) -> EmesaryMessageAndBridge:
    """Missile in flight Emesary message when the missile is lost.
    E.g. lost lock / line of sight, locked on flares/chaff, too slow, ...

    Based on F16/Nasal/missile_code.nas ca. line 2537
    ... me.free or me.lostLOS or me.tooLowSpeed or me.flareLock or me.chaffLock ...
    """
    return _process_outgoing_armament_in_flight_notification(index, shell_id, unique_identity,
                                                             position, heading, pitch,
                                                             speed_ms,
                                                             False, False, "", False)


def _process_outgoing_armament_in_flight_deleted(index: int, shell_id: int, unique_identity: int
                                                 ) -> EmesaryMessageAndBridge:
    """Missile in flight Emesary message when the missile needs to be deleted.

    Based on F16/Nasal/missile_code.nas ca. line 907
    ... me.free or me.lostLOS or me.tooLowSpeed or me.flareLock or me.chaffLock ...
    """
    position = g.Position(0., 0., 0.)
    return _process_outgoing_armament_in_flight_notification(index, shell_id, unique_identity,
                                                             position, 0., 0., 0.,
                                                             False, False, "", True)


def _process_outgoing_armament_in_flight_notification(index: int, shell_id: int, unique_identity: int,
                                                      position: g.Position, heading: float, pitch: float,
                                                      speed_ms: float,  # metres per second
                                                      radar_on: bool, thrust_on: bool,
                                                      remote_callsign: str,
                                                      is_deleted: bool) -> EmesaryMessageAndBridge:
    """Missile in flight Emesary message.
    Based on F16/Nasal/missile_code.nas->notifyInFlight() - ca. line 3976.

    On line ca. 4046 if a missile explodes -> speed = 0, remote_callsign = '', is_deleted=False
    """
    encoded_notification = en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(index, 4)
    encoded_notification += en.SEPARATOR_CHAR
    encoded_notification += e.BinaryAsciiTransfer.encode_int(en.ArmamentInFlightNotification_Id, 1)
    encoded_notification += en.SEPARATOR_CHAR
    secondary_kind = 21 + shell_id
    collision_kind = en.KIND_DELETED if is_deleted else en.KIND_MOVED
    unique_identity = unique_identity
    notification = en.ArmamentInFlightNotification(en.IDENT_MP_BRIDGE, unique_identity, collision_kind, secondary_kind)
    notification.position = position
    notification.heading = heading
    notification.pitch = pitch
    notification.u_fps = g.metres_to_feet(speed_ms)
    notification.set_oprf_flags(radar_on, thrust_on)
    # print('shell id=', shell_id, 'id=', unique_identity, 'lon =', position.lon, 'lat=', position.lat,
    # 'heading=', heading, 'pitch=', pitch, 'alt=', position.alt_m, 'speed=', speed_ms, 'flags=', notification.flags)
    notification.is_distinct = 0 if is_deleted else 1
    notification.remote_callsign = remote_callsign
    encoded_notification += notification.encode_to_message_body()
    encoded_notification += en.MESSAGE_END_CHAR
    return EmesaryMessageAndBridge(encoded_notification, en.OPRF_BRIDGE_CHANNEL_INFLIGHT)


def _calc_bullet_flight_time(distance: float, muzzle_velocity: int) -> float:
    """Primitive exponential function between velocity and distance.
    If muzzle velocity is ca. 930 a linear function for 4000 m distance would result in ca. 4.3 seconds.
    With exponential drag of 0.8 the same results in ca. 9 seconds and a hit velocity at ca. 150 m/s.
    If the bullet's velocity is below 100 m/s we assume that the energy is too little to do any damage and
    just return a very high flight time number.
    """
    current_velocity = muzzle_velocity
    remaining_dist = distance
    seconds = 0.
    while current_velocity > 100:
        if remaining_dist < current_velocity:
            return seconds + remaining_dist / current_velocity
        remaining_dist -= current_velocity
        seconds += 1.
        current_velocity *= 0.8
    return 9999.


BULLETS_MIN_RANGE = 200

# https://en.wikipedia.org/wiki/9K32_Strela-2: 2M variant max fire range 4200 m, 500 m/s max speed
# https://en.wikipedia.org/wiki/9K38_Igla: S variant max fire range 6000 m, 570 m/s max speed
# https://en.wikipedia.org/wiki/FIM-43_Redeye: max fire range 4500 m, 580 max speed
# https://en.wikipedia.org/wiki/FIM-92_Stinger: max fire range 4800 m, 750 m/s max speed, start range 0.16 km
IR_MISSILE_MAX_SPEED = 560
IR_MISSILE_MIN_MANOEUVRE_SPEED = 100
IR_MISSILE_MAX_BURN_TIME = 8
IR_MISSILE_MIN_RANGE = 400
IR_MISSILE_MAX_RANGE = 4500


def _calc_missile_speed(time_since_launch: float, max_velocity: int, max_burn_time: int, air_launched: bool) -> float:
    """Primitive fantasy function - does not take into consideration g-force for turns etc.
    The first few secs the missile accelerates, then for (max_burn_time - time to accelerate) it runs at full speed
    and finally gets exponentially slower over time.
    If launched from a heli (ca. in reality a bit higher because send_freq is 0.5 secs):
    Until 1 sec: 100 m/s, distance 100 m
    Until 2 sec: 140 m/s, distance 100 + 140 = 240 m
    Until 3 sec: 280 m/s, distance 240 + 280 = 520 m
    Until 4 sec: 420 m/s, distance 520 + 420 = 940 m
    Until 5 sec: 560 m/s, distance 940 + 560 = 1500 m
    Until 6 sec: 560 m/s, distance 1500 m + 560 = 2060 m
    Until 7 sec: 560 m/s, distance 2060 m + 560 = 2560 m
    Until 8 sec: 560 m/s, distance 2560 m + 560 = 3020 m
    Until 9 sec: 560 m/s, distance 3020 m + 560 = 3580 m
    Until 10 sec: 0.8^(10 - 8) = 0.64 * 560 m/s = 360 m/s, distance 3580 + 360 = 3940
    Until 11 sec: 0.8^(11 - 8) = 0.51 * 560 m/s = 280 m/s, distance 3940 + 280 = 4220
    Until 12 sec: 0.8^(12 - 8) = 0.4 * 560 m/s = 230 m/s, distance 4220 + 230 = 4450
    """
    if time_since_launch < 1.:
        if air_launched:
            return 100  # velocity of drone/heli plus a bit - but less than IR_MISSILE_MAX_SPEED / 4
        else:  # it takes some time to drop out of the launcher and ignite the rocket motor
            return 0
    elif 1 <= time_since_launch <= 4.:
        return max_velocity * time_since_launch / 4  # accelerate
    elif time_since_launch <= max_burn_time:
        return max_velocity
    else:
        return max_velocity * math.pow(0.8, time_since_launch - max_burn_time)


@dataclass(frozen=True)
class BulletShotInstance:
    timestamp: float
    callsign: str


class MissileState(Enum):
    active = 1
    overshot = 2  # just a special case of lost_tracking
    lost_tracking = 3
    bit_on_counter_measures = 4
    deleted = 5


MAX_LOST_TRACKING_CYCLES = 5


@dataclass(frozen=False)  # will be updated
class MissileLaunchedInstance:
    timestamp: float  # when launched - not updated
    callsign: str  # not updated
    unique_identity: int  # not updated
    radar_on: bool  # not updated
    status: MissileState  # updated sometimes
    heading: float  # updated each second
    pitch: float  # updated each second
    positions: list  # updated each loop with a new position
    lost_tracking_cycles: int  # updated each loop when has lost tracking

    def __str__(self):
        return 'Missile id={} against {} has status {} and radar_on={}'.format(self.unique_identity, self.callsign,
                                                                               self.status.name, self.radar_on)


class AttackerMetric:
    __slots__ = ('_position', '_visible')

    def __init__(self, position: g.Position, visible: bool = True) -> None:
        self._position = position
        self._visible = visible

    @property
    def position(self) -> g.Position:
        return self._position

    @property
    def visible(self) -> bool:
        return self._visible

    @visible.setter
    def visible(self, visible: bool) -> None:
        self._visible = visible


class AttackerMetrics:
    """Keeps some metric for a specific second for the current attacker as seen by the shooter and calculates others.

    To use the properties and be sure that they calculate something useful, check self.calculable.
    """
    __slots__ = ('metrics', 'max_hidden_tracking')

    def __init__(self, max_hidden_tracking) -> None:
        self.metrics = list()
        self.max_hidden_tracking = max_hidden_tracking  # how long can we track although not visible on radar

    def add_metric(self, position: g.Position) -> None:
        self.metrics.append(AttackerMetric(position))

    def update_last_metric_as_hidden(self) -> None:
        if self.metrics:
            self.metrics[-1].visible = False

    @property
    def calculable(self) -> bool:
        return self.metrics and len(self.metrics) > 1

    @property
    def lost_tracking(self) -> bool:
        if self.calculable and len(self.metrics) > self.max_hidden_tracking:
            consecutive_hidden = 0
            for metric in reversed(self.metrics):
                if metric.visible is False:
                    consecutive_hidden += 1
                    if consecutive_hidden > self.max_hidden_tracking:
                        return True
                else:
                    break
        return False

    @property
    def currently_hidden(self) -> bool:
        if self.calculable:
            return not self.metrics[-1].visible
        return False


class AirWeaponType(Enum):  # there is also a WeaponType in messages.py - with another purpose
    bullet = 1,
    missile_ir_sa = 2,  # IR missile surface-to-air
    missile_ir_aa = 3,  # IR missile air-to-air
    missile_fake = 4


PILOT_VISIBILITY_MAX_HORIZONTAL = 60
PILOT_VISIBILITY_MAX_VERTICAL = 60
PILOT_VISIBILITY_MIN_VERTICAL = -45
PILOT_VISIBILITY_DELTA_DIVISION_FACTOR = 15


def _nose_on_factor(pilot_hdg: float, attacker_hdg: float) -> float:
    """If directly nose on, then 1.0, if straight away, then 0.0"""
    delta_headings = math.fabs(g.calc_delta_bearing(pilot_hdg, attacker_hdg))
    # delta_headings: 180 is nose towards nose, less than 90 would be from behind.
    return delta_headings / 180


def _visibility_from_pilots_eye(pilot_pos: g.Position, pilot_hdg: float, pilot_callsign,
                                attacker_position: m.AttackerPosition,
                                for_missile_launch: bool) -> Optional[float]:
    """Check whether attacker is visible from pilot's perspective/cone of visibility.

    Either from a perspective of tracking (for_missile_launch=False) or to determine whether a missile could be
    launched (for_missile_launch=False).

    Returns None if not good for launch. Otherwise, a nose-on factor between 0.0 and 1.0, where 1.0 is nose-on.

    A helicopter or drone (MQ-9) pilot checks for shooting an IR-missile without radar support in cockpit
    (https://en.wikipedia.org/wiki/Air-to-Air_Stinger, https://en.wikipedia.org/wiki/9K38_Igla) at an attacker:
    totally unscientific guess for parameters.

    {a} From missile perspective: if the attacker is flying towards a heli, a missile might hit if fired at 8km.
       distance. If the attacker is flying away from the heli, then maybe it could only catch within 4 km.
       Perpendicular maybe somewhere in the middle.
    {b} From pilot pure visibility: it is easier to see a plane perpendicular than flying nose-2 nose.
       It might be easiest from the rear due to motor exhaust - especially if the pilot has some IR-help (no radar).
    {c} Visual cone for pilot detection plus missile manoeuvres (i.e. if too far off the missile might not hit):
       - left/right from heli centerline: 45 degs
       - upwards from horizontal: 60 degs (it is much more natural for a heli-pilot to look up to scan the sky)
       - downwards from horizontal: 30 degs
    {d} it might take a few seconds to actually fire. 500 kt are ca. 250 m/s -> 500-1000 metres difference.
    {e} https://aviation.stackexchange.com/questions/9594/
    ./what-is-the-longest-distance-at-which-an-aircraft-can-be-visually-identified
    -> 8-12 km for a fighter plane

    Method _check_for_tracking_and_shooting already makes sure that [1] attacker is visible, [2] attacker is
    within self.tracking_dist (see {e}), [3] self.min_attacker_acquisition_time has passed to get a lock (see {d}).

    Inspired by mp_dispensers.py _decide_threat_attacker_pilots_eye(...)
    """
    bearing_to_attacker = g.calc_bearing_pos(pilot_pos, attacker_position.position)
    delta = g.calc_delta_bearing(pilot_hdg, bearing_to_attacker)
    nose_on_factor = _nose_on_factor(pilot_hdg, attacker_position.heading)

    max_horizontal = PILOT_VISIBILITY_MAX_HORIZONTAL
    max_vertical = PILOT_VISIBILITY_MAX_VERTICAL
    min_vertical = PILOT_VISIBILITY_MIN_VERTICAL
    if for_missile_launch:
        # We change angles below a bit, because if flying away we can accept a bit larger cone than if nose-on.
        # E.g. max_horizontal for nose-on is 45 degs, from behind is 60 degs
        max_horizontal -= nose_on_factor * PILOT_VISIBILITY_DELTA_DIVISION_FACTOR
        max_vertical -= nose_on_factor * PILOT_VISIBILITY_DELTA_DIVISION_FACTOR
        min_vertical += nose_on_factor * PILOT_VISIBILITY_DELTA_DIVISION_FACTOR
    # make sure pilot can see attacker horizontally and missile would keep up
    if math.fabs(delta) < max_horizontal:
        angle = g.calc_altitude_angle(pilot_pos, attacker_position.position)
        if min_vertical < angle < max_vertical:
            if for_missile_launch:
                logging.info('%s\' pilot can see attacker %s within launch area', pilot_callsign,
                             attacker_position.callsign)
            else:
                logging.info('%s\' pilot can visually track attacker %s', pilot_callsign,
                             attacker_position.callsign)

            return nose_on_factor
    return None


class Shooter(mat.AttackerTracker):
    __slots__ = ('parent', 'releases_ammunition', 'weapon_type', 'tracking_dist',
                 'max_shooting_dist', 'min_shooting_dist',
                 'max_shooting_angle', 'min_shooting_angle',
                 'muzzle_velocity', 'max_missile_burn_time',
                 'ammo_per_burst', 'ammo_total', 'ammo_current',
                 'reload_time', 'reload_last_time',
                 'ammo_index', 'damage_factor',
                 'radar_visible_dist', 'max_consecutive_bursts',
                 'tracked_attacker', 'tracked_since', 'attacker_metrics',
                 'shooting_history', 'emesary_unique_identity', 'consecutive_bursts',
                 'attacker_acquired', 'last_shot_triggered',
                 'attackers_hist_positions',
                 'min_attacker_acquisition_time', 'max_hidden_tracking', 'fire_minimum_interval',
                 'turret_altitude_speed', 'turret_azimuth_speed',
                 'message_container', 'muzzle_fire', 'turret_azimuth', 'turret_altitude',
                 'max_missile_turn_rate', 'missile_hit_cone', 'missile_is_ir',
                 'radar_elevation_above_terrain_m',
                 'radar_status', 'radar_track_timestamp', 'radar_on_timestamp',
                 'radar_off_timestamp', 'radar_off_duration',
                 'radar_off_time_min', 'radar_off_time_max', 'radar_on_time', 'radar_on_after_detect_time',
                 'hits_on_attacker')

    def __init__(self, parent,  # mpt.MPTarget not imported due to circular reference
                 releases_ammunition: bool,  # does is actually shoot at attackers - or just pretend?
                 weapon_type: AirWeaponType,
                 tracking_dist: int,
                 max_shooting_dist: int, min_shooting_dist: int,
                 max_shooting_angle: int, min_shooting_angle: int, muzzle_velocity: int, max_missile_burn_time: int,
                 ammo_per_burst: int, ammo_total: int, reload_time: int,
                 ammo_index: int, damage_factor: float, radar_visible_dist: int,
                 max_consecutive_bursts: int, fire_minimum_interval: int,
                 turret_altitude_speed: int, turret_azimuth_speed: int,
                 radar_elevation_above_terrain_m: float) -> None:
        super().__init__(parent)

        self.releases_ammunition = releases_ammunition
        self.weapon_type = weapon_type

        self.tracking_dist = tracking_dist  # how far away are even trying to track a plane
        self.max_shooting_dist = max_shooting_dist
        self.min_shooting_dist = min_shooting_dist
        self.max_shooting_angle = max_shooting_angle  # 90 degrees is straight up
        self.min_shooting_angle = min_shooting_angle  # negative is below horizon
        self.muzzle_velocity = muzzle_velocity  # m/s - also used for max missile velocity
        self.max_missile_burn_time = max_missile_burn_time  # how long the missile's motor burns

        self.ammo_per_burst = ammo_per_burst  # 1 for missile, several for bullets
        self.ammo_total = ammo_total
        self.ammo_current = 0  # set to zero at the beginning, so we have a startup time

        self.reload_time = reload_time  # the time it takes to reload and prepare in seconds
        self.reload_last_time = 0  # when reloading was started

        self.ammo_index = ammo_index  # in damage.py -> shells_em
        # how many bullets does it take for a kill in average - based on shells_em in damage.nas
        self.damage_factor = damage_factor  # for shells this is the probability, for warheads the explosive weight
        self.radar_visible_dist = radar_visible_dist  # from how far away the radar is detectable
        self.max_consecutive_bursts = max_consecutive_bursts
        self.fire_minimum_interval = fire_minimum_interval
        self.turret_altitude_speed = turret_altitude_speed  # degs per sec the altitude of the turret can be adjusted
        self.turret_azimuth_speed = turret_azimuth_speed  # degs per sec the azimuth of the turret can be adjusted
        self.radar_elevation_above_terrain_m = radar_elevation_above_terrain_m

        self.tracked_attacker = None  # attacker callsign
        self.tracked_since = 0  # time.time
        self.attacker_metrics = None  # AttackerMetrics
        self.shooting_history = list()  # Either BulletShotInstances or one MissileLaunchedInstance
        self.emesary_unique_identity = 1
        self.consecutive_bursts = 0
        self.attacker_acquired = False
        self.last_shot_triggered = 0  # cannot use shooting_history, because it might be emptied before

        # FIXME hardcoded stuff, which might be different for another shooter
        self.min_attacker_acquisition_time = 8
        # how long we can track the attacker without seeing it - should be OK both for automatic tracker
        # and for a human being guessing where stuff is
        self.max_hidden_tracking = 4
        self.max_missile_turn_rate = 90  # degrees per second
        # degrees within which an attacker is hit. Must be much lower than missile turn rate because turn rate
        # was already possibly used when calculating heading/pitch for the next second
        # on the other hand side we do currently not calculate a lead, so it could perform better
        self.missile_hit_cone = 20
        self.missile_is_ir = True

        # MP stuff
        self.message_container = EmesaryMessageContainer()
        self.muzzle_fire = False  # whether currently there is nozzle fire
        self.turret_azimuth = 0.  # North is 0 or 360. Degrees from the shooter to the attacker.
        self.turret_altitude = 0.  # Horizontal is 0 degrees, straight up is 90, straight down is -90

        # radar stuff dynamically set
        self.radar_status = True
        self.radar_track_timestamp = 0
        self.radar_on_timestamp = 0
        self.radar_off_timestamp = 0
        self.radar_off_duration = 0
        # radar stuff statically set - depends upon SAM model (currently hard-coded for S-200)
        self.radar_off_time_min = 30
        self.radar_off_time_max = 150
        self.radar_on_time = 45
        self.radar_on_after_detect_time = 240

        # just for fun statistics
        self.hits_on_attacker = 0

    @classmethod
    def create_shilka_shooter(cls, parent, releases_ammunition: bool) -> 'Shooter':  # parent is mpt.MP_Target
        """Create the bullet shooting part for a https://en.wikipedia.org/wiki/ZSU-23-4_Shilka
        Mostly based on ref[C].
            * max shooting dist is based on note on page 16 regarding accuracy.
            * bullets per burst = 50 a bit higher than in the source, but in wikipedia 50 per belt
            * using the GSh-23
        """
        import hunter.damage
        shell_data = hunter.damage.shells_em['GSh-23']
        tracking_dist = 10000
        radar_visible_dist = int(1.2 * tracking_dist)

        return cls(parent, releases_ammunition, AirWeaponType.bullet,
                   tracking_dist,
                   2500, BULLETS_MIN_RANGE,  # max/min_shooting_dist
                   85, -10,  # max_shooting_angle, min_shooting_angle
                   930,  # muzzle_velocity
                   0, 50, 2000,  # max_missile_burn_time, ammo_per_burst, ammo_total
                   300,  # 5 min startup / reload time
                   shell_data[0], shell_data[1],  # ammo_index, damage_factor
                   radar_visible_dist,
                   2, 8, 15, 30,  # max_consecutive_bursts, pause_..., turret_altitude_speed, turret_azimuth_speed
                   3,  # ca. height of radar above ground
                   )

    @classmethod
    def create_kashtan_shooter(cls, parent, releases_ammunition: bool) -> 'Shooter':  # parent is mpt.MP_Target
        """Create an imaginary anti-aircraft bullet shooting part for a ship. It is a bit better than the Shilka
        in used shell, tracking_dist, max_shooting_dist, muzzle_velocity, max_consecutive_bursts, view_height as
        well as faster turret. The radar is visible from a longer distance in return.
        Cf. https://en.wikipedia.org/wiki/Close-in_weapon_system, and especially
        https://en.wikipedia.org/wiki/Kashtan_CIWS (Kashtan-M)
        """
        import hunter.damage
        shell_data = hunter.damage.shells_em['GSh-30']
        tracking_dist = 15000
        radar_visible_dist = int(2. * tracking_dist)

        return cls(parent, releases_ammunition, AirWeaponType.bullet,
                   tracking_dist,
                   4700, BULLETS_MIN_RANGE,  # max/min_shooting_dist
                   85, 0,  # max_shooting_angle, min_shooting_angle
                   1100,  # muzzle_velocity
                   0, 50, 3000,  # max_missile_burn_time, ammo_per_burst, ammo_total
                   300,  # 5 min startup / reload time
                   shell_data[0], shell_data[1],  # ammo_index, damage_factor
                   radar_visible_dist,
                   6, 2, 20, 45,  # max_consecutive_bursts, pause_..., turret_altitude_speed, turret_azimuth_speed
                   15,  # ca. height of radar above ground
                   )

    @classmethod
    def create_missile_ir_sa_shooter(cls, parent, releases_ammunition: bool) -> 'Shooter':  # parent is mpt.MP_Target
        """A fictional short range IR missile launcher based on the Shilka firing a man-pad.

        NB: the turret can move faster because assumed less heavy than for Shilka cannons.
        """
        import hunter.damage
        warhead_data = hunter.damage.warheads_em['R-60']
        tracking_dist = 10000
        radar_visible_dist = int(1.2 * tracking_dist)

        instance = cls(parent, releases_ammunition, AirWeaponType.missile_ir_sa,
                       tracking_dist,
                       IR_MISSILE_MAX_RANGE, IR_MISSILE_MIN_RANGE,  # max_shooting_dist
                       85, 0,  # max_shooting_angle, min_shooting_angle
                       IR_MISSILE_MAX_SPEED,  # muzzle_velocity = max speed of missile
                       IR_MISSILE_MAX_BURN_TIME, 1, 8,  # max_missile_burn_time, ammo_per_burst, ammo_total
                       300,  # 5 min startup / reload time
                       warhead_data[0], warhead_data[1],  # ammo_index, damage_factor
                       radar_visible_dist,
                       1, 20, 30, 60,  # max_consecutive_bursts, pause_..., turret_altitude_speed, turret_azimuth_speed
                       3,  # ca. height of radar above ground
                       )
        instance.missile_is_ir = True
        return instance

    @classmethod
    def create_missile_ir_aa_shooter(cls, parent, releases_ammunition: bool) -> 'Shooter':  # parent is mpt.MP_Target
        """A fictional short range IR missile launcher from heli or drone."""
        import hunter.damage
        warhead_data = hunter.damage.warheads_em['R-60']
        tracking_dist = 10000  # a bit more than the THREAT_DISTANCE_IR in mp_dispenser.py
        radar_visible_dist = tracking_dist  # not used - because pilot eyes / IR-scanner only

        instance = cls(parent, releases_ammunition, AirWeaponType.missile_ir_aa,
                       tracking_dist,
                       IR_MISSILE_MAX_RANGE, IR_MISSILE_MIN_RANGE,  # might be reduced the less nose-on
                       85, 0,  # max_shooting_angle, min_shooting_angle
                       IR_MISSILE_MAX_SPEED,  # muzzle_velocity = max speed of missile
                       IR_MISSILE_MAX_BURN_TIME, 1, 4,  # max_missile_burn_time, ammo_per_burst, ammo_total
                       99999,  # no reload
                       warhead_data[0], warhead_data[1],  # ammo_index, damage_factor
                       radar_visible_dist,
                       1, 10, 30, 60,  # max_consecutive_bursts, pause_..., turret_altitude_speed, turret_azimuth_speed
                       1,  # ca. height of radar above ground
                       )
        instance.missile_is_ir = True
        instance.ammo_current = 4  # trick to circumvent the "forever" reload time at startup
        instance.min_attacker_acquisition_time = 3  # pilot is much faster than radar automatics which take 8 secs
        return instance

    @classmethod
    def create_missile_fake_shooter(cls, parent) -> 'Shooter':  # parent is mpt.MP_Target
        """A fake missile shooter (SAM). It tracks targets and locks them, but does not actually
        launch a missile.
        Used to give a more realistic impression of a SAM, give pilots a bit of panic and make it harder
        to distinguish between simulated SAMs as MP targets and FG instances.

        All values below, which are not set before instance = cls(...) do not matter/are not used.

        """
        import hunter.mp_targets as mpt
        import hunter.damage
        sam = mpt.SAM_VARIANTS[parent.name]
        warhead_data = hunter.damage.warheads_em[sam.missile_brevity]  # does not matter

        # the max shooting dist is reduced somewhat because in reality it also might be shorter
        # for this fake shooter the max distance is mainly used to get the radar lock
        max_shooting_dist = int(g.nm_to_metres(sam.missile_max_distance_nm) * 0.75)

        instance = cls(parent, False, AirWeaponType.missile_fake,
                       max_shooting_dist + 1,
                       max_shooting_dist, int(g.nm_to_metres(sam.missile_min_distance_nm)),
                       89, 0,  # max_shooting_angle, min_shooting_angle
                       750,  # muzzle_velocity
                       6, 1, sam.num_missiles,  # max_missile_burn_time, ammo_per_burst, ammo_total
                       sam.reload_time,
                       warhead_data[0], warhead_data[1],  # ammo_index, damage_factor
                       max_shooting_dist + 1,
                       1, sam.fire_minimum_interval,
                       30, 60,  # turret_altitude_speed, turret_azimuth_speed
                       sam.radar_elevation_above_terrain_m,  # ca. height of radar above ground
                       )
        instance.missile_is_ir = False
        instance.radar_off_time_min = sam.radar_off_time_min
        instance.radar_off_time_max = sam.radar_off_time_max
        instance.radar_on_time = sam.radar_on_time
        instance.radar_on_after_detect_time = sam.radar_on_after_detect_time

        return instance

    @property
    def is_out_of_ammunition(self) -> bool:
        return self.ammo_current < self.ammo_per_burst

    # implements AttackerTracker._process_attackers()
    def _process_attackers(self, missiles_in_flight: List[en.ArmamentInFlightNotification]) -> None:
        self.message_container.reset()
        now = time.time()
        # looking back on prev shot bullets / missiles - have they reached the target
        # has to be done before _check_for_tracking_and_shooting
        if self.weapon_type is AirWeaponType.bullet:
            self._check_for_bullet_hits(now)
        elif self.weapon_type in [AirWeaponType.missile_ir_sa, AirWeaponType.missile_ir_aa]:
            self._check_for_missile_hits(now)
        elif self.weapon_type is AirWeaponType.missile_fake:
            pass  # nothing to do
        # now we check whether more shooting should/can be done - includes also radar logic
        self._check_for_tracking_and_shooting(now)

        # finally update the parent with our radar status - we need the radar_status to be local in order to be
        # able to have some radar on/off based on the type of shooter

        # if there is still a missile in the air, then we keep the radar on no matter what (override)
        if self.weapon_type is AirWeaponType.missile_fake and self.shooting_history:
            self.parent.enable_radar()  # but we do not want to overwrite the radar_status
            return
        if self.radar_status:
            self.parent.enable_radar()
        else:
            self.parent.disable_radar()
            self.parent.release_radar_lock()  # just to be sure

    def _check_for_bullet_hits(self, now: float) -> None:
        """Based on prev shooting of bullets -> calculate hits and missed.
        If the flying time of the bullet is the same or longer than the distance, then calculate a probability for
        a hit and maybe send an ArmamentNotification.
        Once a hit probability has been calculated, then discard that part of the shooting history.

        Cannot track and hit more than one attacker even though bullets are some seconds on their way.
        Therefore, it is safe to just take the callsign of the last hit and sum all bullets up. E.g. if the first
        burst was when the plane was longer away and flies towards the bullets, then within 1 second bullets from
        2 different bursts might arrive near the plane.
        """
        total_number_of_bullets = 0
        attacker_callsign = None
        for shoot_instance in self.shooting_history:
            # is the attacker still alive?
            if shoot_instance.callsign not in self.attackers_hist_positions:
                self.shooting_history.remove(shoot_instance)
                continue
            # check whether ammunition has reached the attacker
            attacker_latest_position = self.attackers_hist_positions[shoot_instance.callsign].latest_position
            dist = g.calc_distance_pos_3d(self.parent.position, attacker_latest_position)
            if dist > self.max_shooting_dist:  # bullet has lost all energy and can be removed
                self.shooting_history.remove(shoot_instance)
                continue

            flight_time = _calc_bullet_flight_time(dist, self.muzzle_velocity)
            delta_time = now - shoot_instance.timestamp
            if delta_time >= flight_time:  # bullet has reached (or passed) the attacker
                # calculate the probability of hit
                probability_kill = 0.16 - 0.14 * dist/self.max_shooting_dist  # hard coded for Shilka right now
                logging.info('Probability kill = %f', probability_kill)
                if probability_kill > 0.:
                    number_of_bullets = probability_kill / self.damage_factor
                    logging.info('Number of bullets: %f', number_of_bullets)
                    total_number_of_bullets += number_of_bullets
                    attacker_callsign = shoot_instance.callsign
                # remove from history because processed with hit (cannot hit twice)
                self.shooting_history.remove(shoot_instance)
                continue
            else:
                pass  # nothing to do - maybe in 1 second the bullets will have arrived
        # now do an armament notification for the total bullets
        if total_number_of_bullets < 0.33:
            total_number_of_bullets = 0
        elif total_number_of_bullets < 1.:
            total_number_of_bullets = 1
        else:
            total_number_of_bullets = int(total_number_of_bullets)
        if total_number_of_bullets > 0:
            encoded_msg = _process_outgoing_armament_hit_notification(self.parent.next_emesary_msg_idx(), self.ammo_index,
                                                                      total_number_of_bullets, attacker_callsign, True)
            logging.info('%s sending bullet armament notification for %s', self.parent.callsign, attacker_callsign)
            self.message_container.add_message(encoded_msg)
            self.hits_on_attacker += 1

    def _check_for_missile_hits(self, now: float) -> None:
        """Based on previous shooting of a missile -> calculate hits and missed.
        If the missile has travelled close to the plane or would have passed it, then calculate a probability of hit
        and maybe send an ArmamentNotification if it has exploded.
        Otherwise, send an ArmamentInFlightNotification to give the plane a visual clue of the missile.
        Once a missile has hit a plane or has hit a countermeasure or has lost its energy, then discard that part
        of the shooting history.
        """
        if not self.shooting_history:
            return

        for missile_instance in self.shooting_history:
            delta_time = now - missile_instance.timestamp
            speed = _calc_missile_speed(delta_time, self.muzzle_velocity, self.max_missile_burn_time,
                                        self.weapon_type is AirWeaponType.missile_ir_aa)
            flown_dist = speed * self.parent.send_freq
            can_manoeuvre = True  # only after missile out of fuel
            if delta_time > self.max_missile_burn_time and speed < IR_MISSILE_MIN_MANOEUVRE_SPEED:
                can_manoeuvre = False

            # is the attacker still alive?
            if missile_instance.callsign not in self.attackers_hist_positions:
                missile_instance.status = MissileState.lost_tracking

            # is the missile still alive based on status and time passed?
            if missile_instance.status is MissileState.active and can_manoeuvre is False:
                logging.info('%s has self-destroyed missile[%i] targeting %s due to too low speed',
                             self.parent.callsign, missile_instance.unique_identity, missile_instance.callsign)
                missile_instance.status = MissileState.deleted

            if missile_instance.status is MissileState.deleted:
                encoded_msg = _process_outgoing_armament_in_flight_deleted(self.parent.next_emesary_msg_idx(),
                                                                           self.ammo_index,
                                                                           missile_instance.unique_identity)
                logging.info('%s sending missile[%i] in flight deleted for %s', self.parent.callsign,
                             missile_instance.unique_identity, missile_instance.callsign)
                self.message_container.add_message(encoded_msg)
                self.shooting_history.remove(missile_instance)
                continue

            # does the missile bite on chaff / flare - but we are not fooled before missile has some speed
            if missile_instance.status is MissileState.active and can_manoeuvre:
                lap = self.attackers_hist_positions[missile_instance.callsign].latest_attacker_position
                if self.missile_is_ir and lap.flares_on:
                    if random.random() > FLARES_RESISTANCE:
                        missile_instance.status = MissileState.bit_on_counter_measures
                elif self.missile_is_ir is False and lap.chaff_on:
                    if random.random() > CHAFF_RESISTANCE:
                        missile_instance.status = MissileState.bit_on_counter_measures

            # Can we still try to catch the attacker?
            if missile_instance.status is MissileState.active and can_manoeuvre:
                guessed_next_pos = self.attackers_hist_positions[missile_instance.callsign].guess_next_position()
                latest_pos_missile = missile_instance.positions[-1]
                bearing_to_attacker = g.calc_bearing_pos(latest_pos_missile, guessed_next_pos)
                abs_delta_bearing = math.fabs(missile_instance.heading - bearing_to_attacker)
                # print('missile heading', missile_instance.heading, '; missile bearing to attacker',
                #       bearing_to_attacker, '; abs diff', abs_delta_bearing)
                pitch_to_attacker = g.calc_altitude_angle(latest_pos_missile, guessed_next_pos)
                abs_delta_pitch = math.fabs(missile_instance.pitch - pitch_to_attacker)
                # print('missile pitch', missile_instance.pitch, '; missile pitch to attacker',
                #       pitch_to_attacker, '; abs diff', abs_delta_pitch)

                # first send the notification for continuing the missile path
                # adjust pitch and heading of the missile
                turn_rate_for_freq = self.max_missile_turn_rate * self.parent.send_freq
                # print('allowed turn rate in degrees', turn_rate_for_freq)
                missile_instance.heading = g.calc_limited_bearing(missile_instance.heading, bearing_to_attacker,
                                                                  turn_rate_for_freq)
                # print('new missile heading', missile_instance.heading)
                missile_instance.pitch = g.calc_limited_pitch(missile_instance.pitch, pitch_to_attacker,
                                                              turn_rate_for_freq)
                # print('new missile pitch', missile_instance.pitch)
                # Calculate the new position
                next_pos_missile = g.calc_destination_pos_3d(latest_pos_missile, flown_dist,
                                                             missile_instance.heading, missile_instance.pitch)
                missile_instance.positions.append(next_pos_missile)
                # send notification
                thrust_on = delta_time <= self.max_missile_burn_time
                encoded_msg = _process_outgoing_armament_in_flight_notification(self.parent.next_emesary_msg_idx(),
                                                                                self.ammo_index,
                                                                                missile_instance.unique_identity,
                                                                                missile_instance.positions[-1],
                                                                                missile_instance.heading,
                                                                                missile_instance.pitch,
                                                                                speed,
                                                                                missile_instance.radar_on,
                                                                                thrust_on,
                                                                                missile_instance.callsign,
                                                                                False)
                logging.info('%s sending missile[%i] in flight notification for %s', self.parent.callsign,
                             missile_instance.unique_identity, missile_instance.callsign)
                self.message_container.add_message(encoded_msg)

                # then check for possible hit
                distance_to_attacker = g.calc_distance_pos_3d(latest_pos_missile, guessed_next_pos)
                # print('missile distance to attacker', distance_to_attacker)
                # we use a factor 1.5 to take into account that 0.5 secs is a long time
                # and a real missile would have more time to calculate the trajectory better
                # if we do not have this factor, then most simulated missiles will fly past
                if distance_to_attacker <= flown_dist * 1.5:
                    if abs_delta_bearing <= self.missile_hit_cone and abs_delta_pitch <= self.missile_hit_cone:
                        logging.info('Missile[%i] from %s has hit %s', missile_instance.unique_identity,
                                     self.parent.callsign, missile_instance.callsign)
                        encoded_msg = _process_outgoing_armament_hit_notification(self.parent.next_emesary_msg_idx(),
                                                                                  self.ammo_index,
                                                                                  1, missile_instance.callsign, False)
                        logging.info('%s sending missile[%i] hit armament notification for %s', self.parent.callsign,
                                     missile_instance.unique_identity, missile_instance.callsign)
                        self.message_container.add_message(encoded_msg)
                        self.hits_on_attacker += 1
                        missile_instance.status = MissileState.deleted
                    else:  # missile has overshot the target
                        missile_instance.status = MissileState.overshot
            elif missile_instance.status is MissileState.active and can_manoeuvre is False:
                logging.info('%s has a missile[%i] too slow to continue following %s -> lost tracking',
                             self.parent.callsign, missile_instance.unique_identity, missile_instance.callsign)
                missile_instance.status = MissileState.lost_tracking
            if missile_instance.status is MissileState.bit_on_counter_measures:
                # make sure that the missile is soon destroyed as it has bitten on a chaff/flare
                missile_instance.lost_tracking_cycles = MAX_LOST_TRACKING_CYCLES - 2
                logging.info('%s has a missile[%i] biting on counter measures from %s -> lost tracking',
                             self.parent.callsign, missile_instance.unique_identity, missile_instance.callsign)
                missile_instance.status = MissileState.lost_tracking
            elif missile_instance.status is MissileState.overshot:
                logging.info('%s has missile[%i] overshot targeting %s -> lost tracking',
                             self.parent.callsign, missile_instance.unique_identity, missile_instance.callsign)
                missile_instance.status = MissileState.lost_tracking

            if missile_instance.status is MissileState.lost_tracking:
                missile_instance.lost_tracking_cycles += 1
                # no need to change the heading or pitch - missile keeps orientation in hope to get track again
                next_pos_missile = g.calc_destination_pos_3d(missile_instance.positions[-1], flown_dist,
                                                             missile_instance.heading, missile_instance.pitch)
                missile_instance.positions.append(next_pos_missile)
                encoded_msg = _process_outgoing_armament_in_flight_lost(self.parent.next_emesary_msg_idx(),
                                                                        self.ammo_index,
                                                                        missile_instance.unique_identity,
                                                                        missile_instance.positions[-1],
                                                                        missile_instance.heading,
                                                                        missile_instance.pitch,
                                                                        speed)
                logging.info('%s sending missile[%i] in flight lost for %s', self.parent.callsign,
                             missile_instance.unique_identity, missile_instance.callsign)
                self.message_container.add_message(encoded_msg)
                if missile_instance.lost_tracking_cycles > MAX_LOST_TRACKING_CYCLES:
                    missile_instance.status = MissileState.deleted
                    logging.info('Missile[%i] for %s setting to deleted due to too long lost tracking',
                                 missile_instance.unique_identity, self.parent.callsign)

    def _check_for_tracking_and_shooting(self, now: float) -> None:
        """Engagement logic."""
        self.muzzle_fire = False
        # check whether we still can track and shoot
        if not m.is_capable_of_fighting_health(self.parent.health):
            self._untrack_attacker('health')
            self.radar_status = False
            return
        # do we have any ammo at all? Btw: right at beginning it is out of ammo to have start-up time
        if self.is_out_of_ammunition:
            if self.reload_last_time == 0:
                logging.info('%s is out of ammo - starting to reload', self.parent.callsign)
                self.reload_last_time = now
                self._untrack_attacker('no ammunition')
                self.radar_status = False
                self.radar_track_timestamp = 0
                self.radar_on_timestamp = 0
                self.radar_off_timestamp = 0
            elif self.reload_last_time + self.reload_time < now:
                logging.info('%s has reloaded ammo', self.parent.callsign)
                self.reload_last_time = 0
                self.ammo_current = self.ammo_total
            return  # not ready now -> returning

        # check whether we still can track and shoot at the current chosen attacker
        closest_distance = 999999
        if self.tracked_attacker:
            if self.tracked_attacker not in self.attackers_hist_positions:
                self._untrack_attacker('tracked attacker gone')
            else:
                attacker_pos = self.attackers_hist_positions[self.tracked_attacker].latest_position
                do_untrack = False
                closest_distance = g.calc_distance_pos_3d(self.parent.position,
                                                          attacker_pos)
                if closest_distance > self.tracking_dist:
                    do_untrack = True
                else:
                    self.attacker_metrics.add_metric(attacker_pos)
                    visible = self._check_attacker_visibility(attacker_pos,
                                                              self.weapon_type is AirWeaponType.missile_ir_aa)
                    if not visible:
                        self.attacker_metrics.update_last_metric_as_hidden()
                        if self.attacker_metrics.lost_tracking:
                            do_untrack = True
                    elif self.weapon_type is AirWeaponType.missile_ir_aa:
                        att_pos_object = self.attackers_hist_positions[self.tracked_attacker].latest_attacker_position
                        visible = _visibility_from_pilots_eye(self.parent.position, self.parent.orientation.hdg,
                                                              self.parent.callsign, att_pos_object, False)
                        if visible is None:
                            do_untrack = True
                if do_untrack:
                    self._untrack_attacker('not visible')

        if self.tracked_attacker is None:
            candidate_attacker = None
            closest_distance = 999999
            for attacker_callsign, position_history in self.attackers_hist_positions.items():
                distance = g.calc_distance_pos_3d(self.parent.position, position_history.latest_position)
                visible = self._check_attacker_visibility(position_history.latest_position,
                                                          self.weapon_type is AirWeaponType.missile_ir_aa)
                if distance < closest_distance and distance < self.tracking_dist and visible:
                    if self.weapon_type is AirWeaponType.missile_ir_aa:
                        att_pos_object = self.attackers_hist_positions[attacker_callsign].latest_attacker_position
                        visible = _visibility_from_pilots_eye(self.parent.position, self.parent.orientation.hdg,
                                                              self.parent.callsign, att_pos_object, False)
                        if visible is None:
                            continue
                    closest_distance = distance
                    candidate_attacker = attacker_callsign
            if candidate_attacker:
                self._track_attacker(candidate_attacker, now,
                                     self.attackers_hist_positions[candidate_attacker].latest_position)
        self._radar_logic(now, closest_distance)

        # can we and shall we shoot at the attacker?
        if self.tracked_attacker:
            attacker_pos = self.attackers_hist_positions[self.tracked_attacker].latest_position
            if self.weapon_type is not AirWeaponType.missile_ir_aa:
                # calculate relative orientations - must be done first such that the turret is looking into the correct
                # direction as close as possible despite not shooting
                # we assume some logic to even track hidden attackers and allow turret to be somewhat ready
                turret_alt_is_ready = self._adjust_turret_altitude(attacker_pos)
                turret_azi_is_ready = self._adjust_turret_azimuth(attacker_pos)

                # check whether angles within shooting parameters
                if turret_alt_is_ready is False or turret_azi_is_ready is False:
                    return  # not ready to shoot

            # check whether attacker is currently visible
            if self.attacker_metrics.currently_hidden:
                return  # not ready to shoot
            # make sure attacker is acquired
            if not self.attacker_acquired:
                if now - self.tracked_since > self.min_attacker_acquisition_time:
                    self.attacker_acquired = True
                else:
                    return  # not yet ready to shoot again
            # we take some pauses between consecutive bursts such that the attacker does not know, that we exist
            # and where we are - and for missiles it does not make sense to shoot too many at once
            if self.consecutive_bursts >= self.max_consecutive_bursts:
                if now - self.last_shot_triggered > self.fire_minimum_interval:
                    self.consecutive_bursts = 0
                    self.last_shot_triggered = 0
                else:
                    return  # not yet ready to shoot again

            # adapt the max shooting dist depending on nose-on between pilot and attacker
            att_pos_object = self.attackers_hist_positions[self.tracked_attacker].latest_attacker_position
            if self.weapon_type is AirWeaponType.missile_ir_aa:
                nose_on_factor = _visibility_from_pilots_eye(self.parent.position,
                                                             self.parent.orientation.hdg,
                                                             self.parent.callsign,
                                                             att_pos_object, True)
            else:
                nose_on_factor = _nose_on_factor(self.parent.orientation.hdg, att_pos_object.heading)
            corrected_max_distance = self.max_shooting_dist
            if nose_on_factor:
                corrected_max_distance = self.max_shooting_dist * (0.8 + .4 * nose_on_factor)
            # print('nose on factor', nose_on_factor, '; max shooting dist', self.max_shooting_dist)
            # print('corrected max distance:', corrected_max_distance, '-- closest distance: ', closest_distance)
            if closest_distance <= corrected_max_distance:
                has_shot = False
                if self.weapon_type is AirWeaponType.bullet and closest_distance > self.min_shooting_dist:
                    if self.releases_ammunition:
                        self.shooting_history.append(BulletShotInstance(time.time(), self.tracked_attacker))
                        logging.info('%s shoots bullet at %s', self.parent.callsign, self.tracked_attacker)
                    else:
                        logging.info('%s fakes shooting bullet at %s', self.parent.callsign, self.tracked_attacker)

                    self.muzzle_fire = True  # show muzzle fire
                    has_shot = True
                elif self.weapon_type in [AirWeaponType.missile_ir_sa, AirWeaponType.missile_ir_aa] and (
                        closest_distance > self.min_shooting_dist):
                    launch_ready = nose_on_factor is not None
                    if self.releases_ammunition and launch_ready:
                        heading = g.calc_bearing_pos(self.parent.position, attacker_pos)
                        pitch = g.calc_altitude_angle(self.parent.position, attacker_pos)
                        unique_identity = self.emesary_unique_identity
                        self.emesary_unique_identity += 1
                        positions = [self.parent.position]
                        self.shooting_history.append(MissileLaunchedInstance(time.time(), self.tracked_attacker,
                                                                             unique_identity, False,
                                                                             MissileState.active,
                                                                             heading, pitch, positions, 0))
                        logging.info('%s shoots new missile[%i] at %s', self.parent.callsign,
                                     unique_identity, self.tracked_attacker)
                    elif self.releases_ammunition is False and launch_ready:
                        logging.info('%s fakes shooting missiles at %s', self.parent.callsign, self.tracked_attacker)
                    if launch_ready:
                        self.muzzle_fire = True  # show muzzle fire - until we get something better
                        has_shot = True
                else:  # AirWeaponType.missile_fake
                    pass  # no shooting
                if has_shot:
                    self.consecutive_bursts += 1
                    self.last_shot_triggered = now
                    self.ammo_current -= self.ammo_per_burst
        else:
            pass  # we do nothing, not even adjust the turret into a neutral position

    def _track_attacker(self, callsign: str, now: float, position: g.Position) -> None:
        self.tracked_attacker = callsign
        self.tracked_since = now
        self.attacker_metrics = AttackerMetrics(self.max_hidden_tracking / self.parent.send_freq)
        self.attacker_metrics.add_metric(position)
        if self.weapon_type is not AirWeaponType.missile_ir_aa:
            self.parent.lock_radar_on_callsign(callsign)
        logging.info('%s %s started tracking of %s', self.parent.name, self.parent.callsign, self.tracked_attacker)

    def _untrack_attacker(self, reason: str) -> None:
        if self.tracked_attacker is not None:
            logging.info('%s %s lost tracking of %s due to %s', self.parent.name, self.parent.callsign,
                         self.tracked_attacker, reason)
        self.tracked_attacker = None
        self.tracked_since = 0
        self.attacker_metrics = None
        self.consecutive_bursts = 0
        self.attacker_acquired = False
        self.last_shot_triggered = 0
        if self.weapon_type is not AirWeaponType.missile_ir_aa:
            self.parent.release_radar_lock()

    def _adjust_turret_altitude(self, attacker_pos: g.Position) -> bool:
        """Adjusts the altitude of the turret cannon(s). Returns True if within shooting parameters."""
        turret_could_shoot = True
        attacker_altitude = g.calc_altitude_angle(self.parent.position, attacker_pos)
        # adjust the turret altitude
        if self.turret_altitude < attacker_altitude:
            if (attacker_altitude - self.turret_altitude) > self.turret_altitude_speed * self.parent.send_freq:
                self.turret_altitude += self.turret_altitude_speed * self.parent.send_freq
                turret_could_shoot = False
            else:
                self.turret_altitude = attacker_altitude
            if self.turret_altitude > self.max_shooting_angle:
                self.turret_altitude = self.max_shooting_angle
                turret_could_shoot = False
        elif self.turret_altitude > attacker_altitude:
            if (self.turret_altitude - attacker_altitude) > self.turret_altitude_speed * self.parent.send_freq:
                self.turret_altitude -= self.turret_altitude_speed * self.parent.send_freq
                turret_could_shoot = False
            else:
                self.turret_altitude = attacker_altitude
            if self.turret_altitude < self.min_shooting_angle:
                self.turret_altitude = self.min_shooting_angle
                turret_could_shoot = False
        return turret_could_shoot

    def _adjust_turret_azimuth(self, attacker_pos: g.Position) -> bool:
        """Adjusts the azimuth of the turret cannon(s). Returns True if within shooting parameters."""
        turret_could_shoot = True
        bearing_to_attacker = g.calc_bearing_pos(self.parent.position, attacker_pos)
        delta = g.calc_delta_bearing(self.turret_azimuth, bearing_to_attacker)
        if math.fabs(delta) > self.turret_azimuth_speed * self.parent.send_freq:
            turret_could_shoot = False

        self.turret_azimuth = g.calc_limited_bearing(self.turret_azimuth, bearing_to_attacker,
                                                     self.turret_azimuth_speed * self.parent.send_freq)
        return turret_could_shoot

    def _check_attacker_visibility(self, attacker_pos: g.Position, is_flying: bool) -> bool:
        view_pos = g.Position(self.parent.position.lon, self.parent.position.lat,
                              self.parent.position.alt_m + self.radar_elevation_above_terrain_m)
        attacker_altitude = g.calc_altitude_angle(view_pos, attacker_pos)
        if not self.parent.dem:
            if is_flying:
                return True
            return attacker_altitude > u.EXTRA_ALTITUDE
        return self.parent.dem.is_visible(view_pos, attacker_pos)

    def _set_radar_off_timestamp(self, now: float) -> None:
        self.radar_status = False
        self.radar_off_duration = random.random() * (
                self.radar_off_time_max - self.radar_off_time_min) + self.radar_off_time_min
        self.radar_off_timestamp = now

    def _radar_logic(self, now: float, closest_distance: float) -> None:
        """Turns radar on and off based on type, time and missile status.
        
        For missile_fake based on func acquisitionRadarLoop ca. line 280 in fire-control.nas

        Processing has to be done in a hierarchical order
        """
        # handle radar for real SAMs
        if self.weapon_type is AirWeaponType.missile_fake:
            # check whether we are at the very beginning
            if self.radar_track_timestamp == 0 and self.radar_on_timestamp == 0 and self.radar_off_timestamp == 0:
                self.radar_on_timestamp = now
                self.radar_status = True
                return
            # if we previously found an attacker, then we want to stay on for a while to track it,
            # radar_on_after_detect_time should always be > than radar_on_time,
            # so we can shut it down if time has gone
            if self.radar_track_timestamp > 0:
                if now - self.radar_track_timestamp < self.radar_on_after_detect_time:
                    self.radar_status = True
                else:
                    self.radar_track_timestamp = 0
                    self._set_radar_off_timestamp(now)
            elif self.radar_on_timestamp > 0:
                if self.radar_track_timestamp == 0 and self.tracked_attacker:
                    self.radar_track_timestamp = now
                    self.radar_on_timestamp = 0
                    self.radar_status = True
                elif now - self.radar_on_timestamp < self.radar_on_time:
                    self.radar_status = True
                else:
                    self.radar_on_timestamp = 0
                    self._set_radar_off_timestamp(now)
            elif now - self.radar_off_timestamp < self.radar_off_duration:
                self.radar_status = False
            else:
                self.radar_off_timestamp = 0
                self.radar_on_timestamp = now
                self.radar_status = True
        elif self.weapon_type is AirWeaponType.missile_ir_aa:
            self.radar_status = False  # do not fly with radar on
        else:  # handle radar for tracking shooters like Shilka
            if closest_distance < self.radar_visible_dist:
                self.radar_status = True
            else:
                self.radar_status = False

    def extend_props_with_shooting_stuff(self, props: Dict[int, Any]) -> None:
        """Extend the properties to send to MP with shooting info - maybe including an Emesary notifications.
        Called from the FGMSParticipant - after calling process_attacker_info(...)
        """
        self.message_container.add_messages_to_props(props)
        # visible gun fire
        props[u.MP_PROP_GENERIC_INT_BASE + 1] = 1 if self.muzzle_fire else 0
        # turret position is relative to the vehicle
        delta = g.calc_delta_bearing(self.parent.orientation.hdg, self.turret_azimuth)
        if delta == 0:
            props[u.MP_PROP_GENERIC_FLOAT_BASE] = 0
        elif 0 < delta <= 180:  # if to the right, then negative values
            props[u.MP_PROP_GENERIC_FLOAT_BASE] = -1 * delta
        else:  # else to the left and positive values
            props[u.MP_PROP_GENERIC_FLOAT_BASE] = 360 - delta
        # gun altitude
        props[u.MP_PROP_GENERIC_FLOAT_BASE + 1] = self.turret_altitude


class TestShooting(unittest.TestCase):
    def test_calc_bullet_flight_time(self):
        flight_time = _calc_bullet_flight_time(500, 900)  # Shilka
        self.assertTrue(flight_time < 1.0, 'Distance below muzzle velocity')

        flight_time = _calc_bullet_flight_time(4000, 900)  # Shilka
        self.assertTrue(flight_time < 10, 'Shooting distance within parameters Shilka')

        flight_time = _calc_bullet_flight_time(4700, 1100)  # Kashtan
        self.assertTrue(flight_time < 10, 'Shooting distance within parameters Kashtan')

        flight_time = _calc_bullet_flight_time(10000, 900)
        self.assertTrue(flight_time > 50, 'Shooting distance outside parameters')

    def test_calc_missile_speed(self):
        speed = _calc_missile_speed(1 + 1, IR_MISSILE_MAX_SPEED, IR_MISSILE_MAX_BURN_TIME, False)
        self.assertTrue(speed < IR_MISSILE_MAX_SPEED, 'Missile under acceleration')
        speed = _calc_missile_speed(1 + 4, IR_MISSILE_MAX_SPEED, IR_MISSILE_MAX_BURN_TIME, False)
        self.assertAlmostEqual(speed, IR_MISSILE_MAX_SPEED, 1, 'Missile at full burn')
        speed = _calc_missile_speed(IR_MISSILE_MAX_BURN_TIME + 1, IR_MISSILE_MAX_SPEED, IR_MISSILE_MAX_BURN_TIME, False)
        self.assertTrue(speed < IR_MISSILE_MAX_SPEED)

    def test_emesary_encode(self):
        outgoing_cannon = _process_outgoing_armament_hit_notification(13, 9, 14, 'OPFOR50', True)
        self.assertEqual(outgoing_cannon.encoded_message,
                         b'!\x83\x01\x01\x0e!\x96!\x87w\x83\x01\x83\x93\x83\x8aOPFOR50~',
                         'F-16 cannon outgoing with index 13 and 14 hits')

    def test_emesary_message(self):
        shooter_callsign = 'OPFOR41'
        attacker_callsign = 'HB-VANO'
        position = g.Position(23.312661131045957, 70.05936093479755, 2000)
        import hunter.fgms_io as fio
        import ext.fgms as f
        import hunter.mp_targets as mpt
        test_mp_target = mpt.MPTarget.create_target(mpt.MPTarget.SHILKA, position, 0.)
        scenario_path = '/home/vanosten/develop_hunter/hunter-scenarios'
        scenario_name = 'test'
        scenario = sc.load_scenario(scenario_name, scenario_path)
        test_mp_target.shooter = Shooter.create_shilka_shooter(test_mp_target, True)
        test_mp_target.callsign = shooter_callsign
        participant = fio.FGMSParticipant(test_mp_target, 'localhost', 5000, 5001)
        broker = fio.FGMSThreadedBroker(participant)
        # ca. middle of run method of FGMSSender
        props = test_mp_target.prepare_mp_properties()
        # add to notifications for same bridge for bullets
        number_of_bullets = 1
        encoded_msg = _process_outgoing_armament_hit_notification(11111, 4, number_of_bullets,
                                                                  attacker_callsign, True)
        test_mp_target.shooter.message_container.add_message(encoded_msg)
        encoded_msg = _process_outgoing_armament_hit_notification(11112, 4, number_of_bullets + 10,
                                                                  attacker_callsign, True)
        test_mp_target.shooter.message_container.add_message(encoded_msg)
        # add some missile stuff
        import hunter.damage
        encoded_msg = _process_outgoing_armament_in_flight_deleted(11113, hunter.damage.warheads_em['R-60'][0], 12)
        test_mp_target.shooter.message_container.add_message(encoded_msg)

        test_mp_target.shooter.extend_props_with_shooting_stuff(props)
        packet_position = fio.create_position_message_static(test_mp_target, properties=props)
        packet_header = fio.create_header_packet(test_mp_target, f.position_message_type_code, packet_position)

        udp_packet = packet_header.allData()

        # from Controller _receive_fgms_data(self, udp_packet) -> None:
        fgms_unpacked_data = fio.decode_fgms_data_packet(udp_packet, dict())

        # from Controller _process_emesary_notifications(...)
        decoded_notifications = dict()
        for value in fgms_unpacked_data.encoded_notifications.values():
            if value != '':
                # noinspection PyBroadException
                try:
                    decoded_notifications.update(en.process_incoming_message(value))
                except Exception:
                    logging.error('Cannot process emesary notification')

        for notification in decoded_notifications.values():
            self.assertEqual(attacker_callsign, notification.remote_callsign, 'Attacker callsign')
            self.assertGreater(notification.distance, 0, 'Number of bullets')
            break  # just process the first one

    def test_aa_shooter_tracking(self):
        import hunter.mp_targets as mpt
        import hunter.scenario_tools.dem_grid as dg
        shooter_alt = 1000
        shooter_lon_lat = (25., 70.)
        shooter_hdg = 90.
        shooter_pos = g.Position(shooter_lon_lat[0], shooter_lon_lat[1], shooter_alt)  # south of ENNA
        shooter_target = mpt.MPTarget.create_target(mpt.MPTarget.BUNKER, shooter_pos, shooter_hdg)
        shooter_target.callsign = 'SHOOTER'
        scenario_path = '/home/vanosten/develop_hunter/hunter-scenarios'
        scenario_name = 'test'
        scenario = sc.load_scenario(scenario_name, scenario_path)
        shooter_target.dem = dg.read_scenario_and_load_in(scenario)

        my_shooter = Shooter.create_missile_ir_aa_shooter(shooter_target, True)
        # setting some params just to be sure and be able to reuse
        tracking_dist = 10000
        my_shooter.tracking_dist = tracking_dist
        max_shooting_dist = 8000
        my_shooter.max_shooting_dist = max_shooting_dist
        min_shooting_dist = 1000
        my_shooter.min_shooting_dist = min_shooting_dist

        # [A] ---- test tracking

        att_callsign = 'ATTACK1'
        att_alt = shooter_alt
        att_hdg = 270  # fly nose-on
        att_lon_lat = g.calc_destination_pos(shooter_pos, tracking_dist + 1000, shooter_hdg)  # in front of pilot
        att_pos = g.Position(att_lon_lat[0], att_lon_lat[1], att_hdg)
        att_pos_msg = m.AttackerPosition(att_callsign, att_pos, att_hdg, 0., None, False, False, False, 0., None)
        attacker_positions = {att_pos_msg.callsign: att_pos_msg}
        my_shooter.process_attacker_info(attacker_positions, list())
        self.assertIsNone(my_shooter.tracked_attacker, 'Never seen, outside of tracking distance')

        att_lon_lat = g.calc_destination_pos(shooter_pos, tracking_dist - 1000, shooter_hdg)  # in front of pilot
        att_pos = g.Position(att_lon_lat[0], att_lon_lat[1], att_alt)
        att_pos_msg = m.AttackerPosition(att_callsign, att_pos, att_hdg, 0., None, False, False, False, 0.5, None)
        attacker_positions = {att_pos_msg.callsign: att_pos_msg}
        my_shooter.process_attacker_info(attacker_positions, list())
        self.assertIsNotNone(my_shooter.tracked_attacker, 'First time seen, inside of tracking distance')

        attacker_positions = dict()
        my_shooter.process_attacker_info(attacker_positions, list())
        self.assertIsNone(my_shooter.tracked_attacker, 'Tracked attacker is not anymore in attacker_positions')

        att_lon_lat = g.calc_destination_pos(shooter_pos, tracking_dist - 1000, shooter_hdg)  # in front of pilot
        att_pos = g.Position(att_lon_lat[0], att_lon_lat[1], att_alt)
        att_pos_msg = m.AttackerPosition(att_callsign, att_pos, att_hdg, 0., None, False, False, False, 1., None)
        attacker_positions = {att_pos_msg.callsign: att_pos_msg}
        my_shooter.process_attacker_info(attacker_positions, list())
        self.assertIsNotNone(my_shooter.tracked_attacker, 'Tracking again')

        att_lon_lat = g.calc_destination_pos(shooter_pos, 10, shooter_hdg)  # in front of pilot
        att_pos = g.Position(att_lon_lat[0], att_lon_lat[1], att_alt)
        att_pos_msg = m.AttackerPosition(att_callsign, att_pos, att_hdg, 0., None, False, False, False, 1.5, None)
        attacker_positions = {att_pos_msg.callsign: att_pos_msg}
        my_shooter.process_attacker_info(attacker_positions, list())
        self.assertIsNotNone(my_shooter.tracked_attacker, 'Still tracking despite below min shooting distance')

        # [B] ---- test pilot's eye
        att_lon_lat = g.calc_destination_pos(shooter_pos, tracking_dist - 1000, shooter_hdg + 180)  # behind the pilot
        att_pos = g.Position(att_lon_lat[0], att_lon_lat[1], att_alt)
        att_pos_msg = m.AttackerPosition(att_callsign, att_pos, att_hdg, 0., None, False, False, False, 2., None)
        attacker_positions = {att_pos_msg.callsign: att_pos_msg}
        my_shooter.process_attacker_info(attacker_positions, list())
        self.assertIsNone(my_shooter.tracked_attacker, 'The pilot cannot look behind and looses tracked attacker')

        att_pos_msg = m.AttackerPosition(att_callsign, att_pos, att_hdg, 0., None, False, False, False, 2.5, None)
        attacker_positions = {att_pos_msg.callsign: att_pos_msg}
        my_shooter.process_attacker_info(attacker_positions, list())
        self.assertIsNone(my_shooter.tracked_attacker, 'The pilot cannot look behind and start tracking an attacker')

    def test_visibility_from_pilots_eye(self):
        shooter_alt = 1000
        shooter_lon_lat = (25., 70.)
        shooter_hdg = 90.
        shooter_pos = g.Position(shooter_lon_lat[0], shooter_lon_lat[1], shooter_alt)  # south of ENNA
        shooter_callsign = 'SHOOTER'

        att_callsign = 'ATTACK1'
        att_alt = shooter_alt
        att_hdg = shooter_hdg + 180  # fly nose-on
        att_lon_lat = g.calc_destination_pos(shooter_pos, 10000, shooter_hdg)  # pilot looks straight ahead
        att_pos = g.Position(att_lon_lat[0], att_lon_lat[1], att_alt)
        att_pos_msg = m.AttackerPosition(att_callsign, att_pos, att_hdg, 0., None, False, False, False, 0., None)
        visible_nose = _visibility_from_pilots_eye(shooter_pos, shooter_hdg, shooter_callsign, att_pos_msg, False)
        self.assertIsNotNone(visible_nose, 'Nose on below max_horizontal')

        att_hdg = shooter_hdg  # fly away
        att_pos_msg = m.AttackerPosition(att_callsign, att_pos, att_hdg, 0., None, False, False, False, 0., None)
        visible_away = _visibility_from_pilots_eye(shooter_pos, shooter_hdg, shooter_callsign, att_pos_msg, False)
        self.assertIsNotNone(visible_away, 'Fly away below max_horizontal')
        self.assertGreater(visible_nose, visible_away, 'Nose-on factor is smaller when flying away than if nose-on')

        att_lon_lat = g.calc_destination_pos(shooter_pos, 10000, shooter_hdg - PILOT_VISIBILITY_MAX_HORIZONTAL - 1)
        att_pos = g.Position(att_lon_lat[0], att_lon_lat[1], att_alt)
        att_pos_msg = m.AttackerPosition(att_callsign, att_pos, att_hdg, 0., None, False, False, False, 0., None)
        visible = _visibility_from_pilots_eye(shooter_pos, shooter_hdg, shooter_callsign, att_pos_msg, False)
        self.assertIsNone(visible, 'Pilot looks to left but cannot see it')

        att_lon_lat = g.calc_destination_pos(shooter_pos, 10000, shooter_hdg - PILOT_VISIBILITY_MAX_HORIZONTAL + 1)
        att_pos = g.Position(att_lon_lat[0], att_lon_lat[1], att_alt)
        att_pos_msg = m.AttackerPosition(att_callsign, att_pos, shooter_hdg, 0., None, False, False, False, 0., None)
        visible = _visibility_from_pilots_eye(shooter_pos, shooter_hdg, shooter_callsign, att_pos_msg, True)
        self.assertIsNotNone(visible, 'Pilot looks at attacker 59 degs to left and fly away: launch')

        att_pos_msg = m.AttackerPosition(att_callsign, att_pos, att_hdg, 0., None, False, False, False, 0., None)
        visible = _visibility_from_pilots_eye(shooter_pos, shooter_hdg, shooter_callsign, att_pos_msg, True)
        self.assertIsNotNone(visible, 'Pilot looks at attacker 59 degs to left and fly nose-on: no launch')

    def test_missile_shooting(self):
        """Tests shooting from a static target looking East to an attacker flying North.
        Startpoint for attacker is at lat=25.07 deg a bit to the East (ca. 2670 m direct line) and lon=69.94 a bit
        to the South (ca. 6590 m).
        """
        u.configure_logging('INFO', True, 'ShooterTestHarness', False)

        import hunter.mp_targets as mpt
        shooter_alt = 1000
        shooter_lon_lat = (25., 70.)
        shooter_hdg = 90.  # East
        shooter_pos = g.Position(shooter_lon_lat[0], shooter_lon_lat[1], shooter_alt)  # south of ENNA
        shooter_target = mpt.MPTarget.create_target(mpt.MPTarget.BUNKER, shooter_pos, shooter_hdg)
        shooter_target.callsign = 'SHOOTER'
        my_shooter = Shooter.create_missile_ir_aa_shooter(shooter_target, True)
        shooter_target.shooter = my_shooter
        self.assertAlmostEqual(shooter_target.send_freq, 0.5, 1, 'Send frequency')
        # setting some params just to be sure and be able to reuse
        tracking_dist = 10000
        my_shooter.tracking_dist = tracking_dist
        max_shooting_dist = IR_MISSILE_MAX_RANGE
        my_shooter.max_shooting_dist = max_shooting_dist
        min_shooting_dist = IR_MISSILE_MIN_RANGE
        my_shooter.min_shooting_dist = min_shooting_dist

        att_callsign = 'ATTACK1'
        att_hdg = 0  # perpendicular towards North

        # each test condition is attacker_speed, attacker_altitude, number_of_seconds to run
        test_conditions = [(100, shooter_alt, 80),
                           (200, shooter_alt, int(15000 / 200)),
                           (200, shooter_alt + 200, int(15000 / 200)),
                           (300, shooter_alt - 200, int(15000 / 300)),
                           (500, shooter_alt, int(15000 / 500)),
                           ]
        for test_condition in test_conditions:
            my_shooter.ammo_current = 4
            my_shooter.hits_on_attacker = 0
            prev_att_pos = g.Position(25.07, 69.94, test_condition[1])
            for i in range(int(test_condition[2] * 1/my_shooter.parent.send_freq)):  # each increment is 0.5 secs
                att_lon_lat = g.calc_destination_pos(prev_att_pos, test_condition[0] * my_shooter.parent.send_freq,
                                                     att_hdg)
                att_pos = g.Position(att_lon_lat[0], att_lon_lat[1], test_condition[1])
                print(att_pos)
                print('Current distance target to attacker', g.calc_distance_pos_3d(shooter_pos, att_pos))
                att_pos_msg = m.AttackerPosition(att_callsign, att_pos, att_hdg, 0., None, False, False, False,
                                                 time.time(), None)
                attacker_positions = {att_pos_msg.callsign: att_pos_msg}
                my_shooter.process_attacker_info(attacker_positions, list())
                if i % 2 == 0:
                    print(int(i/2.), 'seconds passed')
                prev_att_pos = att_pos
                time.sleep(my_shooter.parent.send_freq)  # needed because some functions are dependent on real time
            print('####### Total hits', my_shooter.hits_on_attacker, 'with speed =', test_condition[0],
                  'and alt =', test_condition[1], '#######')
            self.assertGreater(my_shooter.hits_on_attacker, 0, 'Has hit target at least once')
