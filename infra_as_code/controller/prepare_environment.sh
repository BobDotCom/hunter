#!/bin/bash

# If this is run in Cloud shell, then the project ID can be accessed as $DEVSHELL_PROJECT_ID
# Otherwise you need to have a local env variable on your developer machine: $GOOGLE_CLOUD_PROJECT, which
# points to the GCP project ID (e.g. by running "gcloud config list project"
export GCLOUD_PROJECT=$GOOGLE_CLOUD_PROJECT

echo "Project ID: $GCLOUD_PROJECT"

# These topics have to be in sync with the topics in hunter/gcp_ps_io.py
echo "Creating Pub/Sub topics"
gcloud pubsub topics create ctrl_to_workers workers_to_ctrl captain_to_carrier

# Firestore in datastore mode needs to be created through the console: europe-west3 is chosen
# gcloud does not have suitable commands: https://cloud.google.com/sdk/gcloud/reference/datastore
# Entities must be created either through a Python script or manually the Console: https://cloud.google.com/datastore/docs/quickstart

# FIXME so long
echo "Creating hunter-integrated Service Account"
gcloud iam service-accounts create hunter-integrated --display-name "Hunter Integrated: controller or worker"

echo "Creating a key for hunter-integrated: move the generated key to a secure storage!!!"
# There must be an environment variable HUNTER_GCP_SA_INTEGRATED which points to the file (cf. hunter/gcp.io)
gcloud iam service-accounts keys create hunter-integrated.json --iam-account=hunter-integrated@$GCLOUD_PROJECT.iam.gserviceaccount.com

echo "Setting hunter_integrated IAM Role"
# See roles in https://cloud.google.com/pubsub/docs/access-control, https://cloud.google.com/datastore/docs/access/iam#iam_roles
gcloud projects add-iam-policy-binding $GCLOUD_PROJECT --member serviceAccount:hunter-integrated@$GCLOUD_PROJECT.iam.gserviceaccount.com --role roles/pubsub.publisher
gcloud projects add-iam-policy-binding $GCLOUD_PROJECT --member serviceAccount:hunter-integrated@$GCLOUD_PROJECT.iam.gserviceaccount.com --role roles/pubsub.subscriber
gcloud projects add-iam-policy-binding $GCLOUD_PROJECT --member serviceAccount:hunter-integrated@$GCLOUD_PROJECT.iam.gserviceaccount.com --role roles/pubsub.editor
gcloud projects add-iam-policy-binding $GCLOUD_PROJECT --member serviceAccount:hunter-integrated@$GCLOUD_PROJECT.iam.gserviceaccount.com --role roles/logging.logWriter
gcloud projects add-iam-policy-binding $GCLOUD_PROJECT --member serviceAccount:hunter-integrated@$GCLOUD_PROJECT.iam.gserviceaccount.com --role roles/datastore.user

# https://cloud.google.com/appengine/docs/standard/python3/access-control#auth-cloud-implicit-python

gcloud services enable compute.googleapis.com
gcloud compute firewall-rules create flightgear-mp-out --direction=EGRESS --priority=1000 --allow udp:5000
gcloud compute firewall-rules create flightgear-mp-in --direction=INGRESS --priority=1000 --allow udp:5000-5200

# echo "Creating Hunter controller compute vm instance"
# cf. https://cloud.google.com/sdk/gcloud/reference/compute/instances/create-with-container
gcloud compute instances create-with-container hunter-controller --project=hunter-all --zone=europe-west3-c \
--machine-type=e2-small --service-account=hunter-integrated@hunter-all.iam.gserviceaccount.com \
--boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=hunter-controller \
--container-arg=-s --container-arg=swiss --container-arg=-i --container-arg=OPFOR \
--container-arg=-g --container-arg=-x --container-arg=2 \
--container-env=CE_VM_INSTANCE=yes,GOOGLE_CLOUD_PROJECT=hunter-all \
--container-image=europe-west3-docker.pkg.dev/hunter-all/controller-repo/hunter_controller:latest \
--container-restart-policy=never \
--maintenance-policy=MIGRATE \
--network-tier=PREMIUM \
--no-restart-on-failure \
--no-shielded-secure-boot \
--tags=flightgear-mp-out,flightgear-mp-in \
--image-project=cos-cloud --image=cos-stable-85-13310-1209-10 \
--scopes=default,datastore
