#!/bin/bash

# See also comments in prepare_environment.sh

export GCLOUD_PROJECT=$GOOGLE_CLOUD_PROJECT

echo "Project ID: $GCLOUD_PROJECT"

echo "Removing Pub/Sub topics"
gcloud pubsub topics delete ctrl_to_workers workers_to_ctrl captain_to_carrier carrier_to_captain

echo "Removing hunter-integrated Service Account"
gcloud iam service-accounts delete hunter-integrated@$GCLOUD_PROJECT.iam.gserviceaccount.com
