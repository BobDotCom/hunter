.. _chapter-end-user-requirements-label:


=====================
End User Requirements
=====================

------------
Installation
------------

Hunter uses FlightGear's `multiplayer <http://wiki.flightgear.org/Howto:Multiplayer>`_ feature, such that several pilots can see the same targets. All of Hunter's processing is on a remote computer (server). Unless you want to run Hunter yourself, nothing has to be installed locally on your machine. I.e. there is no `Nasal <http://wiki.flightgear.org/Nasal_scripting_language>`_ or `AI <http://wiki.flightgear.org/AI_Traffic>`_ module to download and install.

The availability of Hunter is announced on the `OPRF Discord <https://discordapp.com/channels/228041550687371264/228041550687371264>`_ server in the ``hunter-asset-manager`` channel. Use the same channel to request Hunter running with a specific scenario. If you want to run your own Hunter processor, just reach out to ``HB-VANO``.


---------------------
Download of MP assets
---------------------

In order to be able to see the targets properly, shoot at them and get hits, you need to use a compatible FlightGear aircraft from `Operation Red Flag (OPRF) <http://opredflag.com/>`_ and enable MP messaging. Compatible means here amongst others that the aircraft uses the `Emesary multiplayer bridge for FlightGear <http://chateau-logic.com/content/emesary-multiplayer-bridge-flightgear>`_.
Use a recent FlightGear version, which is compatible with the OPRF aircraft.

You need to have a recent version (optimally latest version) of `OPRF Assets <https://github.com/l0k1/oprf_assets>`_ and `US Navy Ships <https://github.com/NikolaiVChr/u.s.navy>`_.

In order to see helicopters and drones, you need the following aircraft:

* `FGAddOn hangar <http://wiki.flightgear.org/FGAddon>`_ Mil-Mi-24
* `FGAddOn hangar <http://wiki.flightgear.org/FGAddon>`_ Ka-50
* `Mil Mi-8 <https://github.com/RenanMsV/Mil-Mi-8>`_
* `MQ-9 <https://github.com/JMaverick16/MQ-9>`_

In order to see automated attacking planes or AWACS or an air-refueling tanker, you need the following aircraft:

* `F-15 <https://github.com/Zaretto/fg-aircraft/tree/OPRF>`_
* `F-16 <https://github.com/NikolaiVChr/f16>`_
* `SU-27 <https://github.com/yanes19/SU-27SK>`_
* `MiG-29 <https://github.com/alexeijd/MiG-29_9-12>`_
* `KC-137R family <https://github.com/JMaverick16/KC-137R>`_


Only for the Swiss Shooting Ranges scenario, you need additionally the following aircraft:

* `Pilatus PC-9M <https://sourceforge.net/p/flightgear/fgaddon/HEAD/tree/trunk/Aircraft/PC-9M/>`_

..  `Northrop F-20 Tigershark <https://github.com/JMaverick16/F-20>`_ (simulating the F-5E flown by the Swiss Air Force)


If you are participating when there is a MultiPlayer scenario including a carrier, then you have the get the MPCarrier from the `FGAddOn hangar <http://wiki.flightgear.org/FGAddon>`_ (this is a specific FG ``aircraft`` different from the carrier AI scenarios, where the carrier models are available by default).

