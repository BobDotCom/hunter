:orphan:

===========
Development
===========

-----------------
Program Structure
-----------------
* Multi-processing over multi-threading has been chosen for several reasons, amongst others:

  * Possibility to combine pure MP assets with running / controlling FG-instances using the same processing model
  * Most processing is CPU bound and/or it is easier to understand sockets per process vs. per thread
  * Inter-process communication is more explicit using multi-processing - and therefore easier to understand
  * No hassle with Python's GIL
  * If a process (apart from the controller) crashes, then only one target is lost
  * A not verified believe that sockets either work better or can only work for several incoming and outgoing MP ports if done in separate processes per MP object.


MPSessionManager manages the bi-directional communication between:

* the user interface and the session
* the session and targets

MPSessionManager is a sub-process of the main thread. Targets are sub-processes of the session. All communication is done with multiprocessing.Pipe channels.

Each session manager has one special MPTarget of type ``receiver``, which is not visible to attackers. The receiver registers with the MP server with the solve purpose of listening to all MP chat message. If a chat message contains shooting info, then it is sent to the session manager, which then re-directs for final interpretation to the specific target's damage processing.


-----------
Other Notes
-----------
* Moving target assets are hard-coded to send every 1 second. That should be good enough for simulation over MP both as a reaction to hits and for movement. Using 1 second generates less network traffic and is easier to use for calculating moving targets (that is also the reason why e.g. speed is configured in m/s.
* The targets do not have any properties telling whether they can be hit by e.g. air munition vs. ground ammunition or cannon. The attacking planes' logic is trusted. A target can be made invulnerable (damage returns always Health.missed) or the max_hp can just be set to a very high number (e.g. 1'0000'000 requiring over 100'000 hits by a cannon before getting broken, but still returning Health.hit)
* Towing targets: Using the towing plane's movement properties with a delay in the towed target did not work. The delay increases a bit over time due to processing time (maybe 0.01 seconds each time), which is enough to make it flickery (the towing plane is also not exact due to processing maybe every 1.01 seconds when it is assumed to be 1 second - plus the time it takes for the pipe between the processes to forward the message). Those two things together make it look wrong. Therefore, the towed plane just uses the same network, start point and start after a delay - using the tractor's (instead of its own) flight model parameters (cruise speed, acceleration, turn rate etc.). The pre-requisite for this is that the network is a directed (and closed loop) graph.
* Network objects: nodes are always geometry.WayPoints. There is no special object for edges, but there are can be two attributes, which are used in moving targets using trips: ``cost`` for pre-calculated weights for Dijkstra shortest path algorithms as well as ``max_speed`` in m/s. Networkx Dijkstra assumes 1 if the cost attribute is not present. If the max_speed attribute is not present or 0, then there are no speed limits.
* The generated road/rail networks are not directly using osm2city code for several reasons: First because a network should not be constrained to a tile and second because osm2city removes/simplifies some info (e.g. highway/rail types) and stuff (e.g. tunnels) and finally because it is in local coordinates and would complicate the logic in roads.py. Instead Hunter has its own logic, duplicates the logic to add additional nodes for elevations and then gets the elevation simply by probing directly on top of a osm2city scenery (incl. bridges) - apart from tunnels, where it interpolates start and stop for inbetween nodes (in the tunnel) - because tunnels are needed.

--------------------------
Updating the Documentation
--------------------------

Follow the following for the end-user part:

* Create the html files for local testing:
    * Change directory to ``hunter\doc``
    * Issue ``sphinx-build -b html . build``
    * Change directory up ``cd ..``
* Deployment is automatically done through GitLab CI/CD to GitLab Pages if it is pushed to main.
* Announce in the `OPRF forum article <http://opredflag.com/forum_threads/3167237>`_.

------------------------------
Updating tags etc. for release
------------------------------

First update the changelog and commit.

Then update the following tags:

* Container registry: log into GCP Artifact Registry, choose the specific image and in the burger menu use "Edit tags"
* Gitlab: go to Repository - Tags: Choose new tag and just fill out the number part. It will use the latest commit by default.


-------------------
Cloud Setup / Notes
-------------------

Overall architecture:

* Controller and Worker(s) interact with each other using message passing (GCP Pub/Sub)
* Controller and Worker(s) store position updates directly in datastore
* Controller stores damage results directly in datastore
* Pages in web-app fetch updates of position updates and damage results on refresh or using ajax.
* The web-app is stateless, so there is no way to send updates to the web-app and be fetched from a global session


---------------
Web application
---------------

Miscellaneous points:

* Map displays are using `Leafletjs <https://leafletjs.com/>`_ (version 1.8.0). The `Leafletjs providers javascript <https://github.com/leaflet-extras/leaflet-providers>`_ is version is commit `49b2501 <https://github.com/leaflet-extras/leaflet-providers/commit/49b250162aa1c9373aee5ed1ce841cf80394ec5a>`_.
* The coordinate controls are derived from `Leaflet-Coordinate_controls <https://github.com/zimmicz/Leaflet-Coordinates-Control>`_ version 0d21a1c using an `MIT License <https://github.com/zimmicz/Leaflet-Coordinates-Control/blob/master/LICENSE.md>`_. The only change form the original is that lon and lat have been switched around.

Deploying: Deploy a new version to GCP AppEngine ``gcloud app deploy``

---------------------
Environment Variables
---------------------

Hunter has some variables in the environment. For development you might want to have some or all of the following in a ``.env`` file at the root of the project (do NOT add the file to Git!).

NB: Most of these variables are only needed for generating networks or DEMs for scenarios - or for running the web-app or Hunter locally with services from the Google Cloud Platform.

.. code-block:: bash

    FG_INSTALL=${HOME}/bin/flightgear/dnc-managed/install
    LD_LIBRARY_PATH=${FG_INSTALL}/simgear/lib:${FG_INSTALL}/openscenegraph/lib:${FG_INSTALL}/openrti/lib:${FG_INSTALL}/plib/lib:${LD_LIBRARY_PATH}

    # osm2city stuff - also needs LD_LIBARY_PATH and FG_ROOT like set above
    O2C_PATH_TO_FG_ELEV=${FG_INSTALL}/flightgear/bin/fgelev
    O2C_DB_HOST=127.0.0.1
    O2C_DB_PORT=5432
    O2C_DB_USER=gisuser
    O2C_DB_PASSWORD=*****

    # Hunter
    HUNTER_MP_SERVER=x
    HUNTER_GCP_SA_INTEGRATED=${HOME}/******/hunter-integrated.json
    GOOGLE_CLOUD_PROJECT=hunter-all

    # Testing
    HUNTER_SCENARIOS_PATH=${HOME}/develop_hunter/hunter-scenarios
    HUNTER_SCENARIO_NAME=test

    PYTHONUNBUFFERED=1

------------
GitLab CI/CD
------------

To install a local runner on the development workstation:

* Install manually from a deb-package cf. https://docs.gitlab.com/runner/install/linux-manually.html. The `official repository <https://docs.gitlab.com/runner/install/linux-repository.html>`_ does not support e.g. Ubuntu/Lunar as of summer 2023.
* In the `Hunter GitLab repo <https://gitlab.com/vanosten/hunter/-/settings/ci_cd>`_ go to ``Settings/CI CD`` and expand section ``Runners``. Press the ``New Project runner`` button and use the token etc. from there to register the installed local runner. Use tag ``linux_local``.


--------------------
Components in Hunter
--------------------

Hunter is handled as a mono-repo, but contains several components as follows:

==================== ========================== ================================================================= =================================================================
Component            Directory                  Execution environment                                             Remarks
==================== ========================== ================================================================= =================================================================
Documentation        ``/doc``                   `GitLab Pages <https://docs.gitlab.com/ee/user/project/pages/>`_  Statically built with Sphinx
                                                at https://vanosten.gitlab.io/hunter
Web-ui               ``/hunter/web_ui``         `Google App Engine <https://cloud.google.com/appengine>`_         Can also be run locally by means of ``start_web_ui.py``
Hunter Controller    ``/hunter``                A container image ("Docker") runtime environment                  The main multi-player application as the core of the project.
                                                (preferably on Linux)                                             Can also be run locally by means of ``start_controller.py``.
Hunter Worker        ``/hunter/fg_*.py``        Local Python 3.1x by means of ``start_worker.py``
Scenario tools       ``/hunter/scenario_tools`` Local Python 3.1x with main method per module
Data processing      ``/infra_as_code/*``       Google Cloud Platform
==================== ========================== ================================================================= =================================================================
