============
About Hunter
============

``Hunter`` is a set of services for the free flight simulator `FlightGear <http://home.flightgear.org/>`_ to place different types of shooting targets into `Multiplayer <http://wiki.flightgear.org/Howto:Multiplayer>`_ mode, such that attacks against air and ground targets can be flown. These services are inspired by the work in the FlightGear flight sim military community `Operation Red Flag (OPRF) <http://opredflag.com/>`_.

The name ``Hunter`` refers to the beautiful British `Hawker Hunter <https://en.wikipedia.org/wiki/Hawker_Hunter>`_, which for many years provided the ground attack capability for the `Swiss Air Force <https://en.wikipedia.org/wiki/Swiss_Air_Force>`_.

``Hunter`` uses FlightGear's `multiplayer <http://wiki.flightgear.org/Howto:Multiplayer>`_ capability, because multiplayer allows several people to see the results (`Nasal <http://wiki.flightgear.org/Nasal_scripting_language>`_ is better suited for single player). And because OPRF-assets are written for multiplayer.

``Hunter`` can be used for training or as the basis of community events, where several pilots fly together or against each other. While ground attack is simulated pretty well, air to air is rudimentary - amongst others because dogfights in OPRF are mostly flown between participants and therefore need not be simulated (which is also much harder to do realistically).

Some capabilities of ``Hunter``:

* Inject dozens of simulated targets into multiplayer. Hunter has been used with 70+ simulated targets.
* Show static targets (e.g. trucks, bunkers).
* Show moving targets (e.g. ships/trucks/helicopters following random routes in a predefined network).
* Tanker for air-to-air refueling, AWACS for situational awareness.
* Depending on a target's strength it takes more explosives to kill it.
* Depending on how broken a target is, it might not move anymore and not shoot anymore. When it is broken, it gives visual feedback (explosion).
* There are some targets, which can catch you on radar and shoot back (currently only static Shilkas by gun, ships with `CIWS <https://en.wikipedia.org/wiki/Close-in_weapon_system>`_ as well as short range IR missiles launched by e.g. helicopters).
* A relatively easy to read and write scenario definition, where static and dynamic targets can be defined including the related networks.
* Scripts to automatically create helicopter and road networks based on `OpenStreetMap <https://www.openstreetmap.org/>`_ and FlightGear scenery data.

``Hunter`` can currently be run in the following modes:

* Based on a Docker image available to all interested: static and moving targets incl. shooting targets (a parameter can determine whether they should shoot back or not).
* Ditto but including a simple `web ui <https://hunter-all.ey.r.appspot.com/>`_ showing damage statistics, a map of airports, a map of targets as well as a list of targets.
* On top a worker node can be added, which gives the capability of controlling FG instances from Hunter (``FG instance`` here refers to an instance of FlightGear running a specific asset on a computer with a graphics processor - like a user of FlightGear does when flying a plane). Such FG instances can be an `MP carrier <https://wiki.flightgear.org/Carrier_over_MP>`_, missile ships (shooting) and SAMs (shooting) as well as automated enemy fighters such as SU-27 and MiG-29 (engaging and shooting). If an MP carrier is running, then it can be controlled through the web UI. Unfortunately, at the time of writing only the author can run in this mode, because the setup is somewhat trickier (and not well described) and a bit of knowledge as well as a key-file are needed. Also, the needed OPRF assets (ships, SAMs, automats) are only available to selected persons to prevent abuse (as a participant you do not need the admin version).


-------
Credits
-------

The interaction with FlightGear multiplayer is using parts of `ATC-pie <http://wiki.flightgear.org/ATC-pie>`_ by Michael Filhol during execution. Also some of the MP interaction code is directly inspired by code in ATC-pie.

Some data preparation like scenery objects (buildings, roads, etc.) as well as routing data for movable targets like ships are based on `OpenStreetMap (OSM) <https://www.openstreetmap.org/>`_ data being processed with `osm2city <http://wiki.flightgear.org/Osm2city.py>`_.

------
People
------

**Core Developers**: Rick Gruber-Riemer (rick AT vanosten DOT net)

**Contributors**: Coaching on Discord by OPRF members like ``Leto``, ``pinto``, ``JMav``, ``Richard``, ``Rudolf`` and others. Damage code and weapons logic is heavily based on `OpRedFlag - Meta data for Operation Red Flag Aircraft and assets <https://github.com/NikolaiVChr/OpRedFlag>`_ by OPRF members.


------------
Contributing
------------

The author could need help for doing 3D models, research of how things work in real life, extend the web UI or programming in Python.


-------
License
-------
This software is licensed under `GNU GPLv2 <https://www.gnu.org/licenses/old-licenses/gpl-2.0.html>`_.
