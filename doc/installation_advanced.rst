:orphan:

================================
More Server Installation Options
================================
------------------------------------------------------
For More Experienced Users Knowing What They Are Doing
------------------------------------------------------
......................................................
Option: Running the Container on GCP in Compute Engine
......................................................

If you want to run the Docker image on another computer for performance (e.g. network), then a cloud provider might be an option. The following gives some hints to run the Docker image in `Google Cloud (GCP) <https://cloud.google.com/>`_ - but any other cloud provider might work equally well.

You need a GCP project and be able to configure a virtual machine in `Compute Engine <https://cloud.google.com/compute>`_ for running the Docker image. Please be aware that `Cloud Run <https://cloud.google.com/run>`_ is not a suitable option, because you need to be able to have non-http network options.

Use the same Docker image as specified in the preceding chapter. A ``e2-small`` instance should be perfectly fine. The important thing is to have firewall options configured - e.g. as follows:

* A `firewall rule <https://cloud.google.com/vpc/docs/using-firewalls>`_ ``flightgear-mp-in``: set protocols/ports to ``udp:5000-5200`` and direction to ``Ingress``, priority=1000.
* A firewall rule ``flightgear-mp-out``: set protocols/ports to ``udp:5000`` and direction to ``Egress``, priority=1000.
* Add the following to the ``network tags`` configuration of the VM: ``flightgear-mp-in, flightgear-mp-out``

To see the logging from inside the container:

* SSH into the compute instance (easiest from the Console into a browser window)
* Get the name of the running container: Run ``docker ps``
* Use the name of the standalone controller to attach the stdout: ``docker attach NAME`` - e.g. ``docker attach klt-hunter-controller-qrun``

Alternatively use the ``-z`` command line argument to use `GCP Logging <https://cloud.google.com/logging/docs>`_.


..............................................
Option: Creating the Docker Image from Scratch
..............................................

NB: This is not needed in most cases, because a Docker image is already provided. You only need to do this if you do not trust the sources or want to have the latest and greatest versions available. If you do this, you need to contact the maintainer because one file will be missing and has to be provided offline.

NB: It might be better to run an `official Docker install <https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository>`_ over using the Ubuntu snap. See e.g. `problem with permissions <https://stackoverflow.com/questions/75651585/docker-permissionerror-errno-13-permission-denied-command-returned-a-non>`_.

* As the working directory choose a directory, which has a local clone of Hunter in a directory called ``hunter`` and a local clone of vanosten's fork of ATC-Pie in a directory called ``vanosten-atc-pie``. Otherwise the directory should be empty. This working directory is called ``build context`` when building Docker containers.
* Make sure that the clones are up-to-date.
* Run ``ln -s hunter/infra_as_code/controller/Dockerfile Dockerfile`` in the working directory to make a symlink
* Run ``docker build -t hunter_controller:latest .``
* Run ``docker run -p 5001-5110:5001-5110/udp -v /home/vanosten/develop_hunter/hunter-scenarios:/hunter-scenarios hunter_controller -d /hunter-scenarios -s test`` for a local test before publishing
* Publish the image on a public registry - follow e.g. `the quickstart for Docker <https://cloud.google.com/artifact-registry/docs/docker/quickstart>`_ for publishing in Google Cloud.:
    * Only once: Authenticate from local Docker ``gcloud beta auth configure-docker europe-west3-docker.pkg.dev`` and configure `public access <https://cloud.google.com/artifact-registry/docs/access-control#public>`_.
    * Tag the image: ``docker tag hunter_controller europe-west3-docker.pkg.dev/hunter-all/controller-repo/hunter_controller:latest``
    * Push to the registry: ``docker push europe-west3-docker.pkg.dev/hunter-all/controller-repo/hunter_controller:latest``
    * In the GCP artifact registry edit the tags.


..................................................................
The Infrastructure Behind the Scenes: Google Cloud Computing (GCP)
..................................................................

The full Hunter solution makes use of a set of more or less proprietary technologies on the the Google Cloud Platform (GCP). NB: it is possible to exchange these technologies with other technologies, but it is not provided as part of the current code (too much work).

* `Pub/Sub <https://cloud.google.com/pubsub>`_ to send messages between the master controller, the web UI and worker servers (for running a set of `OPRF assets <https://github.com/l0k1/oprf_assets>`_ in FlightGear instances.
* `Firestore in Datastore mode <https://cloud.google.com/datastore/docs?hl=en>`_ to store data (also temporarily).
* `App Engine (standard) <https://cloud.google.com/appengine>`_ to run the web application.

The only thing currently not running in the cloud, are the worker nodes.

You will need:

* An account with billing enabled
* A dedicated project
* On you local developer PC: An environment variable ``GOOGLE_CLOUD_PROJECT`` pointing to the project ID in GCP.
* Some knowledge of cloud computing

Everything else is set up using infrastructure as code using the `gcloud CLI <https://cloud.google.com/sdk/docs/how-to>`_ and the scripting in ``infra_as_code/controller/prepare_environment.sh`` (you may want to read the content before blindly running it).

Start with making sure that the correct GCP project is set:

::

    $ gcloud projects list
    $ gcloud config set project $GOOGLE_CLOUD_PROJECT
    $ gcloud config list project

.. As a bare minimum, which only allows you to run all assets on one well-equipped computer, you need to install the `GCP Pub/Sub emulator <https://cloud.google.com/pubsub/docs/emulator>`_ and run it with e.g. ``gcloud beta emulators pubsub start --project=$GOOGLE_CLOUD_PROJECT``.

.. Hunter can interact with not mandatory tools under development by ``vanosten`` on the Google Cloud Platform (e.g. data storage and a web UI for hit statistics). The code of these tools is also included in Hunter. If you run Hunter in a setup agreed with the maintainer of Hunter, then you need to install more libraries and enable additional APIs.


..........................
The Hunter Web Application
..........................

The web application user interface is optimized to run in GCP. It can run in a container, but it has not been tested.

To deploy the web-application:

::

    $ gcloud app deploy


To update the GCP Datastore indices (this can take many minutes - no matter how few records there are - the `GCP dashboard at <https://appengine.google.com/datastore/indexes>`_ will be showing "pending" for a while):

::

    $ gcloud app deploy index.yaml

-------------------------------------------------------------
Installation for Developers: Running Hunter from Command Line
-------------------------------------------------------------

There are two repositories:

* The Hunter program code: https://gitlab.com/vanosten/hunter
* Scenario files for Hunter: https://gitlab.com/vanosten/hunter-scenarios


..........................
Python Basis and Libraries
..........................

Hunter is currently developed with Python 3.10 - newer version might work, older versions will probably not.

In ``infra_as_code\controller`` check for needed packages and libraries in ``Dockerfile`` and ``requirements.txt``.

I suggest to use a Python virtual environment - but it is not a hard requirement. In that case you also need ``python3-venv``.


.................................
Python Path and Working Directory
.................................
* ATC-pie: Need to copy file ``colours.ini`` from ATC-pie/settings into a folder ``settings`` in the working directory. You get this file after the first time having run ATC-pie - or you can use the file in ``atc_pie_settings``.
* ATC-pie: Needs to be added to the `PYTHONPATH <https://docs.python.org/3/using/cmdline.html#envvar-PYTHONPATH>`_
* Hunter: the base directory needs to be added to the PYTHONPATH.

osm2city is not needed at run-time, but is used to create the routes for MP targets.


.......
ATC-Pie
.......

Get ATC-Pie from its `project site <https://sourceforge.net/projects/atc-pie/>`_. However, currently there are open merge requests and therefore you need to use `vanosten's fork <https://sourceforge.net/u/vanosten/atc-pie/ci/master/tree/>`_.

.. The Currently compatible version has commit checksum ``55fee0`` (2019-04-22). Use command ``git log`` to test on your local machine what the last commit checksum.


........
osm2city
........

Not required at runtime. In order to create routes etc. for scenarios, you need to have `osm2city <https://gitlab.com/osm2city/osm2city>`_.

..........
FlightGear
..........

There are no runtime dependencies.

.....................
Environment Variables
.....................
Need to be in bashrc for execution. In Pycharm setup environment variables as explained in https://www.techcoil.com/blog/how-to-set-environment-variables-for-your-python-application-from-pycharm/.

NB: the following is for development only. The only thing you really need is the ``PYTHONPATH`` during runtime.

Need to set the following environment variables:

* FG_ROOT
* FG_TERRASYNC
* FG_AIRCRAFT
* FG_BIN
* HUNTER_GCP_PUB_SUB: In order to access send and receive messages, you need to have a valid service account with Cloud Pub/Sub subscribe and publish rights. A reference to the JSON-file with the credentials must be available as a path in an environment variable (cf. gcp_io.py), which points to the json file with the GCP service account key (e.g. /home/pingu/gcp_access/hunter_pubsub.json
* GOOGLE_CLOUD_PROJECT: the project ID in GCP (e.g. =hunter-123456)
* PUBSUB_EMULATOR_HOST: only set for testing or if you run everything on your local machine (e.g. localhost:8085 based on the output from ``gcloud beta emulators pubsub env-init``)

The ``FG_`` variables could be set as follows - based on a local installation of FlightGear using `scripted compilation <(http://wiki.flightgear.org/Scripted_Compilation_on_Linux_Debian/Ubuntu>`_ on Linux:

::

   FG_INSTALL=$HOME/bin/flightgear/dnc-managed/install
   export FG_INSTALL

   FG_ROOT=$FG_INSTALL/flightgear/fgdata
   export FG_ROOT

   FG_BIN=$FG_INSTALL/flightgear/bin
   export FG_BIN

   LD_LIBRARY_PATH=$LD_LIBRARY_PATH/$FG_INSTALL/simgear/lib:$FG_INSTALL/openscenegraph/lib:$FG_INSTALL/openrti/lib:$FG_INSTALL/plib/lib
   export LD_LIBRARY_PATH

   FG_TERRASYNC=$HOME/bin/TerraSync
   export FG_TERRASYNC

   # important that oprf_assets is last
   FG_AIRCRAFT=$HOME/bin/customaircraft:$HOME/.fgfs/Aircraft/org.flightgear.fgaddon:$HOME/bin/customaircraft/oprf_admin_assets
   export FG_AIRCRAFT

..
    .....
    Nasal
    .....
    There is one user specific Nasal file, which is needed to be able to support flying a FGFS aircraft. It must be copied into FG_ROOT/Nasal.
    .fgfs/Nasal/war_room.nas

..
    --------
    Aircraft
    --------

    In order to have an FGFS aircraft flying, you need to have `Bourrasque <https://github.com/hardba11/bourrasque>`_ available.


....................
Running from the CLI
....................


If you have a virtual environment for Python, then you might have to start with something like:

::

    $ source /home/vanosten/bin/virtualenvs/hunter310/bin/activate

The controller is started as follows (``-x 1`` will only work if you have set all Google stuff up!):

::

    $ python3 /home/vanosten/develop_hunter/hunter/start_controller.py -m 127.0.0.1 -d /home/vanosten/develop_hunter/hunter-scenarios -i OPFOR -x 1 -g -o -s north_norway

The worker is started as follows providing 4 FG target instances (remember to source activate your python if using a virtual environment):

::

    $ python3 /home/vanosten/develop_vcs/hunter/war_room/worker.py -i 4 -d /home/pingu/develop_hunter/hunter-scenarios


.. https://en.wikipedia.org/wiki/Barycentric_coordinate_system

-----------------------------------
Other notes: Server instance on GCP
-----------------------------------

Considerations (from not so new Linux notebook):

* 1 MP target process in Controller takes ca. 50 MB RAM => 40 MP targets take up to 2 GB
* 1 FGFS instance with minimal graphics takes ca. 800 MB RAM => 10 ships/SAMs/automates take ca. 8 GB RAM
* The controller takes ca. 1 CPU with a few MP targets
* The worker is negligible in CPU. An instance take 20-30% of a CPU
* Download and compile takes >> 20 GB disk space
* 10 GB of disk space for terrasync should accommodate many scenarios. An additional 10 GB for osm2city (is not required at runtime, but nice-to-have for taking pictures and placing objects in sensible places).

Create Instance:

* Name: hunter-controller
* Region: europe.west3 (Frankfurt)
* Machine type: n1-standard-8 (8 vCPU, 30 GB memory)
* OS: Ubuntu 19.10 eoan
* Boot disk: standard persistent, 64 GB
* Network tags: "vnc-server"


Firewall NoMachine:

* ingress: yes
* allow: yes
* tag: "nomachine"
* source filter: IP ranges
* source IP ranges: 0.0.0.0/0 (anywhere)
* protocols: TCP:4000


Firewall FlightGear MP:

* egress: yes
* allow: yes
* tag: "flightgear"
* source filter: IP ranges
* source IP ranges: 0.0.0.0/0 (anywhere)
* protocols: UDP:5000
* --
* ingress: yes
* allow: yes
* tag: "flightgear"
* source filter: IP ranges
* source IP ranges: 0.0.0.0/0 (anywhere)
* protocols: UDP:5000-5200


Desktop environment basis installation:

::

    sudo apt-get update
    sudo apt-get upgrade -y
    sudo apt-get install gnome-desktop  (choose lightdm when prompted)
    sudo apt-get install synaptic
    touch ~/.Xresources

Install `Nomachine <https://www.nomachine.com/accessing-your-remote-desktop-on-google-cloud-platform-via-nomachine>`_.

