.. _chapter-end-user-flying-label:

===============
End User Flying
===============

Unless specified otherwise by the person running Hunter: You need to connect to the OPRF's MP-server `<mpserver.opredflag.com>`_. If in doubt, then look at the `Current Session Information </ongoing_session/sessions_list>`_ and look for column ``MP Server Host``. When you are connected, all objects are represented as MP-targets, the same way you would see another player's aircraft.

To make sure that your shooting at targets actually leads to impact, you need to pilot the airplane at the same time as handling the weapons with some accuracy. "Impact" means that with sufficient destructive power an asset visibly takes damage and after some time goes away. Ships stop moving after some impact - and sink, when they are destroyed. Helicopters fall out of the sky.

By default Hunter shows callsigns like 'OPFORxx' and thereby hides the type of the target. Otherwise the callsigns of the targets are built up like follows (e.g. ``xvehi57``):

* 1 letter identifier (per default `x`)
* Up to 4 letters type
* Two digits running index number

The following types exist:

* building (e.g. depot)
* vehicle (e.g. truck, buk-m2)
* ship (e.g. frigate)
* helicopter
* plane
* unknown

A target's callsign can be reused later for a new and different target. I.e. just because a target was once destroyed you cannot count on that the callsign will be gone.

Depending on the scenario, some of the ships, SAMs, aircraft etc. shoot at you - watch out and know your key binding for chaff/flares!


-------
OPFOR?!
-------
`OPFOR <https://en.wikipedia.org/wiki/Opposing_force>`_ is an opposing force. Almost all simulated targets are ``OPFOR``. In most situations you are an ``attacker`` and try to destroy OPFOR targets. When there is an event using Hunter, then sometimes a few participants act as ``defenders`` and try to protects ``OPFOR`` from the attackers.


---------------
Web-application
---------------

Hunter has a simple `web ui <https://hunter-all.ey.r.appspot.com/>`_ showing damage statistics, a map of airports, a map of targets as well as a list of targets. The data for different sessions is kept for some days.

NB: You need to press F5 in your browser to refresh the screen. This is on purpose, because otherwise it can get tricky to interact with information of moving targets on maps.


----------------------------------------
Ground Controlled Interception Interface
----------------------------------------

Some OPRF planes like the `MiG-21bis <https://github.com/l0k1/MiG-21bis>`_, the `Mirage 2000-* <https://github.com/5H1N0B11/Aircraft>`_ and the `F-16 <https://github.com/NikolaiVChr/f16>`_ have menu items or key bindings to request information about flying enemies from a GCI-station:

* ``PICTURE`` - full tactical picture
* ``BOGEY DOPE`` - BRAA of nearest target
* ``CUTOFF`` - vector to nearest target

While the `Ground control interception (GCI) <https://en.wikipedia.org/wiki/Ground-controlled_interception>`_ in OPRF assets implements this as intended, a less realistic feature using the same mechanism can be used in Hunter to find ground-attack targets:

* PICTURE - returns the BRAA of the nearest ship
* BOGEY DOPE - returns the BRAA of the nearest land vehicle (in most scenarios all kinds of vehicles like SAMs, trucks etc. are returned - however, each scenario can specify that only specific types of vehicles are returned)
* CUTOFF - returns the vector to the nearest helicopter or drone (if you are not yet airborne, then additional 30 seconds are added to the ETA assuming an average speed of ca. 400 kt - you can always make a request later again, when you are airborne and at speed to get better indications)

New requests will only be processed 10 seconds after the last one.

If the same request type is sent within 2 minutes again, updated information for the same target will be displayed (if the target is still alive) - no matter whether the target is still the nearest one or not anymore.

It can be that no target seems to be available - that includes situations for CUTOFF, where the target due to aspect and/or speed difference cannot be intercepted. That might change if you increase the speed of your own aircraft and the calculation therefore turns to your advantage.

No information can be requested for static targets like buildings - but their coordinates might be described in a specific scenario. Also, no POPUP calls will come in.

Bearing information is showing geographic/true north north, i.e. not magnetic north. Many fighters will use magnetic North in the HUD and in the instruments, but e.g. the Mirage 2000 has a button where you can switch between magnetic and true north. You can get the magnetic deviation from e.g. `magnetic declination <https://en.wikipedia.org/wiki/Magnetic_declination>`_ - or in FlightGear by going to ``Equipment -> Instrument Settings``.


----------------
Radar of targets
----------------
The following target types have active radar:

* Warships
* GCI
* SAMs and certain other anti-aircraft weapon systems (like the Shilka)
* Fighter aircraft, tanker, AWACS

If a target's health is broken or it is destroyed, then the radar is turned off also on these target types. Some targets might turn the radar off based on heuristics taking the distance to attacking aircraft into account. Some SAMs might randomly turn their radar on and off.


-------------------
Damaging of targets
-------------------

Hunter uses the same mechanism for damage as OPRF Assets. It means that you need to enable multiplayer protocol version 2 and have MP messaging on.

Each target has a number of hitpoints: the stronger built a target is (e.g. a frigate vs. a truck) the higher the number of hitpoints. Each weapon has a strength in hitpoints: bullets have far fewer hitpoints than a missile. When a target is hit, the weapon's hitpoints reduces the hitpoints of the target based on some heuristics (e.g. distance from the target, a bit of randomness). The heuristics in Hunter are the same as in OPRF assets (if it exists), otherwise they are similar. While the hitpoints for weapons are the same as in OPRF assets (``damage.nas``), the hitpoints are the same in most cases.

A target's health has five states:

* ``fit for fight``: not been hit yet
* ``hit``: some reduction of hitpoints. Depending on the target aircraft's modelling, some functions might not work anymore
* ``broken``: when the target's hitpoints have been reduced to below a certain threshold (currently 33%), then the target is just handled as a brick. It does not move/fly anymore and radar is off.
* ``destroyed``: hitpoints have reached 0. Explodes and starts burning (if the model supports it). A ship begins to sink.
* ``dead``: after an amount of time depending on the target, the target is removed from MP and not visible anymore. A new target of any type at any time could pop up again using the same callsign.


-------
Carrier
-------
For landing and taking off from a carrier, you need to follow the instructions closely - amongst others you have to set the additional command line property correctly - or you will not see the carrier. Use the following resources:

* The general description of `how to use carriers <http://wiki.flightgear.org/Howto:Carrier>`_ in FlightGear . Includes amongst others the TACAN codes to use in finding the carrier.
* You need to follow at least the installation pilot sections of the `Carrier over MP <http://wiki.flightgear.org/Carrier_over_MP>`_ wiki article.
* The F-14 is currently the only carrier capable aircraft in OPRF with weapons. There is an extensive description of `how to land on a carrier <http://wiki.flightgear.org/Howto:Carrier_Landing>`_.

The person launching the scenario will announce which carrier (e.g. the Vinson) is active and which callsign it has (needed to configure the property in FG). Most often it will be the Vinson with callsign MP_029X.

NB: as of FG version 2020.1 you cannot spawn on a carrier. However, once landed on a carrier you can launch on the carrier.

Save yourself frustrations by:

* Closely following the instructions for MP carriers. You will not gain time by just skimming the wiki - you need to read and understand.
* Train carrier recoveries with very little fuel and using AI scenarios. It is difficult and the F-14 is a complex and difficult beast. Crashing after a successful hunt and then also having to re-spawn on land instead of launching on the carrier is not fun.


------
Tanker
------
If there is a tanker for air-2-air-refueling, then you can use its callsign to look-up the `TACAN <https://en.wikipedia.org/wiki/Tactical_air_navigation_system>`_ code according to table in `Howto:Aerial refueling <https://wiki.flightgear.org/Howto:Aerial_refueling>`_. By default the tanker uses callsign ``TEXACO1`` and thus its TACAN code is ``050X``, but that can be changed in a scenario.

The tanker serves the attackers and is vulnerable to attacks from OPFOR.


-----
AWACS
-----
If there is an `airborne early warning and control <https://en.wikipedia.org/wiki/Airborne_early_warning_and_control>`_ plane (aka. AWACS), then its name is by default ``SKYEYE``.

The AWACS serves the attackers and vulnerable to attacks from OPFOR.

---
IFF
---
Tankers, AWACS and other planes supporting the attackers (you) use the IFF (`identification of friend or foe <https://en.wikipedia.org/wiki/Identification_friend_or_foe>`_) system, so you can interrogate them. Unless the scenario is run with a different set of parameters, then:

* The OPFOR (the targets plus maybe pilots helping as defenders) will use code ``1``
* The attackers (you) will use code ``9``.

If you get the IFF code wrong, then either the simulated targets or other attackers will shoot at you (or both).

--------
Datalink
--------
If the scenario is run with an AWACS (see above), then the AWACS distributes tactical information over a datalink. See `Link 16 Tactical Data Information Link (TADIL) <https://en.wikipedia.org/wiki/Link_16>`_ as an example - but the OPRF implementation is not trying to mimic any particular system.

Unless the scenario is run with a different set of parameters, then:

* The OPFOR (the simulated targets plus maybe pilots helping as defenders) will use channel ``11``
* The attackers (you) will use code ``99``.


-----------------------
Targets shooting at you
-----------------------

Whoever runs Hunter will be able to provide information about how hostile the currently run scenario is - or have a look at the session info in the web-site whether something might be shooting at you at all:

* Certain attack helicopters like the `Ka-50 <https://en.wikipedia.org/wiki/Kamov_Ka-50>`_ or the `Mil Mi-24 <https://en.wikipedia.org/wiki/Mil_Mi-24>`_ defend themselves using `infrared homing <https://en.wikipedia.org/wiki/Infrared_homing>`_ missiles. The simulation is a compromise between the `9K32 Strela-2M <https://en.wikipedia.org/wiki/9K32_Strela-2>`_, the `9K38 Igla <https://en.wikipedia.org/wiki/9K38_Igla>`_, the `FIM-43 Redeye <https://en.wikipedia.org/wiki/FIM-43_Redeye>`_ and the `FIM-92 Stinger <https://en.wikipedia.org/wiki/FIM-92_Stinger>`_.
* The same applies for the `MQ-9 Reaper <https://en.wikipedia.org/wiki/General_Atomics_MQ-9_Reaper>`_ drone.
* If there are "missile ships" run as ``FG instances``, then they will launch realistically modelled missiles at you over tens of nautical miles. Otherwise ships will shoot anti-aircraft bullets (a loose interpretation of the `Kashtan-M <https://en.wikipedia.org/wiki/Kashtan_CIWS>`_ (only the guns part).
* Often there will be simulated `Shilkas <https://en.wikipedia.org/wiki/ZSU-23-4_Shilka>`_ (see `also <https://archive.org/details/DTIC_ADA392785/page/n3/mode/2up>`_) -- they are less accurately modelled, but still deadly if you fail to respect them (you can get easy kills with `anti-radiation missiles <https://en.wikipedia.org/wiki/Anti-radiation_missile>`_ -- but what if you use conventional weapons?).
* If there are SAMs (e.g. S-300) run as ``FG instances``, then they will launch realistically modelled missiles at you over tens of nautical miles. Otherwise, is SAMs are set to be shooting, then they are replaced with a Shilka-like fantasy vehicle, which launches IR-missiles in `MANPAD <https://en.wikipedia.org/wiki/Man-portable_air-defense_system>`_ style. (The Shilka model is used such that the nozzle fire gives a bit of a hint, where the man-pads come from.)
* If there are "automats" (fighter planes run as ``FG instances``): they shoot bullets or launch missiles in a realistic way until they run out of ammunition. They will actively engage you.

``FG instances`` is a bit special and will have to be announced specifically by the person running Hunter.

The simulated Shilka (ZSU-23-4) has amongst others the following characteristics:

* Has an active radar, which is toggled on if an attacker is within 12 km (amongst others to simulate that the radar is quite weak)
* Active tracking of attackers within 10 km. It takes minimum 8 seconds to acquire and lock a target - but the Shilka can track a previously locked and now hidden target for up to 4 seconds.
* On top of target acquisition the turret also needs to adjust (max 15 degrees per second vertically and 30 degrees per second horizontally).
* Max shooting distance is 2500 metres - it shoots 2 consecutive bursts and then is silent for a few seconds in order not to reveal its position (you can see the muzzle fire, not the bullets).
* The visibility of the Shilka can be constrained by terrain - and it cannot detect attackers below 0.5 degrees above terrain (ca. 8 metres per 1000 metres distance). In hilly surroundings you can come close to the Shilka with the cannon or rockets by hiding behind terrain and then profit from the Shilka's rather slow acquisition time.


-------------------------
Targets with Self Defense
-------------------------

Some targets (e.g. helicopters) can automatically release countermeasures (flares/chaff). So you might need to sneak in and get close enough or wait until they have lost all their countermeasures.
