# Requirements for the web-application running in Googles App Enging.
# The file with this name has to be stored at the root in order for the deployment process
# to pick it up correctly.
-r requirements_web_ui.txt
-r requirements_gcp.txt
-r requirements_geometry.txt
-r requirements_requests.txt
