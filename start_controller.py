import argparse
import logging
import multiprocessing as mp
import signal
import time


import hunter.messages as m
import hunter.controller as c
import hunter.gcp_utils as gu
import hunter.utils as u


# Global variable such that it can be used in exit handler
global sm_parent_conn


def exit_gracefully(signum, frame) -> None:
    """Makes sure that the child processes in main get a chance to exit gracefully and kill their child processes."""
    global sm_parent_conn
    logging.info('Controller is trying to exit gracefully')
    sm_parent_conn.send(m.ExitSession())


if __name__ == '__main__':
    global sm_parent_conn
    signal.signal(signal.SIGINT, exit_gracefully)  # e.g. CTRL-C on Linux
    signal.signal(signal.SIGTERM, exit_gracefully)

    my_parser = argparse.ArgumentParser(description='Hunter runs FlightGear military scenarios over MP - controller')
    my_parser.add_argument('-i', dest='identifier', type=str,
                           help='the identifier of this instance will be used as prefix in callsigns for targets',
                           default='x',
                           required=False)
    my_parser.add_argument("-l", dest="logging_level",
                           help="set logging level. Valid levels are DEBUG, INFO (default), WARNING",
                           required=False)
    my_parser.add_argument('-c', dest='ctrl_callsign', type=str,
                           help='the callsign used for the controller in MP chat',
                           default=u.CTRL_CALLSIGN, required=False)
    my_parser.add_argument('-s', dest='scenario', type=str,
                           help='the scenario to run (e.g. "swiss" for scenario "scenario_swiss.py")',
                           required=True)
    my_parser.add_argument('-d', dest='scenario_path', type=str,
                           help='the path to the directory where the scenarios live',
                           required=True)
    my_parser.add_argument('-g', dest='gci', action='store_true',
                           help='GCI functionality',
                           default=False, required=False)
    my_parser.add_argument('-o', dest='hostility', type=str,
                           help='''Specify how hostile the environment is. Any number > 0 means that part is shooting.
                           A string separated by "_" -> #_#_#_#_#
                           1st position: helicopter (IR missiles)
                           2nd position: drones (IR missiles)
                           3rd position: ships (simulates a CIWS artillery - unless FG instances with missiles)
                           4th position: Shilkas (artillery)
                           5th position: SAMs (get replaced by manpad/IR missiles - unless FG instances with missiles)

                           E.g. 0_0_1_1_0 means only ships and Shilkas are shooting.
                           If the environment is hostile, then ships, drones and helis do not respawn to keep a minimum 
                           number of targets of that category available.
                           ''',
                           required=True)
    my_parser.add_argument('-x', dest='cloud', type=int,
                           help='''
                           Use cloud integration (default is 0 = none [run standalone without cloud provider]).
                           1 means that data is stored in cloud and can be accessed through the web application.
                           2 means that on top of that workers with FG instances can be controlled.
                           ''',
                           default=u.CloudIntegrationLevel.none.value, required=False)
    my_parser.add_argument('-y', dest='defenders', type=str,
                           help='# separated list (no space) of reserved callsigns for human defenders (act as OPFOR)',
                           default='', required=False)
    my_parser.add_argument('-k', dest='kill_prev_session', type=int,
                           help='''Explicitly tell that a not yet finalised session on the same MP server
                           can be overridden. It is ok to start a new session and set a stop timestamp on the existing.
                           In order to do so the specific ID of the not yet finalised session must be submitted. 
                           You get the ID from a previous attempt the start the controller, where an exception tells 
                           you about an existing session. 
                           You have to know what you are doing, so a potentially still running valid session is 
                           not disturbed.
                           ''',
                           default=0, required=False)

    args = my_parser.parse_args()

    # first check cloud level
    try:
        cloud_integration_level = u.parse_cloud_integration_level_as_int(args.cloud)
    except ValueError:
        cloud_integration_level = u.CloudIntegrationLevel.none
        # logging is not available yet -> using print
        print('Could not interpret value for cloud integration: {}. Falling back to default',
              u.CloudIntegrationLevel.none.value)

    cloud_logging = True if cloud_integration_level > 0 else False

    # configure logging
    my_log_level = 'INFO'
    if args.logging_level:
        my_log_level = args.logging_level.upper()
    u.configure_logging(my_log_level, True, 'Controller', cloud_logging)
    logging.info('Cloud integration level used: %s', cloud_integration_level.name)

    if cloud_integration_level is not u.CloudIntegrationLevel.none:
        gu.check_minimal_gcp_env_variables()

    if args.defenders:
        defenders = args.defenders.split('#')
        for callsign in defenders:
            if callsign is None or len(callsign) == 0 or len(callsign) > 7:
                raise ValueError('A defender callsign needs to be max 7 characters and otherwise match the -i argument')
            try:
                _ = c.number_part_of_callsign(callsign)
            except ValueError:
                raise ValueError('A defender callsign demands the last 2 chars to be numeric (e.g. OPFOR03')
    else:
        defenders = list()

    hostility = u.parse_hostility(args.hostility)  # check that max instances has correct format

    sm_parent_conn, sm_child_conn = mp.Pipe()

    controller = c.Controller(sm_child_conn, args.ctrl_callsign,
                              args.identifier, args.gci, cloud_integration_level,
                              args.scenario_path, args.scenario, hostility, defenders,
                              args.kill_prev_session)
    controller.daemon = False
    time.sleep(10)  # Just to give the controller a bit time to initialize and log messages - not really needed
    controller.start()
