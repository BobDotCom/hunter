import argparse
import logging
import multiprocessing as mp
import signal


from hunter import messages as m
from hunter import utils as u
import hunter.gcp_utils as gu
from hunter.worker import FGFSWorker


# Global variable such that it can be used in exit handler
global sm_parent_conn


def exit_gracefully(signum, frame) -> None:
    """Makes sure that the child processes in main get a chance to exit gracefully and kill their child processes."""
    global sm_parent_conn
    logging.info('Worker is trying to exit gracefully')
    sm_parent_conn.send(m.ExitSession())


if __name__ == '__main__':
    global sm_parent_conn
    signal.signal(signal.SIGINT, exit_gracefully)  # e.g. CTRL-C on Linux
    signal.signal(signal.SIGTERM, exit_gracefully)

    parser = argparse.ArgumentParser(description='Hunter runs FlightGear military scenarios over MP - controller')

    parser.add_argument('-i', dest='fg_instances', type=str,
                        help='''Max number of FG instances per type this worker can serve (#_#_#_#), where
                        the parameters are automats, carriers, ships and SAMs.
                        The resulting number run is the minimum number between what is configured here and what is
                        defined in the scenario (if any) per type.''',
                        required=True)
    parser.add_argument('-w', dest='show_world', action='store_true',
                        help='Show the world in FG instances instead of minimal graphics (uses more CPU, same RAM)',
                        default=False, required=False)
    parser.add_argument("-l", dest="logging_level",
                        help="set logging level. Valid levels are DEBUG, INFO (default), WARNING",
                        required=False)
    parser.add_argument("-t", dest="testing", action='store_true',
                        help="standalone testing mode - no interaction with controller",
                        default=False, required=False)
    parser.add_argument('-d', dest='scenario_path', type=str,
                        help='the path to the directory where the scenarios live',
                        required=True)

    gu.check_minimal_gcp_env_variables()

    args = parser.parse_args()
    # configure logging
    my_log_level = 'INFO'
    if args.logging_level:
        my_log_level = args.logging_level.upper()
    u.configure_logging(my_log_level, True, 'Worker')

    sm_parent_conn, sm_child_conn = mp.Pipe()

    _ = u.parse_fg_instances(args.fg_instances)  # check that max instances has correct format

    worker = FGFSWorker(args.fg_instances, args.scenario_path, args.show_world, args.testing, sm_child_conn)
    worker.daemon = False
    worker.start()
