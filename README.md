# Intro

`Hunter` is a military combat scenario generator and runtime environment for the free flight simulator [FlightGear](http://home.flightgear.org/) placing different types of static and moving targets into [Multiplayer](http://wiki.flightgear.org/Howto:Multiplayer#Advanced_usage_of_multiplayer) and thereby allowing multiple players to fly together in the same combat environment. The work is part of the FlightGear flight sim military community [Operation Red Flag (OPRF)](http://opredflag.com/).

You can get an impression of the capabilities from the [end user guide](https://vanosten.gitlab.io/hunter/end_user.html), which is part of the documentation. As FlightGear's scenery covers the whole world, scenarios for `Hunter` can be defined in any part of the world.

For credits / contributions see the [doc index](https://gitlab.com/vanosten/hunter/-/blob/master/doc/index.rst).
